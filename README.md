This is a management system for internal school.

There are 7 types of users in the system.

1.Owner 2.Admin 3.Head Teacher 4.Teacher 5.Accountant 6.Librarian 7.Attendance Taker

To run the project:
------------------------------------
1.Clone the project.

2.From the database folder restore the database in the SQL Server.

3.Open the project in Visual Studio.

4.Connect to the database from VS using server explorer.

5.Take the connectionstring and paste it in App.config -> ConnectionString section.

6.Build the solution.


﻿using SchoolManagement.Data_Layer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SchoolManagement.Helper
{
    public class Email
    {
        public static bool SendForgotMail(string email, LoginData ld)
        {
            using (MailMessage mm = new MailMessage("Your Email Adress", email))
            {
                mm.Subject = "School System Login";
                mm.Body = ld.GetPassword(email);

                mm.IsBodyHtml = false;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential credential = new NetworkCredential("Your Email Address", "Your Password");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = credential;
                smtp.Port = 587;
                smtp.Send(mm);

                return true;
            }
        }

        public static bool SendConfirmMail(string id, string pass, string mail)
        {
            using (MailMessage mm = new MailMessage("Your Email Address", mail))
            {
                mm.Subject = "School System Login";
                mm.Body = "ID: " + id + Environment.NewLine + "Password: " + pass;

                mm.IsBodyHtml = false;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential credential = new NetworkCredential("Your Email Address", "Your Password");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = credential;
                smtp.Port = 587;
                smtp.Send(mm);

                return true;
            }
        }
    }
}

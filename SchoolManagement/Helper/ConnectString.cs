﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolManagement.Helper
{
    public static class ConnectString
    {
        public static string CnnVal()
        {
            return ConfigurationManager.ConnectionStrings["SchoolDB"].ConnectionString;
        }
    }
}

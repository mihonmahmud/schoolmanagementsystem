﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using SchoolManagement.Helper;

namespace SchoolManagement.Data_Layer
{
    public class LoginData
    {
        private SqlConnection con;

        public LoginData()
        {
            con = new SqlConnection(ConnectString.CnnVal());
        }

        public int GetUserStatus(string id, string pass)
        {
            con.Open();
            string query = string.Format($"SELECT status FROM LogIn WHERE employeeId='{id}' AND password='{pass}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                con.Close();
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                int status = int.Parse(dt.Rows[0][0].ToString());

                if (status == 1)
                {
                    con.Close();
                    return 1;
                }
                else if (status == 2)
                {
                    con.Close();
                    return 2;
                }
                else if (status == 3)
                {
                    con.Close();
                    return 3;
                }
                else if (status == 4)
                {
                    con.Close();
                    return 4;
                }
                else if (status == 5)
                {
                    con.Close();
                    return 5;
                }
                else if (status == 6)
                {
                    con.Close();
                    return 6;
                }
                else if (status == 7)
                {
                    con.Close();
                    return 7;
                }
                else 
                {   con.Close(); 
                    return 0; 
                }
            }
            else
            {
                con.Close();
                return -1;
            }
        }

        public DataTable EmployeeList(string uId)
        {
            con.Open();
            string query = string.Format($"SELECT employeeId AS 'ID', employeeName AS 'Name' FROM Employees WHERE employeeId<>'{uId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public bool InsertMail(string uId, string tId, string subject, string description)
        {
            string date = DateTime.Now.ToString("MMMM dd, yyyy");

            con.Open();
            string query = string.Format($"INSERT INTO Mails(mfrom, mto, date, subject, description) VALUES('{uId}', '{tId}', '{date}', '{subject}', '{description}')");
            SqlCommand cmd = new SqlCommand(query, con);

            if(cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public DataTable ReceivedMail(string uId)
        {
            con.Open();
            string query = string.Format($"SELECT date AS 'Date', mfrom AS 'Sender', subject AS 'Subject', description AS 'Mail' FROM Mails WHERE mto='{uId}' ORDER BY id DESC");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public string GetNameById(string uId)
        {
            con.Open();
            string query = string.Format($"SELECT employeeName from Employees WHERE employeeId='{uId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return "";
            }
            else
            {
                con.Close();
                return dt.Rows[0][0].ToString();
            }
        }

        public bool IsEmailExist(string email)
        {
            con.Open();
            string query = string.Format($"SELECT * from Employees WHERE email='{email}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return false;
            }
            else
            {
                con.Close();
                return true;
            }
        }

        public string GetPassword(string email)
        {
            con.Open();
            string query = string.Format($"SELECT employeeId from Employees WHERE email='{email}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return "";
            }
            else
            {
                con.Close();
                return dt.Rows[0][0].ToString() + Environment.NewLine + PasswordById(dt.Rows[0][0].ToString());
            }
        }

        public string PasswordById(string id)
        {
            con.Open();
            string query = string.Format($"SELECT password from LogIn WHERE employeeId='{id}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return "";
            }
            else
            {
                con.Close();
                return dt.Rows[0][0].ToString();
            }
        }

        public DataTable GetMonth()
        {
            con.Open();
            string query = string.Format($"SELECT month FROM EmployeeAttendance");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("month", typeof(string));
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetYear()
        {
            con.Open();
            string query = string.Format($"SELECT year FROM EmployeeAttendance");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("year", typeof(string));
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetAttenByYear(string empId, string year)
        {
            con.Open();
            string query = string.Format($"SELECT month, day, enter, out FROM EmployeeAttendance WHERE employeeId='{empId}' AND year='{year}' ORDER BY month");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetAttenByMonYear(string empId, string mon, string year)
        {
            con.Open();
            string query = string.Format($"SELECT day AS 'Day', enter AS 'Enter', out AS 'Out' FROM EmployeeAttendance WHERE employeeId='{empId}' AND month='{mon}' AND year='{year}' ORDER BY id");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetLeave(string userId)
        {
            con.Open();
            string query = string.Format($"SELECT year AS 'Year', lFrom AS 'From', lTo AS 'To', reason AS 'Reason' FROM EmployeeLeaves WHERE employeeId='{userId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetLeaveByYear(string year, string userId)
        {
            con.Open();
            string query = string.Format($"SELECT lFrom AS 'From', lTo AS 'To', reason AS 'Reason', count AS 'No of Days' FROM EmployeeLeaves WHERE year='{year}' AND employeeId='{userId}' AND approved={1}");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetPaymentByYear(string empId, string year)
        {
            con.Open();
            string query = string.Format($"SELECT month, referenceNo, total, basic, home, medical, tiffin, ta_da, incentive, festival, examScript, examGuard FROM EmployeeSalary WHERE employeeId='{empId}' AND year='{year}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetPaymentByMonYear(string empId, string mon, string year)
        {
            con.Open();
            string query = string.Format($"SELECT referenceNo, total, basic, home, medical, tiffin, ta_da, incentive, festival, examScript, examGuard FROM EmployeeSalary WHERE employeeId='{empId}' AND year='{year}' AND month='{mon}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetExpenseByYear(string year)
        {
            con.Open();
            string query = string.Format($"SELECT month, referenceNo, employeeId, total FROM EmployeeSalary WHERE year='{year}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public float TotalExpenseByYear(string year)
        {
            con.Open();
            string query = string.Format($"SELECT SUM(total) FROM EmployeeSalary WHERE year='{year}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return 0f;
            }
            else
            {
                con.Close();
                return float.Parse(dt.Rows[0][0].ToString());
            }
        }

        public DataTable GetExpenseByMonYear(string mon, string year)
        {
            con.Open();
            string query = string.Format($"SELECT referenceNo, employeeId, total FROM EmployeeSalary WHERE year='{year}' AND month='{mon}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public float TotalExpenseByMonYear(string mon, string year)
        {
            con.Open();
            string query = string.Format($"SELECT SUM(total) FROM EmployeeSalary WHERE year='{year}' AND month='{mon}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return 0f;
            }
            else
            {
                con.Close();
                return float.Parse(dt.Rows[0][0].ToString());
            }
        }

        public DataTable GetInvenExpenseByYear(string year)
        {
            con.Open();
            string query = string.Format($"SELECT referenceNo, day, month, itemName, price, total FROM Inventory WHERE year='{year}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetInvenExpenseByMonYear(string mon, string year)
        {
            con.Open();
            string query = string.Format($"SELECT referenceNo, day, itemName, price, total FROM Inventory WHERE year='{year}' AND month='{mon}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public float TotalInvenExpenseByYear(string year)
        {
            con.Open();
            string query = string.Format($"SELECT SUM(total) FROM Inventory WHERE year='{year}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return 0f;
            }
            else
            {
                con.Close();
                return float.Parse(dt.Rows[0][0].ToString());
            }
        }

        public float TotalInvenExpenseByMonYear(string mon, string year)
        {
            con.Open();
            string query = string.Format($"SELECT SUM(total) FROM Inventory WHERE year='{year}' AND month='{mon}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return 0f;
            }
            else
            {
                con.Close();
                return float.Parse(dt.Rows[0][0].ToString());
            }
        }

        public DataTable GetIncomeByYear(string year)
        {
            con.Open();
            string query = string.Format($"SELECT SUM(admissionFee) AS 'Total Admission Fee', SUM(monthlyFee) AS 'Total Monthly Fee', SUM(examFee) AS 'Total Exam Fee', SUM(fine) AS 'Total Fine', SUM(library) AS 'Total Library Fee', SUM(developFee) AS 'Total Development Fee', SUM(others) AS 'Others' FROM Fees WHERE year='{year}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetIncomeMonth()
        {
            con.Open();
            string query = string.Format($"SELECT month FROM Fees");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("month", typeof(string));
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetIncomeYear()
        {
            con.Open();
            string query = string.Format($"SELECT year FROM Fees");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("year", typeof(string));
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetIncomeByMonYear(string mon, string year)
        {
            con.Open();
            string query = string.Format($"SELECT SUM(admissionFee) AS 'Total Admission Fee', SUM(monthlyFee) AS 'Total Monthly Fee', SUM(examFee) AS 'Total Exam Fee', SUM(fine) AS 'Total Fine', SUM(library) AS 'Total Library Fee', SUM(developFee) AS 'Total Development Fee', SUM(others) AS 'Others' FROM Fees WHERE year='{year}' AND month='{mon}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable ApplicantsIncome(string session)
        {
            con.Open();
            string query = string.Format($"SELECT class, SUM(amount) AS 'Total Amount' FROM AdmissionFees WHERE session='{session}' GROUP BY class");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public bool UpdatePassword(string uId, string pass)
        {
            con.Open();
            string query = string.Format($"UPDATE LogIn SET password='{pass}' WHERE employeeId='{uId}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if(cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public bool RequestLeave(string year, string id, string from, string to, string reason, int count)
        {
            con.Open();
            string query = string.Format($"INSERT INTO EmployeeLeaves(year, employeeId, lfrom, lTo, reason, date, count, approved) VALUES('{year}', '{id}', '{from}', '{to}', '{reason}', '{DateTime.Now.ToShortDateString()}', {count}, {0})");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }
    }
}

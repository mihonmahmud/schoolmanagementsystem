﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using SchoolManagement.Helper;

namespace SchoolManagement.Data_Layer
{
    public class TeacherData
    {
        private SqlConnection con;
        private HeadTeacherData htd = new HeadTeacherData();

        public TeacherData()
        {
            con = new SqlConnection(ConnectString.CnnVal());
        }

        public DataTable GetSessionById(string uId)
        {
            con.Open();
            string query = string.Format($"SELECT session FROM TeacherSubjects WHERE employeeId='{uId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("session", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetSubjectsById(string session, string uId)
        {
            con.Open();
            string query = string.Format($"SELECT subId FROM TeacherSubjects WHERE employeeId='{uId}' AND session='{session}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("subId", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public string GetClassById(string session, string sId)
        {
            string cla;
            con.Open();
            string query = string.Format($"SELECT class FROM TeacherSubjects WHERE subId='{sId}' AND session='{session}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return "";
            }
            else
            {
                con.Close();
                cla = dt.Rows[0][0].ToString();
                return cla;
            }
        }

        public DataTable GetSecsByCla(string session, string sId, string cla, string empId)
        {
            con.Open();
            string query = string.Format($"SELECT sec FROM TeacherSubjects WHERE subId='{sId}' AND session='{session}' AND class='{cla}' AND employeeId='{empId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("sec", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetStudentsByClaSec(string session, string cla, string sec)
        {
            con.Open();
            string query = string.Format($"SELECT StudentsClass.studentId AS 'ID', studentName AS 'Name' FROM StudentsClass,Students WHERE StudentsClass.studentId=Students.studentId AND session='{session}' AND class='{cla}' AND sec='{sec}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetStudentsList(string session, string cla, string sec)
        {
            con.Open();
            string query = string.Format($"SELECT position AS 'Class Position', StudentsClass.studentId AS 'Student ID', studentName AS 'Name' FROM Students, StudentsClass WHERE StudentsClass.studentId=Students.studentId AND session='{session}' AND class='{cla}' AND sec='{sec}' ORDER BY position");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public bool InsertMarks(string session, string term, string subId, string cla, string sec, DataTable dt)
        {
            List<int> count = new List<int>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string stuId = dt.Rows[i][0].ToString();
                int mark = int.Parse(dt.Rows[i][2].ToString());
                string grade = dt.Rows[i][3].ToString();

                if (htd.IsMarkExist(session, term, subId, stuId))
                {
                    count.Add(1);
                    continue;
                }
                else
                {
                    SqlCommand cmd = new SqlCommand($"INSERT INTO StudentsMarks(session, term, subId, class, sec, studentId, marks, grade) VALUES('{session}', '{term}', '{subId}', '{cla}', '{sec}', '{stuId}', {mark}, '{grade}')", con);
                    con.Open();
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        con.Close();
                        count.Add(1);
                    }
                }                  
            }

            if (count.Contains(0))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool MarkAttendance(string session, string stuId, string subId, string cla, string sec, string present, string empId)
        {
            string day = DateTime.Now.Day.ToString();
            string mon = DateTime.Now.ToString("MMM");
            string year = DateTime.Now.Year.ToString();

            con.Open();
            string query = string.Format($"INSERT INTO StudentsAttendance(session, subId, class, sec, studentId, day, month, year, present, employeeId) VALUES('{session}', '{subId}', '{cla}', '{sec}', '{stuId}', '{day}', '{mon}', '{year}', '{present}', '{empId}')");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public DataTable TermSchedule(string uId, string session)
        {
            con.Open();
            string query = string.Format($"SELECT term AS 'Term', class AS 'Class', subId AS 'Subject ID', examDate AS 'Date', examTime AS 'Time' FROM HeadTerm WHERE employeeId='{uId}' AND session='{session}' ORDER BY id DESC");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable AdmissionSchedule(string uId, string session)
        {
            con.Open();
            string query = string.Format($"SELECT class AS 'Class', examDate AS 'Date', examTime AS 'Time' FROM HeadAdmission WHERE employeeId='{uId}' AND session='{session}' ORDER BY id DESC");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetGuardianInfo(string stuId)
        {
            con.Open();
            string query = string.Format($"SELECT * FROM Guardians WHERE studentId='{stuId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetOwnSchedule(string session, string uId)
        {
            con.Open();
            string query = string.Format($"SELECT name AS 'Subject Name', TeacherSubjects.subId AS 'Subject ID', TeacherSubjects.class AS 'Class', sec AS 'Section', Subjects.day AS 'Day', Subjects.time AS 'Time' FROM TeacherSubjects, Subjects WHERE TeacherSubjects.subId=Subjects.subId AND TeacherSubjects.employeeId='{uId}' AND TeacherSubjects.session='{session}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetDayBySession(string session, string subId, string cla, string sec, string uId, string mon)
        {
            con.Open();
            string query = string.Format($"SELECT DISTINCT day FROM StudentsAttendance WHERE session='{session}' AND class='{cla}' AND sec='{sec}' AND subId='{subId}' AND employeeId='{uId}' AND month='{mon}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("studentId", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetStudentsAttendanceRecord(string session, string subId, string cla, string sec, string uId, string day, string month)
        {
            con.Open();
            string query = string.Format($"SELECT StudentsAttendance.studentId AS 'Student ID', studentName AS 'Name', present AS 'Attendance Record' FROM StudentsAttendance, Students WHERE StudentsAttendance.studentId=Students.studentId AND session='{session}' AND class='{cla}' AND sec='{sec}' AND subId='{subId}' AND employeeId='{uId}' AND day='{day}' AND month='{month}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public bool UpdateStudentRecord(string session, string subId, string cla, string sec, string uId, string day, string month, string stuId, string present)
        {
            con.Open();
            string query = string.Format($"UPDATE StudentsAttendance SET present='{present}' WHERE session='{session}' AND class='{cla}' AND sec='{sec}' AND subId='{subId}' AND employeeId='{uId}' AND day='{day}' AND month='{month}' AND studentId='{stuId}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public bool IsAttendanceDataExist(string session, string subId, string cla, string sec, string employeeId)
        {
            con.Open();

            string query = string.Format($"SELECT * FROM StudentsAttendance WHERE session='{session}' AND subId='{subId}' AND class='{cla}' AND sec='{sec}' AND day='{DateTime.Now.Day.ToString()}' AND month='{DateTime.Now.ToString("MMM")}' AND employeeId='{employeeId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return false;
            }
            else
            {
                con.Close();
                return true;
            }
        }
    }
}

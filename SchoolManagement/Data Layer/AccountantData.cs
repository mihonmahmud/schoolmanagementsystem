﻿using SchoolManagement.Helper;
using System;
using System.Data;
using System.Data.SqlClient;

namespace SchoolManagement.Data_Layer
{
    public class AccountantData
    {
        private readonly SqlConnection con;

        public AccountantData()
        {
            con = new SqlConnection(ConnectString.CnnVal());
        }

        public int CapacityBySessionClassSec(string session, string cla, string sec)
        {
            int id;

            if(con.State != ConnectionState.Open)
            {
                con.Open();
            }
            string query = string.Format($"SELECT capacity FROM Sections WHERE session='{session}' AND section='{sec}' AND class='{cla}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return 0;
            }
            else
            {
                con.Close();
                id = int.Parse(dt.Rows[0][0].ToString());
                return id;
            }
        }

        public int GetLastStuId()
        {
            int id;
            con.Open();

            string query = "SELECT TOP 1 id FROM Students ORDER BY id DESC";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return 0;
            }

            else
            {
                con.Close();
                id = int.Parse(dt.Rows[0][0].ToString());
                return id;
            }
        }

        public bool AddStudent(string session, string cla, string sec, string stuId, string name, string fname, string mname, string dob, string religion, string gender, string bg, string present, string permanent, string nation, string gname, string profession, string phone, string mail, int position)
        {
            con.Open();
            string q1 = string.Format($"INSERT INTO Students(studentId, studentName, fatherName, motherName, dob, religion, gender, bloodGroup, presentAddress, permanentAddress, nationality) VALUES('{stuId}', '{name}', '{fname}', '{mname}', '{dob}', '{religion}', '{gender}', '{bg}', '{present}', '{permanent}', '{nation}')");
            string q2 = string.Format($"INSERT INTO Guardians(studentId, gName, gProfession, phoneNo, email) VALUES('{stuId}', '{gname}', '{profession}', '{phone}', '{mail}')");
            string q3 = string.Format($"INSERT INTO StudentsClass(session, studentId, class, sec, position) VALUES('{session}', '{stuId}', '{cla}', '{sec}', {position})");

            SqlCommand cmd1 = new SqlCommand(q1, con);
            SqlCommand cmd2 = new SqlCommand(q2, con);
            SqlCommand cmd3 = new SqlCommand(q3, con);

            if ((cmd1.ExecuteNonQuery() > 0) && (cmd2.ExecuteNonQuery() > 0) && (cmd3.ExecuteNonQuery() > 0))
            {
                if(ReduceCapacity(session, cla, sec))
                {
                    con.Close();
                    return true;
                }
            }

            con.Close();
            return false;
        }

        public bool ReduceCapacity(string session, string cla, string sec)
        {
            int cap = CapacityBySessionClassSec(session, cla, sec);
            int newCap = cap - 1;

            con.Open();
            string query = string.Format($"UPDATE Sections SET capacity={newCap} WHERE session='{session}' AND class='{cla}' AND section='{sec}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if(cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public DataTable GetSession()
        {
            con.Open();
            string query = string.Format($"SELECT session FROM AdminAdmission");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("session", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetSecBySessionClass(string session, string sClass)
        {
            con.Open();
            string query = string.Format($"SELECT section FROM Sections WHERE session='{session}' AND class='{sClass}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("section", typeof(string));
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetFees()
        {
            con.Open();
            string query = string.Format($"SELECT monthly, exam, admission, development, library FROM AdminFees");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public bool AddFees(string session, string stuId, string sclass, string sec, float admission, float month, string fmonth, string year, float exam, string term, float fine, float lib, float dev, float others, float due, float total)
        {
            con.Open();
            string query = string.Format($"INSERT INTO Fees(session, studentId, class, sec, admissionFee, monthlyFee, month, year, examFee, term, fine, library, developFee, others, due, total, date) VALUES('{session}', '{stuId}', '{sclass}', '{sec}', {admission}, {month}, '{fmonth}', '{year}', {exam}, '{term}', {fine}, {lib}, {dev}, {others}, {due}, {total}, '{DateTime.Now.ToString("dd-MMM-yy")}')");
            SqlCommand cmd = new SqlCommand(query, con);

            if(cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public bool AddAdmissionFees(string form, string name, string aclass, int amount)
        {
            con.Open();
            string query = string.Format($"INSERT INTO AdmissionFees(session, month, form, name, class, amount) VALUES('{DateTime.Now.Year.ToString()}', '{DateTime.Now.ToString("MMM")}', '{form}', '{name}', '{aclass}', {amount})");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public int GetLastId()
        {
            int id;
            con.Open();

            string query = string.Format("SELECT TOP 1 id FROM EmployeeSalary ORDER BY id DESC");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return 0;
            }
            else
            {
                con.Close();
                id = int.Parse(dt.Rows[0][0].ToString());
                return id;
            }
        }

        public DataTable GetSalaryInfo(string empId)
        {
            string category = EmployeeCategory(empId);

            con.Open();
            string query = string.Format($"SELECT basic, home, medical, tiffin FROM StartingSalary WHERE category='{category}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public bool InsertSalary(string refNo, string mon, string year, string empId, float basic, float home, float medical, float tiffin, float tada, float incentive, float festival, float exams, float examg, float total)
        {
            con.Open();
            string query = string.Format($"INSERT INTO EmployeeSalary(referenceNo, month, year, employeeId, basic, home, medical, tiffin, ta_da, incentive, festival, examScript, examGuard, total) VALUES('{refNo}', '{mon}', '{year}', '{empId}', {basic}, {home}, {medical}, {tiffin}, {tada}, {incentive}, {festival}, {exams}, {examg}, {total})");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public int GetLastInvenId()
        {
            int id;
            con.Open();

            string query = string.Format("SELECT TOP 1 id FROM Inventory ORDER BY id DESC");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return 0;
            }
            else
            {
                con.Close();
                id = int.Parse(dt.Rows[0][0].ToString());
                return id;
            }
        }

        public bool InsertInventory(string reference, string item, int qunatity, float price, float total)
        {
            con.Open();
            string query = string.Format($"INSERT INTO Inventory(referenceNo, day, month, year, itemName, quantity, price, total) VALUES('{reference}', '{DateTime.Now.Day.ToString()}', '{DateTime.Now.ToString("MMM")}', '{DateTime.Now.Year.ToString()}', '{item}', {qunatity}, {price}, {total})");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public string EmployeeCategory(string empId)
        {
            con.Open();

            string query = string.Format($"SELECT category FROM Employees WHERE employeeId='{empId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return "";
            }
            else
            {
                con.Close();
                return dt.Rows[0][0].ToString();
            }
        }

        public DataTable UnpaidStudents(string session, string cla, string month)
        {
            con.Open();
            string query = string.Format($"SELECT studentId AS 'Student ID', sec AS 'Section' FROM StudentsClass WHERE studentId NOT IN(SELECT studentId FROM Fees WHERE session='{session}' AND class='{cla}' AND month='{month}' AND due={0}) AND session='{session}' AND class='{cla}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable PartialStudents(string session, string cla, string month)
        {
            con.Open();
            string query = string.Format($"SELECT studentId AS 'STUDENT ID', sec AS 'Section', due AS 'DUE AMOUNT' FROM Fees WHERE session='{session}' AND class='{cla}' AND month='{month}' AND due<>{0}");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable PaymentInfo(string session, string cla, string stuId)
        {
            con.Open();
            string query = string.Format($"SELECT month AS 'Month', admissionFee AS 'Admission Fee', monthlyFee AS 'Monthly Fee', examFee AS 'Exam Fee', fine AS 'Fine', library AS 'Library Fee', developFee AS 'Development Fee', others AS 'Others', due AS 'Due' FROM Fees WHERE session='{session}' AND class='{cla}' AND studentId='{stuId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable GetDueInfo(string session, string month, string stuId)
        {
            con.Open();
            string query = string.Format($"SELECT class AS 'Class', sec AS 'Section', due AS 'Due', fine AS 'Fine', total AS 'Total', admissionFee AS 'Admission', monthlyFee AS 'Monthly', examFee AS 'Exam', library AS 'Library', developFee AS 'Development', others AS 'Others' FROM Fees WHERE session='{session}' AND month='{month}' AND studentId='{stuId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public bool UpdateDue(string session, string month, string stuId, float due, float fine, float total)
        {
            con.Open();
            string query = string.Format($"UPDATE Fees SET due={due}, fine={fine}, total={total} WHERE session='{session}' AND month='{month}' AND studentId='{stuId}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public string GetPercentageByStuId(string session, string stu)
        {
            con.Open();

            string query = string.Format($"SELECT percentage FROM ScholarStudents WHERE session='{session}' AND studentId='{stu}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return "0";
            }

            else
            {
                con.Close();
                return dt.Rows[0][0].ToString();
            }
        }

        public DataTable GetPaymentById(string empId)
        {
            con.Open();
            string query = string.Format($"SELECT year, month, referenceNo, total, basic, home, medical, tiffin, ta_da, incentive, festival, examScript, examGuard FROM EmployeeSalary WHERE employeeId='{empId}' ORDER BY year DESC");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetPaymentByRef(string refNo)
        {
            con.Open();
            string query = string.Format($"SELECT year, month, total, basic, home, medical, tiffin, ta_da, incentive, festival, examScript, examGuard FROM EmployeeSalary WHERE referenceNo='{refNo}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetClassInfoById(string uId)
        {
            con.Open();
            string query = string.Format($"SELECT session AS 'Session', class AS 'Class', sec AS 'Section', position AS 'Class Position' FROM StudentsClass WHERE studentId='{uId}' ORDER BY session DESC");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }


        public bool UpdateStuClass(string session, string uId, string cla, string sec, int position)
        {
            con.Open();
            string query = string.Format($"INSERT INTO StudentsClass(session, studentId, class, sec, position) VALUES('{session}', '{uId}', '{cla}', '{sec}', {position})");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public bool IsPositionExist(string session, string cla, string sec, int position)
        {
            int id;

            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
            string query = string.Format($"SELECT * FROM StudentsClass WHERE session='{session}' AND sec='{sec}' AND class='{cla}' AND position={position}");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return true;
            }
            else
            {
                con.Close();
                return false;
            }
        }

        public bool IsStuIdValid(string session, string stuId, string cla, string sec)
        {
            if (con.State != ConnectionState.Open)
            {
                con.Open();
            }
            string query = string.Format($"SELECT * FROM StudentsClass WHERE session='{session}' AND sec='{sec}' AND class='{cla}' AND studentId='{stuId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return false;
            }
            else
            {
                con.Close();
                return true;
            }
        }

        public DataTable HistorySession()
        {
            con.Open();
            string query = string.Format($"SELECT session FROM Fees");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("session", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable CollectionHistory(string session, string stuId)
        {
            con.Open();
            string query = string.Format($"SELECT date AS 'Payment Date', total AS 'Total', month AS 'Month', monthlyFee AS 'Monthly', examFee AS 'Exam', developFee AS 'Development', admissionFee AS 'Admission', library AS 'Library', others AS 'Others', due AS 'Due', fine AS 'Fine' FROM Fees WHERE studentId='{stuId}' AND session='{session}' ORDER BY id DESC");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using SchoolManagement.Helper;

namespace SchoolManagement.Data_Layer
{
    public class LibrarianData
    {
       
        private SqlConnection con;

        public LibrarianData()
        {
            con = new SqlConnection(ConnectString.CnnVal());
        }

        public bool InsertBook(string bookId,string title,int edition,string author,string category,int quantity,string shelf)
        {
            con.Open();
           string q1= string.Format($"INSERT INTO BOOKS(bookId,title,edition,author,category,quantity,shelf)VALUES('{bookId}', '{title}', {edition}, '{author}', '{category}', {quantity}, '{shelf}')");
            SqlCommand cmd1 = new SqlCommand(q1, con);
            int row1 = -1;
            row1 = cmd1.ExecuteNonQuery();
            if ((row1 >= 0))
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public bool InsertBorrowBook(string bookId, string borrowerId, string borrowerName, string phoneNo, string borrowDate, string returnDate, int returned)
        {
            con.Open();
            string q1 = string.Format($"INSERT INTO BORROWBOOKS(bookId,borrowerId,borrowerName,phoneNo,borrowDate,returnDate,returned)VALUES('{bookId}', '{borrowerId}', '{borrowerName}', '{phoneNo}', '{borrowDate}', '{returnDate}', {returned})");
            SqlCommand cmd1 = new SqlCommand(q1, con);
            int row1 = -1;
            row1 = cmd1.ExecuteNonQuery();
            if ((row1 >= 0))
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;           
        }

        public DataTable GetBookDetails(string bookId)
        {
            con.Open();
            string query = string.Format($"SELECT * FROM Books WHERE bookId='{bookId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public bool UpdateBookDetails(string bookId, string title, int edition, string author, string category, int quantity, string shelf)
        {
            con.Open();
            string query = string.Format($"UPDATE Books SET bookId='{bookId}',title='{title}', edition='{edition}', author='{author}', category='{category}', quantity='{quantity}', shelf='{shelf}' WHERE bookId={bookId}");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public DataTable GetBookCategory()
        {
            con.Open();
            string query = string.Format($"SELECT category FROM Books");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("category", typeof(string));
            dt.Load(reader);

            con.Close();
            return dt;
        }

        public bool DeleteBook(string bookId)
        {
            con.Open();
            string q1 = string.Format($"DELETE FROM Books WHERE bookId='{bookId}'");
            SqlCommand cmd = new SqlCommand(q1, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public DataTable Search(string searchBooks, bool withId)
        {
            con.Open();
            string query;
            if (!withId)
                query = string.Format("SELECT title AS 'Book Name', edition AS 'Edition', author AS 'Author', category AS 'Category' FROM Books where lower(title) like '%{0}%'", searchBooks.ToLower());
            else
                query = string.Format("SELECT title AS 'Book Name', edition AS 'Edition', author AS 'Author', category AS 'Category' FROM Books where bookId='{0}'", searchBooks);
           
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable SearchAuthor(string searchBooks)
        {
            con.Open();
            string query;
            query = string.Format("SELECT title AS 'Book Name', edition AS 'Edition', author AS 'Author', category AS 'Category' FROM Books where lower(author) like '%{0}%'", searchBooks.ToLower());

            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable GridUpdate()
        {
            con.Open();
            string query = string.Format("SELECT * FROM Books");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable SearchBorrow(string searchBorrows)
        {
            con.Open();
            string query = string.Format("SELECT bookId,borrowerId,borrowerName,phoneNo,borrowDate,returnDate,returned FROM BorrowBooks where bookId='{0}'", searchBorrows);
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable SearchBorrower(string searchBorrows)
        {
            con.Open();
            string query = string.Format("SELECT bookId,borrowerId,borrowerName,phoneNo,borrowDate,returnDate,returned FROM BorrowBooks where borrowerId ='{0}'", searchBorrows);
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable GridBorrowUpdate()
        {
            con.Open();
            string query = string.Format("SELECT bookId,borrowerId,borrowerName,phoneNo,borrowDate,returnDate,returned FROM BorrowBooks");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public bool UpdateBorrowBook(string bookId, string borrowerId,string borrowerName, string phoneNo, string borrowDate, string returnDate,int returned)
        {
            con.Open();
            string query = string.Format($"UPDATE BorrowBooks SET bookId='{bookId}',borrowerId='{borrowerId}', borrowerName='{borrowerName}', phoneNo='{phoneNo}', borrowDate='{borrowDate}', returnDate='{returnDate}',returned={returned} WHERE bookId='{bookId}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;

        }

        public DataTable GetBorrowBookDetails(string bookId)
        {
            con.Open();
            string query = string.Format($"SELECT * FROM BorrowBooks WHERE bookId='{bookId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public int CheckIdData(string bookId)
        {
            con.Open();
            string query = string.Format("SELECT * FROM Books where bookId = '{0}'", bookId);
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                con.Close();
                return 1;
            }
            else
            {
                con.Close();
                return 0;
            }
        }
    }
}

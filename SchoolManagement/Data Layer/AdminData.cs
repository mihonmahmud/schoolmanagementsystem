﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using SchoolManagement.Helper;

namespace SchoolManagement.Data_Layer
{
    public class AdminData
    {
        private SqlConnection con;

        public AdminData()
        {
            con = new SqlConnection(ConnectString.CnnVal());
        }

        public bool InsertEmployee(string empId, string empPass, string empName, string mail, string phone, string fName, string mName, string dob, string religion, string gender, string bg, string degree, string prAddress, string pmAddress, string category, string nation, int status, string jdate, float basic, float home, float medical, float tiffin)
        {
            con.Open();
            string q1 = string.Format($"INSERT INTO Employees (employeeId, employeeName, email, phone, fatherName, motherName, dob, religion, gender, bloodGroup, degree, presentAddress, permanentAddress, category, nationality, joindate, active) VALUES ('{empId}', '{empName}', '{mail}', '{phone}', '{fName}', '{mName}', '{dob}', '{religion}', '{gender}', '{bg}', '{degree}', '{prAddress}', '{pmAddress}', '{category}', '{nation}', '{jdate}', {1})");
            string q2 = string.Format($"INSERT INTO LogIn (employeeId, password, status) VALUES ('{empId}', '{empPass}', {status})");
            string q3 = string.Format($"INSERT INTO StartingSalary (employeeId, basic, home, medical, tiffin) VALUES ('{empId}', {basic}, {home}, {medical}, {tiffin})");
            SqlCommand cmd1 = new SqlCommand(q1, con);
            SqlCommand cmd2 = new SqlCommand(q2, con);
            SqlCommand cmd3 = new SqlCommand(q3, con);

            int row1 = -1;
            int row2 = -1;
            int row3 = -1;
            row1 = cmd1.ExecuteNonQuery();
            row2 = cmd2.ExecuteNonQuery();
            row3 = cmd3.ExecuteNonQuery();

            if ((row1 >= 0) && (row2 >= 0) && (row3 >= 0))
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        
        public DataTable GetSecByClass(string sClass)
        {
            con.Open();
            string query = string.Format($"SELECT section FROM Sections WHERE class='{sClass}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("section", typeof(string));
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public bool AddSubject(string subid, string subName, string subClass, string subTime, string subDay)
        {
            con.Open();
            string query = string.Format($"INSERT INTO Subjects (subId, name, class, day, time) VALUES('{subid}', '{subName}', '{subClass}', '{subDay}', '{subTime}')");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public bool IsSubIdExist(string subId)
        {
            con.Open();

            string query = string.Format($"SELECT * FROM Subjects WHERE subId='{subId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return false;
            }
            else
            {
                con.Close();
                return true;
            }
        }

        public bool IsStudentExist(string stuId)
        {
            con.Open();

            string query = string.Format($"SELECT * FROM Students WHERE studentId='{stuId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return false;
            }
            else
            {
                con.Close();
                return true;
            }
        }

        public DataTable GetAllSubjects()
        {
            con.Open();
            string query = string.Format($"SELECT subId AS 'Subject ID', name AS 'Subject Name', class AS 'Class', day as 'Day', time AS 'Time' FROM Subjects");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public string GetClassBySub(string subId)
        {
            con.Open();
            string query = string.Format($"SELECT class FROM Subjects WHERE subId='{subId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return "";
            }
            else
            {
                con.Close();
                return dt.Rows[0][0].ToString();
            }
        }

        public DataTable GetSecBySub(string subId)
        {
            con.Open();
            string query = string.Format($"SELECT sec FROM Subjects WHERE subId='{subId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("sec", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public bool CreateExamSchedule(DataTable dt)
        {
            List<int> count = new List<int>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string session = dt.Rows[i][0].ToString();
                string term = dt.Rows[i][1].ToString();
                string subId = dt.Rows[i][2].ToString();
                string cla = dt.Rows[i][3].ToString();
                string date = dt.Rows[i][4].ToString();
                string time = dt.Rows[i][5].ToString();

                SqlCommand cmd = new SqlCommand($"INSERT INTO AdminTerm(session, term, subId, class, examDate, examTime) VALUES('{session}', '{term}', '{subId}', '{cla}', '{date}', '{time}')", con);
                con.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    con.Close();
                    count.Add(1);
                }
            }

            if (count.Contains(0))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool CreateExamSchedule(DataTable dt, string session, string term)
        {
            List<int> count = new List<int>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string subId = dt.Rows[i][0].ToString();
                string cla = dt.Rows[i][1].ToString();
                string date = dt.Rows[i][2].ToString();
                string time = dt.Rows[i][3].ToString();

                SqlCommand cmd = new SqlCommand($"INSERT INTO AdminTerm(session, term, subId, class, examDate, examTime) VALUES('{session}', '{term}', '{subId}', '{cla}', '{date}', '{time}')", con);
                con.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    con.Close();
                    count.Add(1);
                }
            }

            if (count.Contains(0))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public DataTable GetEmployeeDetails(string empId)
        {
            con.Open();
            string query = string.Format($"SELECT * FROM Employees WHERE employeeId='{empId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public bool UpdateEmployeeDetails(string empId, string empName, string phone, string mail, string fName, string mName, string dob, string religion, string gender, string bg, string degree, string prAddress, string pmAddress, string nation)
        {
            con.Open();
            string query = string.Format($"UPDATE Employees SET employeeName='{empName}', email='{mail}', phone='{phone}', fatherName='{fName}', motherName='{mName}', dob='{dob}', religion='{religion}', gender='{gender}', bloodGroup='{bg}', degree='{degree}', presentAddress='{prAddress}', permanentAddress='{pmAddress}', nationality='{nation}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public bool DeleteEmployee(string empId)
        {
            con.Open();
            string q1 = string.Format($"UPDATE Employees SET active={0} WHERE employeeId='{empId}'");
            string q2 = string.Format($"DELETE FROM LogIn WHERE employeeId='{empId}'");
            string q3 = string.Format($"DELETE FROM StartingSalary WHERE employeeId='{empId}'");
            SqlCommand cmd1 = new SqlCommand(q1, con);
            SqlCommand cmd2 = new SqlCommand(q2, con);
            SqlCommand cmd3 = new SqlCommand(q3, con);

            if ((cmd2.ExecuteNonQuery() > 0) && (cmd1.ExecuteNonQuery() > 0) && (cmd3.ExecuteNonQuery() > 0))
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public DataTable AllSubjects()
        {
            con.Open();
            string query = string.Format($"SELECT * FROM Subjects");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable SubjectById(string subId)
        {
            con.Open();
            string query = string.Format($"SELECT * FROM Subjects WHERE subId='{subId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public bool UpdateSubject(int id, string subid, string subName, string subClass, string subTime, string subDay)
        {
            con.Open();
            string query = string.Format($"UPDATE Subjects SET subId='{subid}', name='{subName}', class='{subClass}', day='{subDay}', time='{subTime}' WHERE id={id}");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public bool DeleteSubject(int id)
        {
            con.Open();
            string query = string.Format($"DELETE FROM Subjects WHERE id={id}");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public int GetLastId()
        {
            int id;
            con.Open();

            //string query = string.Format("SELECT TOP 1 id FROM Employees ORDER BY id DESC");
            string query = string.Format("SELECT COUNT(employeeId) FROM Employees");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return 0;
            }
            else
            {
                con.Close();
                id = int.Parse(dt.Rows[0][0].ToString());
                return id;
            }
        }

        public DataTable GetAttendanceMonth()
        {
            con.Open();
            string query = string.Format($"SELECT month FROM EmployeeAttendance");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("month", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetAttendanceYear()
        {
            con.Open();
            string query = string.Format($"SELECT year FROM EmployeeAttendance");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("year", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetAttendanceDay(string mon, string year)
        {
            con.Open();
            string query = string.Format($"SELECT day FROM EmployeeAttendance WHERE month='{mon}' AND year='{year}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("year", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable EmployeeAttendanceByYear(string year)
        {
            con.Open();
            string query = string.Format($"SELECT employeeId, day, month, enter, out FROM EmployeeAttendance WHERE year='{year}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable EmployeeAttendanceByDayMonYear(string day, string mon, string year)
        {
            con.Open();
            string query = string.Format($"SELECT EmployeeAttendance.employeeId AS 'Emp ID', employeeName AS 'Name', enter AS 'Enter', out AS 'Out' FROM Employees, EmployeeAttendance WHERE EmployeeAttendance.employeeId=Employees.employeeId AND day='{day}' AND month='{mon}' AND year='{year}' ORDER BY EmployeeAttendance.employeeId");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable EmployeeAttendanceByCurrentDate()
        {
            con.Open();
            string query = string.Format($"SELECT EmployeeAttendance.employeeId AS 'Emp ID', employeeName AS 'Name', enter AS 'Enter', out AS 'Out' FROM Employees, EmployeeAttendance WHERE EmployeeAttendance.employeeId=Employees.employeeId AND day='{DateTime.Now.Day}' AND month='{DateTime.Now.ToString("MMM")}' AND year='{DateTime.Now.Year}' ORDER BY EmployeeAttendance.employeeId");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable EmployeeAttendanceByMonYearEmp(string year, string mon, string emp)
        {
            con.Open();
            string query = string.Format($"SELECT day AS 'Day', enter AS 'Enter', out AS 'Out' FROM EmployeeAttendance WHERE year='{year}' AND month='{mon}' AND employeeId='{emp}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable EmployeeAttendanceByMonYearDayEmp(string year, string mon, string day, string emp)
        {
            con.Open();
            string query = string.Format($"SELECT enter AS 'Enter', out AS 'Out' FROM EmployeeAttendance WHERE year='{year}' AND month='{mon}' AND employeeId='{emp}' AND day='{day}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable EmployeeAttendanceByIdYear(string empId, string year)
        {
            con.Open();
            string query = string.Format($"SELECT day, month, enter, out FROM EmployeeAttendance WHERE year='{year}' AND employeeId='{empId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable EmployeeAttendanceByIdMonYear(string empId, string mon, string year)
        {
            con.Open();
            string query = string.Format($"SELECT day, enter, out FROM EmployeeAttendance WHERE month='{mon}' AND year='{year}' AND employeeId='{empId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public bool InsertSections(DataTable dt)
        {
            List<int> count = new List<int>();

            for(int i = 0; i < dt.Rows.Count; i++)
            {
                string session = dt.Rows[i][0].ToString();
                string cla = dt.Rows[i][1].ToString();
                string sec = dt.Rows[i][2].ToString();
                int cap = int.Parse(dt.Rows[i][3].ToString());

                SqlCommand cmd = new SqlCommand($"INSERT INTO Sections(session, class, section, capacity) VALUES('{session}', '{cla}', '{sec}', {cap})", con);
                con.Open();
                if(cmd.ExecuteNonQuery() > 0)
                {
                    con.Close();
                    count.Add(1);
                }
            }

            if(count.Contains(0))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public DataTable GetOverStuByClass(string search)
        {
            con.Open();
            string query = string.Format($"SELECT session, sec, Students.studentId, studentName, fatherName, motherName, dob, religion, gender, bloodGroup, presentAddress, permanentAddress, nationality FROM Students, StudentsClass WHERE Students.studentId=StudentsClass.studentId AND class='{search}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable GetOverStuByGender(string search)
        {
            con.Open();
            string query = string.Format($"SELECT session, class, sec, Students.studentId, studentName, fatherName, motherName, dob, religion, bloodGroup, presentAddress, permanentAddress, nationality FROM Students, StudentsClass WHERE Students.studentId=StudentsClass.studentId AND gender='{search}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable GetOverStuByBG(string search)
        {
            con.Open();
            string query = string.Format($"SELECT session, class, sec, Students.studentId, studentName, fatherName, motherName, dob, religion, gender, presentAddress, permanentAddress, nationality FROM Students, StudentsClass WHERE Students.studentId=StudentsClass.studentId AND bloodGroup='{search}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable GetOverStuByReligion(string search)
        {
            con.Open();
            string query = string.Format($"SELECT session, class, sec, Students.studentId, studentName, fatherName, motherName, dob, gender, bloodGroup, presentAddress, permanentAddress, nationality FROM Students, StudentsClass WHERE Students.studentId=StudentsClass.studentId AND bloodGroup='{search}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable GetSpeicificStuInfo(string stuId)
        {
            con.Open();
            string query = string.Format($"SELECT studentName, fatherName, motherName, dob, religion, gender, bloodGroup, presentAddress, permanentAddress, nationality, gName, gProfession, phoneNo, email FROM Students, Guardians WHERE Students.studentId=Guardians.studentId AND Students.studentId='{stuId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable GetSpeicificStuAtten(string session, string mon, string stuId)
        {
            con.Open();
            string query = string.Format($"SELECT subId AS 'Subject ID', class AS 'Class', sec AS 'Section', day AS 'Day', present AS 'Attendance', employeeId AS 'Teacher ID' FROM StudentsAttendance WHERE studentId='{stuId}' AND session='{session}' AND month='{mon}' ORDER BY day");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable GetAttenSession()
        {
            con.Open();
            string query = string.Format($"SELECT session FROM StudentsAttendance");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("session", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetAttenMonth(string session)
        {
            con.Open();
            string query = string.Format($"SELECT month FROM StudentsAttendance WHERE session='{session}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("month", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetSpeicificStuAttenBySession(string stuId, string session)
        {
            con.Open();
            string query = string.Format($"SELECT subId, class, sec, day, month, year, present, employeeId FROM StudentsAttendance WHERE studentId='{stuId}' AND session='{session}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable GetMarkSession()
        {
            con.Open();
            string query = string.Format($"SELECT session FROM StudentsMarks");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("session", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetMarkSub()
        {
            con.Open();
            string query = string.Format($"SELECT subId FROM StudentsMarks");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("subId", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetStuMarks(string session, string term, string cla, string stuId)
        {
            con.Open();
            string query = string.Format($"SELECT Subjects.name AS 'Subject Name', marks AS 'Obtained Marks' FROM StudentsMarks, Subjects WHERE StudentsMarks.subId=Subjects.subId AND studentId='{stuId}' AND session='{session}' AND StudentsMarks.class='{cla}' AND term='{term}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable GetStuMarksBySession(string stuId, string session)
        {
            con.Open();
            string query = string.Format($"SELECT term, subId, class, marks FROM StudentsMarks WHERE studentId='{stuId}' AND session='{session}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable GetStuMarksBySessionSub(string stuId, string session, string subId)
        {
            con.Open();
            string query = string.Format($"SELECT term, class, marks FROM StudentsMarks WHERE studentId='{stuId}' AND session='{session}' AND subId='{subId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public bool InsertAdmissionData(DataTable dt)
        {
            List<int> count = new List<int>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string session = dt.Rows[i][0].ToString();
                string cla = dt.Rows[i][1].ToString();
                string date = dt.Rows[i][2].ToString();
                string time = dt.Rows[i][3].ToString();

                SqlCommand cmd = new SqlCommand($"INSERT INTO AdminAdmission(session, class, date, time) VALUES('{session}', '{cla}', '{date}', '{time}')", con);
                con.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    con.Close();
                    count.Add(1);
                }
            }

            if (count.Contains(0))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool InsertNewAdmissionData(string session, DataTable dt)
        {
            List<int> count = new List<int>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string cla = dt.Rows[i][0].ToString();
                string date = dt.Rows[i][1].ToString();
                string time = dt.Rows[i][2].ToString();

                SqlCommand cmd = new SqlCommand($"INSERT INTO AdminAdmission(session, class, date, time) VALUES('{session}', '{cla}', '{date}', '{time}')", con);
                con.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    con.Close();
                    count.Add(1);
                }
            }

            if (count.Contains(0))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public DataTable GetFees()
        {
            con.Open();
            string query = string.Format($"SELECT * FROM AdminFees");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public bool UpdateFees(int monthly, int exam, int admission, int dev, int lib)
        {
            con.Open();
            string query = string.Format($"UPDATE AdminFees SET monthly={monthly}, exam={exam}, admission={admission}, development={dev}, library={lib}");
            SqlCommand cmd = new SqlCommand(query, con);

            if(cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public DataTable GetAdmissionSession()
        {
            con.Open();
            string query = string.Format($"SELECT session FROM AdmissionFees");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("session", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetApplicantsBySession(string session)
        {
            con.Open();
            string query = string.Format($"SELECT form AS 'Form No', name AS 'Applicant Name', class AS 'Class' FROM AdmissionFees WHERE session='{session}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetApplicantsByClassSession(string cla, string session)
        {
            con.Open();
            string query = string.Format($"SELECT form AS 'Form No', name AS 'Applicant Name' FROM AdmissionFees WHERE session='{session}' AND class='{cla}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public bool IsAdmissionCreated(string session, string cla)
        {
            con.Open();
            string query = string.Format($"SELECT * FROM AdminAdmission WHERE session='{session}' AND class='{cla}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                con.Close();
                return true;
            }
            else
            {
                con.Close();
                return false;
            }
        }

        public bool IsAdmissionDateTimeCreated(string session, string date, string time)
        {
            con.Open();
            string query = string.Format($"SELECT * FROM AdminAdmission WHERE session='{session}' AND date='{date}' AND time='{time}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                con.Close();
                return true;
            }
            else
            {
                con.Close();
                return false;
            }
        }

        public DataTable GetAdmissionScheduleInfo(string session)
        {
            con.Open();
            string query = string.Format($"SELECT class AS 'Class', date AS 'Date', time AS 'Time' FROM AdminAdmission WHERE session='{session}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public bool UpdateAdmissionSchedule(string session, string cla, string date, string time)
        {
            con.Open();
            string query = string.Format($"UPDATE AdminAdmission SET date='{date}', time='{time}' WHERE session='{session}' AND class='{cla}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public bool DeleteAdmissionSchedule(string session, string cla)
        {
            con.Open();
            string query = string.Format($"DELETE FROM AdminAdmission WHERE session='{session}' AND class='{cla}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public bool IsTermCreated(string session, string term, string subId)
        {
            con.Open();
            string query = string.Format($"SELECT * FROM AdminTerm WHERE session='{session}' AND term='{term}' AND subId='{subId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                con.Close();
                return true;
            }
            else
            {
                con.Close();
                return false;
            }
        }

        public bool IsScheduleCreated(string session, string term)
        {
            con.Open();
            string query = string.Format($"SELECT * FROM AdminTerm WHERE session='{session}' AND term='{term}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                con.Close();
                return true;
            }
            else
            {
                con.Close();
                return false;
            }
        }

        public bool IsTermDateTimeCreated(string session, string date, string time)
        {
            con.Open();
            string query = string.Format($"SELECT * FROM AdminAdmission WHERE session='{session}' AND date='{date}' AND time='{time}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                con.Close();
                return true;
            }
            else
            {
                con.Close();
                return false;
            }
        }

        public bool UpdateTermSchedule(string session, string term, string subId, string date, string time)
        {
            con.Open();
            string query = string.Format($"UPDATE AdminTerm SET date='{date}', time='{time}' WHERE session='{session}' AND term='{term}' AND subId='{subId}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public bool DeleteTermSchedule(string session, string term, string subId)
        {
            con.Open();
            string query = string.Format($"DELETE FROM AdminTerm WHERE session='{session}' AND term='{term}' AND subId='{subId}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public DataTable GetTermScheduleBySession(string session)
        {
            con.Open();
            string query = string.Format($"SELECT term AS 'Term', subId AS 'Subject ID', class AS 'Class', examDate AS 'Date', examTime AS 'Time' FROM AdminTerm WHERE session='{session}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetTermScheduleBySessionTerm(string session, string term)
        {
            con.Open();
            string query = string.Format($"SELECT subId AS 'Subject ID', class AS 'Class', examDate AS 'Date', examTime AS 'Time' FROM AdminTerm WHERE session='{session}' AND term='{term}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetScholarInfo(string session)
        {
            con.Open();
            string query = string.Format($"SELECT studentId AS 'Student ID', percentage AS 'Percentage', reason AS 'Reason' FROM ScholarStudents WHERE session='{session}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public bool InsertScholar(string session, string stu, int percent, string reason)
        {
            con.Open();
            string query = string.Format($"INSERT INTO ScholarStudents(session, studentId, percentage, reason) VALUES('{session}', '{stu}', {percent}, '{reason}')");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public bool UpdateScholar(string session, string stu, int percent, string reason)
        {
            con.Open();
            string query = string.Format($"UPDATE ScholarStudents SET percentage={percent}, reason='{reason}' WHERE session='{session}' AND studentId='{stu}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public bool DeleteScholar(string session, string stu)
        {
            con.Open();
            string query = string.Format($"DELETE FROM ScholarStudents WHERE session='{session}' AND studentId='{stu}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public bool UpdateStudentInfo(string stuId, string name, string fname, string mname, string dob, string religion, string gender, string bg, string present, string permanent, string nation)
        {
            con.Open();
            string query = string.Format($"UPDATE Students SET studentName='{name}', fatherName='{fname}', motherName='{mname}', dob='{dob}', religion='{religion}', gender='{gender}', bloodGroup='{bg}', presentAddress='{present}', permanentAddress='{permanent}', nationality='{nation}' WHERE studentId='{stuId}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public bool UpdateGuardianInfo(string stuId, string name, string prof, string phone, string mail)
        {
            con.Open();
            string query = string.Format($"UPDATE Guardians SET gName='{name}', gProfession='{prof}', phoneNo='{phone}', email='{mail}' WHERE studentId='{stuId}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public DataTable GetEmployeeSchedule(string session)
        {
            con.Open();
            string query = string.Format($"SELECT employeeId AS 'Employee ID', subId AS 'Subject ID', class AS 'Class', sec AS 'Section', day AS 'Day', time AS 'Time' FROM TeacherSubjects WHERE session='{session}' ORDER BY employeeId");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetAdmissionSchedule(string session)
        {
            con.Open();
            string query = string.Format($"SELECT class AS 'Class', employeeId AS 'Employee ID', examDate AS 'Date', examTime AS 'Time' FROM HeadAdmission WHERE session='{session}' ORDER BY employeeId");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetAdmissionScheduleById(string session, string emp)
        {
            con.Open();
            string query = string.Format($"SELECT class AS 'Class', examDate AS 'Date', examTime AS 'Time' FROM HeadAdmission WHERE session='{session}' AND employeeId='{emp}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetTermSchedule(string session, string term)
        {
            con.Open();
            string query = string.Format($"SELECT employeeId AS 'Employee ID', subId AS 'Subject ID', class AS 'Class', examDate AS 'Date', examTime AS 'Time' FROM HeadTerm WHERE session='{session}' AND term='{term}' ORDER BY employeeId");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetTermScheduleById(string session, string term, string emp)
        {
            con.Open();
            string query = string.Format($"SELECT subId AS 'Subject ID', class AS 'Class', examDate AS 'Date', examTime AS 'Time' FROM HeadTerm WHERE session='{session}' AND term='{term}' AND employeeId='{emp}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public bool InsertLeaveData(string year, string empId)
        {
            con.Open();
            string query = string.Format($"UPDATE EmployeeLeaves SET approved={1} WHERE year='{year}' AND employeeId='{empId}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public bool UpdateLeaveData(string year, string empId, string from, string to, string reason, int count)
        {
            con.Open();
            string query = string.Format($"UPDATE EmployeeLeaves SET lFrom='{from}', lTo='{to}', reason='{reason}', date='{DateTime.Now.ToShortDateString()}', count={count} WHERE year='{year}' AND employeeId='{empId}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public DataTable GetLeaveData(string year, string empId, int approv)
        {
            con.Open();
            string query = string.Format($"SELECT employeeId as 'Employee ID', lfrom AS 'From', lTo AS 'To', reason AS 'Details', count AS 'No of Days' FROM EmployeeLeaves WHERE year='{year}' AND approved={approv} AND employeeId='{empId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetLeaveDataById(string year, string empId)
        {
            con.Open();
            string query = string.Format($"SELECT employeeId as 'Employee ID', lfrom AS 'From', lTo AS 'To', reason AS 'Details', count AS 'No of Days' FROM EmployeeLeaves WHERE year='{year}' AND approved={1} AND employeeId='{empId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetLeaveDataByYear(string year)
        {
            con.Open();
            string query = string.Format($"SELECT employeeId AS 'Employee ID', lFrom AS 'From', lTo AS 'To', reason AS 'Reason', date AS 'Issue Date', count AS 'Total Days' FROM EmployeeLeaves WHERE year='{year}' AND approved={1} ORDER BY employeeId");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public bool DeleteLeaveData(int id)
        {
            con.Open();
            string query = string.Format($"DELETE FROM EmployeeLeaves WHERE id={id}");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public DataTable EmployeesInfo()
        {
            con.Open();
            string query = string.Format($"SELECT LogIn.employeeId AS 'ID', employeeName AS 'Name', category AS 'Position' FROM Employees, LogIn WHERE Employees.employeeId=LogIn.employeeId AND LogIn.status<>{1} ORDER BY LogIn.status");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetRequestedLeave()
        {
            con.Open();
            string query = string.Format($"SELECT employeeId as 'Employee ID', lfrom AS 'From', lTo AS 'To', reason AS 'Details', count AS 'No of Days' FROM EmployeeLeaves WHERE year='{DateTime.Now.Year.ToString()}' AND approved={0}");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public bool IsSectionExist(string session, string cla, string sec)
        {
            con.Open();
            string query = string.Format($"SELECT capacity FROM Sections WHERE session='{session}' AND class='{cla}' AND section='{sec}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                con.Close();
                return true;
            }
            else
            {
                con.Close();
                return false;
            }
        }

        public bool IsEmployeeIdExist(string id)
        {
            con.Open();

            string query = string.Format($"SELECT * FROM Employees WHERE employeeId='{id}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return false;
            }
            else
            {
                con.Close();
                return true;
            }
        }

        public bool IsLoginIdExist(string id)
        {
            con.Open();

            string query = string.Format($"SELECT * FROM LogIn WHERE employeeId='{id}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return false;
            }
            else
            {
                con.Close();
                return true;
            }
        }

        public bool IsStartingSalaryIdExist(string id)
        {
            con.Open();

            string query = string.Format($"SELECT * FROM StartingSalary WHERE employeeId='{id}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return false;
            }
            else
            {
                con.Close();
                return true;
            }
        }

        public bool IsEmployeeEmailExist(string email)
        {
            con.Open();

            string query = string.Format($"SELECT * FROM Employees WHERE email='{email}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return false;
            }
            else
            {
                con.Close();
                return true;
            }
        }
    }
}

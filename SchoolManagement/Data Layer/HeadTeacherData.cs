﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using SchoolManagement.Helper;

namespace SchoolManagement.Data_Layer
{
    public class HeadTeacherData
    {
        private SqlConnection con;

        public HeadTeacherData()
        {
            con = new SqlConnection(ConnectString.CnnVal());
        }

        public bool CreateSection(string session, string sec, string secClass, int capacity)
        {
            con.Open();
            string query = string.Format($"INSERT INTO Sections (session, section, class, capacity) VALUES ('{session}', '{sec}', '{secClass}', {capacity})");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }
                
            con.Close();
            return false;
        }

        public DataTable GetTeachersID()
        {
            con.Open();
            string query = string.Format($"SELECT employeeId FROM LogIn WHERE status IN ({3}, {4})");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("employeeId", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetSubjectsID()
        {
            con.Open();
            string query = string.Format($"SELECT subId FROM Subjects");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("subId", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public bool IsSubAssigned(string session, string subId, string sec)
        {
            con.Open();
            string query = string.Format($"SELECT * FROM TeacherSubjects WHERE session='{session}' AND subId='{subId}' AND sec='{sec}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            if(dr.HasRows)
            {
                con.Close();
                return true;
            }
            else
            {
                con.Close();
                return false;
            }
        }

        public bool IsDayTimeAssigned(string session, string empdId, string day, string time)
        {
            con.Open();
            string query = string.Format($"SELECT * FROM TeacherSubjects WHERE session='{session}' AND day='{day}' AND time='{time}' AND employeeId='{empdId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                con.Close();
                return true;
            }
            else
            {
                con.Close();
                return false;
            }
        }

        public bool AssignSubject(string teacherId, string subId, string tClass, string tSec)
        {
            con.Open();
            string query = string.Format($"INSERT INTO TeacherSubjects (employeeId, subId, class, sec) VALUES ('{teacherId}', '{subId}', '{tClass}', '{tSec}')");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public DataTable GetClaDayTime(string subId)
        {
            con.Open();
            string query = string.Format($"SELECT class, day, time, name FROM Subjects WHERE subId='{subId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable GetSecsByClass(string session, string cla)
        {
            con.Open();
            string query = string.Format($"SELECT section FROM Sections WHERE class='{cla}' AND session='{session}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("section", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public bool AssignSubjects(DataTable dt)
        {
            List<int> count = new List<int>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string session = dt.Rows[i][0].ToString();
                string empId = dt.Rows[i][1].ToString();
                string subId = dt.Rows[i][2].ToString();
                string cla = dt.Rows[i][3].ToString();
                string sec = dt.Rows[i][4].ToString();
                string day = dt.Rows[i][5].ToString();
                string time = dt.Rows[i][6].ToString();

                SqlCommand cmd = new SqlCommand($"INSERT INTO TeacherSubjects(session, employeeId, subId, class, sec, day, time) VALUES('{session}', '{empId}', '{subId}', '{cla}', '{sec}', '{day}', '{time}')", con);
                con.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    con.Close();
                    count.Add(1);
                }
            }

            if (count.Contains(0))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public DataTable GetTermSession()
        {
            con.Open();
            string query = string.Format($"SELECT session FROM AdminAdmission");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("session", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetTermSubId(string session, string term)
        {
            con.Open();
            string query = string.Format($"SELECT subId FROM AdminTerm WHERE session='{session}' AND term='{term}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("subId", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetTermInfoBySubId(string session, string term, string subId)
        {
            con.Open();
            string query = string.Format($"SELECT class, examDate, examTime FROM AdminTerm WHERE subId='{subId}' AND session='{session}' AND term='{term}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public bool AssignTermExaminer(DataTable dt)
        {
            List<int> count = new List<int>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string session = dt.Rows[i][0].ToString();
                string term = dt.Rows[i][1].ToString();
                string empId = dt.Rows[i][2].ToString();
                string subId = dt.Rows[i][3].ToString();
                string tclass = dt.Rows[i][4].ToString();           
                string date = dt.Rows[i][5].ToString();
                string time = dt.Rows[i][6].ToString();

                SqlCommand cmd = new SqlCommand($"INSERT INTO HeadTerm(session, term, employeeId, subId, class, examDate, examTime) VALUES('{session}', '{term}', '{empId}', '{subId}', '{tclass}', '{date}', '{time}')", con);
                con.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    con.Close();
                    count.Add(1);
                }
            }

            if (count.Contains(0))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool IsExaminerAssignedByDateTime(string session, string term, string date, string time, string empId)
        {
            con.Open();
            string query = string.Format($"SELECT * FROM HeadTerm WHERE session='{session}' AND term='{term}' AND employeeId='{empId}' AND examDate='{date}' AND examTime='{time}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                con.Close();
                return true;
            }
            else
            {
                con.Close();
                return false;
            }
        }

        public bool IsAdmissionExaminerAssignedByDateTime(string session, string date, string time, string empId)
        {
            con.Open();
            string query = string.Format($"SELECT * FROM HeadAdmission WHERE session='{session}' AND employeeId='{empId}' AND examDate='{date}' AND examTime='{time}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                con.Close();
                return true;
            }
            else
            {
                con.Close();
                return false;
            }
        }

        public DataTable GetAdmissionSession()
        {
            con.Open();
            string query = string.Format($"SELECT session FROM AdminAdmission");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("session", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetAdmissionClass(string session)
        {
            con.Open();
            string query = string.Format($"SELECT class FROM AdminAdmission WHERE session='{session}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("class", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetAdmissionInfoByClass(string session, string cla)
        {
            con.Open();
            string query = string.Format($"SELECT time, date FROM AdminAdmission WHERE session='{session}' AND class='{cla}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("class", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public bool AssignAdmissionExaminer(DataTable dt)
        {
            List<int> count = new List<int>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string session = dt.Rows[i][0].ToString();
                string tclass = dt.Rows[i][1].ToString();
                string empId = dt.Rows[i][2].ToString();                
                string date = dt.Rows[i][3].ToString();
                string time = dt.Rows[i][4].ToString();

                SqlCommand cmd = new SqlCommand($"INSERT INTO HeadAdmission(session, class, employeeId, examDate, examTime) VALUES('{session}', '{tclass}', '{empId}', '{date}', '{time}')", con);
                con.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    con.Close();
                    count.Add(1);
                }
            }

            if (count.Contains(0))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public DataTable GetStudentTotalMarks(string session, string term, string cla, string sec)
        {
            con.Open();
            string query = string.Format($"SELECT StudentsMarks.studentId AS 'Student ID', studentName AS 'Name', StudentsMarks.subId AS 'Subject ID', name AS 'Name', marks AS 'Marks' FROM Students, StudentsMarks, Subjects WHERE Students.studentId=StudentsMarks.studentId AND StudentsMarks.subId=Subjects.subId AND StudentsMarks.session='{session}' AND StudentsMarks.term='{term}' AND StudentsMarks.class='{cla}' AND StudentsMarks.sec='{sec}' ORDER BY StudentsMarks.studentId");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public string GetTotalByStuId(string stuId, string session, string term, string cla, string sec)
        {
            con.Open();
            string query = string.Format($"SELECT SUM(marks) FROM StudentsMarks WHERE session='{session}' AND term='{term}' AND class='{cla}' AND sec='{sec}' AND studentId='{stuId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return "";
            }
            else
            {
                con.Close();
                return dt.Rows[0][0].ToString();
            }
        }

        public DataTable GetSecInfoBySession(string session)
        {
            con.Open();
            string query = string.Format($"SELECT class AS 'Class', section AS 'Section', capacity AS 'Capacity' FROM Sections WHERE session='{session}' ORDER BY class");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public bool UpdateSecInfo(string session, string cla, string sec, int capa)
        {
            con.Open();
            string query = string.Format($"UPDATE Sections SET capacity={capa} WHERE session='{session}' AND class='{cla}' AND section='{sec}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }
            else
            {
                con.Close();
                return false;
            }
        }

        public bool DeleteSec(string session, string cla, string sec)
        {
            con.Open();
            string query = string.Format($"DELETE FROM Sections WHERE session='{session}' AND class='{cla}' AND section='{sec}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }
            else
            {
                con.Close();
                return false;
            }
        }

        public bool UpdateAllSecBySession(DataTable dt, string session, int capa)
        {
            List<int> count = new List<int>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string cla = dt.Rows[i][0].ToString();
                string sec = dt.Rows[i][1].ToString();

                SqlCommand cmd = new SqlCommand($"INSERT INTO Sections(session, class, section, capacity) VALUES('{session}', '{cla}', '{sec}', {capa})", con);
                con.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    con.Close();
                    count.Add(1);
                }
            }

            if (count.Contains(0))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public DataTable GetBoardInfoBySession(string session)
        {
            con.Open();
            string query = string.Format($"SELECT exam AS 'Exam', candidates AS 'Total Candidates', passed AS 'Passed', failed AS 'Failed', description AS 'Grade Obtained By Students' FROM BoardExam WHERE session='{session}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetBoardInfoBySessionExam(string session, string exam)
        {
            con.Open();
            string query = string.Format($"SELECT candidates AS 'Total Candidates', passed AS 'Passed', failed AS 'Failed', description AS 'Grade Obtained By Students' FROM BoardExam WHERE session='{session}' AND exam='{exam}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public bool InsertBoardInfo(string session, string exam, int candi, int pass, int fail, string des)
        {
            con.Open();
            string query = string.Format($"INSERT INTO BoardExam(session, exam, candidates, passed, failed, description) VALUES('{session}', '{exam}', {candi}, {pass}, {fail}, '{des}')");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public bool UpdateBoardInfo(string session, string exam, int candi, int pass, int fail, string des)
        {
            con.Open();
            string query = string.Format($"UPDATE BoardExam SET candidates={candi}, passed={pass}, failed={fail}, description='{des}' WHERE session='{session}' AND exam='{exam}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public DataTable GetEmployeeSchedule(string session, string empId)
        {
            con.Open();
            string query = string.Format($"SELECT subId AS 'Subject ID', class AS 'Class', sec AS 'Section', day AS 'Day', time AS 'Time' FROM TeacherSubjects WHERE session='{session}' AND employeeId='{empId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public bool DeleteTeacherSchedule(string session, string empId, string subId, string cla, string sec)
        {
            con.Open();
            string query = string.Format($"DELETE FROM TeacherSubjects WHERE session='{session}' AND employeeId='{empId}' AND subId='{subId}' AND class='{cla}', AND section='{sec}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public DataTable GetEmployeeContactInfo(string uId)
        {
            con.Open();
            string query = string.Format($"SELECT phone AS 'Contact No', email AS 'Email', presentAddress AS 'Present Address', permanentAddress AS 'Permanent Address' FROM Employees WHERE employeeId='{uId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetSubjectDetails()
        {
            con.Open();
            string query = string.Format($"SELECT subId AS 'ID', name AS 'Name', class AS 'Class', day AS 'Day', time AS 'Time' FROM Subjects ORDER BY class");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetSubjectDetailsByClass(string cla)
        {
            con.Open();
            string query = string.Format($"SELECT subId AS 'ID', name AS 'Name', day AS 'Day', time AS 'Time' FROM Subjects WHERE class='{cla}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetUnassignedSubjects(string session)
        {
            con.Open();
            string query = string.Format($"SELECT subId AS 'ID', name AS 'Name', class AS 'Class' FROM Subjects WHERE subId NOT IN (SELECT subId FROM TeacherSubjects WHERE session='{session}') ORDER BY class");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public bool IsSessionExist(string session)
        {
            con.Open();
            string query = string.Format($"SELECT * FROM AdminAdmission WHERE session='{session}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return false;
            }
            else
            {
                con.Close();
                return true;
            }
        }

        public bool IsMarkExist(string session, string term, string sub, string stu)
        {
            con.Open();
            string query = string.Format($"SELECT marks FROM StudentsMarks WHERE session='{session}' AND term='{term}' AND subId='{sub}' AND studentId='{stu}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                con.Close();
                return true;
            }
            else
            {
                con.Close();
                return false;
            }
        }

        public DataTable GetGrading()
        {
            con.Open();
            string query = string.Format($"SELECT id, marks AS 'Marks Range', grade as 'Grading Level' FROM Grading");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetGradingMarks()
        {
            con.Open();
            string query = string.Format($"SELECT marks, grade FROM Grading");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public bool IsLevelExist(string level)
        {
            con.Open();
            string query = string.Format($"SELECT * FROM Grading WHERE grade='{level}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                con.Close();
                return true;
            }
            else
            {
                con.Close();
                return false;
            }
        }

        public bool InsertMarksGrade(string marks, string level)
        {
            con.Open();
            string query = string.Format($"INSERT INTO Grading(marks, grade) VALUES('{marks}', '{level}')");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public DataTable GetUpdableMarks(string session, string term, string subId)
        {
            con.Open();
            string query = string.Format($"SELECT StudentsMarks.studentId AS 'Student ID', studentName AS 'Student Name', marks AS 'Obtained Marks', grade AS 'Garde' FROM StudentsMarks,Students WHERE StudentsMarks.studentId=Students.studentId AND session='{session}' AND term='{term}' AND subId='{subId}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public bool UpdateMarks(string session, string term, string subId, string stu, int marks, string grade)
        {
            con.Open();
            string query = string.Format($"UPDATE StudentsMarks SET marks={marks}, grade='{grade}' WHERE session='{session}' AND term='{term}' AND subId='{subId}' AND studentId='{stu}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public DataTable StudentsByGrade(string session, string cla, string term, string grade)
        {
            con.Open();
            string query = string.Format($"SELECT StudentsMarks.studentId AS 'Student ID', Subjects.name AS 'Subject Name', marks AS 'Obtained Marks' FROM StudentsMarks,Subjects WHERE StudentsMarks.subId=Subjects.subId AND session='{session}' AND term='{term}' AND StudentsMarks.class='{cla}' AND grade='{grade}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetGrades()
        {
            con.Open();
            string query = string.Format($"SELECT grade FROM Grading");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("grade", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }
    }   
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using SchoolManagement.Helper;

namespace SchoolManagement.Data_Layer
{
    public class OwnerData
    {
        private SqlConnection con;

        public OwnerData()
        {
            con = new SqlConnection(ConnectString.CnnVal());
        }

        public DataTable IncomeByMonth(string year)
        {
            con.Open();
            string query = string.Format($"SELECT month, SUM(total) AS 'Total' FROM Fees WHERE year='{year}' GROUP BY month");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable EmployeeExpenseByMonth(string year)
        {
            con.Open();
            string query = string.Format($"SELECT month, SUM(total) AS 'Total' FROM EmployeeSalary WHERE year='{year}' GROUP BY month");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable InventoryExpenseByMonth(string year)
        {
            con.Open();
            string query = string.Format($"SELECT month, SUM(total) AS 'Total' FROM Inventory WHERE year='{year}' GROUP BY month");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }
        
        public DataTable SalaryInfo(string category)
        {
            con.Open();
            string query = string.Format($"SELECT basic, home, medical, tiffin FROM StartingSalary WHERE category='{category}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public bool UpdateSalary(string category, int basic, int home, int medical, int tiffin)
        {
            con.Open();
            string query = string.Format($"UPDATE StartingSalary SET basic={basic}, home={home}, medical={medical}, tiffin={tiffin} WHERE category='{category}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if(cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public DataTable OverEmployees(string uId)
        {
            con.Open();
            string query = string.Format($"SELECT Employees.employeeId AS 'ID', employeeName AS 'Name', category AS 'Position', phone AS 'Contact No', email AS 'Email' FROM Employees, LogIn WHERE LogIn.employeeId=Employees.employeeId AND Employees.employeeId<>'{uId}' ORDER BY LogIn.status");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable OverEmployeesByCategory(string category)
        {
            con.Open();
            string query = string.Format($"SELECT Employees.employeeId AS 'ID', employeeName AS 'Name', category AS 'Position', phone AS 'Contact No', email AS 'Email' FROM Employees, LogIn WHERE LogIn.employeeId=Employees.employeeId AND category='{category}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable OverStudents(string session)
        {
            con.Open();
            string query = string.Format($"SELECT class AS 'Class', COUNT(studentId) AS 'Total Students' FROM StudentsClass WHERE session='{session}' GROUP BY class");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetMonthByYear(string year)
        {
            con.Open();
            string query = string.Format($"SELECT month FROM EmployeeAttendance WHERE year='{year}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public string GetTotalIncomeByMonthYear(string month, string year)
        {
            con.Open();
            string query = string.Format($"SELECT ISNULL(SUM(total), 0) FROM Fees WHERE month='{month}' AND year='{year}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return "0";
            }
            else
            {
                con.Close();
                return dt.Rows[0][0].ToString();
            }
        }

        public string GetTotalApplicantsFees(string month, string year)
        {
            con.Open();
            string query = string.Format($"SELECT ISNULL(SUM(amount), 0) FROM AdmissionFees WHERE month='{month}' AND session='{year}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return "0";
            }
            else
            {
                con.Close();
                return dt.Rows[0][0].ToString();
            }
        }

        public string GetTotalSalaryByMonthYear(string month, string year)
        {
            con.Open();
            string query = string.Format($"SELECT ISNULL(SUM(total), 0) FROM EmployeeSalary WHERE month='{month}' AND year='{year}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return "0";
            }
            else
            {
                con.Close();
                return dt.Rows[0][0].ToString();
            }
        }

        public string GetTotalInventoryByMonthYear(string month, string year)
        {
            con.Open();
            string query = string.Format($"SELECT ISNULL(SUM(total), 0) FROM Inventory WHERE month='{month}' AND year='{year}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (dt.Rows.Count == 0)
            {
                con.Close();
                return "0";
            }
            else
            {
                con.Close();
                return dt.Rows[0][0].ToString();
            }
        }

        public DataTable GetSalaryDetailsByCategory(string cat)
        {
            con.Open();
            string query = string.Format($"SELECT Employees.employeeId AS 'Employee ID', joindate AS 'Joining Date', DATEDIFF(day, joindate, '{DateTime.Now.ToShortDateString()}') AS 'Service In Days', basic AS 'Basic', home AS 'Home', medical AS 'Medical', tiffin AS 'Tiffin'FROM Employees, StartingSalary WHERE Employees.employeeId=StartingSalary.employeeId AND category='{cat}' AND active={1}");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public DataTable GetSalaryDetailsById(string id)
        {
            con.Open();
            string query = string.Format($"SELECT Employees.employeeId AS 'Employee ID', joindate AS 'Joining Date', DATEDIFF(day, joindate, '{DateTime.Now.ToShortDateString()}') AS 'Service In Days', basic AS 'Basic', home AS 'Home', medical AS 'Medical', tiffin AS 'Tiffin'FROM Employees, StartingSalary WHERE Employees.employeeId=StartingSalary.employeeId AND Employees.employeeId='{id}' AND active={1}");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }

        public bool UpdateSalaryById(string id, float basic, float home, float medical, float tiffin)
        {
            con.Open();
            string query = string.Format($"UPDATE StartingSalary SET basic={basic}, home={home}, medical={medical}, tiffin={tiffin} WHERE employeeId='{id}'");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public DataTable GetPaymentYear()
        {
            con.Open();
            string query = string.Format($"SELECT DISTINCT year FROM EmployeeSalary");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("year", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetPaymentMonth(string year)
        {
            con.Open();
            string query = string.Format($"SELECT DISTINCT month FROM EmployeeSalary WHERE year='{year}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("month", typeof(string));
            dt.Load(reader);
            con.Close();

            return dt;
        }

        public DataTable GetPaymentDetailsByMonthYear(string year, string month)
        {
            con.Open();
            string query = string.Format($"SELECT Employees.employeeId AS 'Employee ID', employeeName AS 'Name', category AS 'Designation', total AS 'Total', basic AS 'Basic', home AS 'Home', medical AS 'Medical', tiffin AS 'Tiffin', ta_da AS 'TA_DA', incentive AS 'Incentive', festival AS 'Festival', examScript AS 'Exam Script', examGuard AS 'Exam Guard' FROM Employees, EmployeeSalary WHERE Employees.employeeId=EmployeeSalary.employeeId AND year='{year}' AND month='{month}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(reader);
            con.Close();
            return dt;
        }
    }
}

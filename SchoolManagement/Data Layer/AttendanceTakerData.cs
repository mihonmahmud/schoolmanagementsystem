﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using SchoolManagement.Helper;

namespace SchoolManagement.Data_Layer
{
    public class AttendanceTakerData
    {
        private SqlConnection con;
        private string day = DateTime.Now.Day.ToString();
        private string month = DateTime.Now.ToString("MMM");
        private string year = DateTime.Now.Year.ToString();
        private string time = DateTime.Now.ToString("hh:mm tt");


        public AttendanceTakerData()
        {
            con = new SqlConnection(ConnectString.CnnVal());
        }

        public bool InsertIn(string empId)
        {
            con.Open();
            string query = string.Format($"INSERT INTO EmployeeAttendance (employeeId, day, month, year, enter, value) VALUES ('{empId}', '{day}', '{month}', '{year}', '{time}', {1})");
            SqlCommand cmd = new SqlCommand(query, con);

            if(cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public bool InsertOut(string empId)
        {
            con.Open();
            string query = string.Format($"INSERT INTO EmployeeAttendance (employeeId, day, month, year, out, value) VALUES ('{empId}', '{day}', '{month}', '{year}', '{time}', {0})");
            SqlCommand cmd = new SqlCommand(query, con);

            if (cmd.ExecuteNonQuery() > 0)
            {
                con.Close();
                return true;
            }

            con.Close();
            return false;
        }

        public bool IsEmployeeExist(string id)
        {
            con.Open();
            string query = string.Format($"SELECT * FROM LogIn WHERE employeeId='{id}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                con.Close();
                return true;
            }
            else
            {
                con.Close();
                return false;
            }
        }

        public bool IfDayDataExist(string id)
        {
            con.Open();
            string query = string.Format($"SELECT * FROM EmployeeAttendance WHERE employeeId='{id}' AND day='{day}' AND month='{month}' AND year='{year}'");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                con.Close();
                return true;
            }
            else
            {
                con.Close();
                return false;
            }
        }

        public string CheckStatus(string id)
        {
            con.Open();
            string query = string.Format($"SELECT TOP 1 value FROM EmployeeAttendance WHERE employeeId='{id}' AND day='{day}' AND month='{month}' AND year='{year}' ORDER BY id DESC");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            if (int.Parse(dt.Rows[0][0].ToString()) == 1)
            {
                con.Close();
                return "enter";
            }
            else
            {
                con.Close();
                return "out";
            }
        }
    }
}

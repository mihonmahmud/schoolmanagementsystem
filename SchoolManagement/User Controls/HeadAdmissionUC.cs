﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class HeadAdmissionUC : UserControl
    {
        HeadTeacherData htd = new HeadTeacherData();
        private DataTable ddt = new DataTable();
        private DataRow dr;

        public HeadAdmissionUC()
        {
            InitializeComponent();
        }

        private void HeadAdmission_Load(object sender, EventArgs e)
        {
            try
            {
                teacherIdCombo.ValueMember = "employeeId";
                teacherIdCombo.DisplayMember = "employeeId";
                teacherIdCombo.DataSource = htd.GetTeachersID();
                teacherIdCombo.SelectedIndex = -1;

                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(htd.GetAdmissionSession());
                sessionCombo.SelectedIndex = -1;

            }
            catch (Exception) { }

            ddt.Columns.Add("Session");
            ddt.Columns.Add("Class");
            ddt.Columns.Add("Teacher ID");
            ddt.Columns.Add("Exam Date");
            ddt.Columns.Add("Exam Time");
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void classCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(classCombo.SelectedIndex == -1)
            {
                date.Text = time.Text = "";
            }
            else
            {
                try
                {
                    DataTable info = htd.GetAdmissionInfoByClass(sessionCombo.SelectedValue.ToString(), classCombo.SelectedValue.ToString());

                    date.Text = info.Rows[0][1].ToString();
                    time.Text = info.Rows[0][0].ToString();
                }
                catch (Exception) { }
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if(htd.IsAdmissionExaminerAssignedByDateTime(sessionCombo.SelectedValue.ToString(), date.Text, time.Text, teacherIdCombo.SelectedValue.ToString()))
            {
                MessageBox.Show("ALREADY ASSIGNED ON THIS SLOT!!!");
                teacherIdCombo.SelectedIndex = -1;
            }
            else
            {
                dr = ddt.NewRow();
                dr["Session"] = sessionCombo.SelectedValue.ToString();
                dr["Class"] = classCombo.SelectedValue.ToString();
                dr["Teacher ID"] = teacherIdCombo.SelectedValue.ToString();
                dr["Exam Date"] = date.Text;
                dr["Exam Time"] = time.Text;
                ddt.Rows.Add(dr);

                scheduleGrid.DataSource = ddt;
            }           
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if(scheduleGrid.Rows.Count > 0)
            {
                try
                {
                    if (htd.AssignAdmissionExaminer((DataTable)scheduleGrid.DataSource))
                    {
                        MessageBox.Show("ASSIGNED!");
                        Clear();
                    }
                }
                catch (Exception) { }
            }
            else
            {
                MessageBox.Show("NO DATA FOUND!");
            }
        }

        private void Clear()
        {
            sessionCombo.SelectedIndex = teacherIdCombo.SelectedIndex = classCombo.SelectedIndex = -1;
            date.Text = time.Text = "";
            ddt.Rows.Clear();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void sessionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(sessionCombo.SelectedIndex == -1)
            {
                classCombo.DataSource = null;
            }
            else
            {
                try
                {
                    classCombo.ValueMember = "class";
                    classCombo.DisplayMember = "class";
                    classCombo.DataSource = htd.GetAdmissionClass(sessionCombo.SelectedValue.ToString());
                    classCombo.SelectedIndex = -1;
                }
                catch (Exception) { }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;
using System.Data.SqlClient;

namespace SchoolManagement
{
    public partial class BorrowHistoryUC : UserControl
    {
        private LibrarianData lb = new LibrarianData();
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-E7IA1G1;Initial Catalog=SchoolManagement;Integrated Security=True");
        public BorrowHistoryUC()
        {
            InitializeComponent();
            metroGrid2.DataSource = this.showWGrid();
        }

        public DataTable showWGrid()
        {
            return lb.GridBorrowUpdate();

        }

        public DataTable SearchBorrow(string searchBorrows)
        {
            return lb.SearchBorrow(searchBorrows);
        }

        public DataTable SearchBorrower(string searchBorrows)
        {
            return lb.SearchBorrower(searchBorrows);
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void bookListGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBoxId_TextChanged(object sender, EventArgs e)
        {
            string searchBorrows;
            if (ComboBox1.Text == "")
                MessageBox.Show("Select Category");
            else if (ComboBox1.Text == "BookId")
            {
                searchBorrows = textBoxId.Text;
                metroGrid2.DataSource = this.SearchBorrow(searchBorrows);
            }

            else if(ComboBox1.Text=="BorrowerId")
            {
                searchBorrows = textBoxId.Text;
                metroGrid2.DataSource = this.SearchBorrower(searchBorrows);
            }
            else
            {
               metroGrid2.DataSource = this.showWGrid();
                
            }
            
        }
        
    }
}

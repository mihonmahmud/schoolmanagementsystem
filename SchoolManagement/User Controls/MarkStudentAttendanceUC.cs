﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class MarkStudentAttendanceUC : UserControl
    {
        private TeacherData td = new TeacherData();
        private string _userId;

        public MarkStudentAttendanceUC(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        private void MarkStudentAttendanceUC_Load(object sender, EventArgs e)
        {
            session.Text = DateTime.Now.Year.ToString();
            try
            {
                subIdCombo.ValueMember = "subId";
                subIdCombo.DisplayMember = "subId";
                subIdCombo.DataSource = td.GetSubjectsById(session.Text, _userId);
                subIdCombo.SelectedIndex = -1;
                attendanceGrid.DataSource = null;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void sessionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void subIdCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (subIdCombo.SelectedIndex == -1)
            {
                subClass.Text = "";
                secCombo.SelectedIndex = -1;
                attendanceGrid.DataSource = null;
            }
            else
            {
                try
                {
                    subClass.Text = td.GetClassById(session.Text, subIdCombo.SelectedValue.ToString());

                    if (subClass.Text != null)
                    {
                        try
                        {
                            secCombo.ValueMember = "sec";
                            secCombo.DisplayMember = "sec";
                            secCombo.DataSource = td.GetSecsByCla(session.Text, subIdCombo.SelectedValue.ToString(), subClass.Text, _userId);
                            secCombo.SelectedIndex = -1;
                            attendanceGrid.DataSource = null;
                        }
                        catch (Exception) { }
                    }
                }
                catch (Exception) { }
            }
        }

        private void secCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if(attendanceGrid.Rows.Count > 0)
            {
                DataTable dt = (DataTable)attendanceGrid.DataSource;

                List<int> count = new List<int>();

                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {          
                    try
                    {
                        bool isChecked = Convert.ToBoolean(attendanceGrid.Rows[i].Cells["Mark"].Value);

                        if ((isChecked == true) && (Convert.ToString(attendanceGrid.Rows[i].Cells[0].Value) != null))
                        {
                            if (td.MarkAttendance(session.Text, Convert.ToString(attendanceGrid.Rows[i].Cells[1].Value), subIdCombo.SelectedValue.ToString(), subClass.Text, secCombo.SelectedValue.ToString(), "Y", _userId))
                            {
                                count.Add(1);
                            }
                            else
                            {
                                count.Add(0);
                            }
                        }
                        else if ((isChecked == false) && (Convert.ToString(attendanceGrid.Rows[i].Cells[0].Value) != null))
                        {
                            if (td.MarkAttendance(session.Text, Convert.ToString(attendanceGrid.Rows[i].Cells[1].Value), subIdCombo.SelectedValue.ToString(), subClass.Text, secCombo.SelectedValue.ToString(), "N", _userId))
                            {
                                count.Add(1);
                            }
                            else
                            {
                                count.Add(0);
                            }
                        }
                    }
                    catch (Exception) { }
                }

                if(count.Contains(0))
                {

                }
                else
                {
                    MessageBox.Show("DONE!");
                    attendanceGrid.DataSource = null;
                    subIdCombo.SelectedIndex = secCombo.SelectedIndex = -1;
                    subClass.Text = "";
                }           
            }
            else
            {
                MessageBox.Show("NO DATA FOUND TO BE INSERTED!");
            }
        }

        private void attendanceGrid_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                attendanceGrid.Rows[e.RowIndex].Cells["Mark"].Value = true;
            }
            catch (Exception) { }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if(td.IsAttendanceDataExist(session.Text, subIdCombo.SelectedValue.ToString(), subClass.Text, secCombo.SelectedValue.ToString(), _userId))
            {
                MessageBox.Show("Attendance Already Taken! PLease Use The Update Option For Attendance Modification!");
            }
            else
            {
                try
                {
                    DataTable dt = new DataTable();
                    dt = td.GetStudentsList(session.Text, subClass.Text, secCombo.SelectedValue.ToString());
                    dt.Columns.Add("Mark", typeof(bool));
                    attendanceGrid.DataSource = dt;

                    for (int i = 0; i <= attendanceGrid.Rows.Count - 1; i++)
                        attendanceGrid.Rows[i].Cells["Mark"].Value = false;
                }
                catch (Exception) { }
            }
        }
    }
}

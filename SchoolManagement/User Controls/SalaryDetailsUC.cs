﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class SalaryDetailsUC : UserControl
    {
        private AccountantData ad = new AccountantData();

        public SalaryDetailsUC()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if((empId.Text != "") && (refNo.Text == ""))
            {
                try
                {
                    paymentGrid.DataSource = ad.GetPaymentById(empId.Text);
                }
                catch (Exception) { }
            }
            else if ((refNo.Text != ""))
            {
                try
                {
                    paymentGrid.DataSource = ad.GetPaymentByRef(refNo.Text);
                }
                catch (Exception) { }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}

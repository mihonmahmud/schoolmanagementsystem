﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement
{
    public partial class AddBookUC : UserControl
    {
        private LibrarianData lb = new LibrarianData();
        public AddBookUC()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int check = lb.CheckIdData(bookIdText.Text);

            if (check > 0)
            {
                MessageBox.Show("BookId " + bookIdText.Text + " Already Exists");
            }
            else
            { 
                try
                {
                    if (!string.IsNullOrWhiteSpace(bookIdText.Text) && !string.IsNullOrWhiteSpace(titleText.Text) && !string.IsNullOrWhiteSpace(editionText.Text) && !string.IsNullOrWhiteSpace(authorText.Text) && !string.IsNullOrWhiteSpace(quantityText.Text) && !string.IsNullOrWhiteSpace(shelfText.Text) && categoryCombo.SelectedIndex != -1)
                    {
                        if (lb.InsertBook(bookIdText.Text.Trim(), titleText.Text.Trim(), int.Parse(editionText.Text.Trim()), authorText.Text.Trim(), categoryCombo.SelectedItem.ToString(), int.Parse(quantityText.Text.Trim()), shelfText.Text.Trim()))
                        {
                            MessageBox.Show("Book Added!");

                        }
                        else
                        {
                            MessageBox.Show("Something went wrong! Please try again");
                        }
                    }
                    else
                    {
                        MessageBox.Show("FILL ALL REQUIRED FIELDS!");
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            categoryCombo.SelectedIndex = -1;
            bookIdText.Text = titleText.Text = editionText.Text = authorText.Text = quantityText.Text = shelfText.Text = string.Empty;
        }

        private void bookIdText_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void quantityText_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }
    }
}

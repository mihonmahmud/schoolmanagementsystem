﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;
using System.Data.SqlClient;

namespace SchoolManagement
{
    public partial class ManageBorrowBookUC : UserControl
    {
        private LibrarianData lb = new LibrarianData();
        //private DataTable borrowBookInfo;
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-E7IA1G1;Initial Catalog=SchoolManagement;Integrated Security=True");
        public ManageBorrowBookUC()
        {
            InitializeComponent();
            bookListGrid.DataSource = this.showWGrid();
        }

        public DataTable showWGrid()
        {
            return lb.GridBorrowUpdate();

        }

        public DataTable SearchBorrow(string searchBorrows)
        {
            return lb.SearchBorrow(searchBorrows);
        }

        public DataTable SearchBorrower(string searchBorrows)
        {
            return lb.SearchBorrower(searchBorrows);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {

                if (CheckReturned.Checked)
                {
                    if(!string.IsNullOrWhiteSpace(BookId.Text) && !string.IsNullOrWhiteSpace(BorrowerId.Text) && !string.IsNullOrWhiteSpace(BorrowerName.Text) && !string.IsNullOrWhiteSpace(Contact.Text) && !string.IsNullOrWhiteSpace(BorrowDate.Text) && !string.IsNullOrWhiteSpace(ReturnDate.Text))
                    {
                        int returned = 1;
                        lb.UpdateBorrowBook(BookId.Text, BorrowerId.Text, BorrowerName.Text, Contact.Text, BorrowDate.Text, ReturnDate.Text, returned);
                        MessageBox.Show("Book Updated!");
                        bookListGrid.DataSource = this.showWGrid();
                        CheckReturned.Checked = false;
                        BookId.Text = BorrowerId.Text = BorrowerName.Text = Contact.Text = BorrowDate.Text = ReturnDate.Text = string.Empty;
                    }
                    else
                    {
                        MessageBox.Show("FILL ALL REQUIRED FIELDS!");
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(BookId.Text) && !string.IsNullOrWhiteSpace(BorrowerId.Text) && !string.IsNullOrWhiteSpace(BorrowerName.Text) && !string.IsNullOrWhiteSpace(Contact.Text) && !string.IsNullOrWhiteSpace(BorrowDate.Text) && !string.IsNullOrWhiteSpace(ReturnDate.Text))
                    {
                        int returned = 0;
                        lb.UpdateBorrowBook(BookId.Text, BorrowerId.Text, BorrowerName.Text, Contact.Text, BorrowDate.Text, ReturnDate.Text, returned);
                        MessageBox.Show("Book Updated!");
                        bookListGrid.DataSource = this.showWGrid();
                        CheckReturned.Checked = false;
                        BookId.Text = BorrowerId.Text = BorrowerName.Text = Contact.Text = BorrowDate.Text = ReturnDate.Text = string.Empty;
                    }
                    else
                    {
                        MessageBox.Show("FILL ALL REQUIRED FIELDS!");
                    }
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Something went wrong! Please try again");
            }
        }

        private void BorrowDate_Enter(object sender, EventArgs e)
        {
            if (BorrowDate.Text == "dd-mm-yyyy")
            {
                BorrowDate.Text = "";

                BorrowDate.ForeColor = Color.Black;
            }
        }

        private void BorrowDate_Leave(object sender, EventArgs e)
        {
            if (BorrowDate.Text == "")
            {
                BorrowDate.Text = "dd-mm-yyyy";

                BorrowDate.ForeColor = Color.Silver;
            }
        }

        private void textBox6_Enter(object sender, EventArgs e)
        {
            if (ReturnDate.Text == "dd-mm-yyyy")
            {
                ReturnDate.Text = "";

                ReturnDate.ForeColor = Color.Black;
            }
        }

        private void ReturnDate_Leave(object sender, EventArgs e)
        {
            if (ReturnDate.Text == "")
            {
                ReturnDate.Text = "dd-mm-yyyy";

                ReturnDate.ForeColor = Color.Silver;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

            string searchBorrows;
            if (metroCombo1.Text == "")
                MessageBox.Show("Select Category");
            else if (metroCombo1.Text == "BookId")
            {
                searchBorrows = textBox1.Text;
                bookListGrid.DataSource = this.SearchBorrow(searchBorrows);
            }

            else if (metroCombo1.Text == "BorrowerId")
            {
                searchBorrows = textBox1.Text;
                bookListGrid.DataSource = this.SearchBorrower(searchBorrows);
            }
            else
            {
                bookListGrid.DataSource = this.showWGrid();

            }

        }

        private void bookListGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Contact.ForeColor = BorrowDate.ForeColor = ReturnDate.ForeColor = Color.Black;
            BookId.Text = bookListGrid.Rows[e.RowIndex].Cells[0].Value.ToString();
            BorrowerId.Text = bookListGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
            BorrowerName.Text = bookListGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
            Contact.Text = bookListGrid.Rows[e.RowIndex].Cells[3].Value.ToString();
            BorrowDate.Text = bookListGrid.Rows[e.RowIndex].Cells[4].Value.ToString();
            ReturnDate.Text = bookListGrid.Rows[e.RowIndex].Cells[5].Value.ToString();
            int returned = int.Parse(bookListGrid.Rows[e.RowIndex].Cells[6].Value.ToString());

            if (returned == 1)
            {
                CheckReturned.Checked = true;
            }
            else
            {
                CheckReturned.Checked = false;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            CheckReturned.Checked = false;
            BookId.Text = BorrowerId.Text = BorrowerName.Text = Contact.Text = BorrowDate.Text = ReturnDate.Text = string.Empty;
        }

        private void BookId_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void BorrowerId_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void Contact_Enter(object sender, EventArgs e)
        {
            if (Contact.Text == "880+")
            {
                Contact.Text = "";

                Contact.ForeColor = Color.Black;
            }
        }

        private void Contact_Leave(object sender, EventArgs e)
        {
            if (Contact.Text == "")
            {
                Contact.Text = "880+";

                Contact.ForeColor = Color.Silver;
            }
        }

        private void Contact_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }
    }
}

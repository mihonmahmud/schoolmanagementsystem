﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class ManageSectionsUC : UserControl
    {
        private AccountantData ad = new AccountantData();
        private HeadTeacherData htd = new HeadTeacherData();

        public ManageSectionsUC()
        {
            InitializeComponent();
        }

        private void ManageSectionsUC_Load(object sender, EventArgs e)
        {
            GetSession();
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void sessionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(sessionCombo.SelectedIndex == -1)
            {
                secGrid.DataSource = null;
            }
            else
            {
                try
                {
                    secGrid.DataSource = htd.GetSecInfoBySession(sessionCombo.SelectedValue.ToString());
                }
                catch (Exception) { }
            }
        }

        private void secGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                classCombo.SelectedItem = secGrid.Rows[e.RowIndex].Cells[0].Value.ToString();
                section.Text = secGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
                capacity.Text = secGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
            }
            catch (Exception) { }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {           
            try
            {
                if (htd.UpdateSecInfo(sessionCombo.SelectedValue.ToString(), classCombo.SelectedItem.ToString(), section.Text, int.Parse(capacity.Text)))
                {
                    secGrid.DataSource = htd.GetSecInfoBySession(sessionCombo.SelectedValue.ToString());
                    MessageBox.Show("UPDATED!");
                }
            }
            catch (Exception) { }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            try
            {
                if(htd.DeleteSec(sessionCombo.SelectedValue.ToString(), classCombo.SelectedItem.ToString(), section.Text))
                {
                    secGrid.DataSource = htd.GetSecInfoBySession(sessionCombo.SelectedValue.ToString());
                    MessageBox.Show("DELETED!");                   
                }
            }
            catch (Exception) { }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if(htd.IsSessionExist(newSession.Text) && !string.IsNullOrWhiteSpace(newCapacity.Text))
            {
                if(secGrid.Rows.Count > 0)
                {
                    try
                    {
                        if (htd.UpdateAllSecBySession((DataTable)secGrid.DataSource, newSession.Text, int.Parse(newCapacity.Text)))
                        {
                            MessageBox.Show("SECTIONS ADDED!");
                            GetSession();
                            newSession.Text = newCapacity.Text = "";
                        }
                    }
                    catch (Exception) { }
                }
                else
                {
                    MessageBox.Show("NO DATA FOUND TO BE INSERTED!");
                }
            }
            else
            {
                MessageBox.Show("FILL ALL REQUIRED FIELDS!");
            }
        }

        private void GetSession()
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ad.GetSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            classCombo.SelectedIndex = sessionCombo.SelectedIndex = -1;
            secGrid.DataSource = null;
            newSession.Text = newCapacity.Text = capacity.Text = "";
        }

        private void newCapacity_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }
    }
}

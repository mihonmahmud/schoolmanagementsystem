﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class UploadStudentMarks : UserControl
    {
        private string _userId;
        private TeacherData td = new TeacherData();
        private HeadTeacherData htd = new HeadTeacherData();
        DataTable marksTable = new DataTable();

        public UploadStudentMarks(string userId)
        {
            InitializeComponent();
            _userId = userId;

            marksTable.Columns.Add("ID");
            marksTable.Columns.Add("Name");
            marksTable.Columns.Add("Marks");
            marksTable.Columns.Add("Grade");

            editButton.Enabled = false;
            completeButton.Enabled = false;
            uploadButton.Enabled = false;
        }

        private void UploadStudentMarks_Load(object sender, EventArgs e)
        {
            session.Text = DateTime.Now.Year.ToString();
        }

        private void termCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (termCombo.SelectedIndex == -1)
            {
                subIdCombo.DataSource = null;
            }
            else
            {
                try
                {
                    subIdCombo.ValueMember = "subId";
                    subIdCombo.DisplayMember = "subId";
                    subIdCombo.DataSource = td.GetSubjectsById(session.Text, _userId);
                    subIdCombo.SelectedIndex = -1;
                }
                catch (Exception) { }
            }
        }

        private void subIdCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (subIdCombo.SelectedIndex == -1)
            {
                subClass.Text = "";
                secCombo.DataSource = null;
            }
            else
            {
                try
                {
                    subClass.Text = td.GetClassById(session.Text, subIdCombo.SelectedValue.ToString());

                    if (subClass.Text != null)
                    {
                        try
                        {
                            secCombo.ValueMember = "sec";
                            secCombo.DisplayMember = "sec";
                            secCombo.DataSource = td.GetSecsByCla(session.Text, subIdCombo.SelectedValue.ToString(), subClass.Text, _userId);
                            secCombo.SelectedIndex = -1;
                        }
                        catch (Exception) { }
                    }
                }
                catch (Exception) { }
            }
        }

        private void secCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (secCombo.SelectedIndex == -1)
            {
                marksGrid.DataSource = null;
            }
            else
            {
                try
                {
                    DataTable dt = new DataTable();
                    dt = td.GetStudentsByClaSec(session.Text, subClass.Text, secCombo.SelectedValue.ToString());
                    DataColumn col = new DataColumn("Marks", typeof(String));
                    col.ReadOnly = false;
                    dt.Columns.Add(col);

                    //for (int i = 0; i < dt.Rows.Count; i++)
                    //    dt.Rows[i][2] = 0.ToString();
                    marksGrid.DataSource = dt;

                    marksGrid.Columns["ID"].ReadOnly = true;
                    marksGrid.Columns["Name"].ReadOnly = true;

                    editButton.Enabled = true;
                    completeButton.Enabled = true;
                }
                catch (Exception) { }
            }
        }

        private void marksGrid_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e)
        {
            e.Row.Cells["Marks"].Value = "0";
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            
        }

        private string CalculateGrade(int marks)
        {
            DataTable dt = htd.GetGradingMarks();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string[] r = dt.Rows[i]["marks"].ToString().Split('-');

                    if (marks >= int.Parse(r[0]) && marks <= int.Parse(r[1]))
                    {
                        return dt.Rows[i]["grade"].ToString();
                    }
                }
            }

            return "N/A";
        }

        private void Clear()
        {
            subIdCombo.SelectedIndex = termCombo.SelectedIndex =  secCombo.SelectedIndex = -1;
            session.Text = subClass.Text = "";
            marksGrid.DataSource = null;
        }

        private void marksGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void marksGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int marks = 0;
            DataRow r = marksTable.NewRow();
            r["ID"] = marksGrid.Rows[e.RowIndex].Cells[0].Value.ToString();
            r["Name"] = marksGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
            r["Marks"] = marksGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
            if (marksGrid.Rows[e.RowIndex].Cells["Marks"].Value.ToString().Length != 0)
            {
                marks = int.Parse(marksGrid.Rows[e.RowIndex].Cells["Marks"].Value.ToString());
            }
            r["Grade"] = CalculateGrade(marks);
            marksTable.Rows.Add(r);
        }

        private void uploadButton_Click(object sender, EventArgs e)
        {
            if(marksTable.Rows.Count != marksGrid.Rows.Count)
            {
                MessageBox.Show("PLEASE ENTER MARKS FOR EVERYSTUDENT");
            }
            else
            {
                try
                {
                    if (td.InsertMarks(session.Text, termCombo.SelectedItem.ToString(), subIdCombo.SelectedValue.ToString(), subClass.Text, secCombo.SelectedValue.ToString(), marksTable))
                    {
                        MessageBox.Show("UPLOADED!");
                        marksGrid.DataSource = marksTable;
                        uploadButton.Enabled = false;
                        editButton.Enabled = false;
                        completeButton.Enabled = false;
                        marksGrid.ReadOnly = true;
                    }
                }
                catch (Exception) { }
            }
        }

        private void completeButton_Click(object sender, EventArgs e)
        {
            marksGrid.Columns["Marks"].ReadOnly = true;
            uploadButton.Enabled = true;
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            marksGrid.Columns["Marks"].ReadOnly = false;
            uploadButton.Enabled = false;
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            Clear();
            session.Text = DateTime.Now.Year.ToString();
            editButton.Enabled = false;
            completeButton.Enabled = false;
            uploadButton.Enabled = false;
        }
    }
}

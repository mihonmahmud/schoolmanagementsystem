﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class ManageEmployeeLeaveUC : UserControl
    {
        private string _userId;
        private AdminData ad = new AdminData();

        public ManageEmployeeLeaveUC(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if(empId.Text == _userId)
            {
                MessageBox.Show("SORRY! CAN NOT ISSUE LEAVE REQUEST!!!");
            }
            else if(string.IsNullOrWhiteSpace(empId.Text) || string.IsNullOrWhiteSpace(reason.Text))
            {
                MessageBox.Show("FILL ALL REQUIRED FIELDS");
            }
            else
            {              
                try
                {
                    DateTime dt = DateTime.Parse(from.Value.ToShortDateString());
                    DateTime dt2 = DateTime.Parse(to.Value.ToShortDateString());
                    int count = (int)dt2.Subtract(dt).TotalDays;

                    if (ad.InsertLeaveData(year.Text, empId.Text))
                    {
                        leaveGrid.DataSource = ad.GetLeaveData(year.Text, employeeId.Text, 1);
                        MessageBox.Show($"REQUEST ISSUED! FOR {count} DAYS!");
                    }
                }
                catch (Exception) { }
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void ManageEmployeeLeaveUC_Load(object sender, EventArgs e)
        {
            year.Text = DateTime.Now.Year.ToString();
            try
            {
                leaveGrid.DataSource = ad.GetRequestedLeave();
            }
            catch (Exception) { }           
            approvCombo.SelectedIndex = -1;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(year.Text) || string.IsNullOrWhiteSpace(employeeId.Text) || approvCombo.SelectedIndex == -1)
            {
                MessageBox.Show("FILL REQUIRED FIELDS");
            }
            else
            {             
                try
                {
                    int approv;

                    if((string)approvCombo.SelectedItem == "Approved")
                    {
                        approv = 1;
                    }
                    else if((string)approvCombo.SelectedItem == "Not Approved")
                    {
                        approv = 0;
                    }
                    else
                    {
                        approv = -1;
                    }

                    leaveGrid.DataSource = ad.GetLeaveData(year.Text, employeeId.Text, approv);
                }
                catch (Exception) { }
            }
        }

        private void leaveGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                empId.Text = leaveGrid.Rows[e.RowIndex].Cells[0].Value.ToString();
                reason.Text = leaveGrid.Rows[e.RowIndex].Cells[3].Value.ToString();
                from.Value = Convert.ToDateTime(leaveGrid.Rows[e.RowIndex].Cells[1].Value);
                to.Value = Convert.ToDateTime(leaveGrid.Rows[e.RowIndex].Cells[2].Value);
            }
            catch (Exception) { }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {         
            try
            {
                DateTime dt = DateTime.Parse(from.Value.ToShortDateString());
                DateTime dt2 = DateTime.Parse(to.Value.ToShortDateString());
                int count = (int)dt2.Subtract(dt).TotalDays;

                if (ad.UpdateLeaveData(year.Text, employeeId.Text, from.Value.ToShortDateString(), to.Value.ToShortDateString(), reason.Text, count))
                {
                    leaveGrid.DataSource = ad.GetLeaveData(year.Text, employeeId.Text, 1);
                    MessageBox.Show("UPDATED!");
                }
            }
            catch (Exception) { }
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    if (ad.DeleteLeaveData(int.Parse(id.Text)))
            //    {
            //        leaveGrid.DataSource = ad.GetLeaveData(year.Text, employeeId.Text);
            //        MessageBox.Show("DELETED!");
            //        id.Text = "";
            //    }
            //}
            //catch (Exception) { }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class ClassStudentsUC : UserControl
    {
        private TeacherData td = new TeacherData();
        string _userId;

        public ClassStudentsUC(string userId)
        {
            InitializeComponent();
            _userId = userId;

        }

        private void ClassStudentsUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(td.GetSessionById(_userId));
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void sessionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sessionCombo.SelectedIndex == -1)
            {
                subIdCombo.SelectedIndex = -1;
                attendanceGrid.DataSource = null;
            }
            else
            {
                try
                {
                    subIdCombo.ValueMember = "subId";
                    subIdCombo.DisplayMember = "subId";
                    subIdCombo.DataSource = td.GetSubjectsById(sessionCombo.SelectedValue.ToString(), _userId);
                    subIdCombo.SelectedIndex = -1;
                    attendanceGrid.DataSource = null;
                }
                catch (Exception) { }
            }
        }

        private void subIdCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (subIdCombo.SelectedIndex == -1)
            {
                subClass.Text = "";
                secCombo.SelectedIndex = -1;
                attendanceGrid.DataSource = null;
            }
            else
            {
                try
                {
                    subClass.Text = td.GetClassById(sessionCombo.SelectedValue.ToString(), subIdCombo.SelectedValue.ToString());

                    if (subClass.Text != null)
                    {
                        try
                        {
                            secCombo.ValueMember = "sec";
                            secCombo.DisplayMember = "sec";
                            secCombo.DataSource = td.GetSecsByCla(sessionCombo.SelectedValue.ToString(), subIdCombo.SelectedValue.ToString(), subClass.Text, _userId);
                            secCombo.SelectedIndex = -1;
                            attendanceGrid.DataSource = null;
                        }
                        catch (Exception) { }
                    }
                }
                catch (Exception) { }
            }
        }

        private void secCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (secCombo.SelectedIndex == -1)
            {
                attendanceGrid.DataSource = null;
            }
            else
            {
                try
                {
                    DataTable dt = new DataTable();
                    dt = td.GetStudentsList(sessionCombo.SelectedValue.ToString(), subClass.Text, secCombo.SelectedValue.ToString());
                    attendanceGrid.DataSource = dt;
                }
                catch (Exception) { }
            }
        }
    }
}

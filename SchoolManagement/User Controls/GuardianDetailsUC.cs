﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;


namespace SchoolManagement.User_Controls
{
    public partial class GuardianDetailsUC : UserControl
    {
        private TeacherData td = new TeacherData();

        public GuardianDetailsUC()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = td.GetGuardianInfo(stuId.Text);

                name.Text = dt.Rows[0][2].ToString();
                profession.Text = dt.Rows[0][3].ToString();
                contact.Text = dt.Rows[0][4].ToString();
                email.Text = dt.Rows[0][5].ToString();
            }
            catch (Exception) { }
        }
    }
}

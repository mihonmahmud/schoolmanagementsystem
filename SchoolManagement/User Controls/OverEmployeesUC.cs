﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;


namespace SchoolManagement.User_Controls
{
    public partial class OverEmployeesUC : UserControl
    {
        private OwnerData od = new OwnerData();
        private string _userId;

        public OverEmployeesUC(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        private void OverEmployeesUC_Load(object sender, EventArgs e)
        {
            try
            {
                categoryCombo.SelectedIndex = -1;
                employeesGrid.DataSource = od.OverEmployees(_userId);
            }
            catch (Exception) { }
        }

        private void categoryCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if(categoryCombo.SelectedIndex == -1)
                {
                    employeesGrid.DataSource = od.OverEmployees(_userId);
                }
                else
                {
                    employeesGrid.DataSource = od.OverEmployeesByCategory(categoryCombo.SelectedItem.ToString());
                }
            }
            catch (Exception) { }
        }

        private void search_KeyUp(object sender, KeyEventArgs e)
        {
            string field = "ID";
            try
            {
                ((DataTable)employeesGrid.DataSource).DefaultView.RowFilter = string.Format($"[{field}] LIKE '%{search.Text}%'");
            }
            catch (Exception) { }
        }
    }
}

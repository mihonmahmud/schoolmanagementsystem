﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class OwnAttendanceUC : UserControl
    {
        LoginData lid = new LoginData();
        private string _userId;

        public OwnAttendanceUC(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        private void OwnAttendanceUC_Load(object sender, EventArgs e)
        {
            try
            {
                monthCombo.ValueMember = "month";
                monthCombo.DisplayMember = "month";
                monthCombo.DataSource = RemoveDuplicatesRecords(lid.GetMonth());
                monthCombo.SelectedIndex = -1;

                yearCombo.ValueMember = "year";
                yearCombo.DisplayMember = "year";
                yearCombo.DataSource = RemoveDuplicatesRecords(lid.GetYear());
                yearCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void yearCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (yearCombo.SelectedIndex == -1)
            {
                attenGrid.DataSource = null;
            }
            else
            {               
                try
                {
                    attenGrid.DataSource = lid.GetAttenByYear(_userId, yearCombo.SelectedValue.ToString());
                }
                catch (Exception) { }
            }
        }

        private void monthCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if((monthCombo.SelectedIndex == -1) && (yearCombo.SelectedIndex != -1))
            {               
                try
                {
                    attenGrid.DataSource = lid.GetAttenByYear(_userId, yearCombo.SelectedValue.ToString());
                }
                catch (Exception) { }
            }
            else if ((monthCombo.SelectedIndex != -1) && (yearCombo.SelectedIndex != -1))
            {               
                try
                {
                    attenGrid.DataSource = lid.GetAttenByMonYear(_userId, monthCombo.SelectedValue.ToString(), yearCombo.SelectedValue.ToString());
                }
                catch (Exception) { }
            }
        }
    }
}

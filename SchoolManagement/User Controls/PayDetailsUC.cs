﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class PayDetailsUC : UserControl
    {
        private AccountantData ac = new AccountantData();

        public PayDetailsUC()
        {
            InitializeComponent();
        }

        private void PayDetailsUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ac.GetSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if((sessionCombo.SelectedIndex != -1) && (classCombo.SelectedIndex != -1) && (stuId.Text != null))
            {
                try
                {
                    payGrid.DataSource = ac.PaymentInfo(sessionCombo.SelectedValue.ToString(), classCombo.SelectedItem.ToString(), stuId.Text);
                }
                catch (Exception) { }
            }
            else
            {
                MessageBox.Show("NO DATA FOUND!");
            }
        }
    }
}

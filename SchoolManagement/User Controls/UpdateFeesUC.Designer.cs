﻿namespace SchoolManagement.User_Controls
{
    partial class UpdateFeesUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateFeesUC));
            this.admission = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.exam = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.development = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.library = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.monthly = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // admission
            // 
            this.admission.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.admission.Location = new System.Drawing.Point(729, 249);
            this.admission.Margin = new System.Windows.Forms.Padding(4);
            this.admission.Multiline = true;
            this.admission.Name = "admission";
            this.admission.Size = new System.Drawing.Size(190, 40);
            this.admission.TabIndex = 16;
            this.admission.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.admission_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(594, 253);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(130, 32);
            this.label17.TabIndex = 15;
            this.label17.Text = "Admission:";
            // 
            // exam
            // 
            this.exam.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exam.Location = new System.Drawing.Point(729, 178);
            this.exam.Margin = new System.Windows.Forms.Padding(4);
            this.exam.Multiline = true;
            this.exam.Name = "exam";
            this.exam.Size = new System.Drawing.Size(190, 40);
            this.exam.TabIndex = 18;
            this.exam.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.exam_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(647, 181);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 32);
            this.label1.TabIndex = 17;
            this.label1.Text = "Exam:";
            // 
            // development
            // 
            this.development.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.development.Location = new System.Drawing.Point(729, 320);
            this.development.Margin = new System.Windows.Forms.Padding(4);
            this.development.Multiline = true;
            this.development.Name = "development";
            this.development.Size = new System.Drawing.Size(190, 40);
            this.development.TabIndex = 20;
            this.development.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.development_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(559, 322);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 32);
            this.label2.TabIndex = 19;
            this.label2.Text = "Development:";
            // 
            // library
            // 
            this.library.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.library.Location = new System.Drawing.Point(729, 393);
            this.library.Margin = new System.Windows.Forms.Padding(4);
            this.library.Multiline = true;
            this.library.Name = "library";
            this.library.Size = new System.Drawing.Size(190, 40);
            this.library.TabIndex = 22;
            this.library.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.library_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(632, 395);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 32);
            this.label3.TabIndex = 21;
            this.label3.Text = "Library:";
            // 
            // monthly
            // 
            this.monthly.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monthly.Location = new System.Drawing.Point(729, 107);
            this.monthly.Margin = new System.Windows.Forms.Padding(4);
            this.monthly.Multiline = true;
            this.monthly.Name = "monthly";
            this.monthly.Size = new System.Drawing.Size(190, 40);
            this.monthly.TabIndex = 24;
            this.monthly.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.monthly_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(614, 110);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 32);
            this.label4.TabIndex = 23;
            this.label4.Text = "Monthly:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(879, 457);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 31);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 48;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(697, 8);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(193, 38);
            this.label14.TabIndex = 122;
            this.label14.Text = "UPDATE FEES";
            // 
            // UpdateFeesUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.label14);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.monthly);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.library);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.development);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.exam);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.admission);
            this.Controls.Add(this.label17);
            this.Name = "UpdateFeesUC";
            this.Size = new System.Drawing.Size(1587, 671);
            this.Load += new System.EventHandler(this.UpdateFeesUC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox admission;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox exam;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox development;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox library;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox monthly;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label14;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class TeacherClassScheduleUC : UserControl
    {
        private AccountantData ac = new AccountantData();
        private TeacherData td = new TeacherData();
        private string _userId;

        public TeacherClassScheduleUC(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void TeacherClassScheduleUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ac.GetSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private void sessionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(sessionCombo.SelectedIndex == -1)
            {
                scheduleGrid.DataSource = null;
            }
            else
            {               
                try
                {
                    scheduleGrid.DataSource = td.GetOwnSchedule(sessionCombo.SelectedValue.ToString(), _userId);
                }
                catch (Exception) { }
            }
        }
    }
}

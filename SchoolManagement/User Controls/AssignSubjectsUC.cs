﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class AssignSubjectsUC : UserControl
    {
        private HeadTeacherData htd = new HeadTeacherData();
        private AdminData ad = new AdminData();
        private AccountantData ac = new AccountantData();
        private DataTable ddt = new DataTable();
        private DataRow dr;

        public AssignSubjectsUC()
        {
            InitializeComponent();
        }

        private void AssignSubjectsUC_Load(object sender, EventArgs e)
        {
            try
            {
                teacherIdCombo.ValueMember = "employeeId";
                teacherIdCombo.DisplayMember = "employeeId";
                teacherIdCombo.DataSource = htd.GetTeachersID();
                teacherIdCombo.SelectedIndex = -1;

                subIdCombo.ValueMember = "subId";
                subIdCombo.DisplayMember = "subId";
                subIdCombo.DataSource = htd.GetSubjectsID();
                subIdCombo.SelectedIndex = -1;

                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ac.GetSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }

            ddt.Columns.Add("Session");
            ddt.Columns.Add("Teacher ID");
            ddt.Columns.Add("Subject ID");
            ddt.Columns.Add("Class");
            ddt.Columns.Add("Section");
            ddt.Columns.Add("Day");
            ddt.Columns.Add("Time");
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void classCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string sclass = (string)classCombo.SelectedItem;
            secCombo.ValueMember = "section";
            secCombo.DisplayMember = "section";
            //secCombo.DataSource = ad.GetSecByClass(sclass);
        }

        private void subIdCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(subIdCombo.SelectedIndex == -1)
            {
                subClass.Text = subDay.Text = subTime.Text = subName.Text = "";
            }
            else
            {
                try
                {
                    DataTable dt = htd.GetClaDayTime(subIdCombo.SelectedValue.ToString());
                    subClass.Text = dt.Rows[0][0].ToString();
                    subDay.Text = dt.Rows[0][1].ToString();
                    subTime.Text = dt.Rows[0][2].ToString();
                    subName.Text = dt.Rows[0][3].ToString();

                    if(subClass.Text != null)
                    {                       
                        try
                        {
                            if(sessionCombo.SelectedIndex == -1)
                            {
                                secCombo.DataSource = null;
                            }
                            secCombo.ValueMember = "section";
                            secCombo.DisplayMember = "section";
                            secCombo.DataSource = htd.GetSecsByClass(sessionCombo.SelectedValue.ToString(), subClass.Text);
                            secCombo.SelectedIndex = -1;
                        }
                        catch (Exception) { }
                    }
                }
                catch (Exception) { }
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
                if ((htd.IsSubAssigned(sessionCombo.SelectedValue.ToString(), subIdCombo.SelectedValue.ToString(), secCombo.SelectedValue.ToString())) || (htd.IsDayTimeAssigned(sessionCombo.SelectedValue.ToString(), teacherIdCombo.SelectedValue.ToString(), subDay.Text, subTime.Text)))
                {
                    MessageBox.Show("Already Assigned!");
                    subIdCombo.SelectedIndex = -1;
                }
                else
                {
                    dr = ddt.NewRow();
                    dr["Session"] = sessionCombo.SelectedValue.ToString();
                    dr["Teacher ID"] = teacherIdCombo.SelectedValue.ToString();
                    dr["Subject ID"] = subIdCombo.SelectedValue.ToString();
                    dr["Class"] = subClass.Text;
                    dr["Section"] = secCombo.SelectedValue.ToString();
                    dr["Day"] = subDay.Text;
                    dr["Time"] = subTime.Text;
                    ddt.Rows.Add(dr);

                    subjectGrid.DataSource = ddt;
                }
            }
            catch (Exception) { }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            DataTable finalTable = RemoveDuplicatesRecords((DataTable)subjectGrid.DataSource);

            if(htd.AssignSubjects(finalTable))
            {
                MessageBox.Show("ASSIGNED!");
                Clear();
            }
        }

        private void Clear()
        {
            sessionCombo.SelectedIndex = teacherIdCombo.SelectedIndex = subIdCombo.SelectedIndex = secCombo.SelectedIndex = -1;
            subClass.Text = subName.Text = subDay.Text = subTime.Text = "";

            ddt.Rows.Clear();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            subjectGrid.Rows.RemoveAt(subjectGrid.CurrentCell.RowIndex);
            sessionCombo.SelectedIndex = teacherIdCombo.SelectedIndex = subIdCombo.SelectedIndex = secCombo.SelectedIndex = -1;
            subClass.Text = subName.Text = subDay.Text = subTime.Text = "";
        }

        private void subjectGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            sessionCombo.SelectedItem = subjectGrid.Rows[e.RowIndex].Cells[0].Value.ToString();
            teacherIdCombo.SelectedItem = subjectGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Clear();
        }
    }
}

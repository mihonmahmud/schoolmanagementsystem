﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class CollectionHistoryUC : UserControl
    {
        private AccountantData ac = new AccountantData();

        public CollectionHistoryUC()
        {
            InitializeComponent();
        }

        private void CollectionHistoryUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ac.HistorySession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if(sessionCombo.SelectedIndex != -1 && !string.IsNullOrWhiteSpace(stuId.Text))
            {
                try
                {
                    historyGrid.DataSource = ac.CollectionHistory(sessionCombo.SelectedValue.ToString(), stuId.Text);
                }
                catch (Exception) { }
            }
        }
    }
}

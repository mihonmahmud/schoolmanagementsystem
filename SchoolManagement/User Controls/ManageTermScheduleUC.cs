﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class ManageTermScheduleUC : UserControl
    {
        private AdminData ad = new AdminData();
        private HeadTeacherData htd = new HeadTeacherData();

        public ManageTermScheduleUC()
        {
            InitializeComponent();
        }

        private void sessionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(sessionCombo.SelectedIndex == -1)
            {
                termGrid.DataSource = null;
            }
            else
            {
                termGrid.DataSource = ad.GetTermScheduleBySession(sessionCombo.SelectedValue.ToString());
            }
        }

        private void ManageTermScheduleUC_Load(object sender, EventArgs e)
        {
            GetSession();
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void termGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                termCombo.SelectedItem = termGrid.Rows[e.RowIndex].Cells[0].Value.ToString();
                subId.Text = termGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
                examDate.Text = termGrid.Rows[e.RowIndex].Cells[3].Value.ToString();
                ExamTime.Text = termGrid.Rows[e.RowIndex].Cells[4].Value.ToString();
            }
            catch (Exception) { }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
                if(ad.IsTermDateTimeCreated(sessionCombo.SelectedValue.ToString(), examDate.Text, ExamTime.Text))
                {
                    MessageBox.Show("SCHEDULED DATE AND TIME ALREADY EXISTS!!!");
                    examDate.Text = ExamTime.Text = "";
                }
                else
                {
                    if(!string.IsNullOrWhiteSpace(subId.Text) && !string.IsNullOrWhiteSpace(examDate.Text) && !string.IsNullOrWhiteSpace(ExamTime.Text) && sessionCombo.SelectedIndex != -1 && termCombo.SelectedIndex != -1)
                    {
                        if (ad.UpdateTermSchedule(sessionCombo.SelectedValue.ToString(), termCombo.SelectedItem.ToString(), subId.Text, examDate.Text, ExamTime.Text))
                        {
                            MessageBox.Show("UPDATED!");
                            termGrid.DataSource = ad.GetTermScheduleBySession(sessionCombo.SelectedValue.ToString());
                            termCombo.SelectedIndex = -1;
                            subId.Text = examDate.Text = ExamTime.Text = "";
                        }
                    }
                    else
                    {
                        MessageBox.Show("FILL ALL REQUIRED FIELDS!");
                    }
                }
            }
            catch (Exception) { }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                if(ad.DeleteTermSchedule(sessionCombo.SelectedValue.ToString(), termCombo.SelectedItem.ToString(), subId.Text))
                {
                    MessageBox.Show("SCHEDULE DELETED!");
                    termGrid.DataSource = ad.GetTermScheduleBySession(sessionCombo.SelectedValue.ToString());
                    termCombo.SelectedIndex = -1;
                    subId.Text = examDate.Text = ExamTime.Text = "";
                }
            }
            catch (Exception) { }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void termCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(termCombo.SelectedIndex == -1)
            {
                termGrid.DataSource = null;
            }
            else if((sessionCombo.SelectedIndex != -1) && (termCombo.SelectedIndex == -1))
            {
                try
                {
                    termGrid.DataSource = ad.GetTermScheduleBySession(sessionCombo.SelectedValue.ToString());
                }
                catch (Exception) { }
            }
            else if ((sessionCombo.SelectedIndex != -1) && (termCombo.SelectedIndex != -1))
            {
                try
                {
                    termGrid.DataSource = ad.GetTermScheduleBySessionTerm(sessionCombo.SelectedValue.ToString(), termCombo.SelectedItem.ToString());
                }
                catch (Exception) { }
            }
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            if(ad.IsScheduleCreated(newSession.Text, newTermCombo.SelectedItem.ToString()))
            {
                MessageBox.Show("SCHEDULE ALREADY EXIST'S!!!");
            }
            else
            {
                try
                {
                    if(termGrid.Rows.Count > 0)
                    {
                        if (ad.CreateExamSchedule((DataTable)termGrid.DataSource, newSession.Text, newTermCombo.SelectedItem.ToString()))
                        {
                            MessageBox.Show("SCHEDULE CREATED!");
                            GetSession();
                            Clear();
                        }
                    }
                    else
                    {
                        MessageBox.Show("NO SCHEDULE FOUND!");
                    }
                }
                catch (Exception) { }
            }          
        }

        private void GetSession()
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(htd.GetTermSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private void Clear()
        {
            termGrid.DataSource = null;
            sessionCombo.SelectedIndex = termCombo.SelectedIndex = newTermCombo.SelectedIndex = -1;
            newSession.Text = subId.Text = examDate.Text = ExamTime.Text = "";
        }
    }
}

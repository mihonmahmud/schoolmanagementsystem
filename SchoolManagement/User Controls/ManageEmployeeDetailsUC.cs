﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;
using System.Text.RegularExpressions;

namespace SchoolManagement.User_Controls
{
    public partial class ManageEmployeeDetailsUC : UserControl
    {
        private AdminData ad = new AdminData();
        private DataTable empInfo;
        private Regex mRegxExpression = new Regex(@"^([a-zA-Z0-9_\-])([a-zA-Z0-9_\-\.]*)@(\[((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\.){3}|((([a-zA-Z0-9\-]+)\.)+))([a-zA-Z]{2,}|(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\])$");

        public ManageEmployeeDetailsUC()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            empInfo = ad.GetEmployeeDetails(employeeId.Text);

            try
            {
                if(empInfo.Rows.Count > 0)
                {
                    empName.Text = empInfo.Rows[0][2].ToString();
                    email.Text = empInfo.Rows[0][3].ToString();
                    phone.Text = empInfo.Rows[0][4].ToString();
                    fName.Text = empInfo.Rows[0][5].ToString();
                    mName.Text = empInfo.Rows[0][6].ToString();
                    empDOB.Text = empInfo.Rows[0][7].ToString();
                    religion.Text = empInfo.Rows[0][8].ToString();
                    genderCombo.SelectedItem = empInfo.Rows[0][9].ToString();
                    bgCombo.SelectedItem = empInfo.Rows[0][10].ToString();
                    empDegree.Text = empInfo.Rows[0][11].ToString();
                    empPresent.Text = empInfo.Rows[0][12].ToString();
                    empPermanent.Text = empInfo.Rows[0][13].ToString();
                    empNation.Text = empInfo.Rows[0][15].ToString();
                }
                else
                {
                    MessageBox.Show("No Data Found!");
                }
            }
            catch (Exception) { }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(employeeId.Text) && !string.IsNullOrWhiteSpace(empName.Text) && !string.IsNullOrWhiteSpace(phone.Text) && !string.IsNullOrWhiteSpace(email.Text) && !string.IsNullOrWhiteSpace(fName.Text) && !string.IsNullOrWhiteSpace(mName.Text) && !string.IsNullOrWhiteSpace(religion.Text) && !string.IsNullOrWhiteSpace(empDegree.Text) && !string.IsNullOrWhiteSpace(empPermanent.Text) && !string.IsNullOrWhiteSpace(empPresent.Text) && !string.IsNullOrWhiteSpace(empNation.Text))
            {
                if (!mRegxExpression.IsMatch(email.Text.Trim()))
                {
                    MessageBox.Show("E-mail address format is not correct.", "Invalid Email", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    email.Focus();
                }
                else
                {
                    if (ad.UpdateEmployeeDetails(employeeId.Text, empName.Text, phone.Text, email.Text, fName.Text, mName.Text, empDOB.Text, religion.Text, genderCombo.SelectedItem.ToString(), bgCombo.SelectedItem.ToString(), empDegree.Text, empPresent.Text, empPermanent.Text, empNation.Text))
                    {
                        MessageBox.Show("Updated!");

                        empInfo = ad.GetEmployeeDetails(employeeId.Text);

                        empName.Text = empInfo.Rows[0][2].ToString();
                        email.Text = empInfo.Rows[0][3].ToString();
                        phone.Text = empInfo.Rows[0][4].ToString();
                        fName.Text = empInfo.Rows[0][5].ToString();
                        mName.Text = empInfo.Rows[0][6].ToString();
                        empDOB.Text = empInfo.Rows[0][7].ToString();
                        religion.Text = empInfo.Rows[0][8].ToString();
                        genderCombo.SelectedItem = empInfo.Rows[0][9].ToString();
                        bgCombo.SelectedItem = empInfo.Rows[0][10].ToString();
                        empDegree.Text = empInfo.Rows[0][11].ToString();
                        empPresent.Text = empInfo.Rows[0][12].ToString();
                        empPermanent.Text = empInfo.Rows[0][13].ToString();
                        empNation.Text = empInfo.Rows[0][15].ToString();
                    }
                }              
            }
            else
            {
                MessageBox.Show("FILL ALL REQUIRED FIELDS!");
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            try
            {
                if (ad.DeleteEmployee(employeeId.Text))
                {
                    MessageBox.Show("Removed!");
                    CleanTextFields();
                    try
                    {
                        empGrid.DataSource = ad.EmployeesInfo();
                    }
                    catch (Exception) { }
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Something went wrong!");
            }
        }

        public void CleanTextFields()
        {
            empName.Text = email.Text = employeeId.Text = phone.Text = fName.Text = mName.Text = empDOB.Text = religion.Text = empDegree.Text = empPresent.Text = empPermanent.Text = empNation.Text = string.Empty;
            genderCombo.SelectedIndex = bgCombo.SelectedIndex = -1;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            CleanTextFields();
        }

        private void empName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void phone_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void fName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void mName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void religion_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void empNation_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void ManageEmployeeDetailsUC_Load(object sender, EventArgs e)
        {
            try
            {
                empGrid.DataSource = ad.EmployeesInfo();
            }
            catch (Exception) { }
        }
    }
}

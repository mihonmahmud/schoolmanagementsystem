﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class UpdateSalaryUC : UserControl
    {
        private OwnerData od = new OwnerData();

        public UpdateSalaryUC()
        {
            InitializeComponent();
        }

        private void UpdateSalaryUC_Load(object sender, EventArgs e)
        {
            employeeCategoryCombo.SelectedIndex = -1;
        }

        private void employeeCategoryCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(employeeCategoryCombo.SelectedIndex == -1)
            {
                salaryGrid.DataSource = null;
            }
            else
            {              
                try
                {
                    salaryGrid.DataSource = od.GetSalaryDetailsByCategory(employeeCategoryCombo.SelectedItem.ToString());
                }
                catch (Exception) { }
            }
        }

        private void salaryGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                empId.Text = salaryGrid.Rows[e.RowIndex].Cells[0].Value.ToString();
                basic.Text = salaryGrid.Rows[e.RowIndex].Cells[3].Value.ToString();
                home.Text = salaryGrid.Rows[e.RowIndex].Cells[4].Value.ToString();
                medical.Text = salaryGrid.Rows[e.RowIndex].Cells[5].Value.ToString();
                tiffin.Text = salaryGrid.Rows[e.RowIndex].Cells[6].Value.ToString();
            }
            catch (Exception) { }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(empId.Text))
            {
                MessageBox.Show("FILL REQUIRED INPUT FIELDS!");
            }
            try
            {
                salaryGrid.DataSource = od.GetSalaryDetailsById(empId.Text);
            }
            catch (Exception) { }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(empId.Text) || string.IsNullOrWhiteSpace(basic.Text) || string.IsNullOrWhiteSpace(medical.Text) || string.IsNullOrWhiteSpace(home.Text) || string.IsNullOrWhiteSpace(tiffin.Text))
            {
                MessageBox.Show("FILL REQUIRED INPUT FIELDS!");
            }
            else
            {
                try
                {
                    if (od.UpdateSalaryById(empId.Text, float.Parse(basic.Text), float.Parse(home.Text), float.Parse(medical.Text), float.Parse(tiffin.Text)))
                    {
                        salaryGrid.DataSource = od.GetSalaryDetailsById(empId.Text);
                        MessageBox.Show("SALARY UPDATED!");
                    }
                }
                catch (Exception) { }
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            salaryGrid.DataSource = null;
            employeeCategoryCombo.SelectedIndex = -1;
            empId.Text = basic.Text = home.Text = medical.Text = tiffin.Text = "";
        }
    }
}

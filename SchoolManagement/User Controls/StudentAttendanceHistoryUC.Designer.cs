﻿namespace SchoolManagement.User_Controls
{
    partial class StudentAttendanceHistoryUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label14 = new System.Windows.Forms.Label();
            this.monthCombo = new MetroFramework.Controls.MetroComboBox();
            this.dayCombo = new MetroFramework.Controls.MetroComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.attendanceGrid = new MetroFramework.Controls.MetroGrid();
            this.subClass = new MetroFramework.Controls.MetroTextBox();
            this.secCombo = new MetroFramework.Controls.MetroComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.subIdCombo = new MetroFramework.Controls.MetroComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.sessionCombo = new MetroFramework.Controls.MetroComboBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.attendanceGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(517, 11);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(475, 38);
            this.label14.TabIndex = 123;
            this.label14.Text = "STUDENTS ATTENDANCE HISTORY";
            // 
            // monthCombo
            // 
            this.monthCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.monthCombo.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.monthCombo.FormattingEnabled = true;
            this.monthCombo.ItemHeight = 24;
            this.monthCombo.Items.AddRange(new object[] {
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"});
            this.monthCombo.Location = new System.Drawing.Point(1144, 123);
            this.monthCombo.Margin = new System.Windows.Forms.Padding(4);
            this.monthCombo.Name = "monthCombo";
            this.monthCombo.Size = new System.Drawing.Size(71, 30);
            this.monthCombo.TabIndex = 135;
            this.monthCombo.UseSelectable = true;
            this.monthCombo.SelectedIndexChanged += new System.EventHandler(this.monthCombo_SelectedIndexChanged);
            // 
            // dayCombo
            // 
            this.dayCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.dayCombo.FormattingEnabled = true;
            this.dayCombo.ItemHeight = 24;
            this.dayCombo.Location = new System.Drawing.Point(1228, 123);
            this.dayCombo.Margin = new System.Windows.Forms.Padding(4);
            this.dayCombo.Name = "dayCombo";
            this.dayCombo.Size = new System.Drawing.Size(71, 30);
            this.dayCombo.TabIndex = 134;
            this.dayCombo.UseSelectable = true;
            this.dayCombo.SelectedIndexChanged += new System.EventHandler(this.dayCombo_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1067, 119);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 32);
            this.label2.TabIndex = 133;
            this.label2.Text = "Date:";
            // 
            // attendanceGrid
            // 
            this.attendanceGrid.AllowUserToResizeRows = false;
            this.attendanceGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.attendanceGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.attendanceGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.attendanceGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.attendanceGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.attendanceGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.attendanceGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.attendanceGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.attendanceGrid.DefaultCellStyle = dataGridViewCellStyle22;
            this.attendanceGrid.EnableHeadersVisualStyles = false;
            this.attendanceGrid.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.attendanceGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.attendanceGrid.Location = new System.Drawing.Point(558, 201);
            this.attendanceGrid.Name = "attendanceGrid";
            this.attendanceGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.attendanceGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.attendanceGrid.RowHeadersVisible = false;
            this.attendanceGrid.RowHeadersWidth = 51;
            this.attendanceGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle24.Padding = new System.Windows.Forms.Padding(5);
            this.attendanceGrid.RowsDefaultCellStyle = dataGridViewCellStyle24;
            this.attendanceGrid.RowTemplate.Height = 24;
            this.attendanceGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.attendanceGrid.Size = new System.Drawing.Size(606, 450);
            this.attendanceGrid.TabIndex = 132;
            // 
            // subClass
            // 
            // 
            // 
            // 
            this.subClass.CustomButton.Image = null;
            this.subClass.CustomButton.Location = new System.Drawing.Point(54, 2);
            this.subClass.CustomButton.Name = "";
            this.subClass.CustomButton.Size = new System.Drawing.Size(23, 23);
            this.subClass.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.subClass.CustomButton.TabIndex = 1;
            this.subClass.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.subClass.CustomButton.UseSelectable = true;
            this.subClass.CustomButton.Visible = false;
            this.subClass.Enabled = false;
            this.subClass.Lines = new string[0];
            this.subClass.Location = new System.Drawing.Point(793, 123);
            this.subClass.MaxLength = 32767;
            this.subClass.Multiline = true;
            this.subClass.Name = "subClass";
            this.subClass.PasswordChar = '\0';
            this.subClass.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.subClass.SelectedText = "";
            this.subClass.SelectionLength = 0;
            this.subClass.SelectionStart = 0;
            this.subClass.ShortcutsEnabled = true;
            this.subClass.Size = new System.Drawing.Size(80, 28);
            this.subClass.TabIndex = 131;
            this.subClass.UseSelectable = true;
            this.subClass.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.subClass.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // secCombo
            // 
            this.secCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.secCombo.FormattingEnabled = true;
            this.secCombo.ItemHeight = 24;
            this.secCombo.Location = new System.Drawing.Point(963, 122);
            this.secCombo.Margin = new System.Windows.Forms.Padding(4);
            this.secCombo.Name = "secCombo";
            this.secCombo.Size = new System.Drawing.Size(71, 30);
            this.secCombo.TabIndex = 130;
            this.secCombo.UseSelectable = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(900, 118);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 32);
            this.label4.TabIndex = 129;
            this.label4.Text = "Sec:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(715, 119);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 32);
            this.label3.TabIndex = 128;
            this.label3.Text = "Class:";
            // 
            // subIdCombo
            // 
            this.subIdCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.subIdCombo.FormattingEnabled = true;
            this.subIdCombo.ItemHeight = 24;
            this.subIdCombo.Location = new System.Drawing.Point(576, 121);
            this.subIdCombo.Margin = new System.Windows.Forms.Padding(4);
            this.subIdCombo.Name = "subIdCombo";
            this.subIdCombo.Size = new System.Drawing.Size(112, 30);
            this.subIdCombo.TabIndex = 127;
            this.subIdCombo.UseSelectable = true;
            this.subIdCombo.SelectedIndexChanged += new System.EventHandler(this.subIdCombo_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(471, 118);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 32);
            this.label1.TabIndex = 126;
            this.label1.Text = "Subject:";
            // 
            // sessionCombo
            // 
            this.sessionCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.sessionCombo.FormattingEnabled = true;
            this.sessionCombo.ItemHeight = 24;
            this.sessionCombo.Location = new System.Drawing.Point(320, 121);
            this.sessionCombo.Margin = new System.Windows.Forms.Padding(4);
            this.sessionCombo.Name = "sessionCombo";
            this.sessionCombo.Size = new System.Drawing.Size(126, 30);
            this.sessionCombo.TabIndex = 125;
            this.sessionCombo.UseSelectable = true;
            this.sessionCombo.SelectedIndexChanged += new System.EventHandler(this.sessionCombo_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(215, 118);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 32);
            this.label5.TabIndex = 124;
            this.label5.Text = "Session:";
            // 
            // StudentAttendanceHistoryUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.monthCombo);
            this.Controls.Add(this.dayCombo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.attendanceGrid);
            this.Controls.Add(this.subClass);
            this.Controls.Add(this.secCombo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.subIdCombo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.sessionCombo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label14);
            this.Name = "StudentAttendanceHistoryUC";
            this.Size = new System.Drawing.Size(1600, 700);
            this.Load += new System.EventHandler(this.StudentAttendanceHistoryUC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.attendanceGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label14;
        private MetroFramework.Controls.MetroComboBox monthCombo;
        private MetroFramework.Controls.MetroComboBox dayCombo;
        private System.Windows.Forms.Label label2;
        private MetroFramework.Controls.MetroGrid attendanceGrid;
        private MetroFramework.Controls.MetroTextBox subClass;
        private MetroFramework.Controls.MetroComboBox secCombo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private MetroFramework.Controls.MetroComboBox subIdCombo;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroComboBox sessionCombo;
        private System.Windows.Forms.Label label5;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class UploadMarksUC : UserControl
    {
        private TeacherData td = new TeacherData();
        private HeadTeacherData htd = new HeadTeacherData();
        private string _userId;
        private DataTable ddt = new DataTable();
        private DataRow dr;

        public UploadMarksUC(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        private void UploadMarksUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(td.GetSessionById(_userId));
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }

            ddt.Columns.Add("Session");
            ddt.Columns.Add("Term");
            ddt.Columns.Add("Subject ID");
            ddt.Columns.Add("Class");
            ddt.Columns.Add("Section");
            ddt.Columns.Add("Student ID");
            ddt.Columns.Add("Marks");
            ddt.Columns.Add("Grade");
        }

        private void termCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(termCombo.SelectedIndex == -1)
            {
                subIdCombo.DataSource = null;
            }
            else
            {
                try
                {
                    subIdCombo.ValueMember = "subId";
                    subIdCombo.DisplayMember = "subId";
                    subIdCombo.DataSource = td.GetSubjectsById(sessionCombo.SelectedValue.ToString(), _userId);
                    subIdCombo.SelectedIndex = -1;
                }
                catch (Exception) { }
            }
        }

        private void subIdCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(subIdCombo.SelectedIndex == -1)
            {
                subClass.Text = "";
                secCombo.DataSource = null;
            }
            else
            {
                try
                {
                    subClass.Text = td.GetClassById(sessionCombo.SelectedValue.ToString(), subIdCombo.SelectedValue.ToString());

                    if(subClass.Text != null)
                    {
                        try
                        {
                            secCombo.ValueMember = "sec";
                            secCombo.DisplayMember = "sec";
                            secCombo.DataSource = td.GetSecsByCla(sessionCombo.SelectedValue.ToString(), subIdCombo.SelectedValue.ToString(), subClass.Text, _userId);
                            secCombo.SelectedIndex = -1;
                        }
                        catch (Exception) { }
                    }
                }
                catch (Exception) { }
            }
        }

        private void secCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(secCombo.SelectedIndex == -1)
            {
                studentIdCombo.DataSource = null;
            }
            else
            {
                try
                {
                    studentIdCombo.ValueMember = "studentId";
                    studentIdCombo.DisplayMember = "studentId";
                    studentIdCombo.DataSource = td.GetStudentsByClaSec(sessionCombo.SelectedValue.ToString(), subClass.Text, secCombo.SelectedValue.ToString());
                    studentIdCombo.SelectedIndex = -1;
                }
                catch (Exception) { }
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if(sessionCombo.SelectedIndex != -1 && termCombo.SelectedIndex != -1 && subIdCombo.SelectedIndex != -1 && studentIdCombo.SelectedIndex != -1 && secCombo.SelectedIndex != -1 && !string.IsNullOrWhiteSpace(marks.Text))
            {
                if (htd.IsMarkExist(sessionCombo.SelectedValue.ToString(), termCombo.SelectedItem.ToString(), subIdCombo.SelectedValue.ToString(), studentIdCombo.SelectedValue.ToString()))
                {
                    MessageBox.Show("SAME DATA ALREADY EXISTS!");
                }
                else
                {
                    try
                    {
                        dr = ddt.NewRow();
                        dr["Session"] = sessionCombo.SelectedValue.ToString();
                        dr["Term"] = termCombo.SelectedItem.ToString();
                        dr["Subject ID"] = subIdCombo.SelectedValue.ToString();
                        dr["Class"] = subClass.Text;
                        dr["Section"] = secCombo.SelectedValue.ToString();
                        dr["Student ID"] = studentIdCombo.SelectedValue.ToString();
                        dr["Marks"] = marks.Text;
                        dr["Grade"] = CalculateGrade();
                        ddt.Rows.Add(dr);

                        marksGrid.DataSource = ddt;

                        studentIdCombo.SelectedIndex = -1;
                        marks.Text = "";
                    }
                    catch (Exception) { }
                }
            }
            else
            {
                MessageBox.Show("FILL ALL REQUIRED FIELDS!");
            }
        }

        private void marksGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                studentIdCombo.SelectedValue = marksGrid.Rows[e.RowIndex].Cells[5].Value.ToString();
                marks.Text = marksGrid.Rows[e.RowIndex].Cells[6].Value.ToString();
            }
            catch (Exception) { }
        }

        private void Clear()
        {
            sessionCombo.SelectedIndex = subIdCombo.SelectedIndex = termCombo.SelectedIndex = studentIdCombo.SelectedIndex = secCombo.SelectedIndex = -1;
            marks.Text = subClass.Text = "";
            marksGrid.DataSource = null;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            marksGrid.Rows.RemoveAt(marksGrid.CurrentCell.RowIndex);
            studentIdCombo.SelectedIndex = -1;
            marks.Text = "";
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            DataTable finalTable = RemoveDuplicatesRecords((DataTable)marksGrid.DataSource);

            //try
            //{
            //    if (td.InsertMarks(finalTable))
            //    {
            //        MessageBox.Show("UPLOADED!");
            //        Clear();
            //    }
            //}
            //catch (Exception) { }
        }

        private void marks_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private string CalculateGrade()
        {
            DataTable dt = htd.GetGradingMarks();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string[] r = dt.Rows[i]["marks"].ToString().Split('-');

                    if ((int.Parse(marks.Text) >= int.Parse(r[0]) && int.Parse(marks.Text) <= int.Parse(r[1])))
                    {
                        return dt.Rows[i]["grade"].ToString();
                    }
                }
            }

            return "N/A";
        }
    }
}

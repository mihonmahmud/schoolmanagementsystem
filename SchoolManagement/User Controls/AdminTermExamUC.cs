﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class AdminTermExamUC : UserControl
    {
        private HeadTeacherData htd = new HeadTeacherData();
        private AdminData ad = new AdminData();
        private AccountantData ac = new AccountantData();
        private DataTable ddt = new DataTable();
        private DataRow dr;

        public AdminTermExamUC()
        {
            InitializeComponent();
        }

        private void AdTermExam_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ac.GetSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }

            ddt.Columns.Add("Session");
            ddt.Columns.Add("Term");
            ddt.Columns.Add("Subject ID");
            ddt.Columns.Add("Class");
            ddt.Columns.Add("Date");
            ddt.Columns.Add("Time");
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void termCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            try
            {
                subIdCombo.ValueMember = "subId";
                subIdCombo.DisplayMember = "subId";
                subIdCombo.DataSource = htd.GetSubjectsID();
                subIdCombo.SelectedIndex = -1;                
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void subIdCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(subIdCombo.SelectedIndex == -1)
            {
                subClass.Text = "";
            }
            else
            {
                try
                {
                    subClass.Text = ad.GetClassBySub(subIdCombo.SelectedValue.ToString()).ToString();
                }
                catch (Exception) { }
            }            
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            string ExamTime = from.Value.ToShortTimeString() + " - " + to.Value.ToShortTimeString();

            if(ad.IsTermCreated(sessionCombo.SelectedValue.ToString(), termCombo.SelectedItem.ToString(), subIdCombo.SelectedValue.ToString()))
            {
                MessageBox.Show("SCHEDULE ALREADY CREATED!");
                subIdCombo.SelectedIndex = -1;
            }
            else
            {
                dr = ddt.NewRow();
                dr["Session"] = sessionCombo.SelectedValue.ToString();
                dr["Term"] = termCombo.SelectedItem.ToString();
                dr["Subject ID"] = subIdCombo.SelectedValue.ToString();
                dr["Class"] = subClass.Text;
                dr["Date"] = examDate.Value.ToShortDateString();
                dr["Time"] = ExamTime;
                ddt.Rows.Add(dr);

                termGrid.DataSource = ddt;
            }          
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            try
            {
                if (ad.CreateExamSchedule((DataTable)termGrid.DataSource))
                {
                    MessageBox.Show("SCHEDULE CREATED!");
                    Clear();
                }
            }
            catch (Exception) { }
        }

        private void Clear()
        {
            sessionCombo.SelectedIndex = subIdCombo.SelectedIndex = termCombo.SelectedIndex = -1;
            subClass.Text = "";
            ddt.Rows.Clear();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Clear();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;


namespace SchoolManagement.User_Controls
{
    public partial class OverStudentDetailsUC : UserControl
    {
        private AdminData ad = new AdminData();
        private List<string> classes = new List<string>() { "1", "2", "3", "4", "5", "6", "7", "8" };
        private List<string> bg = new List<string>() { "Apos", "Aneg", "Bpos", "Bneg", "ABpos", "ABneg", "Opos", "Oneg" };
        private List<string> gender = new List<string>() { "Male", "Female" };
        private List<string> religion = new List<string>() { "Islam", "Hindu", "Christian", "Buddha" };

        public OverStudentDetailsUC()
        {
            InitializeComponent();
        }

        private void OverStudentDetailsUC_Load(object sender, EventArgs e)
        {
            searchCombo.DataSource = null;
        }

        private void sortCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(sortCombo.SelectedIndex == -1)
            {
                searchCombo.DataSource = null;
            }
            else
            {
                var value = sortCombo.SelectedItem.ToString();

                if (value == "Class")
                {
                    searchCombo.DataSource = classes;
                    searchCombo.SelectedIndex = -1;
                }
                else if (value == "Blood Group")
                {
                    searchCombo.DataSource = bg;
                    searchCombo.SelectedIndex = -1;
                }
                else if (value == "Gender")
                {
                    searchCombo.DataSource = gender;
                    searchCombo.SelectedIndex = -1;
                }
                else if (value == "Religion")
                {
                    searchCombo.DataSource = religion;
                    searchCombo.SelectedIndex = -1;
                }
                else { }
            }
        }

        private void searchCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if((sortCombo.SelectedIndex == -1) && (searchCombo.SelectedIndex == -1))
            {
                overStuGrid.DataSource = null;
            }
            else if ((sortCombo.SelectedIndex != -1) && (searchCombo.SelectedIndex == -1))
            {
                overStuGrid.DataSource = null;
            }
            else
            {          
                try
                {
                    if (sortCombo.SelectedItem.ToString() == "Class")
                    {
                        overStuGrid.DataSource = ad.GetOverStuByClass((string)searchCombo.SelectedItem.ToString());
                    }
                    else if(sortCombo.SelectedItem.ToString() == "Gender")
                    {
                        overStuGrid.DataSource = ad.GetOverStuByGender((string)searchCombo.SelectedItem.ToString());
                    }
                    else if (sortCombo.SelectedItem.ToString() == "Blood Group")
                    {
                        overStuGrid.DataSource = ad.GetOverStuByBG((string)searchCombo.SelectedItem.ToString());
                    }
                    else if (sortCombo.SelectedItem.ToString() == "Religion")
                    {
                        overStuGrid.DataSource = ad.GetOverStuByReligion((string)searchCombo.SelectedItem.ToString());
                    }
                }
                catch (Exception) { }
            }
        }
    }
}

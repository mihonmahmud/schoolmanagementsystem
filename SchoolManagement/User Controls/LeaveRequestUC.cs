﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class LeaveRequestUC : UserControl
    {
        private string _userId;
        LoginData lid = new LoginData();

        public LeaveRequestUC(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        private void LeaveRequestUC_Load(object sender, EventArgs e)
        {
            year.Text = DateTime.Now.Year.ToString();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime dt = DateTime.Parse(from.Value.ToShortDateString());
                DateTime dt2 = DateTime.Parse(to.Value.ToShortDateString());
                int count = (int)dt2.Subtract(dt).TotalDays;

                if(lid.RequestLeave(year.Text, _userId, from.Value.ToShortDateString(), to.Value.ToShortDateString(), reason.Text, count))
                {
                    MessageBox.Show($"REQUEST SENT FOR {count} DAYS!");
                }
            }
            catch (Exception) { }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class StudentDetailsUC : UserControl
    {
        private AdminData ad = new AdminData();

        public StudentDetailsUC()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                stuInfoGrid.DataSource = ad.GetSpeicificStuInfo(stuId.Text);
            }
            catch (Exception) { }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            stuId.Text = "";
            stuInfoGrid.DataSource = null;
        }
    }
}

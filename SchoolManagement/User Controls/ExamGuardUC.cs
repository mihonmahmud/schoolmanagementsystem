﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class ExamGuardUC : UserControl
    {
        private TeacherData td = new TeacherData();
        private HeadTeacherData htd = new HeadTeacherData();
        private string _userId;

        public ExamGuardUC(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void classCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(sortCombo.SelectedIndex == -1)
            {
                guardGrid.DataSource = null;
            }
            else
            {
                
            }
        }

        private void ExamGuardUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(htd.GetTermSession());
                sessionCombo.SelectedIndex = -1;

                sortCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private void sessionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(sessionCombo.SelectedIndex == -1)
            {
                guardGrid.DataSource = null;
            }
            else
            {
                try
                {
                    if ((sortCombo.SelectedItem.ToString() == "Term") && (sessionCombo.SelectedIndex != -1))
                    {
                        try
                        {
                            guardGrid.DataSource = td.TermSchedule(_userId, sessionCombo.SelectedValue.ToString());
                        }
                        catch (Exception) { }
                    }
                    else if ((sortCombo.SelectedItem.ToString() == "Admission") && (sessionCombo.SelectedIndex != -1))
                    {
                        try
                        {
                            guardGrid.DataSource = td.AdmissionSchedule(_userId, sessionCombo.SelectedValue.ToString());
                        }
                        catch (Exception) { }
                    }
                }
                catch (Exception) { }
            }
        }
    }
}

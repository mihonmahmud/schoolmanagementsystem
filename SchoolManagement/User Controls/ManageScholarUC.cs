﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class ManageScholarUC : UserControl
    {
        private AdminData ad = new AdminData();
        private AccountantData ac = new AccountantData();

        public ManageScholarUC()
        {
            InitializeComponent();
        }

        private void ManageScholarUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ac.GetSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void sessionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sessionCombo.SelectedIndex == -1)
            {
                percentGrid.DataSource = null;
            }
            else
            {
                try
                {
                    percentGrid.DataSource = ad.GetScholarInfo(sessionCombo.SelectedValue.ToString());
                }
                catch (Exception) { }
            }
        }

        private void percentGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                stuId.Text = percentGrid.Rows[e.RowIndex].Cells[0].Value.ToString();
                percent.Text = percentGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
                reason.Text = percentGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
            }
            catch (Exception) { }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if(ad.IsStudentExist(stuId.Text))
            {
                try
                {
                    if(sessionCombo.SelectedIndex != -1 && !string.IsNullOrWhiteSpace(stuId.Text) && !string.IsNullOrWhiteSpace(percent.Text) && !string.IsNullOrWhiteSpace(reason.Text))
                    {
                        if (ad.InsertScholar(sessionCombo.SelectedValue.ToString(), stuId.Text, int.Parse(percent.Text), reason.Text))
                        {
                            percentGrid.DataSource = ad.GetScholarInfo(sessionCombo.SelectedValue.ToString());
                            MessageBox.Show("ADDED!");
                            stuId.Text = percent.Text = reason.Text = "";
                        }
                    }
                    else
                    {
                        MessageBox.Show("FILL ALL REQUIRED FILELDS!");
                    }
                }
                catch (Exception) { }
            }
            else
            {
                MessageBox.Show("STUDENT COULDN'T FOUND!!!");
            }          
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                if (sessionCombo.SelectedIndex != -1 && !string.IsNullOrWhiteSpace(stuId.Text) && !string.IsNullOrWhiteSpace(percent.Text) && !string.IsNullOrWhiteSpace(reason.Text))
                {
                    if (ad.UpdateScholar(sessionCombo.SelectedValue.ToString(), stuId.Text, int.Parse(percent.Text), reason.Text))
                    {
                        percentGrid.DataSource = ad.GetScholarInfo(sessionCombo.SelectedValue.ToString());
                        MessageBox.Show("UPDATED!");
                        stuId.Text = percent.Text = reason.Text = "";
                    }
                }
                else
                {
                    MessageBox.Show("FILL ALL REQUIRED FILELDS!");
                }
            }
            catch (Exception) { }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            try
            {
                if (ad.DeleteScholar(sessionCombo.SelectedValue.ToString(), stuId.Text))
                {
                    percentGrid.DataSource = ad.GetScholarInfo(sessionCombo.SelectedValue.ToString());
                    MessageBox.Show("DELETED!");
                    stuId.Text = percent.Text = reason.Text = "";
                }
            }
            catch (Exception) { }
        }

        private void Clear()
        {
            sessionCombo.SelectedIndex = -1;
            stuId.Text = percent.Text = reason.Text = "";
            percentGrid.DataSource = null;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void percent_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class StudentTotalMarksUC : UserControl
    {
        private AccountantData ad = new AccountantData();
        private HeadTeacherData htd = new HeadTeacherData();
        private DataTable ddt = new DataTable();
        private DataRow dr;

        public StudentTotalMarksUC()
        {
            InitializeComponent();
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void StudentTotalMarksUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ad.GetSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }

            ddt.Columns.Add("Student ID");
            ddt.Columns.Add("Total Marks");
        }

        private void classCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (classCombo.SelectedIndex == -1)
            {
                secCombo.DataSource = null;
            }
            else
            {
                try
                {
                    secCombo.ValueMember = "section";
                    secCombo.DisplayMember = "section";
                    secCombo.DataSource = ad.GetSecBySessionClass(sessionCombo.SelectedValue.ToString(), classCombo.SelectedItem.ToString());
                    secCombo.SelectedIndex = -1;
                }
                catch (Exception) { }
            }
        }

        private void secCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if((classCombo.SelectedIndex != -1) && (secCombo.SelectedIndex == -1))
            {
                marksGrid.DataSource = null;
                totalgrid.DataSource = null;
            }
            else
            {               
                try
                {
                    marksGrid.DataSource = htd.GetStudentTotalMarks(sessionCombo.SelectedValue.ToString(), termCombo.SelectedItem.ToString(), classCombo.SelectedItem.ToString(), secCombo.SelectedValue.ToString());

                    if(marksGrid.Rows.Count > 0)
                    {
                        for (int i = 0; i < marksGrid.Rows.Count; i++)
                        {
                            string stuId = Convert.ToString(marksGrid.Rows[i].Cells[0].Value);

                            dr = ddt.NewRow();
                            dr["Student ID"] = stuId;
                            dr["Total Marks"] = htd.GetTotalByStuId(stuId, sessionCombo.SelectedValue.ToString(), termCombo.SelectedItem.ToString(), classCombo.SelectedItem.ToString(), secCombo.SelectedValue.ToString()).ToString();
                            ddt.Rows.Add(dr);
                        }

                        totalgrid.DataSource = RemoveDuplicatesRecords(ddt);
                    }
                }
                catch (Exception) { }
            }
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            sessionCombo.SelectedIndex = termCombo.SelectedIndex = classCombo.SelectedIndex = secCombo.SelectedIndex = -1;
            marksGrid.DataSource = null;
        }
    }
}

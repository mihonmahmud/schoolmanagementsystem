﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class UpdateClassUC : UserControl
    {
        private AccountantData ad = new AccountantData();
        private HeadTeacherData htd = new HeadTeacherData();

        public UpdateClassUC()
        {
            InitializeComponent();
        }

        private void UpdateClassUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ad.GetSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
                classGrid.DataSource = ad.GetClassInfoById(stuId.Text);
            }
            catch (Exception) { }
        }

        private void classCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(classCombo.SelectedIndex == -1)
            {
                secCombo.DataSource = null;
            }
            else
            {
                try
                {
                    secCombo.ValueMember = "section";
                    secCombo.DisplayMember = "section";
                    secCombo.DataSource = htd.GetSecsByClass(sessionCombo.SelectedValue.ToString(), classCombo.SelectedItem.ToString());
                    secCombo.SelectedIndex = -1;
                }
                catch (Exception) { }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                if(sessionCombo.SelectedIndex != -1 && classCombo.SelectedIndex != -1 && secCombo.SelectedIndex != -1 && !string.IsNullOrWhiteSpace(position.Text))
                {
                    if (int.Parse(sessionCombo.SelectedValue.ToString()) <= int.Parse(DateTime.Now.Year.ToString()))
                    {
                        MessageBox.Show("CAN NOT UPDATE CLASS FOR PREVIOUS SESSIONS!");
                    }
                    else
                    {
                        if (ad.IsPositionExist(sessionCombo.SelectedValue.ToString(), classCombo.SelectedItem.ToString(), secCombo.SelectedValue.ToString(), int.Parse(position.Text)))
                        {
                            if (ad.UpdateStuClass(sessionCombo.SelectedValue.ToString(), stuId.Text, classCombo.SelectedItem.ToString(), secCombo.SelectedValue.ToString(), int.Parse(position.Text)))
                            {
                                classGrid.DataSource = ad.GetClassInfoById(stuId.Text);
                                MessageBox.Show("UPDATED!");
                                sessionCombo.SelectedIndex = classCombo.SelectedIndex = secCombo.SelectedIndex = -1;
                            }
                        }
                        else
                        {
                            MessageBox.Show("POSITION ALREADY EXIST!");
                        }
                    }                  
                }
                else
                {
                    MessageBox.Show("FILL ALL REQUIRED FIELDS!");
                }
            }
            catch (Exception) { }
        }
    }
}

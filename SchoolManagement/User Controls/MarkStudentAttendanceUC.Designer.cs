﻿namespace SchoolManagement.User_Controls
{
    partial class MarkStudentAttendanceUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MarkStudentAttendanceUC));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label5 = new System.Windows.Forms.Label();
            this.subIdCombo = new MetroFramework.Controls.MetroComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.subClass = new MetroFramework.Controls.MetroTextBox();
            this.secCombo = new MetroFramework.Controls.MetroComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.attendanceGrid = new MetroFramework.Controls.MetroGrid();
            this.label14 = new System.Windows.Forms.Label();
            this.session = new MetroFramework.Controls.MetroTextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.attendanceGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(273, 89);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 32);
            this.label5.TabIndex = 97;
            this.label5.Text = "Session:";
            // 
            // subIdCombo
            // 
            this.subIdCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.subIdCombo.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.subIdCombo.FormattingEnabled = true;
            this.subIdCombo.ItemHeight = 24;
            this.subIdCombo.Location = new System.Drawing.Point(699, 94);
            this.subIdCombo.Margin = new System.Windows.Forms.Padding(4);
            this.subIdCombo.Name = "subIdCombo";
            this.subIdCombo.Size = new System.Drawing.Size(212, 30);
            this.subIdCombo.TabIndex = 100;
            this.subIdCombo.UseSelectable = true;
            this.subIdCombo.SelectedIndexChanged += new System.EventHandler(this.subIdCombo_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(595, 91);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 32);
            this.label1.TabIndex = 99;
            this.label1.Text = "Subject:";
            // 
            // subClass
            // 
            // 
            // 
            // 
            this.subClass.CustomButton.Image = null;
            this.subClass.CustomButton.Location = new System.Drawing.Point(46, 1);
            this.subClass.CustomButton.Name = "";
            this.subClass.CustomButton.Size = new System.Drawing.Size(33, 33);
            this.subClass.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.subClass.CustomButton.TabIndex = 1;
            this.subClass.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.subClass.CustomButton.UseSelectable = true;
            this.subClass.CustomButton.Visible = false;
            this.subClass.Enabled = false;
            this.subClass.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.subClass.Lines = new string[0];
            this.subClass.Location = new System.Drawing.Point(1041, 88);
            this.subClass.MaxLength = 32767;
            this.subClass.Multiline = true;
            this.subClass.Name = "subClass";
            this.subClass.PasswordChar = '\0';
            this.subClass.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.subClass.SelectedText = "";
            this.subClass.SelectionLength = 0;
            this.subClass.SelectionStart = 0;
            this.subClass.ShortcutsEnabled = true;
            this.subClass.Size = new System.Drawing.Size(80, 35);
            this.subClass.TabIndex = 104;
            this.subClass.UseSelectable = true;
            this.subClass.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.subClass.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // secCombo
            // 
            this.secCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.secCombo.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.secCombo.FormattingEnabled = true;
            this.secCombo.ItemHeight = 24;
            this.secCombo.Location = new System.Drawing.Point(1215, 94);
            this.secCombo.Margin = new System.Windows.Forms.Padding(4);
            this.secCombo.Name = "secCombo";
            this.secCombo.Size = new System.Drawing.Size(71, 30);
            this.secCombo.TabIndex = 103;
            this.secCombo.UseSelectable = true;
            this.secCombo.SelectedIndexChanged += new System.EventHandler(this.secCombo_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1152, 90);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 32);
            this.label4.TabIndex = 102;
            this.label4.Text = "Sec:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(963, 89);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 32);
            this.label3.TabIndex = 101;
            this.label3.Text = "Class:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1103, 633);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 31);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 106;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // attendanceGrid
            // 
            this.attendanceGrid.AllowUserToAddRows = false;
            this.attendanceGrid.AllowUserToDeleteRows = false;
            this.attendanceGrid.AllowUserToResizeColumns = false;
            this.attendanceGrid.AllowUserToResizeRows = false;
            this.attendanceGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.attendanceGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.attendanceGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.attendanceGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.attendanceGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.attendanceGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.attendanceGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.attendanceGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.attendanceGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.attendanceGrid.EnableHeadersVisualStyles = false;
            this.attendanceGrid.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.attendanceGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.attendanceGrid.Location = new System.Drawing.Point(537, 160);
            this.attendanceGrid.Name = "attendanceGrid";
            this.attendanceGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.attendanceGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.attendanceGrid.RowHeadersVisible = false;
            this.attendanceGrid.RowHeadersWidth = 51;
            this.attendanceGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Padding = new System.Windows.Forms.Padding(5);
            this.attendanceGrid.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.attendanceGrid.RowTemplate.Height = 24;
            this.attendanceGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.attendanceGrid.ShowCellErrors = false;
            this.attendanceGrid.ShowCellToolTips = false;
            this.attendanceGrid.ShowEditingIcon = false;
            this.attendanceGrid.ShowRowErrors = false;
            this.attendanceGrid.Size = new System.Drawing.Size(906, 455);
            this.attendanceGrid.TabIndex = 105;
            this.attendanceGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.attendanceGrid_CellClick_1);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(574, 17);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(425, 38);
            this.label14.TabIndex = 122;
            this.label14.Text = "TAKE STUDENTS ATTENDANCE";
            // 
            // session
            // 
            // 
            // 
            // 
            this.session.CustomButton.Image = null;
            this.session.CustomButton.Location = new System.Drawing.Point(146, 1);
            this.session.CustomButton.Name = "";
            this.session.CustomButton.Size = new System.Drawing.Size(33, 33);
            this.session.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.session.CustomButton.TabIndex = 1;
            this.session.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.session.CustomButton.UseSelectable = true;
            this.session.CustomButton.Visible = false;
            this.session.Enabled = false;
            this.session.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.session.Lines = new string[0];
            this.session.Location = new System.Drawing.Point(379, 90);
            this.session.MaxLength = 32767;
            this.session.Multiline = true;
            this.session.Name = "session";
            this.session.PasswordChar = '\0';
            this.session.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.session.SelectedText = "";
            this.session.SelectionLength = 0;
            this.session.SelectionStart = 0;
            this.session.ShortcutsEnabled = true;
            this.session.Size = new System.Drawing.Size(180, 35);
            this.session.TabIndex = 123;
            this.session.UseSelectable = true;
            this.session.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.session.WaterMarkFont = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1315, 94);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 31);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 124;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // MarkStudentAttendanceUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.session);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.attendanceGrid);
            this.Controls.Add(this.subClass);
            this.Controls.Add(this.secCombo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.subIdCombo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MarkStudentAttendanceUC";
            this.Size = new System.Drawing.Size(1600, 700);
            this.Load += new System.EventHandler(this.MarkStudentAttendanceUC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.attendanceGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label5;
        private MetroFramework.Controls.MetroComboBox subIdCombo;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroTextBox subClass;
        private MetroFramework.Controls.MetroComboBox secCombo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroGrid attendanceGrid;
        private System.Windows.Forms.Label label14;
        private MetroFramework.Controls.MetroTextBox session;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class ManageBookUC : UserControl
    {
        private LibrarianData lb = new LibrarianData();
        private DataTable bookInfo;

        public ManageBookUC()
        {
            InitializeComponent();
            metroGrid1.DataSource = this.showWGrid();
            //categoryCombo.ValueMember = "category";
            //categoryCombo.DisplayMember = "category";
            //categoryCombo.DataSource = lb.GetBookCategory();
            //categoryCombo.SelectedIndex = -1;
        }
        public DataTable showWGrid()
        {
            return lb.GridUpdate();

        }
        public DataTable Search(string searchBooks, bool withId)
        {
            return lb.Search(searchBooks, withId);
        }
        public DataTable SearchAuthor(string searchBooks)
        {
            return lb.SearchAuthor(searchBooks);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(bookIdText.Text) && !string.IsNullOrWhiteSpace(tittleText.Text) && !string.IsNullOrWhiteSpace(editionText.Text) && !string.IsNullOrWhiteSpace(authorText.Text) && !string.IsNullOrWhiteSpace(quantityText.Text) && !string.IsNullOrWhiteSpace(shelfText.Text) && categoryCombo.SelectedIndex != -1)
            {
                if (lb.UpdateBookDetails(bookIdText.Text, tittleText.Text, int.Parse(editionText.Text), authorText.Text, categoryCombo.SelectedItem.ToString(), int.Parse(quantityText.Text), shelfText.Text))
                {
                    MessageBox.Show("Book Updated!");
                    metroGrid1.DataSource = this.showWGrid();
                    bookInfo = lb.GetBookDetails(bookIdText.Text);
                    categoryCombo.SelectedIndex = -1;
                    bookIdText.Text = tittleText.Text = editionText.Text = authorText.Text = quantityText.Text = shelfText.Text = string.Empty;

                }
            }               
            else
            {
                MessageBox.Show("FILL ALL REQUIRED FIELDS!");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //bookInfo = lb.GetBookDetails(bookId.Text);
            //try
            //{

            //if (bookInfo.Rows.Count > 0)
            //   {
            //    bookIdText.Text = bookInfo.Rows[0][1].ToString();
            //    tittleText.Text = bookInfo.Rows[0][2].ToString();
            //    editionText.Text = bookInfo.Rows[0][3].ToString();
            //    authorText.Text = bookInfo.Rows[0][4].ToString();
            //    categoryCombo.SelectedItem = bookInfo.Rows[0][5].ToString();
            //    quantityText.Text = bookInfo.Rows[0][6].ToString();
            //    shelfText.Text = bookInfo.Rows[0][7].ToString();
                
            //   }
            //    else
            //    {
            //        categoryCombo.SelectedIndex = -1;
            //        bookIdText.Text = tittleText.Text = editionText.Text = authorText.Text = quantityText.Text = shelfText.Text = string.Empty;
            //        MessageBox.Show("No Data Found!");
            //    }
            //}
            //catch (Exception) { }
        
    }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (lb.DeleteBook(bookIdText.Text))
                {
                    MessageBox.Show("Removed!");
                    categoryCombo.SelectedIndex = -1;
                    bookIdText.Text = tittleText.Text = editionText.Text = authorText.Text = quantityText.Text = shelfText.Text = string.Empty;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Something went wrong!");
            }
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            categoryCombo.SelectedIndex = -1;
            bookIdText.Text = tittleText.Text = editionText.Text = authorText.Text = quantityText.Text = shelfText.Text = string.Empty;
        }

        private void ManageBookUC_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string searchBooks;
            if (ComboBox1.Text == "")
                MessageBox.Show("Select Category");
            else if (ComboBox1.Text == "BookId")
            {
                searchBooks = textBox1.Text;
                metroGrid1.DataSource = this.Search(searchBooks, true);
            }

            else if (ComboBox1.Text == "Title")
            {
                searchBooks = textBox1.Text;
                metroGrid1.DataSource = this.Search(searchBooks, false);
            }
            else
            {
                searchBooks = textBox1.Text;
                metroGrid1.DataSource = this.SearchAuthor(searchBooks);
            }
        }

        private void metroGrid1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bookIdText.Text = metroGrid1.Rows[e.RowIndex].Cells[1].Value.ToString();
            tittleText.Text = metroGrid1.Rows[e.RowIndex].Cells[2].Value.ToString();
            editionText.Text = metroGrid1.Rows[e.RowIndex].Cells[3].Value.ToString();
            authorText.Text = metroGrid1.Rows[e.RowIndex].Cells[4].Value.ToString();
            categoryCombo.SelectedItem = metroGrid1.Rows[e.RowIndex].Cells[5].Value.ToString();
            quantityText.Text = metroGrid1.Rows[e.RowIndex].Cells[6].Value.ToString();
            shelfText.Text = metroGrid1.Rows[e.RowIndex].Cells[7].Value.ToString();

        }

        private void bookIdText_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void quantityText_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }
    }
}

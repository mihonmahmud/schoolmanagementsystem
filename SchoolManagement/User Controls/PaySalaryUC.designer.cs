﻿namespace SchoolManagement.User_Controls
{
    partial class PaySalaryUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PaySalaryUC));
            this.empId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.basic = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.home = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.medical = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tiffin = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tada = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.incentive = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.festival = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.examScript = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.examGuard = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.total = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.monthCombo = new MetroFramework.Controls.MetroComboBox();
            this.year = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.reference = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // empId
            // 
            this.empId.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.empId.Location = new System.Drawing.Point(450, 163);
            this.empId.Margin = new System.Windows.Forms.Padding(4);
            this.empId.Multiline = true;
            this.empId.Name = "empId";
            this.empId.Size = new System.Drawing.Size(236, 31);
            this.empId.TabIndex = 25;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(403, 162);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 32);
            this.label1.TabIndex = 24;
            this.label1.Text = "ID:";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(711, 164);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 31);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 26;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(620, 95);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(176, 32);
            this.label2.TabIndex = 27;
            this.label2.Text = "Reference No:";
            // 
            // basic
            // 
            this.basic.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.basic.Location = new System.Drawing.Point(507, 253);
            this.basic.Margin = new System.Windows.Forms.Padding(4);
            this.basic.Multiline = true;
            this.basic.Name = "basic";
            this.basic.Size = new System.Drawing.Size(236, 31);
            this.basic.TabIndex = 29;
            this.basic.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.basic_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(429, 251);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 32);
            this.label3.TabIndex = 28;
            this.label3.Text = "Basic:";
            // 
            // home
            // 
            this.home.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.home.Location = new System.Drawing.Point(999, 253);
            this.home.Margin = new System.Windows.Forms.Padding(4);
            this.home.Multiline = true;
            this.home.Name = "home";
            this.home.Size = new System.Drawing.Size(236, 31);
            this.home.TabIndex = 31;
            this.home.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.home_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(908, 251);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 32);
            this.label4.TabIndex = 30;
            this.label4.Text = "Home:";
            // 
            // medical
            // 
            this.medical.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.medical.Location = new System.Drawing.Point(507, 318);
            this.medical.Margin = new System.Windows.Forms.Padding(4);
            this.medical.Multiline = true;
            this.medical.Name = "medical";
            this.medical.Size = new System.Drawing.Size(236, 31);
            this.medical.TabIndex = 33;
            this.medical.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.medical_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(398, 316);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 32);
            this.label5.TabIndex = 32;
            this.label5.Text = "Medical:";
            // 
            // tiffin
            // 
            this.tiffin.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tiffin.Location = new System.Drawing.Point(999, 314);
            this.tiffin.Margin = new System.Windows.Forms.Padding(4);
            this.tiffin.Multiline = true;
            this.tiffin.Name = "tiffin";
            this.tiffin.Size = new System.Drawing.Size(236, 31);
            this.tiffin.TabIndex = 35;
            this.tiffin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tiffin_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(920, 312);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 32);
            this.label6.TabIndex = 34;
            this.label6.Text = "Tiffin:";
            // 
            // tada
            // 
            this.tada.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tada.Location = new System.Drawing.Point(507, 387);
            this.tada.Margin = new System.Windows.Forms.Padding(4);
            this.tada.Multiline = true;
            this.tada.Name = "tada";
            this.tada.Size = new System.Drawing.Size(236, 31);
            this.tada.TabIndex = 37;
            this.tada.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tada_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(415, 386);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 32);
            this.label7.TabIndex = 36;
            this.label7.Text = "TA_DA:";
            // 
            // incentive
            // 
            this.incentive.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.incentive.Location = new System.Drawing.Point(999, 387);
            this.incentive.Margin = new System.Windows.Forms.Padding(4);
            this.incentive.Multiline = true;
            this.incentive.Name = "incentive";
            this.incentive.Size = new System.Drawing.Size(236, 31);
            this.incentive.TabIndex = 39;
            this.incentive.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.incentive_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(877, 384);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(117, 32);
            this.label8.TabIndex = 38;
            this.label8.Text = "Incentive:";
            // 
            // festival
            // 
            this.festival.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.festival.Location = new System.Drawing.Point(507, 458);
            this.festival.Margin = new System.Windows.Forms.Padding(4);
            this.festival.Multiline = true;
            this.festival.Name = "festival";
            this.festival.Size = new System.Drawing.Size(236, 31);
            this.festival.TabIndex = 41;
            this.festival.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.festival_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(403, 455);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 32);
            this.label9.TabIndex = 40;
            this.label9.Text = "Festival:";
            // 
            // examScript
            // 
            this.examScript.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examScript.Location = new System.Drawing.Point(999, 458);
            this.examScript.Margin = new System.Windows.Forms.Padding(4);
            this.examScript.Multiline = true;
            this.examScript.Name = "examScript";
            this.examScript.Size = new System.Drawing.Size(236, 31);
            this.examScript.TabIndex = 43;
            this.examScript.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.examScript_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(850, 456);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(143, 32);
            this.label10.TabIndex = 42;
            this.label10.Text = "Exam Script:";
            // 
            // examGuard
            // 
            this.examGuard.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examGuard.Location = new System.Drawing.Point(507, 522);
            this.examGuard.Margin = new System.Windows.Forms.Padding(4);
            this.examGuard.Multiline = true;
            this.examGuard.Name = "examGuard";
            this.examGuard.Size = new System.Drawing.Size(236, 31);
            this.examGuard.TabIndex = 45;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(355, 519);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(147, 32);
            this.label11.TabIndex = 44;
            this.label11.Text = "Exam Guard:";
            // 
            // total
            // 
            this.total.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total.Location = new System.Drawing.Point(711, 594);
            this.total.Margin = new System.Windows.Forms.Padding(4);
            this.total.Multiline = true;
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(236, 31);
            this.total.TabIndex = 47;
            this.total.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.total_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(635, 591);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 32);
            this.label12.TabIndex = 46;
            this.label12.Text = "Total:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(757, 523);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 31);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 48;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.White;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(972, 594);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(40, 31);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 49;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // monthCombo
            // 
            this.monthCombo.FormattingEnabled = true;
            this.monthCombo.ItemHeight = 24;
            this.monthCombo.Items.AddRange(new object[] {
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"});
            this.monthCombo.Location = new System.Drawing.Point(916, 168);
            this.monthCombo.Margin = new System.Windows.Forms.Padding(4);
            this.monthCombo.Name = "monthCombo";
            this.monthCombo.Size = new System.Drawing.Size(212, 30);
            this.monthCombo.TabIndex = 97;
            this.monthCombo.UseSelectable = true;
            // 
            // year
            // 
            this.year.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.year.Location = new System.Drawing.Point(1147, 166);
            this.year.Margin = new System.Windows.Forms.Padding(4);
            this.year.Multiline = true;
            this.year.Name = "year";
            this.year.Size = new System.Drawing.Size(102, 35);
            this.year.TabIndex = 96;
            this.year.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.year_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(817, 166);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 32);
            this.label13.TabIndex = 95;
            this.label13.Text = "Month:";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // reference
            // 
            this.reference.Enabled = false;
            this.reference.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reference.Location = new System.Drawing.Point(802, 98);
            this.reference.Margin = new System.Windows.Forms.Padding(4);
            this.reference.Multiline = true;
            this.reference.Name = "reference";
            this.reference.Size = new System.Drawing.Size(236, 31);
            this.reference.TabIndex = 98;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(669, 14);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(344, 38);
            this.label14.TabIndex = 122;
            this.label14.Text = "PAY EMPLOYEES SALARY";
            // 
            // PaySalaryUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.label14);
            this.Controls.Add(this.reference);
            this.Controls.Add(this.monthCombo);
            this.Controls.Add(this.year);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.total);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.examGuard);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.examScript);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.festival);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.incentive);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tada);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tiffin);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.medical);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.home);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.basic);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.empId);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "PaySalaryUC";
            this.Size = new System.Drawing.Size(1650, 700);
            this.Load += new System.EventHandler(this.PaySalaryUC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox empId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox basic;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox home;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox medical;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tiffin;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tada;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox incentive;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox festival;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox examScript;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox examGuard;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox total;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private MetroFramework.Controls.MetroComboBox monthCombo;
        private System.Windows.Forms.TextBox year;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox reference;
        private System.Windows.Forms.Label label14;
    }
}

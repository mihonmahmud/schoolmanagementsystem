﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class ManageAdmissionScheduleUC : UserControl
    {
        private AdminData ad = new AdminData();
        private HeadTeacherData htd = new HeadTeacherData();

        public ManageAdmissionScheduleUC()
        {
            InitializeComponent();
        }

        private void ManageAdmissionScheduleUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(htd.GetAdmissionSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void sessionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(sessionCombo.SelectedIndex == -1)
            {
                admissionGrid.DataSource = null;
            }
            else
            {               
                try
                {
                    admissionGrid.DataSource = ad.GetAdmissionScheduleInfo(sessionCombo.SelectedValue.ToString());
                }
                catch (Exception) { }
            }
        }

        private void admissionGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            classCombo.SelectedItem = admissionGrid.Rows[e.RowIndex].Cells[0].Value.ToString();
            date.Text = admissionGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
            time.Text = admissionGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if(ad.IsAdmissionDateTimeCreated(sessionCombo.SelectedValue.ToString(), date.Text, time.Text))
            {
                MessageBox.Show("SCHEDULED DATE AND TIME ALREADY EXISTS!!!");
                date.Text = time.Text = "";
            }
            else
            {
                if(sessionCombo.SelectedIndex != -1 && classCombo.SelectedIndex != -1 && !string.IsNullOrWhiteSpace(date.Text) && !string.IsNullOrWhiteSpace(time.Text))
                {
                    if (ad.UpdateAdmissionSchedule(sessionCombo.SelectedValue.ToString(), classCombo.SelectedItem.ToString(), date.Text, time.Text))
                    {
                        MessageBox.Show("UPDATED!");
                        admissionGrid.DataSource = ad.GetAdmissionScheduleInfo(sessionCombo.SelectedValue.ToString());
                        classCombo.SelectedIndex = -1;
                        date.Text = time.Text = "";
                    }
                }
                else
                {
                    MessageBox.Show("FILL ALL REQUIRED FIELDS!");
                }
            }          
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if(ad.DeleteAdmissionSchedule(sessionCombo.SelectedValue.ToString(), classCombo.SelectedItem.ToString()))
            {
                MessageBox.Show("SCHEDULE DELETED!");
                admissionGrid.DataSource = ad.GetAdmissionScheduleInfo(sessionCombo.SelectedValue.ToString());
                classCombo.SelectedIndex = -1;
                date.Text = time.Text = "";
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            sessionCombo.SelectedIndex = classCombo.SelectedIndex = -1;
            date.Text = time.Text = "";
            admissionGrid.DataSource = null;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            if(admissionGrid.Rows.Count > 0)
            {
                if (ad.InsertNewAdmissionData(newSession.Text, (DataTable)admissionGrid.DataSource))
                {
                    MessageBox.Show("SCHEDULE CREATED!");
                }
            }
            else
            {
                MessageBox.Show("NO SCHEDULE FOUND!");
            }
        }
    }
}

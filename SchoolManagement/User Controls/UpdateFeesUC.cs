﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class UpdateFeesUC : UserControl
    {
        private AdminData ad = new AdminData();

        public UpdateFeesUC()
        {
            InitializeComponent();
        }

        private void UpdateFeesUC_Load(object sender, EventArgs e)
        {
            Fees();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                if (ad.UpdateFees(int.Parse(monthly.Text), int.Parse(exam.Text), int.Parse(admission.Text), int.Parse(development.Text), int.Parse(library.Text)))
                {
                    MessageBox.Show("UPDATED");
                    Fees();
                }
            }
            catch (Exception) { }
        }

        private void Fees()
        {
            DataTable table = ad.GetFees();

            monthly.Text = table.Rows[0][1].ToString();
            exam.Text = table.Rows[0][2].ToString();
            admission.Text = table.Rows[0][3].ToString();
            development.Text = table.Rows[0][4].ToString();
            library.Text = table.Rows[0][5].ToString();
        }

        private void monthly_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void exam_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void admission_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void development_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void library_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }
    }
}

﻿namespace SchoolManagement.User_Controls
{
    partial class UpdateMarksUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateMarksUC));
            this.label14 = new System.Windows.Forms.Label();
            this.termCombo = new MetroFramework.Controls.MetroComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.sessionCombo = new MetroFramework.Controls.MetroComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.subIdCombo = new MetroFramework.Controls.MetroComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.marksGrid = new MetroFramework.Controls.MetroGrid();
            this.marks = new MetroFramework.Controls.MetroTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.stuId = new MetroFramework.Controls.MetroTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.marksGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(614, 9);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(231, 38);
            this.label14.TabIndex = 123;
            this.label14.Text = "UPDATE MARKS";
            // 
            // termCombo
            // 
            this.termCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.termCombo.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.termCombo.FormattingEnabled = true;
            this.termCombo.ItemHeight = 24;
            this.termCombo.Items.AddRange(new object[] {
            "Mid",
            "Final"});
            this.termCombo.Location = new System.Drawing.Point(662, 82);
            this.termCombo.Margin = new System.Windows.Forms.Padding(4);
            this.termCombo.Name = "termCombo";
            this.termCombo.Size = new System.Drawing.Size(212, 30);
            this.termCombo.TabIndex = 129;
            this.termCombo.UseSelectable = true;
            this.termCombo.SelectedIndexChanged += new System.EventHandler(this.termCombo_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(583, 78);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 32);
            this.label9.TabIndex = 128;
            this.label9.Text = "Term:";
            // 
            // sessionCombo
            // 
            this.sessionCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.sessionCombo.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.sessionCombo.FormattingEnabled = true;
            this.sessionCombo.ItemHeight = 24;
            this.sessionCombo.Location = new System.Drawing.Point(349, 81);
            this.sessionCombo.Margin = new System.Windows.Forms.Padding(4);
            this.sessionCombo.Name = "sessionCombo";
            this.sessionCombo.Size = new System.Drawing.Size(212, 30);
            this.sessionCombo.TabIndex = 127;
            this.sessionCombo.UseSelectable = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(244, 77);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 32);
            this.label5.TabIndex = 126;
            this.label5.Text = "Session:";
            // 
            // subIdCombo
            // 
            this.subIdCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.subIdCombo.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.subIdCombo.FormattingEnabled = true;
            this.subIdCombo.ItemHeight = 24;
            this.subIdCombo.Location = new System.Drawing.Point(1051, 82);
            this.subIdCombo.Margin = new System.Windows.Forms.Padding(4);
            this.subIdCombo.Name = "subIdCombo";
            this.subIdCombo.Size = new System.Drawing.Size(212, 30);
            this.subIdCombo.TabIndex = 125;
            this.subIdCombo.UseSelectable = true;
            this.subIdCombo.SelectedIndexChanged += new System.EventHandler(this.subIdCombo_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(916, 78);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 32);
            this.label1.TabIndex = 124;
            this.label1.Text = "Subject ID:";
            // 
            // marksGrid
            // 
            this.marksGrid.AllowUserToResizeRows = false;
            this.marksGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.marksGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.marksGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.marksGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.marksGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.marksGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.marksGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.marksGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.marksGrid.DefaultCellStyle = dataGridViewCellStyle6;
            this.marksGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.marksGrid.EnableHeadersVisualStyles = false;
            this.marksGrid.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.marksGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.marksGrid.Location = new System.Drawing.Point(508, 163);
            this.marksGrid.Name = "marksGrid";
            this.marksGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.marksGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.marksGrid.RowHeadersVisible = false;
            this.marksGrid.RowHeadersWidth = 51;
            this.marksGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle8.Padding = new System.Windows.Forms.Padding(6);
            this.marksGrid.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.marksGrid.RowTemplate.Height = 24;
            this.marksGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.marksGrid.Size = new System.Drawing.Size(944, 447);
            this.marksGrid.TabIndex = 130;
            this.marksGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.marksGrid_CellClick);
            // 
            // marks
            // 
            // 
            // 
            // 
            this.marks.CustomButton.Image = null;
            this.marks.CustomButton.Location = new System.Drawing.Point(186, 2);
            this.marks.CustomButton.Name = "";
            this.marks.CustomButton.Size = new System.Drawing.Size(23, 23);
            this.marks.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.marks.CustomButton.TabIndex = 1;
            this.marks.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.marks.CustomButton.UseSelectable = true;
            this.marks.CustomButton.Visible = false;
            this.marks.Lines = new string[0];
            this.marks.Location = new System.Drawing.Point(883, 648);
            this.marks.MaxLength = 32767;
            this.marks.Multiline = true;
            this.marks.Name = "marks";
            this.marks.PasswordChar = '\0';
            this.marks.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.marks.SelectedText = "";
            this.marks.SelectionLength = 0;
            this.marks.SelectionStart = 0;
            this.marks.ShortcutsEnabled = true;
            this.marks.Size = new System.Drawing.Size(212, 28);
            this.marks.TabIndex = 132;
            this.marks.UseSelectable = true;
            this.marks.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.marks.WaterMarkFont = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(792, 644);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 32);
            this.label7.TabIndex = 131;
            this.label7.Text = "Marks:";
            // 
            // stuId
            // 
            // 
            // 
            // 
            this.stuId.CustomButton.Image = null;
            this.stuId.CustomButton.Location = new System.Drawing.Point(186, 2);
            this.stuId.CustomButton.Name = "";
            this.stuId.CustomButton.Size = new System.Drawing.Size(23, 23);
            this.stuId.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.stuId.CustomButton.TabIndex = 1;
            this.stuId.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.stuId.CustomButton.UseSelectable = true;
            this.stuId.CustomButton.Visible = false;
            this.stuId.Enabled = false;
            this.stuId.Lines = new string[0];
            this.stuId.Location = new System.Drawing.Point(498, 648);
            this.stuId.MaxLength = 32767;
            this.stuId.Multiline = true;
            this.stuId.Name = "stuId";
            this.stuId.PasswordChar = '\0';
            this.stuId.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.stuId.SelectedText = "";
            this.stuId.SelectionLength = 0;
            this.stuId.SelectionStart = 0;
            this.stuId.ShortcutsEnabled = true;
            this.stuId.Size = new System.Drawing.Size(212, 28);
            this.stuId.TabIndex = 134;
            this.stuId.UseSelectable = true;
            this.stuId.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.stuId.WaterMarkFont = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(355, 644);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 32);
            this.label2.TabIndex = 133;
            this.label2.Text = "Student ID:";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1109, 646);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 31);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 135;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1166, 644);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 31);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 136;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // UpdateMarksUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.stuId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.marks);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.marksGrid);
            this.Controls.Add(this.termCombo);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.sessionCombo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.subIdCombo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label14);
            this.Name = "UpdateMarksUC";
            this.Size = new System.Drawing.Size(1650, 700);
            this.Load += new System.EventHandler(this.UpdateMarksUC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.marksGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label14;
        private MetroFramework.Controls.MetroComboBox termCombo;
        private System.Windows.Forms.Label label9;
        private MetroFramework.Controls.MetroComboBox sessionCombo;
        private System.Windows.Forms.Label label5;
        private MetroFramework.Controls.MetroComboBox subIdCombo;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroGrid marksGrid;
        private MetroFramework.Controls.MetroTextBox marks;
        private System.Windows.Forms.Label label7;
        private MetroFramework.Controls.MetroTextBox stuId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

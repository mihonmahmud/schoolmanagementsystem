﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class InventoryUC : UserControl
    {
        private AccountantData ad = new AccountantData();

        public InventoryUC()
        {
            InitializeComponent();
        }

        private void InventoryUC_Load(object sender, EventArgs e)
        {
            CreateRef();
        }

        private void CreateRef()
        {
            reference.Text = "In-" + DateTime.Now.Year.ToString() + "-" + (ad.GetLastInvenId() + 1);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            CreateRef();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if(ad.InsertInventory(reference.Text, name.Text, int.Parse(quantity.Text), float.Parse(price.Text), float.Parse(total.Text)))
            {
                MessageBox.Show("ADDED!");
                name.Text = quantity.Text = price.Text = total.Text = "";
            }
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            total.Text = (float.Parse(quantity.Text) * float.Parse(price.Text)).ToString();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            reference.Text = name.Text = quantity.Text = price.Text = total.Text = "";
        }
    }
}

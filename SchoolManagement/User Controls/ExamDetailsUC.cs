﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class ExamDetailsUC : UserControl
    {
        private AdminData ad = new AdminData();

        public ExamDetailsUC()
        {
            InitializeComponent();
        }

        private void ExamDetailsUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ad.GetMarkSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if(sessionCombo.SelectedIndex != -1 && termCombo.SelectedIndex != -1 && classCombo.SelectedIndex != -1 && !string.IsNullOrWhiteSpace(stuId.Text))
            {               
                try
                {
                    stuMarkGrid.DataSource = ad.GetStuMarks(sessionCombo.SelectedValue.ToString(), termCombo.SelectedItem.ToString(), classCombo.SelectedItem.ToString(), stuId.Text);
                }
                catch (Exception) { }
            }
            
        }

        private void sessionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if(sessionCombo.SelectedIndex == -1)
            //{
            //    stuMarkGrid.DataSource = null;
            //}
            //else
            //{
            //    try
            //    {
            //        stuMarkGrid.DataSource = ad.GetStuMarksBySession(stuId.Text, sessionCombo.SelectedValue.ToString());
            //    }
            //    catch (Exception) { }
            //}
        }

        private void subIdCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if((classCombo.SelectedIndex != -1) && sessionCombo.SelectedIndex != -1)
            //{
            //    try
            //    {
            //        stuMarkGrid.DataSource = ad.GetStuMarksBySessionSub(stuId.Text, sessionCombo.SelectedValue.ToString(), classCombo.SelectedValue.ToString());
            //    }
            //    catch (Exception) { }
            //}
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            stuId.Text = "";
            sessionCombo.SelectedIndex = classCombo.SelectedIndex = -1;
            stuMarkGrid.DataSource = null;
        }
    }
}

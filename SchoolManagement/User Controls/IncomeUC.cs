﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class IncomeUC : UserControl
    {
        private LoginData lid = new LoginData();

        public IncomeUC()
        {
            InitializeComponent();
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void IncomeUC_Load(object sender, EventArgs e)
        {
            try
            {
                monthCombo.ValueMember = "month";
                monthCombo.DisplayMember = "month";
                monthCombo.DataSource = RemoveDuplicatesRecords(lid.GetIncomeMonth());
                monthCombo.SelectedIndex = -1;

                yearCombo.ValueMember = "year";
                yearCombo.DisplayMember = "year";
                yearCombo.DataSource = RemoveDuplicatesRecords(lid.GetIncomeYear());
                yearCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private void yearCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(yearCombo.SelectedIndex == -1)
            {
                incomeGrid.DataSource = null;
            }
            else
            {
                try
                {
                    incomeGrid.DataSource = lid.GetIncomeByYear(yearCombo.SelectedValue.ToString());
                }
                catch (Exception) { }
            }
        }

        private void monthCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if((monthCombo.SelectedIndex == -1) && (monthCombo.SelectedIndex != -1))
            {
                incomeGrid.DataSource = lid.GetIncomeByYear(yearCombo.SelectedValue.ToString());
            }
            else if((monthCombo.SelectedIndex != -1) && (monthCombo.SelectedIndex != -1))
            {
                try
                {
                    incomeGrid.DataSource = lid.GetIncomeByMonYear(monthCombo.SelectedValue.ToString(), yearCombo.SelectedValue.ToString());
                }
                catch (Exception) { }
            }
        }
    }
}

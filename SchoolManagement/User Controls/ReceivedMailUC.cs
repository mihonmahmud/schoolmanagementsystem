﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;
using SchoolManagement.User_Controls;

namespace SchoolManagement.User_Controls
{
    public partial class ReceivedMailUC : UserControl
    {
        private LoginData lid = new LoginData();
        private string _userId;
        private string from;

        public ReceivedMailUC(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        private void ReceivedMailUC_Load(object sender, EventArgs e)
        {
            try
            {
                mailsGrid.DataSource = lid.ReceivedMail(_userId);
            }
            catch (Exception) { }
        }

        private void mailsGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                from = mailsGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
                mFrom.Text = lid.GetNameById(from);
                mSubject.Text = mailsGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
                mDescription.Text = mailsGrid.Rows[e.RowIndex].Cells[3].Value.ToString();
            }
            catch (Exception) { }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if(mFrom.Text != null)
            {
                this.Hide();
                this.Parent.Controls.Add(new SendMailUC(_userId, from));
            }
        }
    }
}

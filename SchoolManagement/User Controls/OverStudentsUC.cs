﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class OverStudentsUC : UserControl
    {
        private AccountantData ac = new AccountantData();
        private OwnerData od = new OwnerData();

        public OverStudentsUC()
        {
            InitializeComponent();
        }

        private void OverStudentsUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ac.GetSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void sessionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(sessionCombo.SelectedIndex == -1)
            {
                studentsGrid.DataSource = null;
            }
            else
            {
                try
                {
                    studentsGrid.DataSource = od.OverStudents(sessionCombo.SelectedValue.ToString());
                }
                catch (Exception) { }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class TeachersScheduleUC : UserControl
    {
        private AccountantData ac = new AccountantData();
        private HeadTeacherData htd = new HeadTeacherData();
        private AdminData ad = new AdminData();

        public TeachersScheduleUC()
        {
            InitializeComponent();
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void TeachersScheduleUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ac.GetSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
                scheduleGrid.DataSource = htd.GetEmployeeSchedule(sessionCombo.SelectedValue.ToString(), empId.Text);
            }
            catch (Exception) { }
        }

        private void scheduleGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                subId.Text = scheduleGrid.Rows[e.RowIndex].Cells[0].Value.ToString();
                subClass.Text = scheduleGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
                sec.Text = scheduleGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
            }
            catch (Exception) { }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                if (htd.DeleteTeacherSchedule(sessionCombo.SelectedValue.ToString(), empId.Text, subId.Text, subClass.Text, sec.Text))
                {
                    scheduleGrid.DataSource = htd.GetEmployeeSchedule(sessionCombo.SelectedValue.ToString(), empId.Text);
                    MessageBox.Show("DELETED!");
                    subId.Text = "";
                }
            }
            catch (Exception) { }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            empId.Text = subId.Text = "";
            scheduleGrid.DataSource = null;
            sessionCombo.SelectedIndex = -1;
        }

        private void sessionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(sessionCombo.SelectedIndex == -1)
            {
                scheduleGrid.DataSource = null;
            }
            else
            {
                scheduleGrid.DataSource = ad.GetEmployeeSchedule(sessionCombo.SelectedValue.ToString());
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class EmployeeExamScheduleUC : UserControl
    {
        private HeadTeacherData htd = new HeadTeacherData();
        private AdminData ad = new AdminData();

        public EmployeeExamScheduleUC()
        {
            InitializeComponent();
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void EmployeeExamScheduleUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(htd.GetAdmissionSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }

            termCombo.Enabled = false;
        }

        private void sessionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(sessionCombo.SelectedIndex == -1)
            {
                examGrid.DataSource = null;
            }
            else
            {
                try
                {
                    if((examCombo.SelectedItem.ToString() == "Term") && (sessionCombo.SelectedIndex != -1))
                    {
                        examGrid.DataSource = ad.GetTermSchedule(sessionCombo.SelectedValue.ToString(), termCombo.SelectedItem.ToString());
                    }
                    else if ((examCombo.SelectedItem.ToString() == "Admission") && (sessionCombo.SelectedIndex != -1))
                    {
                        examGrid.DataSource = ad.GetAdmissionSchedule(sessionCombo.SelectedValue.ToString());
                    }
                }
                catch (Exception) { }
            }
        }

        private void examCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if((examCombo.SelectedIndex == -1) || (examCombo.SelectedItem.ToString() == "Admission"))
            {
                termCombo.Enabled = false;
            }
            else
            {
                termCombo.Enabled = true;
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if(examCombo.SelectedIndex == -1)
            {
                MessageBox.Show("FILL ALL REQUIRED FIELDS");
            }
            else if((string)examCombo.SelectedItem.ToString() == "Admission")
            {
                if(sessionCombo.SelectedIndex != -1 && !string.IsNullOrWhiteSpace(empId.Text))
                {
                    try
                    {
                        examGrid.DataSource = ad.GetAdmissionScheduleById(sessionCombo.SelectedValue.ToString(), empId.Text);
                    }
                    catch (Exception) { }
                }
            }
            else if (examCombo.SelectedItem.ToString() == "Term")
            {
                if (sessionCombo.SelectedIndex != -1 && !string.IsNullOrWhiteSpace(empId.Text) && termCombo.SelectedIndex != -1)
                {
                    try
                    {
                        examGrid.DataSource = ad.GetTermScheduleById(sessionCombo.SelectedValue.ToString(), termCombo.SelectedItem.ToString(), empId.Text);
                    }
                    catch (Exception) { }
                }
            }
            else
            {
                MessageBox.Show("FILL ALL REQUIRED FIELDS");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class HeadTermExamUC : UserControl
    {
        private HeadTeacherData htd = new HeadTeacherData();
        private DataTable ddt = new DataTable();
        private DataRow dr;

        public HeadTermExamUC()
        {
            InitializeComponent();
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void HeadTermExamUC_Load(object sender, EventArgs e)
        {
            try
            {
                teacherIdCombo.ValueMember = "employeeId";
                teacherIdCombo.DisplayMember = "employeeId";
                teacherIdCombo.DataSource = htd.GetTeachersID();
                teacherIdCombo.SelectedIndex = -1;

                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(htd.GetTermSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }

            ddt.Columns.Add("Session");
            ddt.Columns.Add("Teacher ID");
            ddt.Columns.Add("Subject ID");
            ddt.Columns.Add("Class");
            ddt.Columns.Add("Term");
            ddt.Columns.Add("Exam Date");
            ddt.Columns.Add("Exam Time");
        }

        private void subIdCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(subIdCombo.SelectedIndex == -1)
            {
                subClass.Text = time.Text = date.Text = "";
            }
            else
            {
                try
                {
                    DataTable info = htd.GetTermInfoBySubId(sessionCombo.SelectedValue.ToString(), termCombo.SelectedItem.ToString(), subIdCombo.SelectedValue.ToString());

                    subClass.Text = info.Rows[0][0].ToString();
                    date.Text = info.Rows[0][1].ToString();
                    time.Text = info.Rows[0][2].ToString();
                }
                catch (Exception) { }
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (htd.IsExaminerAssignedByDateTime(sessionCombo.SelectedValue.ToString(), termCombo.SelectedItem.ToString(), date.Text, time.Text, teacherIdCombo.SelectedValue.ToString()))
            {
                MessageBox.Show("EXAMINER ALREADY ASSIGNED ON THIS SLOT!!!");
                teacherIdCombo.SelectedIndex = -1;
            }
            else
            {
                dr = ddt.NewRow();
                dr["Session"] = sessionCombo.SelectedValue.ToString();
                dr["Term"] = termCombo.SelectedItem.ToString();
                dr["Teacher ID"] = teacherIdCombo.SelectedValue.ToString();
                dr["Subject ID"] = subIdCombo.SelectedValue.ToString();
                dr["Class"] = subClass.Text;
                dr["Exam Date"] = date.Text;
                dr["Exam Time"] = time.Text;
                ddt.Rows.Add(dr);

                scheduleGrid.DataSource = ddt;
            }           
        }

        private void scheduleGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            teacherIdCombo.SelectedItem = scheduleGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
            subClass.Text = scheduleGrid.Rows[e.RowIndex].Cells[4].Value.ToString();
            date.Text = scheduleGrid.Rows[e.RowIndex].Cells[5].Value.ToString();
            time.Text = scheduleGrid.Rows[e.RowIndex].Cells[6].Value.ToString();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            scheduleGrid.Rows.RemoveAt(scheduleGrid.CurrentCell.RowIndex);
            sessionCombo.SelectedIndex = teacherIdCombo.SelectedIndex = subIdCombo.SelectedIndex = -1;
            subClass.Text = time.Text = date.Text = "";
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                if (htd.AssignTermExaminer((DataTable)scheduleGrid.DataSource))
                {
                    MessageBox.Show("ASSIGNED!");
                    Clear();
                }             
            }
            catch (Exception) { }
        }

        private void Clear()
        {
            sessionCombo.SelectedIndex = teacherIdCombo.SelectedIndex = subIdCombo.SelectedIndex = termCombo.SelectedIndex = -1;
            subClass.Text = time.Text = date.Text = "";
            ddt.Rows.Clear();
        }

        private void termCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(termCombo.SelectedIndex == -1)
            {
                subIdCombo.DataSource = null;
            }
            else
            {
                try
                {
                    subIdCombo.ValueMember = "subId";
                    subIdCombo.DisplayMember = "subId";
                    subIdCombo.DataSource = htd.GetTermSubId(sessionCombo.SelectedValue.ToString(), termCombo.SelectedItem.ToString());
                    subIdCombo.SelectedIndex = -1;
                }
                catch (Exception) { }
            }
        }
    }
}

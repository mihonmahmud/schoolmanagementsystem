﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class ManageStudentInfoUC : UserControl
    {
        private AdminData ad = new AdminData();

        public ManageStudentInfoUC()
        {
            InitializeComponent();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = ad.GetSpeicificStuInfo(stuId.Text);

                name.Text = dt.Rows[0][0].ToString();
                fName.Text = dt.Rows[0][1].ToString();
                mName.Text = dt.Rows[0][2].ToString();
                dob.Text = dt.Rows[0][3].ToString();
                religion.Text = dt.Rows[0][4].ToString();
                genderCombo.SelectedItem = dt.Rows[0][5].ToString();
                bgCombo.SelectedItem = dt.Rows[0][6].ToString();
                present.Text = dt.Rows[0][7].ToString();
                permanent.Text = dt.Rows[0][8].ToString();
                nation.Text = dt.Rows[0][9].ToString();
                gName.Text = dt.Rows[0][10].ToString();
                gProfession.Text = dt.Rows[0][11].ToString();
                gPhone.Text = dt.Rows[0][12].ToString();
                gMail.Text = dt.Rows[0][13].ToString();
            }
            catch (Exception) { }
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(stuId.Text) && !string.IsNullOrWhiteSpace(name.Text) && !string.IsNullOrWhiteSpace(fName.Text) && !string.IsNullOrWhiteSpace(mName.Text) && !string.IsNullOrWhiteSpace(dob.Text) && !string.IsNullOrWhiteSpace(religion.Text) && !string.IsNullOrWhiteSpace(present.Text) && !string.IsNullOrWhiteSpace(permanent.Text) && !string.IsNullOrWhiteSpace(nation.Text) && genderCombo.SelectedIndex != -1 && bgCombo.SelectedIndex != -1)
            {
                try
                {
                    if (ad.UpdateStudentInfo(stuId.Text.Trim(), name.Text.Trim(), fName.Text.Trim(), mName.Text.Trim(), dob.Text.Trim(), religion.Text.Trim(), genderCombo.SelectedItem.ToString(), bgCombo.SelectedItem.ToString(), present.Text.Trim(), permanent.Text.Trim(), nation.Text.Trim()))
                    {
                        DataTable dt = ad.GetSpeicificStuInfo(stuId.Text);

                        name.Text = dt.Rows[0][0].ToString();
                        fName.Text = dt.Rows[0][1].ToString();
                        mName.Text = dt.Rows[0][2].ToString();
                        dob.Text = dt.Rows[0][3].ToString();
                        religion.Text = dt.Rows[0][4].ToString();
                        genderCombo.SelectedItem = dt.Rows[0][5].ToString();
                        bgCombo.SelectedItem = dt.Rows[0][6].ToString();
                        present.Text = dt.Rows[0][7].ToString();
                        permanent.Text = dt.Rows[0][8].ToString();
                        nation.Text = dt.Rows[0][9].ToString();
                        MessageBox.Show("STUDENT INFORMATION UPDATED!");
                    }
                }
                catch (Exception) { }
            }
            else
            {
                MessageBox.Show("FILL ALL REQUIRED FIELDS!");
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(stuId.Text) && !string.IsNullOrWhiteSpace(gName.Text) && !string.IsNullOrWhiteSpace(gProfession.Text) && !string.IsNullOrWhiteSpace(gPhone.Text) && !string.IsNullOrWhiteSpace(gMail.Text))
            {
                try
                {
                    if (ad.UpdateGuardianInfo(stuId.Text, gName.Text, gProfession.Text, gPhone.Text, gMail.Text))
                    {
                        DataTable dt = ad.GetSpeicificStuInfo(stuId.Text);

                        gName.Text = dt.Rows[0][10].ToString();
                        gProfession.Text = dt.Rows[0][11].ToString();
                        gPhone.Text = dt.Rows[0][12].ToString();
                        gMail.Text = dt.Rows[0][13].ToString();
                        MessageBox.Show("GUARDIAN INFORMATION UPDATED!");
                    }
                }
                catch (Exception) { }
            }
            else
            {
                MessageBox.Show("FILL ALL REQUIRED FIELDS!");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            stuId.Text = name.Text = fName.Text = mName.Text = dob.Text = religion.Text = present.Text = permanent.Text = nation.Text = gName.Text = gProfession.Text = gPhone.Text = gMail.Text = "";
            genderCombo.SelectedIndex = bgCombo.SelectedIndex = -1;
        }

        private void name_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        private void fName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        private void mName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        private void religion_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void nation_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void gName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        private void gProfession_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        private void gPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }
    }
}

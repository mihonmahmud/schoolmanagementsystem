﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class StudentsByGradesUC : UserControl
    {
        private AccountantData ac = new AccountantData();
        private HeadTeacherData htd = new HeadTeacherData();

        public StudentsByGradesUC()
        {
            InitializeComponent();
        }

        private void FailedStudentsUC_Load(object sender, EventArgs e)
        {           
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ac.GetSession());
                sessionCombo.SelectedIndex = -1;

                gradeCombo.ValueMember = "grade";
                gradeCombo.DisplayMember = "grade";
                gradeCombo.DataSource = htd.GetGrades();
                gradeCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void gardeCombo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void gradeCombo_SelectedIndexChanged(object sender, EventArgs e)
        {            
            try
            {
                if (gradeCombo.SelectedIndex == -1)
                {
                    failedGrid.DataSource = null;
                }
                else if (sessionCombo.SelectedIndex != -1 && classCombo.SelectedIndex != -1 && termCombo.SelectedIndex != -1 && gradeCombo.SelectedIndex != -1)
                {
                    failedGrid.DataSource = htd.StudentsByGrade(sessionCombo.SelectedValue.ToString(), classCombo.SelectedItem.ToString(), termCombo.SelectedItem.ToString(), gradeCombo.SelectedValue.ToString());
                }
            }
            catch (Exception) { }
        }
    }
}

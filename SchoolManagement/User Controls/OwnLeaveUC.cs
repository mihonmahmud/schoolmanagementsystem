﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class OwnLeaveUC : UserControl
    {
        private LoginData lid = new LoginData();
        private string _userId;

        public OwnLeaveUC(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        private void OwnLeaveUC_Load(object sender, EventArgs e)
        {
            try
            {
                leaveGrid.DataSource = lid.GetLeave(_userId);
            }
            catch (Exception) { }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                leaveGrid.DataSource = lid.GetLeaveByYear(year.Text, _userId);
            }
            catch (Exception) { }
        }

        private void year_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }
    }
}

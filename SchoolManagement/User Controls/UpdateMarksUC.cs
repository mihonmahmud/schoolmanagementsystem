﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class UpdateMarksUC : UserControl
    {
        private TeacherData td = new TeacherData();
        private HeadTeacherData htd = new HeadTeacherData();
        private string _userId;

        public UpdateMarksUC(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        private void UpdateMarksUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(td.GetSessionById(_userId));
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void termCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (termCombo.SelectedIndex == -1)
            {
                subIdCombo.DataSource = null;
            }
            else
            {
                try
                {
                    subIdCombo.ValueMember = "subId";
                    subIdCombo.DisplayMember = "subId";
                    subIdCombo.DataSource = td.GetSubjectsById(sessionCombo.SelectedValue.ToString(), _userId);
                    subIdCombo.SelectedIndex = -1;
                }
                catch (Exception) { }
            }
        }

        private void subIdCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (subIdCombo.SelectedIndex == -1)
            {
                marksGrid.DataSource = null;
            }
            else
            {
                try
                {
                    marksGrid.DataSource = htd.GetUpdableMarks(sessionCombo.SelectedValue.ToString(), termCombo.SelectedItem.ToString(), subIdCombo.SelectedValue.ToString());
                }
                catch (Exception) { }
            }
        }

        private void secCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void marksGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                stuId.Text = marksGrid.Rows[e.RowIndex].Cells[0].Value.ToString();
                marks.Text = marksGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
            }
            catch (Exception) { }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {          
            try
            {
                if (htd.UpdateMarks(sessionCombo.SelectedValue.ToString(), termCombo.SelectedItem.ToString(), subIdCombo.SelectedValue.ToString(), stuId.Text, int.Parse(marks.Text), CalculateGrade()))
                {
                    marksGrid.DataSource = htd.GetUpdableMarks(sessionCombo.SelectedValue.ToString(), termCombo.SelectedItem.ToString(), subIdCombo.SelectedValue.ToString());
                    MessageBox.Show($"MARKS UPDATED FOR {stuId.Text} TO MARKS={marks.Text} AND GRADE={CalculateGrade()}");
                    stuId.Text = marks.Text = string.Empty;
                }
            }
            catch (Exception) { }
        }

        private string CalculateGrade()
        {
            DataTable dt = htd.GetGradingMarks();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string[] r = dt.Rows[i]["marks"].ToString().Split('-');

                    if ((int.Parse(marks.Text) >= int.Parse(r[0]) && int.Parse(marks.Text) <= int.Parse(r[1])))
                    {
                        return dt.Rows[i]["grade"].ToString();
                    }
                }
            }

            return "N/A";
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            sessionCombo.SelectedIndex = termCombo.SelectedIndex = subIdCombo.SelectedIndex = -1;
            marksGrid.DataSource = null;
            stuId.Text = marks.Text = string.Empty;
        }
    }
}

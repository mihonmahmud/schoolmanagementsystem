﻿namespace SchoolManagement.User_Controls
{
    partial class StudentTotalMarksUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StudentTotalMarksUC));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.sessionCombo = new MetroFramework.Controls.MetroComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.termCombo = new MetroFramework.Controls.MetroComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.classCombo = new MetroFramework.Controls.MetroComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.secCombo = new MetroFramework.Controls.MetroComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.marksGrid = new MetroFramework.Controls.MetroGrid();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.totalgrid = new MetroFramework.Controls.MetroGrid();
            this.label14 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.marksGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalgrid)).BeginInit();
            this.SuspendLayout();
            // 
            // sessionCombo
            // 
            this.sessionCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.sessionCombo.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.sessionCombo.FormattingEnabled = true;
            this.sessionCombo.ItemHeight = 24;
            this.sessionCombo.Location = new System.Drawing.Point(216, 70);
            this.sessionCombo.Margin = new System.Windows.Forms.Padding(4);
            this.sessionCombo.Name = "sessionCombo";
            this.sessionCombo.Size = new System.Drawing.Size(212, 30);
            this.sessionCombo.TabIndex = 98;
            this.sessionCombo.UseSelectable = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(111, 68);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 32);
            this.label5.TabIndex = 97;
            this.label5.Text = "Session:";
            // 
            // termCombo
            // 
            this.termCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.termCombo.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.termCombo.FormattingEnabled = true;
            this.termCombo.ItemHeight = 24;
            this.termCombo.Items.AddRange(new object[] {
            "Mid",
            "Final"});
            this.termCombo.Location = new System.Drawing.Point(545, 71);
            this.termCombo.Margin = new System.Windows.Forms.Padding(4);
            this.termCombo.Name = "termCombo";
            this.termCombo.Size = new System.Drawing.Size(212, 30);
            this.termCombo.TabIndex = 109;
            this.termCombo.UseSelectable = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(467, 67);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 32);
            this.label9.TabIndex = 108;
            this.label9.Text = "Term:";
            // 
            // classCombo
            // 
            this.classCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.classCombo.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.classCombo.FormattingEnabled = true;
            this.classCombo.ItemHeight = 24;
            this.classCombo.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.classCombo.Location = new System.Drawing.Point(882, 71);
            this.classCombo.Margin = new System.Windows.Forms.Padding(4);
            this.classCombo.Name = "classCombo";
            this.classCombo.Size = new System.Drawing.Size(212, 30);
            this.classCombo.TabIndex = 111;
            this.classCombo.UseSelectable = true;
            this.classCombo.SelectedIndexChanged += new System.EventHandler(this.classCombo_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(803, 67);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 32);
            this.label1.TabIndex = 110;
            this.label1.Text = "Class:";
            // 
            // secCombo
            // 
            this.secCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.secCombo.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.secCombo.FormattingEnabled = true;
            this.secCombo.ItemHeight = 24;
            this.secCombo.Location = new System.Drawing.Point(1183, 71);
            this.secCombo.Margin = new System.Windows.Forms.Padding(4);
            this.secCombo.Name = "secCombo";
            this.secCombo.Size = new System.Drawing.Size(212, 30);
            this.secCombo.TabIndex = 113;
            this.secCombo.UseSelectable = true;
            this.secCombo.SelectedIndexChanged += new System.EventHandler(this.secCombo_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1120, 68);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 32);
            this.label2.TabIndex = 112;
            this.label2.Text = "Sec:";
            // 
            // marksGrid
            // 
            this.marksGrid.AllowUserToResizeRows = false;
            this.marksGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.marksGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.marksGrid.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.marksGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.marksGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.marksGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.marksGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.marksGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.marksGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.marksGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.marksGrid.EnableHeadersVisualStyles = false;
            this.marksGrid.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.marksGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.marksGrid.Location = new System.Drawing.Point(105, 140);
            this.marksGrid.Name = "marksGrid";
            this.marksGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.marksGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.marksGrid.RowHeadersVisible = false;
            this.marksGrid.RowHeadersWidth = 51;
            this.marksGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Padding = new System.Windows.Forms.Padding(6);
            this.marksGrid.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.marksGrid.RowTemplate.Height = 24;
            this.marksGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.marksGrid.Size = new System.Drawing.Size(816, 504);
            this.marksGrid.TabIndex = 114;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.White;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(1417, 71);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(40, 31);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 115;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // totalgrid
            // 
            this.totalgrid.AllowUserToResizeRows = false;
            this.totalgrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.totalgrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.totalgrid.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.totalgrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.totalgrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.totalgrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.totalgrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.totalgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.totalgrid.DefaultCellStyle = dataGridViewCellStyle6;
            this.totalgrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.totalgrid.EnableHeadersVisualStyles = false;
            this.totalgrid.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.totalgrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.totalgrid.Location = new System.Drawing.Point(959, 140);
            this.totalgrid.Name = "totalgrid";
            this.totalgrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.totalgrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.totalgrid.RowHeadersVisible = false;
            this.totalgrid.RowHeadersWidth = 51;
            this.totalgrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle8.Padding = new System.Windows.Forms.Padding(6);
            this.totalgrid.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.totalgrid.RowTemplate.Height = 24;
            this.totalgrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.totalgrid.Size = new System.Drawing.Size(499, 504);
            this.totalgrid.TabIndex = 116;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(587, 6);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(357, 38);
            this.label14.TabIndex = 122;
            this.label14.Text = "STUDENTS TOTAL MARKS";
            // 
            // StudentTotalMarksUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.label14);
            this.Controls.Add(this.totalgrid);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.marksGrid);
            this.Controls.Add(this.secCombo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.classCombo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.termCombo);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.sessionCombo);
            this.Controls.Add(this.label5);
            this.Name = "StudentTotalMarksUC";
            this.Size = new System.Drawing.Size(1587, 671);
            this.Load += new System.EventHandler(this.StudentTotalMarksUC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.marksGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalgrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroComboBox sessionCombo;
        private System.Windows.Forms.Label label5;
        private MetroFramework.Controls.MetroComboBox termCombo;
        private System.Windows.Forms.Label label9;
        private MetroFramework.Controls.MetroComboBox classCombo;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroComboBox secCombo;
        private System.Windows.Forms.Label label2;
        private MetroFramework.Controls.MetroGrid marksGrid;
        private System.Windows.Forms.PictureBox pictureBox4;
        private MetroFramework.Controls.MetroGrid totalgrid;
        private System.Windows.Forms.Label label14;
    }
}

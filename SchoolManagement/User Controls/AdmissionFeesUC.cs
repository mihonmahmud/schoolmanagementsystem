﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class AdmissionFeesUC : UserControl
    {
        private AccountantData ad = new AccountantData();

        public AdmissionFeesUC()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (ad.AddAdmissionFees(formNo.Text, appName.Text, classCombo.SelectedItem.ToString(), int.Parse(amount.Text)))
            {
                MessageBox.Show("ADDED!");
                formNo.Text = appName.Text = amount.Text = string.Empty;
                classCombo.SelectedIndex = -1;
            }
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            formNo.Text = appName.Text = amount.Text = string.Empty;
            classCombo.SelectedIndex = -1;
        }

        private void appName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void amount_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }
    }
}

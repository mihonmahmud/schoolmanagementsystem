﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement
{
    public partial class AddStudentUC : UserControl
    {
        private AccountantData ac = new AccountantData();

        public AddStudentUC()
        {
            InitializeComponent();
        }

        private void AddStudentUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ac.GetSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void classCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if((classCombo.SelectedIndex == -1) && (sessionCombo.SelectedIndex == -1))
            {
                secCombo.SelectedIndex = -1;
            }
            else
            {               
                try
                {
                    secCombo.ValueMember = "section";
                    secCombo.DisplayMember = "section";
                    secCombo.DataSource = ac.GetSecBySessionClass(sessionCombo.SelectedValue.ToString(), classCombo.SelectedItem.ToString());
                    secCombo.SelectedIndex = -1;
                }
                catch (Exception) { }
            }
        }

        private void secCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(secCombo.SelectedIndex == -1)
            {
                capLabel.Text = "";
                stuId.Text = "";
            }
            else
            {
                try
                {
                    capLabel.Text = ac.CapacityBySessionClassSec(sessionCombo.SelectedValue.ToString(), classCombo.SelectedItem.ToString(), secCombo.SelectedValue.ToString()).ToString();
                    stuId.Text = DateTime.Now.Year.ToString() + "-" + (ac.GetLastStuId() + 1);
                }
                catch (Exception) { }
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {           
            try
            {
                if(ac.CapacityBySessionClassSec(sessionCombo.SelectedValue.ToString(), classCombo.SelectedItem.ToString(), secCombo.SelectedValue.ToString()) > 0)
                {
                    if (sessionCombo.SelectedIndex != -1 && classCombo.SelectedIndex != -1 && secCombo.SelectedIndex != -1 && !string.IsNullOrWhiteSpace(position.Text) && !string.IsNullOrWhiteSpace(name.Text) && !string.IsNullOrWhiteSpace(fName.Text) && !string.IsNullOrWhiteSpace(mName.Text) && !string.IsNullOrWhiteSpace(religion.Text) && genderCombo.SelectedIndex != -1 && bgCombo.SelectedIndex != -1 && !string.IsNullOrWhiteSpace(present.Text) && !string.IsNullOrWhiteSpace(permanent.Text) && !string.IsNullOrWhiteSpace(nation.Text) && !string.IsNullOrWhiteSpace(gName.Text) && !string.IsNullOrWhiteSpace(gProfession.Text) && !string.IsNullOrWhiteSpace(gPhone.Text) && !string.IsNullOrWhiteSpace(gMail.Text) && !string.IsNullOrWhiteSpace(position.Text))
                    {
                        if(ac.IsPositionExist(sessionCombo.SelectedValue.ToString(), classCombo.SelectedItem.ToString(), secCombo.SelectedValue.ToString(), int.Parse(position.Text)))
                        {
                            if (ac.AddStudent(sessionCombo.SelectedValue.ToString(), classCombo.SelectedItem.ToString(), secCombo.SelectedValue.ToString(), stuId.Text.Trim(), name.Text.Trim(), fName.Text.Trim(), mName.Text.Trim(), dob.Value.ToShortDateString(), religion.Text.Trim(), genderCombo.SelectedItem.ToString(), bgCombo.SelectedItem.ToString(), present.Text.Trim(), permanent.Text.Trim(), nation.Text.Trim(), gName.Text.Trim(), gProfession.Text.Trim(), gPhone.Text.Trim(), gMail.Text.Trim(), int.Parse(position.Text)))
                            {
                                MessageBox.Show("STUDENT ADDED!");
                                Clear();
                            }
                        }
                        else
                        {
                            MessageBox.Show("POSITION ALREADY EXIST!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("PLEASE FILL ALL FIELDS CORRECTLY!");
                    }
                }
                else
                {
                    MessageBox.Show("SECTION IS FULL!");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("SOMETHING WENT WRONG! PLEASE RECHECK AND TRY AGAIN!");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            sessionCombo.SelectedIndex = classCombo.SelectedIndex = secCombo.SelectedIndex = genderCombo.SelectedIndex = bgCombo.SelectedIndex = -1;
            capLabel.Text = stuId.Text = name.Text = fName.Text = mName.Text = dob.Text = religion.Text = present.Text = permanent.Text = nation.Text = gName.Text = gProfession.Text = gPhone.Text = gMail.Text = "";
        }

        private void name_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        private void fName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        private void mName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        private void religion_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void nation_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void gName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        private void gProfession_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        private void gPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void position_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class TeacherDetailsUC : UserControl
    {
        private HeadTeacherData htd = new HeadTeacherData();

        public TeacherDetailsUC()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
                infoGrid.DataSource = htd.GetEmployeeContactInfo(empId.Text);
            }
            catch (Exception) { }
        }
    }
}

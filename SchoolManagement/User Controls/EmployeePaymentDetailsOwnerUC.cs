﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class EmployeePaymentDetailsOwnerUC : UserControl
    {
        private OwnerData od = new OwnerData();

        public EmployeePaymentDetailsOwnerUC()
        {
            InitializeComponent();
        }

        private void EmployeePaymentDetailsOwnerUC_Load(object sender, EventArgs e)
        {
            try
            {
                yearCombo.ValueMember = "year";
                yearCombo.DisplayMember = "year";
                yearCombo.DataSource = od.GetPaymentYear();
                yearCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private void yearCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (yearCombo.SelectedIndex == -1)
                monthCombo.DataSource = null;
            else
            {
                try
                {
                    monthCombo.ValueMember = "month";
                    monthCombo.DisplayMember = "month";
                    monthCombo.DataSource = od.GetPaymentMonth(yearCombo.SelectedValue.ToString());
                    monthCombo.SelectedIndex = -1;
                }
                catch (Exception) { }
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if(yearCombo.SelectedIndex != -1 && monthCombo.SelectedIndex != -1)
            {             
                try
                {
                    detailsGrid.DataSource = od.GetPaymentDetailsByMonthYear(yearCombo.SelectedValue.ToString(), monthCombo.SelectedValue.ToString());
                }
                catch (Exception) { }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;
using System.Text.RegularExpressions;
using SchoolManagement.Helper;

namespace SchoolManagement.User_Controls
{
    public partial class AddAdminUC : UserControl
    {
        private AdminData ad = new AdminData();
        private OwnerData od = new OwnerData();
        private int _min = 1000;
        private int _max = 9999;
        private Random _rdm = new Random();
        private int status;
        private Regex mRegxExpression = new Regex(@"^([a-zA-Z0-9_\-])([a-zA-Z0-9_\-\.]*)@(\[((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\.){3}|((([a-zA-Z0-9\-]+)\.)+))([a-zA-Z]{2,}|(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\])$");

        public AddAdminUC()
        {
            InitializeComponent();
        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void AddAdminUC_Load(object sender, EventArgs e)
        {
            CreateAdmin();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {           
            try
            {
                if (!string.IsNullOrWhiteSpace(employeeName.Text) && !string.IsNullOrWhiteSpace(email.Text) && !string.IsNullOrWhiteSpace(phone.Text) && !string.IsNullOrWhiteSpace(employeeFName.Text) && !string.IsNullOrWhiteSpace(employeeMName.Text) && !string.IsNullOrWhiteSpace(employeeReligion.Text) && !string.IsNullOrWhiteSpace(employeeDegree.Text) && employeeGenderCombo.SelectedIndex != -1 && employeeBGCombo.SelectedIndex != -1 && !string.IsNullOrWhiteSpace(employeePresent.Text) && !string.IsNullOrWhiteSpace(employeePermanent.Text) && !string.IsNullOrWhiteSpace(employeeNation.Text) && !string.IsNullOrWhiteSpace(basic.Text) && !string.IsNullOrWhiteSpace(home.Text) && !string.IsNullOrWhiteSpace(medical.Text) && !string.IsNullOrWhiteSpace(tiffin.Text))
                {
                    if (!mRegxExpression.IsMatch(email.Text.Trim()))
                    {
                        MessageBox.Show("E-mail address format is not correct.", "Invalid Email", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        email.Focus();
                    }
                    else
                    {
                        if ((ad.IsEmployeeIdExist(employeeIdText.Text) == true) || (ad.IsLoginIdExist(employeeIdText.Text) == true) || (ad.IsStartingSalaryIdExist(employeeIdText.Text) == true) || (ad.IsEmployeeEmailExist(email.Text) == true))
                        {
                            MessageBox.Show("ID OR EMAIL ALREADY EXISTS!");
                        }
                        else
                        {
                            if (ad.InsertEmployee(employeeIdText.Text.Trim(), employeePassText.Text.Trim(), employeeName.Text.Trim(), email.Text.Trim(), phone.Text.Trim(), employeeFName.Text.Trim(), employeeMName.Text.Trim(), employeeDOB.Value.ToShortDateString(), employeeReligion.Text.Trim(), employeeGenderCombo.SelectedItem.ToString(), employeeBGCombo.SelectedItem.ToString(), employeeDegree.Text.Trim(), employeePresent.Text.Trim(), employeePermanent.Text.Trim(), "Admin", employeeNation.Text.Trim(), status, jdate.Value.ToShortDateString(), float.Parse(basic.Text), float.Parse(home.Text), float.Parse(medical.Text), float.Parse(tiffin.Text)))
                            {
                                if (Email.SendConfirmMail(employeeIdText.Text.Trim(), employeePassText.Text.Trim(), email.Text.Trim()))
                                {
                                    MessageBox.Show("ADMIN ADDED! AND LOGIN INFORMATION SENT TO USER MAIL!");
                                    clear();
                                    CreateAdmin();
                                }
                                else
                                {
                                    MessageBox.Show("ADMIN ADDED!");
                                    clear();
                                    CreateAdmin();
                                }
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("PLEASE FILL ALL FIELDS CORRECTLY!");
                }
            }
            catch (Exception) { }
        }

        private void employeeName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        private void phone_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void employeeFName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        private void employeeMName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        private void employeeReligion_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void employeeNation_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void employeeName_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            clear();
            CreateAdmin();
        }

        void clear()
        {
            employeeGenderCombo.SelectedIndex = employeeBGCombo.SelectedIndex = -1;
            phone.Text = employeeIdText.Text = employeePassText.Text = employeeName.Text = email.Text = employeeFName.Text = employeeMName.Text = employeeDOB.Text = employeeReligion.Text = employeeDegree.Text = employeePresent.Text = employeePermanent.Text = employeeNation.Text = basic.Text = home.Text = medical.Text = tiffin.Text = string.Empty;
        }

        void CreateAdmin()
        {
            employeeIdText.Text = "1112-" + (ad.GetLastId() + 1);
            employeePassText.Text = _rdm.Next(_min, _max).ToString();
            status = 2;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class UpdateStudentAttendanceUC : UserControl
    {
        private TeacherData td = new TeacherData();
        private string _userId;

        public UpdateStudentAttendanceUC(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void UpdateStudentAttendanceUC_Load(object sender, EventArgs e)
        {
            session.Text = DateTime.Now.Year.ToString();
            try
            {
                subIdCombo.ValueMember = "subId";
                subIdCombo.DisplayMember = "subId";
                subIdCombo.DataSource = td.GetSubjectsById(session.Text, _userId);
                subIdCombo.SelectedIndex = -1;
                attendanceGrid.DataSource = null;
            }
            catch (Exception) { }
        }

        private void sessionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void subIdCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (subIdCombo.SelectedIndex == -1)
            {
                subClass.Text = "";
                secCombo.SelectedIndex = -1;
                attendanceGrid.DataSource = null;
            }
            else
            {
                try
                {
                    subClass.Text = td.GetClassById(session.Text, subIdCombo.SelectedValue.ToString());

                    if (subClass.Text != null)
                    {
                        try
                        {
                            secCombo.ValueMember = "sec";
                            secCombo.DisplayMember = "sec";
                            secCombo.DataSource = td.GetSecsByCla(session.Text, subIdCombo.SelectedValue.ToString(), subClass.Text, _userId);
                            secCombo.SelectedIndex = -1;
                            attendanceGrid.DataSource = null;
                        }
                        catch (Exception) { }
                    }
                }
                catch (Exception) { }
            }
        }

        private void secCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void monthCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (monthCombo.SelectedIndex == -1)
            {
                dayCombo.DataSource = null;
            }
            else
            {
                try
                {
                    dayCombo.ValueMember = "day";
                    dayCombo.DisplayMember = "day";
                    dayCombo.DataSource = td.GetDayBySession(session.Text, subIdCombo.SelectedValue.ToString(), subClass.Text, secCombo.SelectedValue.ToString(), _userId, monthCombo.SelectedItem.ToString());
                    dayCombo.SelectedIndex = -1;
                }
                catch (Exception) { }
            }
        }

        private void attendanceGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                stuId.Text = attendanceGrid.Rows[e.RowIndex].Cells[0].Value.ToString();
                attendance.Text = attendanceGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
            }
            catch (Exception) { }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                if(td.UpdateStudentRecord(session.Text, subIdCombo.SelectedValue.ToString(), subClass.Text, secCombo.SelectedValue.ToString(), _userId, dayCombo.SelectedValue.ToString(), monthCombo.SelectedItem.ToString(), stuId.Text, attendance.Text))
                {
                    attendanceGrid.DataSource = td.GetStudentsAttendanceRecord(session.Text, subIdCombo.SelectedValue.ToString(), subClass.Text, secCombo.SelectedValue.ToString(), _userId, dayCombo.SelectedValue.ToString(), monthCombo.SelectedItem.ToString());
                    MessageBox.Show("UPDATED!");
                    stuId.Text = attendance.Text = "";                   
                }
            }
            catch (Exception) { }
        }

        private void attendance_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void dayCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dayCombo.SelectedIndex == -1)
            {
                attendanceGrid.DataSource = null;
            }
            else
            {
                try
                {
                    attendanceGrid.DataSource = td.GetStudentsAttendanceRecord(session.Text, subIdCombo.SelectedValue.ToString(), subClass.Text, secCombo.SelectedValue.ToString(), _userId, dayCombo.SelectedValue.ToString(), monthCombo.SelectedItem.ToString());
                }
                catch (Exception) { }
            }
        }
    }
}

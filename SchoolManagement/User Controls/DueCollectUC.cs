﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class DueCollectUC : UserControl
    {
        private AccountantData ad = new AccountantData();

        public DueCollectUC()
        {
            InitializeComponent();
        }

        private void DueCollectUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ad.GetSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {           
            try
            {
                dueGrid.DataSource = ad.GetDueInfo(sessionCombo.SelectedValue.ToString(), monthCombo.SelectedItem.ToString(), stuId.Text);
            }
            catch (Exception) { }
        }

        private void dueGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                due.Text = dueGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
                fine.Text = dueGrid.Rows[e.RowIndex].Cells[3].Value.ToString();
                total.Text = dueGrid.Rows[e.RowIndex].Cells[4].Value.ToString();
            }
            catch (Exception) { }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                total.Text = (float.Parse(due.Text) + float.Parse(fine.Text) + float.Parse(total.Text)).ToString();
            }
            catch (Exception) { }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {          
            try
            {
                if (ad.UpdateDue(sessionCombo.SelectedValue.ToString(), monthCombo.SelectedItem.ToString(), stuId.Text, float.Parse(due.Text), float.Parse(fine.Text), float.Parse(total.Text)))
                {
                    MessageBox.Show("UPDATED!");
                    dueGrid.DataSource = ad.GetDueInfo(sessionCombo.SelectedValue.ToString(), monthCombo.SelectedItem.ToString(), stuId.Text);
                    due.Text = fine.Text = total.Text = "";
                }
            }
            catch (Exception) { }
        }

        private void due_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void fine_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void total_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }
    }
}

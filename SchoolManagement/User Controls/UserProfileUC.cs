﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class UserProfileUC : UserControl
    {
        private LoginData lid = new LoginData();
        private string _userId;
        private AdminData ad = new AdminData();

        public UserProfileUC(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(password.Text == confirm.Text)
            {
                if(lid.UpdatePassword(_userId, confirm.Text))
                {
                    MessageBox.Show("UPDATED!");
                    password.Text = confirm.Text = "";
                }
                else
                {
                    MessageBox.Show("PASSWORD DON'T MATCH!");
                }
            }
        }

        private void UserProfileUC_Load(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = ad.GetEmployeeDetails(_userId);

                id.Text = dt.Rows[0][1].ToString();
                employeeName.Text = dt.Rows[0][2].ToString();
                email.Text = dt.Rows[0][3].ToString();
                phone.Text = dt.Rows[0][4].ToString();
                employeeFName.Text = dt.Rows[0][5].ToString();
                employeeMName.Text = dt.Rows[0][6].ToString();
                dob.Text = dt.Rows[0][7].ToString();
                employeeReligion.Text = dt.Rows[0][8].ToString();
                gender.Text = dt.Rows[0][9].ToString();
                bg.Text = dt.Rows[0][10].ToString();
                employeeDegree.Text = dt.Rows[0][11].ToString();
                employeePresent.Text = dt.Rows[0][12].ToString();
                employeePermanent.Text = dt.Rows[0][13].ToString();
                employeeNation.Text = dt.Rows[0][15].ToString();
            }
            catch (Exception) { }
        }
    }
}

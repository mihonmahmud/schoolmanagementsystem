﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using SchoolManagement.Data_Layer;

namespace SchoolManagement
{
    public partial class SendMailUC : UserControl
    {
        private string _userId;
        private string _to;
        private LoginData lid = new LoginData();

        public SendMailUC(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        public SendMailUC(string userId, string to)
        {
            InitializeComponent();
            _userId = userId;
            _to = to;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(mTo.Text) || string.IsNullOrWhiteSpace(mSubject.Text) || string.IsNullOrWhiteSpace(mDescription.Text))
            {
                MessageBox.Show("PLEASE FILL ALL REQUIRED FIELDS");
            }
            else
            {
                if (lid.InsertMail(_userId, mTo.Text, mSubject.Text, mDescription.Text))
                {
                    MessageBox.Show("SENT!");
                    mTo.Text = mSubject.Text = mDescription.Text = "";
                }
            }   
        }

        private void SendMailUC_Load(object sender, EventArgs e)
        {
            try
            {
                employeeGrid.DataSource = lid.EmployeeList(_userId);
            }
            catch (Exception) { }

            if (_to != null)
            {
                mTo.Text = _to.ToString();
            }
        }

        private void employeeGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            mTo.Text = employeeGrid.Rows[e.RowIndex].Cells[0].Value.ToString();
        }
    }
}

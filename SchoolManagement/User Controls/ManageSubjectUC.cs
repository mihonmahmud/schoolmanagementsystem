﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class ManageSubjectUC : UserControl
    {
        private AdminData ad = new AdminData();

        public ManageSubjectUC()
        {
            InitializeComponent();
        }

        private void ManageSubjectUC_Load(object sender, EventArgs e)
        {
            allSubjectsGrid.DataSource = ad.AllSubjects();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            allSubjectsGrid.DataSource = ad.SubjectById(subId.Text);
        }

        private void allSubjectsGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            idLabel.Text = allSubjectsGrid.Rows[e.RowIndex].Cells[0].Value.ToString();
            subId.Text = allSubjectsGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
            subName.Text = allSubjectsGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
            subClass.Text = allSubjectsGrid.Rows[e.RowIndex].Cells[3].Value.ToString();
            subTime.Text = allSubjectsGrid.Rows[e.RowIndex].Cells[5].Value.ToString();
            subDay.Text = allSubjectsGrid.Rows[e.RowIndex].Cells[4].Value.ToString();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(subId.Text) && !string.IsNullOrWhiteSpace(subName.Text) && !string.IsNullOrWhiteSpace(subClass.Text) && !string.IsNullOrWhiteSpace(subTime.Text) && !string.IsNullOrWhiteSpace(subDay.Text))
            {
                try
                {
                    if (ad.UpdateSubject(int.Parse(idLabel.Text), subId.Text, subName.Text, subClass.Text, subTime.Text, subDay.Text))
                    {
                        MessageBox.Show("Updated!");
                        ClearFields();
                        allSubjectsGrid.DataSource = ad.AllSubjects();

                    }
                    else
                    {
                        MessageBox.Show("Something went wrong!");
                    }
                }
                catch (Exception)
                {

                }
            }
            else
            {
                MessageBox.Show("FILL ALL REQUIRED FIELDS!");
            }
        }

        public void ClearFields()
        {
            idLabel.Text = subId.Text = subName.Text = subClass.Text = subTime.Text = subDay.Text = string.Empty;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            ClearFields();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            try
            {
                if (ad.DeleteSubject(int.Parse(idLabel.Text)))
                {                   
                    ClearFields();
                    allSubjectsGrid.DataSource = ad.AllSubjects();
                    MessageBox.Show("DELETED!");
                }
                else
                {
                    MessageBox.Show("Something went wrong!");
                }
            }
            catch (Exception)
            {

            }
        }

        private void subName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void pictureBox4_Click_1(object sender, EventArgs e)
        {
            allSubjectsGrid.DataSource = ad.AllSubjects();
            ClearFields();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using SchoolManagement.Helper;

namespace SchoolManagement.User_Controls
{
    public partial class AddEmployeeUC : UserControl
    {
        private int _min = 1000;
        private int _max = 9999;
        private Random _rdm = new Random();
        private int status;
        private AdminData ad = new AdminData();
        private string employee;
        private Regex mRegxExpression = new Regex(@"^([a-zA-Z0-9_\-])([a-zA-Z0-9_\-\.]*)@(\[((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\.){3}|((([a-zA-Z0-9\-]+)\.)+))([a-zA-Z]{2,}|(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\])$");

        public AddEmployeeUC()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(employeeName.Text) && !string.IsNullOrWhiteSpace(email.Text) && !string.IsNullOrWhiteSpace(phone.Text) && !string.IsNullOrWhiteSpace(employeeFName.Text) && !string.IsNullOrWhiteSpace(employeeMName.Text) && !string.IsNullOrWhiteSpace(employeeReligion.Text) && !string.IsNullOrWhiteSpace(employeeDegree.Text) && employeeGenderCombo.SelectedIndex != -1 && employeeBGCombo.SelectedIndex != -1 && !string.IsNullOrWhiteSpace(employeePresent.Text) && !string.IsNullOrWhiteSpace(employeePermanent.Text) && !string.IsNullOrWhiteSpace(employeeNation.Text) && !string.IsNullOrWhiteSpace(basic.Text) && !string.IsNullOrWhiteSpace(home.Text) && !string.IsNullOrWhiteSpace(medical.Text) && !string.IsNullOrWhiteSpace(tiffin.Text))
                {
                    if (!mRegxExpression.IsMatch(email.Text.Trim()))
                    {
                        MessageBox.Show("E-mail address format is not correct.", "Invalid Email", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        email.Focus();
                    }
                    else
                    {
                        if ((ad.IsEmployeeIdExist(employeeIdText.Text) == true) || (ad.IsLoginIdExist(employeeIdText.Text) == true) || (ad.IsStartingSalaryIdExist(employeeIdText.Text) == true) || (ad.IsEmployeeEmailExist(email.Text) == true))
                        {
                            MessageBox.Show("ID OR EMAIL ALREADY EXISTS!");
                        }
                        else
                        {
                            if (ad.InsertEmployee(employeeIdText.Text.Trim(), employeePassText.Text.Trim(), employeeName.Text.Trim(), email.Text.Trim(), phone.Text.Trim(), employeeFName.Text.Trim(), employeeMName.Text.Trim(), employeeDOB.Value.ToShortDateString(), employeeReligion.Text.Trim(), employeeGenderCombo.SelectedItem.ToString(), employeeBGCombo.SelectedItem.ToString(), employeeDegree.Text.Trim(), employeePresent.Text.Trim(), employeePermanent.Text.Trim(), employee, employeeNation.Text.Trim(), status, jdate.Value.ToShortDateString(), float.Parse(basic.Text), float.Parse(home.Text), float.Parse(medical.Text), float.Parse(tiffin.Text)))
                            {
                                if (Email.SendConfirmMail(employeeIdText.Text.Trim(), employeePassText.Text.Trim(), email.Text.Trim()))
                                {
                                    MessageBox.Show("EMPLOYEE ADDED! AND LOGIN INFORMATION SENT TO USER MAIL!");
                                    clear();
                                }
                                else
                                {
                                    MessageBox.Show("EMPLOYEE ADDED!");
                                    clear();
                                }
                            }
                        }                 
                    }                  
                }                   
                else
                {
                    MessageBox.Show("PLEASE FILL ALL FIELDS CORRECTLY!");
                }
            }
            catch (Exception) { }
        }

        private void employeeCategoryCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            employee = (string)employeeCategoryCombo.SelectedItem;

            if(employee == "Head Teacher")
            {
                employeeIdText.Text = "1113-" + (ad.GetLastId() + 1);
                employeePassText.Text = _rdm.Next(_min, _max).ToString();
                status = 3;
            }
            else if (employee == "Teacher")
            {
                employeeIdText.Text = "1114-" + (ad.GetLastId() + 1);
                employeePassText.Text = _rdm.Next(_min, _max).ToString();
                status = 4;
            }
            else if (employee == "Accountant")
            {
                employeeIdText.Text = "1115-" + (ad.GetLastId() + 1);
                employeePassText.Text = _rdm.Next(_min, _max).ToString();
                status = 5;
            }
            else if (employee == "Librarian")
            {
                employeeIdText.Text = "1116-" + (ad.GetLastId() + 1);
                employeePassText.Text = _rdm.Next(_min, _max).ToString();
                status = 6;
            }
            else if (employee == "Attendance Taker")
            {
                employeeIdText.Text = "1117-" + (ad.GetLastId() + 1);
                employeePassText.Text = _rdm.Next(_min, _max).ToString();
                status = 7;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void employeeName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        private void phone_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void employeeFName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        private void employeeMName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        private void employeeReligion_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void employeeNation_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        void clear()
        {
            employeeCategoryCombo.SelectedIndex = employeeGenderCombo.SelectedIndex = employeeBGCombo.SelectedIndex = -1;
            phone.Text = employeeIdText.Text = employeePassText.Text = employeeName.Text = email.Text = employeeFName.Text = employeeMName.Text = employeeDOB.Text = employeeReligion.Text = employeeDegree.Text = employeePresent.Text = employeePermanent.Text = employeeNation.Text = basic.Text = home.Text = medical.Text = tiffin.Text = string.Empty;
        }
    }
}

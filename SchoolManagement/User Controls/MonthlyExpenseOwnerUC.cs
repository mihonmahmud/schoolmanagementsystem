﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class MonthlyExpenseOwnerUC : UserControl
    {
        private OwnerData od = new OwnerData();
        private LoginData lid = new LoginData();

        public MonthlyExpenseOwnerUC()
        {
            InitializeComponent();
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void sortCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(sortCombo.SelectedIndex == -1)
            {
                yearCombo.DataSource = null;
            }
            else if(sortCombo.SelectedIndex != -1 && yearCombo.SelectedIndex == -1)
            {
                expenseChart.DataSource = null;
            }
            else
            {              
                
            }
        }

        private void yearCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((sortCombo.SelectedIndex == -1) && (yearCombo.SelectedIndex == -1))
            {
                expenseChart.DataSource = null;
            }
            else if ((sortCombo.SelectedIndex != -1) && (yearCombo.SelectedIndex == -1))
            {
                expenseChart.DataSource = null;
            }
            else if ((sortCombo.SelectedIndex == -1) && (yearCombo.SelectedIndex != -1))
            {
                expenseChart.DataSource = null;
            }
            else if ((sortCombo.SelectedItem.ToString() == "Employee Salary") && (yearCombo.SelectedIndex != -1))
            {
                try
                {
                    expenseChart.DataSource = null;
                    DataTable dt = od.EmployeeExpenseByMonth(yearCombo.SelectedValue.ToString());

                    expenseChart.DataSource = dt;
                    expenseChart.Series["Series1"].XValueMember = "month";
                    expenseChart.Series["Series1"].YValueMembers = "Total";
                    expenseChart.Titles.Add("");
                    expenseChart.Series["Series1"].IsValueShownAsLabel = true;
                }
                catch (Exception) { }
            }
            else if ((sortCombo.SelectedItem.ToString() == "Inventory") && (yearCombo.SelectedIndex != -1))
            {
                try
                {
                    expenseChart.DataSource = null;
                    DataTable dt = od.InventoryExpenseByMonth(yearCombo.SelectedValue.ToString());

                    expenseChart.DataSource = dt;
                    expenseChart.Series["Series1"].XValueMember = "month";
                    expenseChart.Series["Series1"].YValueMembers = "Total";
                    expenseChart.Titles.Add("");
                    expenseChart.Series["Series1"].IsValueShownAsLabel = true;
                }
                catch (Exception) { }
            }
            else { }
        }

        private void MonthlyExpenseOwnerUC_Load(object sender, EventArgs e)
        {
            try
            {
                yearCombo.ValueMember = "year";
                yearCombo.DisplayMember = "year";
                yearCombo.DataSource = RemoveDuplicatesRecords(lid.GetYear());
                yearCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            
        }
    }
}

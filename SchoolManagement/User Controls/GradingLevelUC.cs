﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class GradingLevelUC : UserControl
    {
        private HeadTeacherData htd = new HeadTeacherData();
        private DataTable dt;

        public GradingLevelUC()
        {
            InitializeComponent();
        }

        private void GradingLevelUC_Load(object sender, EventArgs e)
        {
            try
            {
                levelGrid.DataSource = htd.GetGrading();
            }
            catch (Exception) { }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(marks.Text) && !string.IsNullOrWhiteSpace(grade.Text))
            {
                if(!marks.Text.Contains("-"))
                {
                    MessageBox.Show("FORMAT ISN'T CORRECT!");
                }
                else
                {
                    string[] m = marks.Text.Split('-');
                    if(int.Parse(m[0]) >= int.Parse(m[1]))
                    {
                        MessageBox.Show("FORMAT ISN'T CORRECT!");
                    }
                    else
                    {
                        dt = htd.GetGradingMarks();
                        if(dt.Rows.Count > 0)
                        {
                            for(int i = 0; i < dt.Rows.Count; i++)
                            {
                                string[] r = dt.Rows[i]["marks"].ToString().Split('-');

                                if((int.Parse(m[0]) >= int.Parse(r[0]) && int.Parse(m[0]) <= int.Parse(r[1])) || (int.Parse(m[1]) >= int.Parse(r[0]) && int.Parse(m[1]) <= int.Parse(r[1])))
                                {
                                    MessageBox.Show("GIVEN MARKS ALREADY EXISTS GOR ANOTHER GRADING LEVEL");
                                }
                                else
                                {
                                    if (htd.IsLevelExist(grade.Text))
                                    {
                                        MessageBox.Show("GRADE LEVEL ALREADY EXISTS!");
                                    }
                                    else
                                    {
                                        try
                                        {
                                            if (htd.InsertMarksGrade(marks.Text, grade.Text))
                                            {
                                                MessageBox.Show("GRADE LEVEL ADDED!");
                                                levelGrid.DataSource = htd.GetGrading();
                                            }
                                        }
                                        catch (Exception) { }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (htd.IsLevelExist(grade.Text))
                            {
                                MessageBox.Show("GRADE LEVEL ALREADY EXISTS!");
                            }
                            else
                            {
                                try
                                {
                                    if (htd.InsertMarksGrade(marks.Text, grade.Text))
                                    {
                                        MessageBox.Show("GRADE LEVEL ADDED!");
                                        levelGrid.DataSource = htd.GetGrading();
                                    }
                                }
                                catch (Exception) { }
                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("FILL ALL REQUIRED FIELDS!");
            }
        }
    }
}

﻿namespace SchoolManagement.User_Controls
{
    partial class UploadStudentMarks
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UploadStudentMarks));
            this.label14 = new System.Windows.Forms.Label();
            this.session = new MetroFramework.Controls.MetroTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.termCombo = new MetroFramework.Controls.MetroComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.subIdCombo = new MetroFramework.Controls.MetroComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.subClass = new MetroFramework.Controls.MetroTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.secCombo = new MetroFramework.Controls.MetroComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.marksGrid = new MetroFramework.Controls.MetroGrid();
            this.completeButton = new System.Windows.Forms.Button();
            this.uploadButton = new System.Windows.Forms.Button();
            this.editButton = new System.Windows.Forms.Button();
            this.refreshButton = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.marksGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.refreshButton)).BeginInit();
            this.SuspendLayout();
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(656, 6);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(236, 38);
            this.label14.TabIndex = 123;
            this.label14.Text = "UPLOAD MARKS";
            // 
            // session
            // 
            // 
            // 
            // 
            this.session.CustomButton.Image = null;
            this.session.CustomButton.Location = new System.Drawing.Point(129, 1);
            this.session.CustomButton.Name = "";
            this.session.CustomButton.Size = new System.Drawing.Size(31, 31);
            this.session.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.session.CustomButton.TabIndex = 1;
            this.session.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.session.CustomButton.UseSelectable = true;
            this.session.CustomButton.Visible = false;
            this.session.Lines = new string[0];
            this.session.Location = new System.Drawing.Point(275, 69);
            this.session.MaxLength = 32767;
            this.session.Multiline = true;
            this.session.Name = "session";
            this.session.PasswordChar = '\0';
            this.session.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.session.SelectedText = "";
            this.session.SelectionLength = 0;
            this.session.SelectionStart = 0;
            this.session.ShortcutsEnabled = true;
            this.session.Size = new System.Drawing.Size(161, 33);
            this.session.TabIndex = 125;
            this.session.UseSelectable = true;
            this.session.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.session.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(170, 67);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 32);
            this.label7.TabIndex = 124;
            this.label7.Text = "Session:";
            // 
            // termCombo
            // 
            this.termCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.termCombo.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.termCombo.FormattingEnabled = true;
            this.termCombo.ItemHeight = 24;
            this.termCombo.Items.AddRange(new object[] {
            "Mid",
            "Final"});
            this.termCombo.Location = new System.Drawing.Point(529, 73);
            this.termCombo.Margin = new System.Windows.Forms.Padding(4);
            this.termCombo.Name = "termCombo";
            this.termCombo.Size = new System.Drawing.Size(158, 30);
            this.termCombo.TabIndex = 127;
            this.termCombo.UseSelectable = true;
            this.termCombo.SelectedIndexChanged += new System.EventHandler(this.termCombo_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(450, 69);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 32);
            this.label9.TabIndex = 126;
            this.label9.Text = "Term:";
            // 
            // subIdCombo
            // 
            this.subIdCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.subIdCombo.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.subIdCombo.FormattingEnabled = true;
            this.subIdCombo.ItemHeight = 24;
            this.subIdCombo.Location = new System.Drawing.Point(845, 74);
            this.subIdCombo.Margin = new System.Windows.Forms.Padding(4);
            this.subIdCombo.Name = "subIdCombo";
            this.subIdCombo.Size = new System.Drawing.Size(161, 30);
            this.subIdCombo.TabIndex = 129;
            this.subIdCombo.UseSelectable = true;
            this.subIdCombo.SelectedIndexChanged += new System.EventHandler(this.subIdCombo_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(710, 70);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 32);
            this.label1.TabIndex = 128;
            this.label1.Text = "Subject ID:";
            // 
            // subClass
            // 
            // 
            // 
            // 
            this.subClass.CustomButton.Image = null;
            this.subClass.CustomButton.Location = new System.Drawing.Point(131, 2);
            this.subClass.CustomButton.Name = "";
            this.subClass.CustomButton.Size = new System.Drawing.Size(23, 23);
            this.subClass.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.subClass.CustomButton.TabIndex = 1;
            this.subClass.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.subClass.CustomButton.UseSelectable = true;
            this.subClass.CustomButton.Visible = false;
            this.subClass.Enabled = false;
            this.subClass.Lines = new string[0];
            this.subClass.Location = new System.Drawing.Point(1114, 75);
            this.subClass.MaxLength = 32767;
            this.subClass.Multiline = true;
            this.subClass.Name = "subClass";
            this.subClass.PasswordChar = '\0';
            this.subClass.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.subClass.SelectedText = "";
            this.subClass.SelectionLength = 0;
            this.subClass.SelectionStart = 0;
            this.subClass.ShortcutsEnabled = true;
            this.subClass.Size = new System.Drawing.Size(157, 28);
            this.subClass.TabIndex = 131;
            this.subClass.UseSelectable = true;
            this.subClass.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.subClass.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1035, 70);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 32);
            this.label3.TabIndex = 130;
            this.label3.Text = "Class:";
            // 
            // secCombo
            // 
            this.secCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.secCombo.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.secCombo.FormattingEnabled = true;
            this.secCombo.ItemHeight = 24;
            this.secCombo.Location = new System.Drawing.Point(1358, 76);
            this.secCombo.Margin = new System.Windows.Forms.Padding(4);
            this.secCombo.Name = "secCombo";
            this.secCombo.Size = new System.Drawing.Size(102, 30);
            this.secCombo.TabIndex = 133;
            this.secCombo.UseSelectable = true;
            this.secCombo.SelectedIndexChanged += new System.EventHandler(this.secCombo_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1295, 71);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 32);
            this.label4.TabIndex = 132;
            this.label4.Text = "Sec:";
            // 
            // marksGrid
            // 
            this.marksGrid.AllowUserToAddRows = false;
            this.marksGrid.AllowUserToDeleteRows = false;
            this.marksGrid.AllowUserToResizeColumns = false;
            this.marksGrid.AllowUserToResizeRows = false;
            this.marksGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.marksGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.marksGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.marksGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.marksGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.marksGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.marksGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.marksGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.marksGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.marksGrid.EnableHeadersVisualStyles = false;
            this.marksGrid.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.marksGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.marksGrid.Location = new System.Drawing.Point(617, 148);
            this.marksGrid.Name = "marksGrid";
            this.marksGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.marksGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.marksGrid.RowHeadersVisible = false;
            this.marksGrid.RowHeadersWidth = 51;
            this.marksGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Padding = new System.Windows.Forms.Padding(6);
            this.marksGrid.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.marksGrid.RowTemplate.Height = 24;
            this.marksGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.marksGrid.ShowCellErrors = false;
            this.marksGrid.ShowCellToolTips = false;
            this.marksGrid.ShowEditingIcon = false;
            this.marksGrid.ShowRowErrors = false;
            this.marksGrid.Size = new System.Drawing.Size(843, 490);
            this.marksGrid.TabIndex = 134;
            this.marksGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.marksGrid_CellClick);
            this.marksGrid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.marksGrid_CellValueChanged);
            this.marksGrid.DefaultValuesNeeded += new System.Windows.Forms.DataGridViewRowEventHandler(this.marksGrid_DefaultValuesNeeded);
            // 
            // completeButton
            // 
            this.completeButton.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.completeButton.Location = new System.Drawing.Point(814, 681);
            this.completeButton.Name = "completeButton";
            this.completeButton.Size = new System.Drawing.Size(135, 40);
            this.completeButton.TabIndex = 135;
            this.completeButton.Text = "Complete";
            this.completeButton.UseVisualStyleBackColor = true;
            this.completeButton.Click += new System.EventHandler(this.completeButton_Click);
            // 
            // uploadButton
            // 
            this.uploadButton.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uploadButton.Location = new System.Drawing.Point(979, 681);
            this.uploadButton.Name = "uploadButton";
            this.uploadButton.Size = new System.Drawing.Size(134, 40);
            this.uploadButton.TabIndex = 136;
            this.uploadButton.Text = "Upload";
            this.uploadButton.UseVisualStyleBackColor = true;
            this.uploadButton.Click += new System.EventHandler(this.uploadButton_Click);
            // 
            // editButton
            // 
            this.editButton.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editButton.Location = new System.Drawing.Point(655, 681);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(135, 40);
            this.editButton.TabIndex = 137;
            this.editButton.Text = "Edit";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // refreshButton
            // 
            this.refreshButton.BackColor = System.Drawing.Color.White;
            this.refreshButton.Image = ((System.Drawing.Image)(resources.GetObject("refreshButton.Image")));
            this.refreshButton.Location = new System.Drawing.Point(589, 681);
            this.refreshButton.Margin = new System.Windows.Forms.Padding(4);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(40, 40);
            this.refreshButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.refreshButton.TabIndex = 138;
            this.refreshButton.TabStop = false;
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // UploadStudentMarks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.refreshButton);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.uploadButton);
            this.Controls.Add(this.completeButton);
            this.Controls.Add(this.marksGrid);
            this.Controls.Add(this.secCombo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.subClass);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.subIdCombo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.termCombo);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.session);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label14);
            this.Name = "UploadStudentMarks";
            this.Size = new System.Drawing.Size(1660, 740);
            this.Load += new System.EventHandler(this.UploadStudentMarks_Load);
            ((System.ComponentModel.ISupportInitialize)(this.marksGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.refreshButton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label14;
        private MetroFramework.Controls.MetroTextBox session;
        private System.Windows.Forms.Label label7;
        private MetroFramework.Controls.MetroComboBox termCombo;
        private System.Windows.Forms.Label label9;
        private MetroFramework.Controls.MetroComboBox subIdCombo;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroTextBox subClass;
        private System.Windows.Forms.Label label3;
        private MetroFramework.Controls.MetroComboBox secCombo;
        private System.Windows.Forms.Label label4;
        private MetroFramework.Controls.MetroGrid marksGrid;
        private System.Windows.Forms.Button completeButton;
        private System.Windows.Forms.Button uploadButton;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.PictureBox refreshButton;
    }
}

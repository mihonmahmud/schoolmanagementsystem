﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class MonthlyExpenseUC : UserControl
    {
        private LoginData lid = new LoginData();

        public MonthlyExpenseUC()
        {
            InitializeComponent();
        }

        private void classCombo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void MonthlyExpenseUC_Load(object sender, EventArgs e)
        {
            sortCombo.SelectedIndex = -1;
            try
            {
                monthCombo.ValueMember = "month";
                monthCombo.DisplayMember = "month";
                monthCombo.DataSource = RemoveDuplicatesRecords(lid.GetMonth());
                monthCombo.SelectedIndex = -1;

                yearCombo.ValueMember = "year";
                yearCombo.DisplayMember = "year";
                yearCombo.DataSource = RemoveDuplicatesRecords(lid.GetYear());
                yearCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void section_TextChanged(object sender, EventArgs e)
        {

        }

        private void yearCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sortCombo.SelectedIndex == -1)
            {
                expenseGrid.DataSource = null;
            }
            else if ((sortCombo.SelectedItem.ToString() == "Employee Salary") && (yearCombo.SelectedIndex != -1))
            {
                try
                {
                    expenseGrid.DataSource = lid.GetExpenseByYear(yearCombo.SelectedValue.ToString());
                    total.Text = lid.TotalExpenseByYear(yearCombo.SelectedValue.ToString()).ToString();
                }
                catch (Exception) { }
            }
            else if ((sortCombo.SelectedItem.ToString() == "Employee Salary") && (yearCombo.SelectedIndex != -1) && (monthCombo.SelectedIndex != -1))
            {
                try
                {
                    expenseGrid.DataSource = lid.GetExpenseByMonYear(monthCombo.SelectedValue.ToString(), yearCombo.SelectedValue.ToString());
                    total.Text = lid.TotalExpenseByMonYear(monthCombo.SelectedValue.ToString(), yearCombo.SelectedValue.ToString()).ToString();
                }
                catch (Exception) { }
            }
            else if ((sortCombo.SelectedItem.ToString() == "Inventory") && (yearCombo.SelectedIndex != -1))
            {
                try
                {
                    expenseGrid.DataSource = lid.GetInvenExpenseByYear(yearCombo.SelectedValue.ToString());
                    total.Text = lid.TotalInvenExpenseByYear(yearCombo.SelectedValue.ToString()).ToString();
                }
                catch (Exception) { }
            }
            else if ((sortCombo.SelectedItem.ToString() == "Inventory") && (yearCombo.SelectedIndex != -1) && (monthCombo.SelectedIndex != -1))
            {
                try
                {
                    expenseGrid.DataSource = lid.GetInvenExpenseByMonYear(monthCombo.SelectedValue.ToString(), yearCombo.SelectedValue.ToString());
                    total.Text = lid.TotalInvenExpenseByMonYear(monthCombo.SelectedValue.ToString(), yearCombo.SelectedValue.ToString()).ToString();
                }
                catch (Exception) { }
            }
            else { }
        }
    }
}

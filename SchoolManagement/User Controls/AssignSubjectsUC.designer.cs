﻿namespace SchoolManagement.User_Controls
{
    partial class AssignSubjectsUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AssignSubjectsUC));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.secCombo = new MetroFramework.Controls.MetroComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.teacherIdCombo = new MetroFramework.Controls.MetroComboBox();
            this.subIdCombo = new MetroFramework.Controls.MetroComboBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.subjectGrid = new MetroFramework.Controls.MetroGrid();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.sessionCombo = new MetroFramework.Controls.MetroComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.subClass = new MetroFramework.Controls.MetroTextBox();
            this.subDay = new MetroFramework.Controls.MetroTextBox();
            this.subTime = new MetroFramework.Controls.MetroTextBox();
            this.subName = new MetroFramework.Controls.MetroTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subjectGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(135, 164);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(148, 32);
            this.label2.TabIndex = 55;
            this.label2.Text = "Teacher\'s ID:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(155, 224);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 32);
            this.label1.TabIndex = 58;
            this.label1.Text = "Subject ID:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(329, 353);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 32);
            this.label3.TabIndex = 60;
            this.label3.Text = "Class:";
            // 
            // secCombo
            // 
            this.secCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.secCombo.FormattingEnabled = true;
            this.secCombo.ItemHeight = 24;
            this.secCombo.Location = new System.Drawing.Point(290, 419);
            this.secCombo.Margin = new System.Windows.Forms.Padding(4);
            this.secCombo.Name = "secCombo";
            this.secCombo.Size = new System.Drawing.Size(212, 30);
            this.secCombo.TabIndex = 63;
            this.secCombo.UseSelectable = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(227, 415);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 32);
            this.label4.TabIndex = 62;
            this.label4.Text = "Sec:";
            // 
            // teacherIdCombo
            // 
            this.teacherIdCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.teacherIdCombo.FormattingEnabled = true;
            this.teacherIdCombo.ItemHeight = 24;
            this.teacherIdCombo.Location = new System.Drawing.Point(290, 167);
            this.teacherIdCombo.Margin = new System.Windows.Forms.Padding(4);
            this.teacherIdCombo.Name = "teacherIdCombo";
            this.teacherIdCombo.Size = new System.Drawing.Size(212, 30);
            this.teacherIdCombo.TabIndex = 64;
            this.teacherIdCombo.UseSelectable = true;
            // 
            // subIdCombo
            // 
            this.subIdCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.subIdCombo.FormattingEnabled = true;
            this.subIdCombo.ItemHeight = 24;
            this.subIdCombo.Location = new System.Drawing.Point(290, 226);
            this.subIdCombo.Margin = new System.Windows.Forms.Padding(4);
            this.subIdCombo.Name = "subIdCombo";
            this.subIdCombo.Size = new System.Drawing.Size(212, 30);
            this.subIdCombo.TabIndex = 65;
            this.subIdCombo.UseSelectable = true;
            this.subIdCombo.SelectedIndexChanged += new System.EventHandler(this.subIdCombo_SelectedIndexChanged);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(462, 474);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 31);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 71;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // subjectGrid
            // 
            this.subjectGrid.AllowUserToResizeRows = false;
            this.subjectGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.subjectGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.subjectGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.subjectGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.subjectGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.subjectGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.subjectGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.subjectGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.subjectGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.subjectGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.subjectGrid.EnableHeadersVisualStyles = false;
            this.subjectGrid.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.subjectGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.subjectGrid.Location = new System.Drawing.Point(583, 103);
            this.subjectGrid.Name = "subjectGrid";
            this.subjectGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.subjectGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.subjectGrid.RowHeadersVisible = false;
            this.subjectGrid.RowHeadersWidth = 51;
            this.subjectGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.subjectGrid.RowTemplate.Height = 24;
            this.subjectGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.subjectGrid.Size = new System.Drawing.Size(924, 346);
            this.subjectGrid.TabIndex = 72;
            this.subjectGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.subjectGrid_CellClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1288, 474);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 31);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 73;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // sessionCombo
            // 
            this.sessionCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
            this.sessionCombo.FormattingEnabled = true;
            this.sessionCombo.ItemHeight = 24;
            this.sessionCombo.Location = new System.Drawing.Point(290, 103);
            this.sessionCombo.Margin = new System.Windows.Forms.Padding(4);
            this.sessionCombo.Name = "sessionCombo";
            this.sessionCombo.Size = new System.Drawing.Size(212, 30);
            this.sessionCombo.TabIndex = 75;
            this.sessionCombo.UseSelectable = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(183, 100);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 32);
            this.label5.TabIndex = 74;
            this.label5.Text = "Session:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(68, 290);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 32);
            this.label6.TabIndex = 76;
            this.label6.Text = "Day:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(46, 352);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 32);
            this.label7.TabIndex = 77;
            this.label7.Text = "Time:";
            // 
            // subClass
            // 
            // 
            // 
            // 
            this.subClass.CustomButton.Image = null;
            this.subClass.CustomButton.Location = new System.Drawing.Point(68, 2);
            this.subClass.CustomButton.Name = "";
            this.subClass.CustomButton.Size = new System.Drawing.Size(23, 23);
            this.subClass.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.subClass.CustomButton.TabIndex = 1;
            this.subClass.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.subClass.CustomButton.UseSelectable = true;
            this.subClass.CustomButton.Visible = false;
            this.subClass.Enabled = false;
            this.subClass.Lines = new string[0];
            this.subClass.Location = new System.Drawing.Point(408, 357);
            this.subClass.MaxLength = 32767;
            this.subClass.Multiline = true;
            this.subClass.Name = "subClass";
            this.subClass.PasswordChar = '\0';
            this.subClass.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.subClass.SelectedText = "";
            this.subClass.SelectionLength = 0;
            this.subClass.SelectionStart = 0;
            this.subClass.ShortcutsEnabled = true;
            this.subClass.Size = new System.Drawing.Size(94, 28);
            this.subClass.TabIndex = 78;
            this.subClass.UseSelectable = true;
            this.subClass.WaterMarkColor = System.Drawing.Color.Black;
            this.subClass.WaterMarkFont = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // subDay
            // 
            // 
            // 
            // 
            this.subDay.CustomButton.Image = null;
            this.subDay.CustomButton.Location = new System.Drawing.Point(110, 2);
            this.subDay.CustomButton.Name = "";
            this.subDay.CustomButton.Size = new System.Drawing.Size(23, 23);
            this.subDay.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.subDay.CustomButton.TabIndex = 1;
            this.subDay.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.subDay.CustomButton.UseSelectable = true;
            this.subDay.CustomButton.Visible = false;
            this.subDay.Enabled = false;
            this.subDay.Lines = new string[0];
            this.subDay.Location = new System.Drawing.Point(136, 295);
            this.subDay.MaxLength = 32767;
            this.subDay.Multiline = true;
            this.subDay.Name = "subDay";
            this.subDay.PasswordChar = '\0';
            this.subDay.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.subDay.SelectedText = "";
            this.subDay.SelectionLength = 0;
            this.subDay.SelectionStart = 0;
            this.subDay.ShortcutsEnabled = true;
            this.subDay.Size = new System.Drawing.Size(136, 28);
            this.subDay.TabIndex = 79;
            this.subDay.UseSelectable = true;
            this.subDay.WaterMarkColor = System.Drawing.Color.Black;
            this.subDay.WaterMarkFont = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // subTime
            // 
            // 
            // 
            // 
            this.subTime.CustomButton.Image = null;
            this.subTime.CustomButton.Location = new System.Drawing.Point(164, 2);
            this.subTime.CustomButton.Name = "";
            this.subTime.CustomButton.Size = new System.Drawing.Size(23, 23);
            this.subTime.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.subTime.CustomButton.TabIndex = 1;
            this.subTime.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.subTime.CustomButton.UseSelectable = true;
            this.subTime.CustomButton.Visible = false;
            this.subTime.Enabled = false;
            this.subTime.Lines = new string[0];
            this.subTime.Location = new System.Drawing.Point(125, 357);
            this.subTime.MaxLength = 32767;
            this.subTime.Multiline = true;
            this.subTime.Name = "subTime";
            this.subTime.PasswordChar = '\0';
            this.subTime.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.subTime.SelectedText = "";
            this.subTime.SelectionLength = 0;
            this.subTime.SelectionStart = 0;
            this.subTime.ShortcutsEnabled = true;
            this.subTime.Size = new System.Drawing.Size(190, 28);
            this.subTime.TabIndex = 80;
            this.subTime.UseSelectable = true;
            this.subTime.WaterMarkColor = System.Drawing.Color.Black;
            this.subTime.WaterMarkFont = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // subName
            // 
            // 
            // 
            // 
            this.subName.CustomButton.Image = null;
            this.subName.CustomButton.Location = new System.Drawing.Point(106, 2);
            this.subName.CustomButton.Name = "";
            this.subName.CustomButton.Size = new System.Drawing.Size(23, 23);
            this.subName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.subName.CustomButton.TabIndex = 1;
            this.subName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.subName.CustomButton.UseSelectable = true;
            this.subName.CustomButton.Visible = false;
            this.subName.Enabled = false;
            this.subName.Lines = new string[0];
            this.subName.Location = new System.Drawing.Point(370, 294);
            this.subName.MaxLength = 32767;
            this.subName.Multiline = true;
            this.subName.Name = "subName";
            this.subName.PasswordChar = '\0';
            this.subName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.subName.SelectedText = "";
            this.subName.SelectionLength = 0;
            this.subName.SelectionStart = 0;
            this.subName.ShortcutsEnabled = true;
            this.subName.Size = new System.Drawing.Size(132, 28);
            this.subName.TabIndex = 82;
            this.subName.UseSelectable = true;
            this.subName.WaterMarkColor = System.Drawing.Color.Black;
            this.subName.WaterMarkFont = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(280, 289);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 32);
            this.label8.TabIndex = 81;
            this.label8.Text = "Name:";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.White;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(408, 474);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(40, 31);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 83;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.White;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(354, 474);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(40, 31);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 84;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(586, 11);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(253, 34);
            this.label14.TabIndex = 85;
            this.label14.Text = "ASSIGN SUBJECTS";
            // 
            // AssignSubjectsUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.label14);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.subName);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.subTime);
            this.Controls.Add(this.subDay);
            this.Controls.Add(this.subClass);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.sessionCombo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.subjectGrid);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.subIdCombo);
            this.Controls.Add(this.teacherIdCombo);
            this.Controls.Add(this.secCombo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "AssignSubjectsUC";
            this.Size = new System.Drawing.Size(1587, 671);
            this.Load += new System.EventHandler(this.AssignSubjectsUC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subjectGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private MetroFramework.Controls.MetroComboBox secCombo;
        private System.Windows.Forms.Label label4;
        private MetroFramework.Controls.MetroComboBox teacherIdCombo;
        private MetroFramework.Controls.MetroComboBox subIdCombo;
        private System.Windows.Forms.PictureBox pictureBox2;
        private MetroFramework.Controls.MetroGrid subjectGrid;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroComboBox sessionCombo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private MetroFramework.Controls.MetroTextBox subClass;
        private MetroFramework.Controls.MetroTextBox subDay;
        private MetroFramework.Controls.MetroTextBox subTime;
        private MetroFramework.Controls.MetroTextBox subName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label14;
    }
}

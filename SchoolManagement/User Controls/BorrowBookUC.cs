﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;
using System.Data.SqlClient;

namespace SchoolManagement
{
    public partial class BorrowBookUC : UserControl
    {
        bool flag = false;
        private SqlConnection con;
        private LibrarianData lb = new LibrarianData();
        public BorrowBookUC()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            con = new SqlConnection(@"Data Source=DESKTOP-E7IA1G1;Initial Catalog=SchoolManagement;Integrated Security=True");
            con.Open();
            string q = string.Format($"SELECT * FROM BOOKS");
            SqlCommand cmd = new SqlCommand(q, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            da.Fill(dt);

            for(int i = 0; i < dt.Rows.Count; i++)
            {
                if(dt.Rows[i][1].ToString() == BookId.Text)
                {
                    flag = true;
                    break;
                }
            }
            

            if (flag == true)
            {
                if(!string.IsNullOrWhiteSpace(BookId.Text) && !string.IsNullOrWhiteSpace(BorrowerId.Text) && !string.IsNullOrWhiteSpace(BorrowerName.Text) && !string.IsNullOrWhiteSpace(Contact.Text) && !string.IsNullOrWhiteSpace(BorrowDate.Text) && !string.IsNullOrWhiteSpace(ReturnDate.Text))
                {
                    if (lb.InsertBorrowBook(BookId.Text, BorrowerId.Text, BorrowerName.Text, Contact.Text, BorrowDate.Text, ReturnDate.Text, 0))
                    {
                        MessageBox.Show("Borrow Book Added!");

                    }
                }               
                else
                {
                    MessageBox.Show("FILL ALL REQUIRED FIELDS!");
                }

            }
            else
            {
                MessageBox.Show("Book Id or Borrower Id does not exist!");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            BookId.Text = BorrowerId.Text = BorrowerName.Text = Contact.Text = BorrowDate.Text = ReturnDate.Text = String.Empty;
        }

        private void BorrowDate_Enter(object sender, EventArgs e)
        {
            if(BorrowDate.Text=="dd-mm-yyyy")
            {
                BorrowDate.Text = "";

                BorrowDate.ForeColor = Color.Black;
            }
        }

        private void BorrowDate_Leave(object sender, EventArgs e)
        {
            if (BorrowDate.Text == "")
            {
                BorrowDate.Text = "dd-mm-yyyy";

                BorrowDate.ForeColor = Color.Silver;
            }
        }

        private void ReturnDate_Enter(object sender, EventArgs e)
        {
            if (ReturnDate.Text == "dd-mm-yyyy")
            {
                ReturnDate.Text = "";

                ReturnDate.ForeColor = Color.Black;
            }
        }

        private void ReturnDate_Leave(object sender, EventArgs e)
        {
            if (ReturnDate.Text == "")
            {
                ReturnDate.Text = "dd-mm-yyyy";

                ReturnDate.ForeColor = Color.Silver;
            }
        }

        private void BookId_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void BorrowerId_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void Contact_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void Contact_Enter(object sender, EventArgs e)
        {
            if(Contact.Text=="880+")
            {
                Contact.Text = "";

                Contact.ForeColor = Color.Black;
            }
        }

        private void Contact_Leave(object sender, EventArgs e)
        {
            if (Contact.Text == "")
            {
                Contact.Text = "880+";

                Contact.ForeColor = Color.Silver;
            }
        }
    }
}

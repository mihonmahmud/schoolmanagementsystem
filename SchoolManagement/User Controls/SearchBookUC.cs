﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;
using System.Data.SqlClient;


namespace SchoolManagement
{
    public partial class SearchBookUC : UserControl
    {
        private LibrarianData lb = new LibrarianData();
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-E7IA1G1;Initial Catalog=SchoolManagement;Integrated Security=True");

        public SearchBookUC()
        {
            InitializeComponent();
            metroGrid1.DataSource = this.showWGrid();
            //con = new SqlConnection(@"Data Source=DESKTOP-TD8UJR4\SQLEXPRESS;Initial Catalog=MedicalShop;Integrated Security=True");
        }

        public DataTable showWGrid()
        {
            return lb.GridUpdate();

        }

        public DataTable Search(string searchBooks, bool withId)
        {
            return lb.Search(searchBooks, withId);
        }
        public DataTable SearchAuthor(string searchBooks)
        {
            return lb.SearchAuthor(searchBooks);
        }


        private void button1_Click(object sender, EventArgs e)
        {
            //string searchMedicine;
            //if (textBox1.Text == "" || ComboBox1.Text == "")
            //    MessageBox.Show("Fill up the boxes");
            //else if (ComboBox1.Text == "BookId")
            //{
            //    searchMedicine = textBox1.Text;
            //    metroGrid1.DataSource = this.Search(searchMedicine, false);
            //}

            //else if(ComboBox1.Text=="Title")
            //{
            //    searchMedicine = textBox1.Text;
            //    metroGrid1.DataSource = this.Search(searchMedicine, true);
            //}
            //else
            //{
            //    searchMedicine = textBox1.Text;
            //    metroGrid1.DataSource = this.SearchAuthor(searchMedicine);
            //}
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string searchBooks;
            if (ComboBox1.Text == "")
                MessageBox.Show("Select Category");
            else if (ComboBox1.Text == "BookId")
            {
                searchBooks = textBox1.Text;
                metroGrid1.DataSource = this.Search(searchBooks, true);
            }

            else if (ComboBox1.Text == "Title")
            {
                searchBooks = textBox1.Text;
                metroGrid1.DataSource = this.Search(searchBooks, false);
            }
            else
            {
                searchBooks = textBox1.Text;
                metroGrid1.DataSource = this.SearchAuthor(searchBooks);
            }
        }
    }
}

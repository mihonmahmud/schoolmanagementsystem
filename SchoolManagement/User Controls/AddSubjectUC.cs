﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class AddSubjectUC : UserControl
    {
        private string sClass;

        private AdminData ad = new AdminData();

        public AddSubjectUC()
        {
            InitializeComponent();
        }

        private void metroComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            
        }

        private void AddSubjectUC_Load(object sender, EventArgs e)
        {
            try
            {
                subjectsGrid.DataSource = ad.GetAllSubjects();
            }
            catch (Exception) { }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            string subTime = from.Value.ToShortTimeString() + " - " + to.Value.ToShortTimeString();

            try
            {
                if(ad.IsSubIdExist(subId.Text))
                {
                    MessageBox.Show("ID ALREADY EXISTS!!!");
                    subId.Text = "";
                }
                else
                {
                    if (ad.AddSubject(subId.Text.Trim(), subName.Text.Trim(), subjectClassCombo.SelectedItem.ToString(), subTime, subDay.Text.Trim()))
                    {
                        MessageBox.Show("Subject Added!");
                        subjectsGrid.DataSource = ad.GetAllSubjects();
                        subId.Text = subName.Text = subDay.Text = string.Empty;
                        subjectClassCombo.SelectedIndex = -1;
                    }
                }               
            }
            catch (Exception) { }
        }

        private void search_TextChanged(object sender, EventArgs e)
        {
            string field = "Class";
            try
            {
                ((DataTable)subjectsGrid.DataSource).DefaultView.RowFilter = string.Format($"[{field}] LIKE '%{search.Text}%'");
            }
            catch (Exception) { }
        }
    }
}

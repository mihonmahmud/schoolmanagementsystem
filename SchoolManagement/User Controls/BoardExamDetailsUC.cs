﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class BoardExamDetailsUC : UserControl
    {
        private AccountantData ad = new AccountantData();
        private HeadTeacherData htd = new HeadTeacherData();

        public BoardExamDetailsUC()
        {
            InitializeComponent();
        }

        private void BoardExamDetailsUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ad.GetSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }

            total.Text = passed.Text = failed.Text = 0.ToString();
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void sessionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(sessionCombo.SelectedIndex == -1)
            {
                boardGrid.DataSource = null;
            }
            else
            {
                try
                {
                    boardGrid.DataSource = htd.GetBoardInfoBySession(sessionCombo.SelectedValue.ToString());
                }
                catch (Exception) { }
            }
        }

        private void examCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(examCombo.SelectedIndex == -1)
            {
                boardGrid.DataSource = null;
            }
            else if((sessionCombo.SelectedIndex != -1) && (examCombo.SelectedIndex == -1))
            {
                try
                {
                    boardGrid.DataSource = htd.GetBoardInfoBySession(sessionCombo.SelectedValue.ToString());
                }
                catch (Exception) { }
            }
            else if ((sessionCombo.SelectedIndex != -1) && (examCombo.SelectedIndex != -1))
            {
                try
                {
                    boardGrid.DataSource = htd.GetBoardInfoBySessionExam(sessionCombo.SelectedValue.ToString(), examCombo.SelectedItem.ToString());
                }
                catch (Exception) { }
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            try
            {
                if(htd.InsertBoardInfo(sessionCombo.SelectedValue.ToString(), examCombo.SelectedItem.ToString(), int.Parse(total.Text), int.Parse(passed.Text), int.Parse(failed.Text), description.Text))
                {
                    boardGrid.DataSource = htd.GetBoardInfoBySessionExam(sessionCombo.SelectedValue.ToString(), examCombo.SelectedItem.ToString());
                    MessageBox.Show("ADDED!");
                }
            }
            catch (Exception) { }
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            boardGrid.DataSource = null;
            sessionCombo.SelectedIndex = examCombo.SelectedIndex = -1;
            total.Text = passed.Text = failed.Text = description.Text = "";
        }

        private void boardGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                total.Text = boardGrid.Rows[e.RowIndex].Cells[0].Value.ToString();
                passed.Text = boardGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
                failed.Text = boardGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
                description.Text = boardGrid.Rows[e.RowIndex].Cells[3].Value.ToString();
            }
            catch (Exception) { }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                if (htd.UpdateBoardInfo(sessionCombo.SelectedValue.ToString(), examCombo.SelectedItem.ToString(), int.Parse(total.Text), int.Parse(passed.Text), int.Parse(failed.Text), description.Text))
                {
                    boardGrid.DataSource = htd.GetBoardInfoBySessionExam(sessionCombo.SelectedValue.ToString(), examCombo.SelectedItem.ToString());
                    MessageBox.Show("UPDATED!");
                    total.Text = passed.Text = failed.Text = 0.ToString();
                    description.Text = "";
                    sessionCombo.SelectedIndex = examCombo.SelectedIndex = -1;
                }
            }
            catch (Exception) { }
        }

        private void total_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void passed_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void failed_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }
    }
}

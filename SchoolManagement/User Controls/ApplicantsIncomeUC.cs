﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class ApplicantsIncomeUC : UserControl
    {
        private AdminData ad = new AdminData();
        private LoginData lid = new LoginData();

        public ApplicantsIncomeUC()
        {
            InitializeComponent();
        }

        private void ApplicantsIncomeUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ad.GetAdmissionSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void sessionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(sessionCombo.SelectedIndex == -1)
            {
                incomeGrid.DataSource = null;
            }
            else
            {
                try
                {
                    incomeGrid.DataSource = lid.ApplicantsIncome(sessionCombo.SelectedValue.ToString());
                }
                catch (Exception) { }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class ProfitUC : UserControl
    {
        private LoginData lid = new LoginData();
        private OwnerData od = new OwnerData();
       
        public ProfitUC()
        {
            InitializeComponent();
        }

        private void ProfitUC_Load(object sender, EventArgs e)
        {
            try
            {               
                yearCombo.ValueMember = "year";
                yearCombo.DisplayMember = "year";
                yearCombo.DataSource = RemoveDuplicatesRecords(lid.GetYear());
                yearCombo.SelectedIndex = -1;               
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void yearCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = RemoveDuplicatesRecords(od.GetMonthByYear(yearCombo.SelectedValue.ToString()));

                DataTable ddt = new DataTable();
                DataTable total = new DataTable();
                DataRow dr1;
                DataRow dr2;

                ddt.Columns.Add("Month");
                ddt.Columns.Add("Income");
                ddt.Columns.Add("AppFees");
                ddt.Columns.Add("Salary");
                ddt.Columns.Add("Inventory");

                total.Columns.Add("Month");
                total.Columns.Add("Profit");

                
                try
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dr1 = ddt.NewRow();
                        dr1["Month"] = dt.Rows[i][0].ToString();
                        dr1["Income"] = od.GetTotalIncomeByMonthYear(dt.Rows[i][0].ToString(), yearCombo.SelectedValue.ToString()).ToString();
                        dr1["AppFees"] = od.GetTotalApplicantsFees(dt.Rows[i][0].ToString(), yearCombo.SelectedValue.ToString()).ToString();
                        dr1["Salary"] = od.GetTotalSalaryByMonthYear(dt.Rows[i][0].ToString(), yearCombo.SelectedValue.ToString()).ToString();
                        dr1["Inventory"] = od.GetTotalInventoryByMonthYear(dt.Rows[i][0].ToString(), yearCombo.SelectedValue.ToString()).ToString();
                        ddt.Rows.Add(dr1);
                    }
                }
                catch (Exception) { }

                for (int i = 0; i < ddt.Rows.Count; i++)
                {
                    //string month = ddt.Rows[i][0].ToString();
                    string income = ddt.Rows[i][1].ToString();
                    string appfees = ddt.Rows[i][2].ToString();
                    string salary = ddt.Rows[i][3].ToString();
                    string inven = ddt.Rows[i][4].ToString();

                    dr2 = total.NewRow();
                    dr2["Month"] = ddt.Rows[i][0].ToString();
                    dr2["Profit"] = ((float.Parse(income) + float.Parse(appfees)) - (float.Parse(salary) + float.Parse(inven))).ToString();
                    total.Rows.Add(dr2);
                }

                ddt.Rows.Clear();

                //profitChart.Series.Clear();
                profitChart.DataSource = total;
                profitChart.Series["profit"].XValueMember = "Month";
                profitChart.Series["profit"].YValueMembers = "Profit";
                profitChart.Titles.Add("");
                profitChart.Series["profit"].IsValueShownAsLabel = true;
                profitChart.DataBind();
                
                total.Rows.Clear();
            }
            catch (Exception) { }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class UnpaidUC : UserControl
    {
        private AccountantData ac = new AccountantData();

        public UnpaidUC()
        {
            InitializeComponent();
        }

        private void UnpaidUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ac.GetSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void monthCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(monthCombo.SelectedIndex == -1)
            {
                unpaidGrid.DataSource = null;
            }
            else
            {
                Unpaid();
                Partial();
            }
        }

        private void Unpaid()
        {
            try
            {
                if (monthCombo.SelectedIndex != -1)
                {
                    unpaidGrid.DataSource = ac.UnpaidStudents(sessionCombo.SelectedValue.ToString(), classCombo.SelectedItem.ToString(), monthCombo.SelectedItem.ToString());
                }
            }
            catch (Exception) { }
        }

        private void Partial()
        {
            try
            {
                if (monthCombo.SelectedIndex != -1)
                {
                    partialGrid.DataSource = ac.PartialStudents(sessionCombo.SelectedValue.ToString(), classCombo.SelectedItem.ToString(), monthCombo.SelectedItem.ToString());
                }
            }
            catch (Exception) { }
        }
    }
}

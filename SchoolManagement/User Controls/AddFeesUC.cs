﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class AddFeesUC : UserControl
    {
        private AccountantData ad = new AccountantData();
        private AdminData a = new AdminData();

        string adminFee;
        string monthFee;
        string examFee;
        string developFee;
        string libFee;
        float due;
        float pay;

        public AddFeesUC()
        {
            InitializeComponent();
        }

        private void AddFeesUC_Load(object sender, EventArgs e)
        {
            session.Text = DateTime.Now.Year.ToString();

            feesGrid.DataSource = ad.GetFees();
            termCombo.SelectedItem = "None";

            fine.Text = 0.ToString();
            others.Text = 0.ToString();

            pictureBox2.Enabled = false;
            amount.Enabled = false;
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void feesGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void classCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(classCombo.SelectedIndex == -1)
            {
                secCombo.DataSource = null;
            }
            else
            {
                try
                {
                    secCombo.ValueMember = "section";
                    secCombo.DisplayMember = "section";
                    secCombo.DataSource = a.GetSecByClass(classCombo.SelectedItem.ToString());
                    secCombo.SelectedIndex = -1;
                }
                catch (Exception) { }
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if(float.Parse(amount.Text) > 0)
            {
                if (float.Parse(amount.Text) < float.Parse(total.Text))
                {
                    due = float.Parse(total.Text) - float.Parse(amount.Text);
                    pay = float.Parse(amount.Text);
                }
                else if (float.Parse(amount.Text) >= float.Parse(total.Text))
                {
                    due = 0;
                    pay = float.Parse(total.Text);
                }
                    

                if (ad.AddFees(session.Text, stuId.Text, classCombo.SelectedItem.ToString(), secCombo.SelectedValue.ToString(), float.Parse(adminFee), float.Parse(monthFee), monthCombo.SelectedItem.ToString(), session.Text, float.Parse(examFee), termCombo.SelectedItem.ToString(), float.Parse(fine.Text), float.Parse(libFee), float.Parse(developFee), float.Parse(others.Text), due, pay))
                {
                    MessageBox.Show("ADDED!");
                    clear();
                }
            }
            else
            {
                MessageBox.Show("ENTER PAYABLE AMOUNT!");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            if(!monthly.Checked || string.IsNullOrWhiteSpace(stuId.Text) || classCombo.SelectedIndex == -1 || secCombo.SelectedIndex == -1 || monthCombo.SelectedIndex == -1)
            {
                MessageBox.Show("FILL REQUIRED FIELDS");
            }
            else
            {
                if(ad.IsStuIdValid(session.Text, stuId.Text, classCombo.SelectedItem.ToString(), secCombo.SelectedValue.ToString()))
                {
                    monthFee = monthly.Checked ? feesGrid.Rows[0].Cells[0].Value.ToString() : "0";
                    examFee = exam.Checked ? feesGrid.Rows[0].Cells[1].Value.ToString() : "0";
                    adminFee = admission.Checked ? feesGrid.Rows[0].Cells[2].Value.ToString() : "0";    
                    developFee = development.Checked ? feesGrid.Rows[0].Cells[3].Value.ToString() : "0";
                    libFee = library.Checked ? feesGrid.Rows[0].Cells[4].Value.ToString() : "0";

                    if (int.Parse(percentage.Text) > 0)
                    {
                        float sum = float.Parse(adminFee) + float.Parse(monthFee) + float.Parse(examFee) + float.Parse(fine.Text) + float.Parse(developFee) + float.Parse(libFee) + float.Parse(others.Text);
                        float percent = ((sum * int.Parse(percentage.Text)) / 100);
                        total.Text = (sum - percent).ToString();
                    }
                    else
                    {
                        total.Text = (float.Parse(adminFee) + float.Parse(monthFee) + float.Parse(examFee) + float.Parse(fine.Text) + float.Parse(developFee) + float.Parse(libFee) + float.Parse(others.Text)).ToString();
                    }

                    pictureBox2.Enabled = true;
                    amount.Enabled = true;
                }
                else
                {
                    MessageBox.Show("STUDENT ID IS NOT VALID");
                }
            }
        }

        void clear()
        {
            stuId.Text = library.Text = total.Text = amount.Text = percentage.Text = string.Empty;
            others.Text = fine.Text = "0";
            classCombo.SelectedIndex = secCombo.SelectedIndex = termCombo.SelectedIndex = monthCombo.SelectedIndex = -1;
            monthly.Checked = development.Checked = exam.Checked = library.Checked = admission.Checked = false;
            amount.Enabled = pictureBox2.Enabled = false;
        }

        private void stuId_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                percentage.Text = ad.GetPercentageByStuId(session.Text, stuId.Text).ToString();
            }
            catch (Exception) { }
        }

        private void admission_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void monthly_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void year_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void exam_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void fine_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void development_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void library_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void others_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void due_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void total_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }
    }
}

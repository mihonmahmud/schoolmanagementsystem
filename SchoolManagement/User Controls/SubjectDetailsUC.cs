﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class SubjectDetailsUC : UserControl
    {
        private HeadTeacherData htd = new HeadTeacherData();

        public SubjectDetailsUC()
        {
            InitializeComponent();
        }

        private void SubjectDetailsUC_Load(object sender, EventArgs e)
        {
            try
            {
                subjectsGrid.DataSource = htd.GetSubjectDetails();
            }
            catch (Exception) { }
        }

        private void subjectClassCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(classCombo.SelectedIndex == -1)
            {
                subjectsGrid.DataSource = htd.GetSubjectDetails();
            }
            else
            {
                try
                {
                    subjectsGrid.DataSource = htd.GetSubjectDetailsByClass(classCombo.SelectedItem.ToString());
                }
                catch (Exception) { }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class BoardExamOwnerUC : UserControl
    {
        private AccountantData ad = new AccountantData();
        private HeadTeacherData htd = new HeadTeacherData();

        public BoardExamOwnerUC()
        {
            InitializeComponent();
        }

        private void BoardExamOwnerUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ad.GetSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void sessionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sessionCombo.SelectedIndex == -1)
            {
                boardGrid.DataSource = null;
            }
            else
            {
                try
                {
                    boardGrid.DataSource = htd.GetBoardInfoBySession(sessionCombo.SelectedValue.ToString());
                }
                catch (Exception) { }
            }
        }

        private void examCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (examCombo.SelectedIndex == -1)
            {
                boardGrid.DataSource = null;
            }
            else if ((sessionCombo.SelectedIndex != -1) && (examCombo.SelectedIndex == -1))
            {
                try
                {
                    boardGrid.DataSource = htd.GetBoardInfoBySession(sessionCombo.SelectedValue.ToString());
                }
                catch (Exception) { }
            }
            else if ((sessionCombo.SelectedIndex != -1) && (examCombo.SelectedIndex != -1))
            {
                try
                {
                    boardGrid.DataSource = htd.GetBoardInfoBySessionExam(sessionCombo.SelectedValue.ToString(), examCombo.SelectedItem.ToString());
                }
                catch (Exception) { }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class StudentAttendanceDetailsUC : UserControl
    {
        private AdminData ad = new AdminData();

        public StudentAttendanceDetailsUC()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if(sessionCombo.SelectedIndex != -1 && monthCombo.SelectedIndex != -1 && !string.IsNullOrWhiteSpace(stuId.Text))
            {
                try
                {
                    stuAttenGrid.DataSource = ad.GetSpeicificStuAtten(sessionCombo.SelectedValue.ToString(), monthCombo.SelectedValue.ToString(), stuId.Text);
                }
                catch (Exception) { }
            }
            else
            {
                MessageBox.Show("FILL ALL REQUIRED FIELDS!");
            }
        }

        private void StudentAttendanceDetailsUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(ad.GetAttenSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void sessionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(sessionCombo.SelectedIndex == -1)
            {
                monthCombo.DataSource = null;
            }
            else
            {
                try
                {
                    monthCombo.ValueMember = "month";
                    monthCombo.DisplayMember = "month";
                    monthCombo.DataSource = ad.GetAttenMonth(sessionCombo.SelectedValue.ToString());
                    monthCombo.SelectedIndex = -1;
                    //stuAttenGrid.DataSource = ad.GetSpeicificStuAttenBySession(stuId.Text, sessionCombo.SelectedValue.ToString());
                }
                catch (Exception) { }
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            stuId.Text = "";
            monthCombo.SelectedIndex = sessionCombo.SelectedIndex = -1;
            stuAttenGrid.DataSource = null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class AdminAdmissionUC : UserControl
    {
        private AdminData ad = new AdminData();
        private DataTable ddt = new DataTable();
        private DataRow dr;

        public AdminAdmissionUC()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            string time = from.Value.ToShortTimeString() + " - " + to.Value.ToShortTimeString();

            if(ad.IsAdmissionCreated(session.Text.Trim(), classCombo.SelectedItem.ToString()))
            {
                MessageBox.Show("SCHEDULE ALREADY CREATED!");
                classCombo.SelectedIndex = -1;
            }
            else
            {
                dr = ddt.NewRow();
                dr["Session"] = session.Text;
                dr["Class"] = classCombo.SelectedItem.ToString();
                dr["Date"] = date.Value.ToShortDateString();
                dr["Time"] = time;
                ddt.Rows.Add(dr);

                admissionGrid.DataSource = ddt;
            }
        }

        private void Clear()
        {
            session.Text = date.Text = "";
            classCombo.SelectedIndex = -1;
            ddt.Rows.Clear();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void AdminAdmissionUC_Load(object sender, EventArgs e)
        {
            ddt.Columns.Add("Session");
            ddt.Columns.Add("Class");
            ddt.Columns.Add("Date");
            ddt.Columns.Add("Time");
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            if (ad.InsertAdmissionData((DataTable)admissionGrid.DataSource))
            {
                MessageBox.Show("SCHEDULE CREATED!");
                Clear();
            }
        }

        private void session_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class AddSectionUC : UserControl
    {
        private AdminData ad = new AdminData();
        private HeadTeacherData htd = new HeadTeacherData();
        private DataTable ddt = new DataTable();
        private DataRow dr;

        public AddSectionUC()
        {
            InitializeComponent();
        }

        private void AddSectionUC_Load(object sender, EventArgs e)
        {
            try
            {
                sessionCombo.ValueMember = "session";
                sessionCombo.DisplayMember = "session";
                sessionCombo.DataSource = RemoveDuplicatesRecords(htd.GetAdmissionSession());
                sessionCombo.SelectedIndex = -1;
            }
            catch (Exception) { }

            ddt.Columns.Add("Session");
            ddt.Columns.Add("Class");
            ddt.Columns.Add("Section");
            ddt.Columns.Add("Capacity");
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if(ad.IsSectionExist(sessionCombo.SelectedValue.ToString(), classCombo.SelectedItem.ToString(), section.Text))
            {
                MessageBox.Show("SECTION ALREADY EXISTS!");
            }
            else
            {
                try
                {
                    dr = ddt.NewRow();
                    dr["Session"] = sessionCombo.SelectedValue.ToString();
                    dr["Class"] = classCombo.SelectedItem.ToString();
                    dr["Section"] = section.Text;
                    dr["Capacity"] = capacity.Text;
                    ddt.Rows.Add(dr);

                    secGrid.DataSource = ddt;
                }
                catch (Exception) { }
            }
        }

        private void secGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            sessionCombo.SelectedItem = secGrid.Rows[e.RowIndex].Cells[0].Value.ToString();
            classCombo.SelectedItem = secGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
            section.Text = secGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
            capacity.Text = secGrid.Rows[e.RowIndex].Cells[3].Value.ToString();
        }

        private void Clear()
        {
            section.Text = capacity.Text = "";
            sessionCombo.SelectedIndex = classCombo.SelectedIndex = -1;
            ddt.Rows.Clear();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            secGrid.Rows.RemoveAt(secGrid.CurrentCell.RowIndex);
            Clear();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                if (ad.InsertSections((DataTable)secGrid.DataSource))
                {
                    MessageBox.Show("SECTIONS ADDED!");
                    Clear();
                }
            }
            catch(Exception)
            {
                MessageBox.Show("SOMETHING WENT WRONG! PLEASE TRY AGAIN");
            }
        }

        private void section_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void capacity_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }
    }
}

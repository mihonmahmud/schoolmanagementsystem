﻿namespace SchoolManagement.User_Controls
{
    partial class AddAdminUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddAdminUC));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.phone = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.email = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.employeeIdText = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.employeeDegree = new System.Windows.Forms.TextBox();
            this.employeePassText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.employeeBGCombo = new System.Windows.Forms.ComboBox();
            this.employeeGenderCombo = new System.Windows.Forms.ComboBox();
            this.employeePermanent = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.employeePresent = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.employeeNation = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.employeeReligion = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.employeeMName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.employeeFName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.employeeName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.jdate = new System.Windows.Forms.DateTimePicker();
            this.label22 = new System.Windows.Forms.Label();
            this.employeeDOB = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tiffin = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.medical = new System.Windows.Forms.TextBox();
            this.home = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.basic = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1267, 585);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 31);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 38;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // phone
            // 
            this.phone.Location = new System.Drawing.Point(192, 207);
            this.phone.Margin = new System.Windows.Forms.Padding(4);
            this.phone.Name = "phone";
            this.phone.Size = new System.Drawing.Size(236, 32);
            this.phone.TabIndex = 31;
            this.phone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.phone_KeyPress);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(47, 204);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(141, 32);
            this.label20.TabIndex = 30;
            this.label20.Text = "Contact No:";
            // 
            // email
            // 
            this.email.Location = new System.Drawing.Point(748, 45);
            this.email.Margin = new System.Windows.Forms.Padding(4);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(236, 32);
            this.email.TabIndex = 29;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(667, 42);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 32);
            this.label19.TabIndex = 28;
            this.label19.Text = "Email:";
            // 
            // employeeIdText
            // 
            this.employeeIdText.Enabled = false;
            this.employeeIdText.Location = new System.Drawing.Point(192, 42);
            this.employeeIdText.Margin = new System.Windows.Forms.Padding(4);
            this.employeeIdText.Name = "employeeIdText";
            this.employeeIdText.Size = new System.Drawing.Size(236, 32);
            this.employeeIdText.TabIndex = 27;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(144, 40);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(43, 32);
            this.label18.TabIndex = 26;
            this.label18.Text = "ID:";
            // 
            // employeeDegree
            // 
            this.employeeDegree.Location = new System.Drawing.Point(749, 207);
            this.employeeDegree.Margin = new System.Windows.Forms.Padding(4);
            this.employeeDegree.Name = "employeeDegree";
            this.employeeDegree.Size = new System.Drawing.Size(236, 32);
            this.employeeDegree.TabIndex = 24;
            // 
            // employeePassText
            // 
            this.employeePassText.Enabled = false;
            this.employeePassText.Location = new System.Drawing.Point(192, 94);
            this.employeePassText.Margin = new System.Windows.Forms.Padding(4);
            this.employeePassText.Name = "employeePassText";
            this.employeePassText.Size = new System.Drawing.Size(236, 32);
            this.employeePassText.TabIndex = 23;
            this.employeePassText.UseSystemPasswordChar = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(70, 92);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 32);
            this.label1.TabIndex = 22;
            this.label1.Text = "Password:";
            // 
            // employeeBGCombo
            // 
            this.employeeBGCombo.FormattingEnabled = true;
            this.employeeBGCombo.Items.AddRange(new object[] {
            "Apos",
            "Aneg",
            "ABpos",
            "ABneg",
            "Bpos",
            "Bneg",
            "Opos",
            "Oneg"});
            this.employeeBGCombo.Location = new System.Drawing.Point(748, 149);
            this.employeeBGCombo.Margin = new System.Windows.Forms.Padding(4);
            this.employeeBGCombo.Name = "employeeBGCombo";
            this.employeeBGCombo.Size = new System.Drawing.Size(236, 31);
            this.employeeBGCombo.TabIndex = 20;
            // 
            // employeeGenderCombo
            // 
            this.employeeGenderCombo.FormattingEnabled = true;
            this.employeeGenderCombo.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.employeeGenderCombo.Location = new System.Drawing.Point(749, 95);
            this.employeeGenderCombo.Margin = new System.Windows.Forms.Padding(4);
            this.employeeGenderCombo.Name = "employeeGenderCombo";
            this.employeeGenderCombo.Size = new System.Drawing.Size(236, 31);
            this.employeeGenderCombo.TabIndex = 19;
            // 
            // employeePermanent
            // 
            this.employeePermanent.Location = new System.Drawing.Point(749, 318);
            this.employeePermanent.Margin = new System.Windows.Forms.Padding(4);
            this.employeePermanent.Name = "employeePermanent";
            this.employeePermanent.Size = new System.Drawing.Size(236, 32);
            this.employeePermanent.TabIndex = 18;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(518, 316);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(226, 32);
            this.label11.TabIndex = 17;
            this.label11.Text = "Permanent Address:";
            // 
            // employeePresent
            // 
            this.employeePresent.Location = new System.Drawing.Point(748, 261);
            this.employeePresent.Margin = new System.Windows.Forms.Padding(4);
            this.employeePresent.Name = "employeePresent";
            this.employeePresent.Size = new System.Drawing.Size(236, 32);
            this.employeePresent.TabIndex = 16;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(553, 259);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(190, 32);
            this.label12.TabIndex = 15;
            this.label12.Text = "Present Address:";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(649, 204);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(151, 37);
            this.label10.TabIndex = 14;
            this.label10.Text = "Degree:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(589, 146);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(155, 32);
            this.label9.TabIndex = 13;
            this.label9.Text = "Blood Group:";
            // 
            // employeeNation
            // 
            this.employeeNation.Location = new System.Drawing.Point(749, 377);
            this.employeeNation.Margin = new System.Windows.Forms.Padding(4);
            this.employeeNation.Name = "employeeNation";
            this.employeeNation.Size = new System.Drawing.Size(236, 32);
            this.employeeNation.TabIndex = 12;
            this.employeeNation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.employeeNation_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(608, 375);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 32);
            this.label7.TabIndex = 11;
            this.label7.Text = "Nationality:";
            // 
            // employeeReligion
            // 
            this.employeeReligion.Location = new System.Drawing.Point(192, 421);
            this.employeeReligion.Margin = new System.Windows.Forms.Padding(4);
            this.employeeReligion.Name = "employeeReligion";
            this.employeeReligion.Size = new System.Drawing.Size(236, 32);
            this.employeeReligion.TabIndex = 10;
            this.employeeReligion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.employeeReligion_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(81, 418);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 32);
            this.label8.TabIndex = 9;
            this.label8.Text = "Religion:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(119, 363);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 32);
            this.label6.TabIndex = 7;
            this.label6.Text = "DOB:";
            // 
            // employeeMName
            // 
            this.employeeMName.Location = new System.Drawing.Point(192, 310);
            this.employeeMName.Margin = new System.Windows.Forms.Padding(4);
            this.employeeMName.Name = "employeeMName";
            this.employeeMName.Size = new System.Drawing.Size(236, 32);
            this.employeeMName.TabIndex = 6;
            this.employeeMName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.employeeMName_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 308);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(180, 32);
            this.label5.TabIndex = 5;
            this.label5.Text = "Mothers Name:";
            // 
            // employeeFName
            // 
            this.employeeFName.Location = new System.Drawing.Point(192, 259);
            this.employeeFName.Margin = new System.Windows.Forms.Padding(4);
            this.employeeFName.Name = "employeeFName";
            this.employeeFName.Size = new System.Drawing.Size(236, 32);
            this.employeeFName.TabIndex = 4;
            this.employeeFName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.employeeFName_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(21, 256);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(167, 32);
            this.label4.TabIndex = 3;
            this.label4.Text = "Fathers Name:";
            // 
            // employeeName
            // 
            this.employeeName.Location = new System.Drawing.Point(192, 149);
            this.employeeName.Margin = new System.Windows.Forms.Padding(4);
            this.employeeName.Name = "employeeName";
            this.employeeName.Size = new System.Drawing.Size(236, 32);
            this.employeeName.TabIndex = 2;
            this.employeeName.TextChanged += new System.EventHandler(this.employeeName_TextChanged);
            this.employeeName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.employeeName_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(646, 91);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 32);
            this.label3.TabIndex = 1;
            this.label3.Text = "Gender:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(103, 146);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 32);
            this.label2.TabIndex = 0;
            this.label2.Text = "Name:";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1324, 585);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 31);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 36;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.jdate);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.employeeDOB);
            this.groupBox1.Controls.Add(this.phone);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.email);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.employeeIdText);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.employeeDegree);
            this.groupBox1.Controls.Add(this.employeePassText);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.employeeBGCombo);
            this.groupBox1.Controls.Add(this.employeeGenderCombo);
            this.groupBox1.Controls.Add(this.employeePermanent);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.employeePresent);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.employeeNation);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.employeeReligion);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.employeeMName);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.employeeFName);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.employeeName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(9, 85);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox1.Size = new System.Drawing.Size(1019, 482);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Admin Information";
            // 
            // jdate
            // 
            this.jdate.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jdate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.jdate.Location = new System.Drawing.Point(748, 431);
            this.jdate.Name = "jdate";
            this.jdate.Size = new System.Drawing.Size(233, 32);
            this.jdate.TabIndex = 44;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(589, 429);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(154, 32);
            this.label22.TabIndex = 43;
            this.label22.Text = "Joining Date:";
            // 
            // employeeDOB
            // 
            this.employeeDOB.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeDOB.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.employeeDOB.Location = new System.Drawing.Point(192, 365);
            this.employeeDOB.Name = "employeeDOB";
            this.employeeDOB.Size = new System.Drawing.Size(233, 32);
            this.employeeDOB.TabIndex = 33;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Blue;
            this.label13.Location = new System.Drawing.Point(656, 2);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(186, 38);
            this.label13.TabIndex = 39;
            this.label13.Text = "ADD ADMIN";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tiffin);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.medical);
            this.groupBox2.Controls.Add(this.home);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.basic);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(1050, 85);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(314, 484);
            this.groupBox2.TabIndex = 42;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Salary";
            // 
            // tiffin
            // 
            this.tiffin.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tiffin.Location = new System.Drawing.Point(157, 300);
            this.tiffin.Margin = new System.Windows.Forms.Padding(4);
            this.tiffin.Multiline = true;
            this.tiffin.Name = "tiffin";
            this.tiffin.Size = new System.Drawing.Size(122, 41);
            this.tiffin.TabIndex = 46;
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(75, 304);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(75, 32);
            this.label17.TabIndex = 45;
            this.label17.Text = "Tiffin:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(47, 226);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(104, 32);
            this.label16.TabIndex = 44;
            this.label16.Text = "Medical:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.AllowDrop = true;
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(66, 141);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 32);
            this.label15.TabIndex = 43;
            this.label15.Text = "Home:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // medical
            // 
            this.medical.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.medical.Location = new System.Drawing.Point(157, 222);
            this.medical.Margin = new System.Windows.Forms.Padding(4);
            this.medical.Multiline = true;
            this.medical.Name = "medical";
            this.medical.Size = new System.Drawing.Size(122, 41);
            this.medical.TabIndex = 42;
            // 
            // home
            // 
            this.home.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.home.Location = new System.Drawing.Point(157, 138);
            this.home.Margin = new System.Windows.Forms.Padding(4);
            this.home.Multiline = true;
            this.home.Name = "home";
            this.home.Size = new System.Drawing.Size(122, 41);
            this.home.TabIndex = 41;
            // 
            // label21
            // 
            this.label21.AllowDrop = true;
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(79, 65);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(73, 32);
            this.label21.TabIndex = 39;
            this.label21.Text = "Basic:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // basic
            // 
            this.basic.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.basic.Location = new System.Drawing.Point(157, 62);
            this.basic.Margin = new System.Windows.Forms.Padding(4);
            this.basic.Multiline = true;
            this.basic.Name = "basic";
            this.basic.Size = new System.Drawing.Size(122, 41);
            this.basic.TabIndex = 40;
            // 
            // AddAdminUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "AddAdminUC";
            this.Size = new System.Drawing.Size(1587, 671);
            this.Load += new System.EventHandler(this.AddAdminUC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox phone;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox email;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox employeeIdText;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox employeeDegree;
        private System.Windows.Forms.TextBox employeePassText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox employeeBGCombo;
        private System.Windows.Forms.ComboBox employeeGenderCombo;
        private System.Windows.Forms.TextBox employeePermanent;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox employeePresent;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox employeeNation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox employeeReligion;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox employeeMName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox employeeFName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox employeeName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker employeeDOB;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tiffin;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox medical;
        private System.Windows.Forms.TextBox home;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox basic;
        private System.Windows.Forms.DateTimePicker jdate;
        private System.Windows.Forms.Label label22;
    }
}

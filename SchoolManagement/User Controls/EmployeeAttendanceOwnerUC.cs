﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class EmployeeAttendanceOwnerUC : UserControl
    {
        private AdminData ad = new AdminData();

        public EmployeeAttendanceOwnerUC()
        {
            InitializeComponent();
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void EmployeeAttendanceOwnerUC_Load(object sender, EventArgs e)
        {
            try
            {
                monthCombo.ValueMember = "month";
                monthCombo.DisplayMember = "month";
                monthCombo.DataSource = RemoveDuplicatesRecords(ad.GetAttendanceMonth());
                monthCombo.SelectedIndex = -1;

                yearCombo.ValueMember = "year";
                yearCombo.DisplayMember = "year";
                yearCombo.DataSource = RemoveDuplicatesRecords(ad.GetAttendanceYear());
                yearCombo.SelectedIndex = -1;

                attendanceGrid.DataSource = ad.EmployeeAttendanceByCurrentDate();
            }
            catch (Exception) { }
        }

        private void monthCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if((yearCombo.SelectedIndex == -1) && (monthCombo.SelectedIndex != -1))
            {
                dayCombo.DataSource = null;
            }
            else if((yearCombo.SelectedIndex != -1) && (monthCombo.SelectedIndex != -1))
            {
                try
                {
                    dayCombo.ValueMember = "day";
                    dayCombo.DisplayMember = "day";
                    dayCombo.DataSource = RemoveDuplicatesRecords(ad.GetAttendanceDay(monthCombo.SelectedValue.ToString(), yearCombo.SelectedValue.ToString()));
                    dayCombo.SelectedIndex = -1;
                }
                catch (Exception) { }
            }
        }

        private void dayCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if((yearCombo.SelectedIndex != -1) && (monthCombo.SelectedIndex != -1) && (dayCombo.SelectedIndex != -1))
            {
                try
                {
                    attendanceGrid.DataSource = ad.EmployeeAttendanceByDayMonYear(dayCombo.SelectedValue.ToString(), monthCombo.SelectedValue.ToString(), yearCombo.SelectedValue.ToString());
                }
                catch (Exception) { }
            }
            else
            {
                attendanceGrid.DataSource = null;
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if((yearCombo.SelectedIndex != -1) && (monthCombo.SelectedIndex != -1) && !string.IsNullOrWhiteSpace(empId.Text))
            {
                try
                {
                    attendanceGrid.DataSource = ad.EmployeeAttendanceByMonYearEmp(yearCombo.SelectedValue.ToString(), monthCombo.SelectedValue.ToString(), empId.Text);
                }
                catch (Exception) { }
            }
            else if((yearCombo.SelectedIndex != -1) && (monthCombo.SelectedIndex != -1) && !string.IsNullOrWhiteSpace(empId.Text) && (dayCombo.SelectedIndex != -1))
            {
                try
                {
                    attendanceGrid.DataSource = ad.EmployeeAttendanceByMonYearDayEmp(yearCombo.SelectedValue.ToString(), monthCombo.SelectedValue.ToString(), dayCombo.SelectedValue.ToString(), empId.Text);
                }
                catch (Exception) { }
            }
        }
    }
}

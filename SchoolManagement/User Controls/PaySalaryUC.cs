﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class PaySalaryUC : UserControl
    {
        private AccountantData ad = new AccountantData();

        public PaySalaryUC()
        {
            InitializeComponent();
        }

        private void PaySalaryUC_Load(object sender, EventArgs e)
        {
            CreateRef();
            year.Text = DateTime.Now.Year.ToString();

            tada.Text = incentive.Text = festival.Text = examGuard.Text = examScript.Text = 0.ToString();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {          
            try
            {
                DataTable dt = ad.GetSalaryInfo(empId.Text);

                basic.Text = dt.Rows[0][0].ToString();
                home.Text = dt.Rows[0][1].ToString();
                medical.Text = dt.Rows[0][2].ToString();
                tiffin.Text = dt.Rows[0][3].ToString();
            }
            catch (Exception) { }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            total.Text = (float.Parse(basic.Text) + float.Parse(home.Text) + float.Parse(medical.Text) + float.Parse(tiffin.Text) + float.Parse(tada.Text) + float.Parse(incentive.Text) + float.Parse(festival.Text) + float.Parse(examGuard.Text) + float.Parse(examScript.Text)).ToString();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            if(ad.InsertSalary(reference.Text, monthCombo.SelectedItem.ToString(), year.Text, empId.Text, float.Parse(basic.Text), float.Parse(home.Text), float.Parse(medical.Text), float.Parse(tiffin.Text), float.Parse(tada.Text), float.Parse(incentive.Text), float.Parse(festival.Text), float.Parse(examScript.Text), float.Parse(examGuard.Text), float.Parse(total.Text)))
            {
                MessageBox.Show("PAID!");
                empId.Text = basic.Text = home.Text = medical.Text = tiffin.Text = string.Empty;
                CreateRef();
            }
        }

        private void CreateRef()
        {
            reference.Text = "ref-" + DateTime.Now.Year.ToString() + "-" + (ad.GetLastId() + 1);
        }

        private void basic_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void home_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void year_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void medical_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void tiffin_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void tada_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void incentive_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void festival_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void examScript_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void total_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsNumber(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void label13_Click(object sender, EventArgs e)
        {

        }
    }
}

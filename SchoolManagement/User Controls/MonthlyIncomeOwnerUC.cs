﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class MonthlyIncomeOwnerUC : UserControl
    {
        private OwnerData od = new OwnerData();
        private LoginData lid = new LoginData();

        public MonthlyIncomeOwnerUC()
        {
            InitializeComponent();
        }

        private void MonthlySummeryUC_Load(object sender, EventArgs e)
        {
            try
            {
                yearCombo.ValueMember = "year";
                yearCombo.DisplayMember = "year";
                yearCombo.DataSource = RemoveDuplicatesRecords(lid.GetIncomeYear());
                yearCombo.SelectedIndex = -1;
            }
            catch (Exception) { }
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private void yearCombo_SelectedIndexChanged(object sender, EventArgs e)
        {           
            try
            {
                if(yearCombo.SelectedIndex == -1)
                {
                    incomeChart.Visible = false;
                }
                else
                {
                    if (yearCombo.SelectedIndex != -1)
                    {
                        DataTable dt = od.IncomeByMonth(yearCombo.SelectedValue.ToString());

                        incomeChart.Visible = true;
                        incomeChart.DataSource = dt;
                        incomeChart.Series["Series1"].XValueMember = "month";
                        incomeChart.Series["Series1"].YValueMembers = "Total";
                        incomeChart.Series["Series1"].IsValueShownAsLabel = true;
                        incomeChart.Titles.Add("");
                    }
                }
            }
            catch (Exception) { }
        }
    }
}

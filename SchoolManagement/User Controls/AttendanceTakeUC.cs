﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class AttendanceTakeUC : UserControl
    {
        private AttendanceTakerData atd = new AttendanceTakerData();

        public AttendanceTakeUC()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {           
            try
            {
                if (inCheck.Checked && outCheck.Checked)
                {
                    MessageBox.Show("PLEASE SELECT CORRECT ONE!");
                }
                else if (!string.IsNullOrWhiteSpace(empId.Text) && inCheck.Checked)
                {
                    if (atd.IsEmployeeExist(empId.Text))
                    {
                        if (atd.IfDayDataExist(empId.Text))
                        {
                            if (atd.CheckStatus(empId.Text) == "enter")
                            {
                                MessageBox.Show("EMPLOYEE MUST OUT BEFORE ENTER!");                               
                            }
                            else
                            {
                                if (atd.InsertIn(empId.Text))
                                {
                                    MessageBox.Show("Done!");
                                }
                            }
                        }
                        else
                        {
                            if (atd.InsertIn(empId.Text))
                            {
                                MessageBox.Show("Done!");
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("INVALID ID!");
                    }
                }
                else if (!string.IsNullOrWhiteSpace(empId.Text) && outCheck.Checked)
                {
                    if (atd.IsEmployeeExist(empId.Text))
                    {
                        if (atd.CheckStatus(empId.Text) == "out")
                        {
                            MessageBox.Show("EMPLOYEE MUST ENTER BEFORE OUT!");                            
                        }
                        else
                        {
                            if (atd.InsertOut(empId.Text))
                            {
                                MessageBox.Show("Done!");
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("INVALID ID!");
                    }
                }
                else
                {
                    MessageBox.Show("PLEASE GIVE ALL INPUT CORRECTLY!");
                }
            }
            catch(Exception)
            {
                MessageBox.Show("SOMETHING WENT WRONG! PLEASE TRY AGAIN");
            }
        }

        private void Clear()
        {
            empId.Text = "";
            inCheck.Checked = false;
            outCheck.Checked = false;
        }
    }
}

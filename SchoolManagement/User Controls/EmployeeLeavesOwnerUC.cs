﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolManagement.Data_Layer;

namespace SchoolManagement.User_Controls
{
    public partial class EmployeeLeavesOwnerUC : UserControl
    {
        private AdminData ad = new AdminData();

        public EmployeeLeavesOwnerUC()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(year.Text) && string.IsNullOrWhiteSpace(employeeId.Text))
            {
                try
                {
                    leaveGrid.DataSource = ad.GetLeaveDataByYear(year.Text);
                }
                catch (Exception) { }
            }
            else if(!string.IsNullOrWhiteSpace(year.Text) && !string.IsNullOrWhiteSpace(employeeId.Text))
            {              
                try
                {
                    leaveGrid.DataSource = ad.GetLeaveDataById(year.Text, employeeId.Text);
                }
                catch (Exception) { }
            }
            else
            {
                MessageBox.Show("FILL ALL REQUIRED INPUT!");
            }
        }
    }
}

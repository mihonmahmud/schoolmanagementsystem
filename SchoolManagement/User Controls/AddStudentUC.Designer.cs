﻿namespace SchoolManagement
{
    partial class AddStudentUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddStudentUC));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.gMail = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.gPhone = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.gProfession = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.gName = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dob = new System.Windows.Forms.DateTimePicker();
            this.stuId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.bgCombo = new System.Windows.Forms.ComboBox();
            this.genderCombo = new System.Windows.Forms.ComboBox();
            this.permanent = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.present = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.nation = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.religion = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.mName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.fName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.classCombo = new MetroFramework.Controls.MetroComboBox();
            this.secCombo = new MetroFramework.Controls.MetroComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.capLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label18 = new System.Windows.Forms.Label();
            this.sessionCombo = new MetroFramework.Controls.MetroComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.position = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.gMail);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.gPhone);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.gProfession);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.gName);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(1089, 161);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(456, 403);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Guardian Information";
            // 
            // gMail
            // 
            this.gMail.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gMail.Location = new System.Drawing.Point(182, 262);
            this.gMail.Margin = new System.Windows.Forms.Padding(4);
            this.gMail.Name = "gMail";
            this.gMail.Size = new System.Drawing.Size(236, 32);
            this.gMail.TabIndex = 14;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(102, 261);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(129, 32);
            this.label16.TabIndex = 13;
            this.label16.Text = "Email:";
            // 
            // gPhone
            // 
            this.gPhone.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gPhone.Location = new System.Drawing.Point(182, 213);
            this.gPhone.Margin = new System.Windows.Forms.Padding(4);
            this.gPhone.Name = "gPhone";
            this.gPhone.Size = new System.Drawing.Size(236, 32);
            this.gPhone.TabIndex = 12;
            this.gPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gPhone_KeyPress);
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(54, 212);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(154, 41);
            this.label13.TabIndex = 11;
            this.label13.Text = "Phone No:";
            // 
            // gProfession
            // 
            this.gProfession.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gProfession.Location = new System.Drawing.Point(182, 159);
            this.gProfession.Margin = new System.Windows.Forms.Padding(4);
            this.gProfession.Name = "gProfession";
            this.gProfession.Size = new System.Drawing.Size(236, 32);
            this.gProfession.TabIndex = 10;
            this.gProfession.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gProfession_KeyPress);
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(50, 158);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(174, 35);
            this.label14.TabIndex = 9;
            this.label14.Text = "Profession:";
            // 
            // gName
            // 
            this.gName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gName.Location = new System.Drawing.Point(180, 102);
            this.gName.Margin = new System.Windows.Forms.Padding(4);
            this.gName.Name = "gName";
            this.gName.Size = new System.Drawing.Size(236, 32);
            this.gName.TabIndex = 8;
            this.gName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gName_KeyPress);
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(97, 101);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(131, 36);
            this.label15.TabIndex = 7;
            this.label15.Text = "Name:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dob);
            this.groupBox1.Controls.Add(this.stuId);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.bgCombo);
            this.groupBox1.Controls.Add(this.genderCombo);
            this.groupBox1.Controls.Add(this.permanent);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.present);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.nation);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.religion);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.mName);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.fName);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.name);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(84, 161);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox1.Size = new System.Drawing.Size(986, 454);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Student Information";
            // 
            // dob
            // 
            this.dob.CalendarTrailingForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dob.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dob.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dob.Location = new System.Drawing.Point(192, 318);
            this.dob.Name = "dob";
            this.dob.Size = new System.Drawing.Size(236, 32);
            this.dob.TabIndex = 24;
            // 
            // stuId
            // 
            this.stuId.Enabled = false;
            this.stuId.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stuId.Location = new System.Drawing.Point(192, 98);
            this.stuId.Margin = new System.Windows.Forms.Padding(4);
            this.stuId.Name = "stuId";
            this.stuId.Size = new System.Drawing.Size(236, 32);
            this.stuId.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(147, 95);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 32);
            this.label1.TabIndex = 22;
            this.label1.Text = "ID:";
            // 
            // bgCombo
            // 
            this.bgCombo.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bgCombo.FormattingEnabled = true;
            this.bgCombo.Items.AddRange(new object[] {
            "Apos",
            "Aneg",
            "Bpos",
            "Bneg",
            "ABpos",
            "ABneg",
            "Opos",
            "Oneg"});
            this.bgCombo.Location = new System.Drawing.Point(710, 149);
            this.bgCombo.Margin = new System.Windows.Forms.Padding(4);
            this.bgCombo.Name = "bgCombo";
            this.bgCombo.Size = new System.Drawing.Size(236, 31);
            this.bgCombo.TabIndex = 20;
            // 
            // genderCombo
            // 
            this.genderCombo.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.genderCombo.FormattingEnabled = true;
            this.genderCombo.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.genderCombo.Location = new System.Drawing.Point(711, 95);
            this.genderCombo.Margin = new System.Windows.Forms.Padding(4);
            this.genderCombo.Name = "genderCombo";
            this.genderCombo.Size = new System.Drawing.Size(236, 31);
            this.genderCombo.TabIndex = 19;
            // 
            // permanent
            // 
            this.permanent.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.permanent.Location = new System.Drawing.Point(710, 297);
            this.permanent.Margin = new System.Windows.Forms.Padding(4);
            this.permanent.Multiline = true;
            this.permanent.Name = "permanent";
            this.permanent.Size = new System.Drawing.Size(236, 50);
            this.permanent.TabIndex = 18;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(479, 304);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(276, 38);
            this.label11.TabIndex = 17;
            this.label11.Text = "Permanent Address:";
            // 
            // present
            // 
            this.present.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.present.Location = new System.Drawing.Point(710, 213);
            this.present.Margin = new System.Windows.Forms.Padding(4);
            this.present.Multiline = true;
            this.present.Name = "present";
            this.present.Size = new System.Drawing.Size(236, 50);
            this.present.TabIndex = 16;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(515, 218);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(251, 38);
            this.label12.TabIndex = 15;
            this.label12.Text = "Present Address:";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(551, 145);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(201, 38);
            this.label9.TabIndex = 13;
            this.label9.Text = "Blood Group:";
            // 
            // nation
            // 
            this.nation.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nation.Location = new System.Drawing.Point(711, 377);
            this.nation.Margin = new System.Windows.Forms.Padding(4);
            this.nation.Name = "nation";
            this.nation.Size = new System.Drawing.Size(236, 32);
            this.nation.TabIndex = 12;
            this.nation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nation_KeyPress);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(569, 374);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(224, 37);
            this.label7.TabIndex = 11;
            this.label7.Text = "Nationality:";
            // 
            // religion
            // 
            this.religion.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.religion.Location = new System.Drawing.Point(192, 372);
            this.religion.Margin = new System.Windows.Forms.Padding(4);
            this.religion.Name = "religion";
            this.religion.Size = new System.Drawing.Size(236, 32);
            this.religion.TabIndex = 10;
            this.religion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.religion_KeyPress);
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(82, 369);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(156, 34);
            this.label8.TabIndex = 9;
            this.label8.Text = "Religion:";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(120, 316);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 30);
            this.label6.TabIndex = 7;
            this.label6.Text = "DOB:";
            // 
            // mName
            // 
            this.mName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mName.Location = new System.Drawing.Point(192, 261);
            this.mName.Margin = new System.Windows.Forms.Padding(4);
            this.mName.Name = "mName";
            this.mName.Size = new System.Drawing.Size(236, 32);
            this.mName.TabIndex = 6;
            this.mName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mName_KeyPress);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 258);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(209, 38);
            this.label5.TabIndex = 5;
            this.label5.Text = "Mothers Name:";
            // 
            // fName
            // 
            this.fName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fName.Location = new System.Drawing.Point(192, 210);
            this.fName.Margin = new System.Windows.Forms.Padding(4);
            this.fName.Name = "fName";
            this.fName.Size = new System.Drawing.Size(236, 32);
            this.fName.TabIndex = 4;
            this.fName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.fName_KeyPress);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(24, 207);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(207, 35);
            this.label4.TabIndex = 3;
            this.label4.Text = "Fathers Name:";
            // 
            // name
            // 
            this.name.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name.Location = new System.Drawing.Point(192, 153);
            this.name.Margin = new System.Windows.Forms.Padding(4);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(236, 32);
            this.name.TabIndex = 2;
            this.name.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.name_KeyPress);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(609, 90);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 36);
            this.label3.TabIndex = 1;
            this.label3.Text = "Gender:";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(107, 149);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 35);
            this.label2.TabIndex = 0;
            this.label2.Text = "Name:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(511, 103);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 32);
            this.label10.TabIndex = 22;
            this.label10.Text = "Class:";
            // 
            // classCombo
            // 
            this.classCombo.FormattingEnabled = true;
            this.classCombo.ItemHeight = 24;
            this.classCombo.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.classCombo.Location = new System.Drawing.Point(585, 107);
            this.classCombo.Margin = new System.Windows.Forms.Padding(4);
            this.classCombo.Name = "classCombo";
            this.classCombo.Size = new System.Drawing.Size(213, 30);
            this.classCombo.TabIndex = 23;
            this.classCombo.UseSelectable = true;
            this.classCombo.SelectedIndexChanged += new System.EventHandler(this.classCombo_SelectedIndexChanged);
            // 
            // secCombo
            // 
            this.secCombo.FormattingEnabled = true;
            this.secCombo.ItemHeight = 24;
            this.secCombo.Location = new System.Drawing.Point(902, 107);
            this.secCombo.Margin = new System.Windows.Forms.Padding(4);
            this.secCombo.Name = "secCombo";
            this.secCombo.Size = new System.Drawing.Size(213, 30);
            this.secCombo.TabIndex = 25;
            this.secCombo.UseSelectable = true;
            this.secCombo.SelectedIndexChanged += new System.EventHandler(this.secCombo_SelectedIndexChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(844, 103);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(57, 32);
            this.label17.TabIndex = 24;
            this.label17.Text = "Sec:";
            // 
            // capLabel
            // 
            this.capLabel.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.capLabel.Location = new System.Drawing.Point(1498, 74);
            this.capLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.capLabel.Name = "capLabel";
            this.capLabel.Size = new System.Drawing.Size(87, 49);
            this.capLabel.TabIndex = 26;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1445, 584);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 31);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 34;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1502, 584);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 31);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 33;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(131, 103);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(100, 32);
            this.label18.TabIndex = 35;
            this.label18.Text = "Session:";
            // 
            // sessionCombo
            // 
            this.sessionCombo.FormattingEnabled = true;
            this.sessionCombo.ItemHeight = 24;
            this.sessionCombo.Location = new System.Drawing.Point(231, 107);
            this.sessionCombo.Margin = new System.Windows.Forms.Padding(4);
            this.sessionCombo.Name = "sessionCombo";
            this.sessionCombo.Size = new System.Drawing.Size(213, 30);
            this.sessionCombo.TabIndex = 36;
            this.sessionCombo.UseSelectable = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Blue;
            this.label19.Location = new System.Drawing.Point(710, 11);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(197, 34);
            this.label19.TabIndex = 41;
            this.label19.Text = "ADD STUDENT";
            // 
            // position
            // 
            this.position.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.position.Location = new System.Drawing.Point(1254, 107);
            this.position.Margin = new System.Windows.Forms.Padding(4);
            this.position.Name = "position";
            this.position.Size = new System.Drawing.Size(97, 34);
            this.position.TabIndex = 43;
            this.position.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.position_KeyPress);
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(1148, 104);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(149, 35);
            this.label20.TabIndex = 42;
            this.label20.Text = "Position:";
            // 
            // AddStudentUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.position);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.sessionCombo);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.capLabel);
            this.Controls.Add(this.secCombo);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.classCombo);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "AddStudentUC";
            this.Size = new System.Drawing.Size(1650, 700);
            this.Load += new System.EventHandler(this.AddStudentUC_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox gMail;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox gPhone;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox gProfession;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox gName;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox bgCombo;
        private System.Windows.Forms.ComboBox genderCombo;
        private System.Windows.Forms.TextBox permanent;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox present;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox nation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox religion;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox mName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox fName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox stuId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private MetroFramework.Controls.MetroComboBox classCombo;
        private MetroFramework.Controls.MetroComboBox secCombo;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label capLabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label18;
        private MetroFramework.Controls.MetroComboBox sessionCombo;
        private System.Windows.Forms.DateTimePicker dob;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox position;
        private System.Windows.Forms.Label label20;
    }
}

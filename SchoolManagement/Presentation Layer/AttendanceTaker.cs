﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using SchoolManagement.User_Controls;

namespace SchoolManagement.Presentation_Layer
{
    public partial class AttendanceTaker : MetroForm
    {
        private string _userId;

        public AttendanceTaker(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            
        }

        private void employeesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in attendanceTakerPanel.Controls)
                ct.Dispose();

            attendanceTakerPanel.Controls.Add(new AttendanceTakeUC());
        }

        private void attendanceRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in attendanceTakerPanel.Controls)
                ct.Dispose();

            attendanceTakerPanel.Controls.Add(new OwnAttendanceUC(_userId));
        }

        private void leaveRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in attendanceTakerPanel.Controls)
                ct.Dispose();

            attendanceTakerPanel.Controls.Add(new OwnLeaveUC(_userId));
        }

        private void paymentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in attendanceTakerPanel.Controls)
                ct.Dispose();

            attendanceTakerPanel.Controls.Add(new OwnPaymentUC(_userId));
        }

        private void sendMailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in attendanceTakerPanel.Controls)
                ct.Dispose();

            attendanceTakerPanel.Controls.Add(new SendMailUC(_userId));
        }

        private void receivedMailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in attendanceTakerPanel.Controls)
                ct.Dispose();

            attendanceTakerPanel.Controls.Add(new ReceivedMailUC(_userId));
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            new LogIn().Show();
        }

        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            foreach (Control ct in attendanceTakerPanel.Controls)
                ct.Dispose();

            attendanceTakerPanel.Controls.Add(new UserProfileUC(_userId));
        }

        private void AttendanceTaker_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}

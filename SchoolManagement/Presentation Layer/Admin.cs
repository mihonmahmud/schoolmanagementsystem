﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using SchoolManagement.User_Controls;

namespace SchoolManagement
{
    public partial class Admin : MetroForm
    {
        private string _userId;
        public Admin()
        {
            InitializeComponent();
        }
        public Admin(string userId)
        {
            InitializeComponent();
            _userId = userId;
            adminPanel.Location = new Point(5, 97);
            adminPanel.Width = 1690;
            adminPanel.Height = 780;

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            LogIn li = new LogIn();
            li.Show();
            this.Hide();
        }

        private void addEmployeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new AddEmployeeUC());
        }

        private void manageEmployeeDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new ManageEmployeeDetailsUC());
        }

        private void assignSubjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new AssignSubjectsUC());
        }

        private void studentDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new OverStudentDetailsUC());
        }

        private void profileDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new StudentDetailsUC());
        }

        private void attendanceDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new StudentAttendanceDetailsUC());
        }

        private void examDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new ExamDetailsUC());
        }

        private void addNewSubjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new AddSubjectUC());
        }

        private void manageSubjectDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new ManageSubjectUC());
        }

        private void libraryDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new SearchBookUC());
        }

        private void viewEmployeeAttendanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new EmployeeAttendanceOwnerUC());
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new UserProfileUC(_userId));
        }

        private void createScheduleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void createToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new AddSectionUC());
        }

        private void manageScheduleToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void createScheduleToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new AdminTermExamUC());
        }

        private void sendMailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new SendMailUC(_userId));
        }

        private void receivedMailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new ReceivedMailUC(_userId));
        }

        private void updateFeesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new UpdateFeesUC());
        }

        private void attendanceRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new OwnAttendanceUC(_userId));
        }

        private void leaveRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new OwnLeaveUC(_userId));
        }

        private void paymentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new OwnPaymentUC(_userId));
        }

        private void monthlyExpenseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new MonthlyExpenseUC());
        }

        private void createScheduleToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new AdminAdmissionUC());
        }

        private void incomeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void applicantsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new ApplicantsUC());
        }

        private void regularStudentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new IncomeUC());
        }

        private void applicantsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new ApplicantsIncomeUC());
        }

        private void unpaidToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new UnpaidUC());
        }

        private void paymentDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new PayDetailsUC());
        }

        private void manageScheduleToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new ManageAdmissionScheduleUC());
        }

        private void manageScheduleToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new ManageTermScheduleUC());
        }

        private void manageScholarShipToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new ManageScholarUC());
        }

        private void manageStudentInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new ManageStudentInfoUC());
        }

        private void unassignedSubjectsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new UnassignedUC());
        }

        private void examScheduleToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new EmployeeExamScheduleUC());
        }

        private void manageEmployeeLeavesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new ManageEmployeeLeaveUC(_userId));
        }

        private void employeeClassScheduleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new TeachersScheduleUC());
        }

        private void borrowHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new BorrowHistoryUC());
        }

        private void Admin_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void attendanceToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void leaveRequestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in adminPanel.Controls)
                ct.Dispose();

            adminPanel.Controls.Add(new LeaveRequestUC(_userId));
        }
    }
}

﻿namespace SchoolManagement
{
    partial class Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Admin));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.employeesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addEmployeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageEmployeeDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewEmployeeAttendanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageEmployeeLeavesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assignSubjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeClassScheduleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.examScheduleToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.studentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageStudentInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.specificStudentDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profileDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attendanceDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.examDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applicantsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unpaidToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subjectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewSubjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageSubjectDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unassignedSubjectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.examScheduleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.termToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createScheduleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageScheduleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.admissionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createScheduleToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.manageScheduleToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.attendanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attendanceRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leaveRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leaveRequestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.libraryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.libraryDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.financialsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateFeesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incomeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.regularStudentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applicantsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.monthlyExpenseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageScholarShipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendMailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.receivedMailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.adminPanel = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.menuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.employeesToolStripMenuItem,
            this.studentsToolStripMenuItem,
            this.subjectsToolStripMenuItem,
            this.examScheduleToolStripMenuItem,
            this.attendanceToolStripMenuItem,
            this.paymentToolStripMenuItem,
            this.libraryToolStripMenuItem,
            this.financialsToolStripMenuItem,
            this.mailToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(1, 34);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1697, 43);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // employeesToolStripMenuItem
            // 
            this.employeesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addEmployeToolStripMenuItem,
            this.manageEmployeeDetailsToolStripMenuItem,
            this.viewEmployeeAttendanceToolStripMenuItem,
            this.manageEmployeeLeavesToolStripMenuItem,
            this.assignSubjectToolStripMenuItem,
            this.employeeClassScheduleToolStripMenuItem,
            this.examScheduleToolStripMenuItem1});
            this.employeesToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeesToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.employeesToolStripMenuItem.Name = "employeesToolStripMenuItem";
            this.employeesToolStripMenuItem.Size = new System.Drawing.Size(151, 39);
            this.employeesToolStripMenuItem.Text = "Employees";
            // 
            // addEmployeToolStripMenuItem
            // 
            this.addEmployeToolStripMenuItem.Name = "addEmployeToolStripMenuItem";
            this.addEmployeToolStripMenuItem.Size = new System.Drawing.Size(282, 36);
            this.addEmployeToolStripMenuItem.Text = "Add Employee";
            this.addEmployeToolStripMenuItem.Click += new System.EventHandler(this.addEmployeToolStripMenuItem_Click);
            // 
            // manageEmployeeDetailsToolStripMenuItem
            // 
            this.manageEmployeeDetailsToolStripMenuItem.Name = "manageEmployeeDetailsToolStripMenuItem";
            this.manageEmployeeDetailsToolStripMenuItem.Size = new System.Drawing.Size(282, 36);
            this.manageEmployeeDetailsToolStripMenuItem.Text = "Manage Details";
            this.manageEmployeeDetailsToolStripMenuItem.Click += new System.EventHandler(this.manageEmployeeDetailsToolStripMenuItem_Click);
            // 
            // viewEmployeeAttendanceToolStripMenuItem
            // 
            this.viewEmployeeAttendanceToolStripMenuItem.Name = "viewEmployeeAttendanceToolStripMenuItem";
            this.viewEmployeeAttendanceToolStripMenuItem.Size = new System.Drawing.Size(282, 36);
            this.viewEmployeeAttendanceToolStripMenuItem.Text = "Attendance";
            this.viewEmployeeAttendanceToolStripMenuItem.Click += new System.EventHandler(this.viewEmployeeAttendanceToolStripMenuItem_Click);
            // 
            // manageEmployeeLeavesToolStripMenuItem
            // 
            this.manageEmployeeLeavesToolStripMenuItem.Name = "manageEmployeeLeavesToolStripMenuItem";
            this.manageEmployeeLeavesToolStripMenuItem.Size = new System.Drawing.Size(282, 36);
            this.manageEmployeeLeavesToolStripMenuItem.Text = "Manage Leaves";
            this.manageEmployeeLeavesToolStripMenuItem.Click += new System.EventHandler(this.manageEmployeeLeavesToolStripMenuItem_Click);
            // 
            // assignSubjectToolStripMenuItem
            // 
            this.assignSubjectToolStripMenuItem.Name = "assignSubjectToolStripMenuItem";
            this.assignSubjectToolStripMenuItem.Size = new System.Drawing.Size(282, 36);
            this.assignSubjectToolStripMenuItem.Text = "Assign Subjects";
            this.assignSubjectToolStripMenuItem.Click += new System.EventHandler(this.assignSubjectToolStripMenuItem_Click);
            // 
            // employeeClassScheduleToolStripMenuItem
            // 
            this.employeeClassScheduleToolStripMenuItem.Name = "employeeClassScheduleToolStripMenuItem";
            this.employeeClassScheduleToolStripMenuItem.Size = new System.Drawing.Size(282, 36);
            this.employeeClassScheduleToolStripMenuItem.Text = "Class Schedule";
            this.employeeClassScheduleToolStripMenuItem.Click += new System.EventHandler(this.employeeClassScheduleToolStripMenuItem_Click);
            // 
            // examScheduleToolStripMenuItem1
            // 
            this.examScheduleToolStripMenuItem1.Name = "examScheduleToolStripMenuItem1";
            this.examScheduleToolStripMenuItem1.Size = new System.Drawing.Size(282, 36);
            this.examScheduleToolStripMenuItem1.Text = "Exam Schedule";
            this.examScheduleToolStripMenuItem1.Click += new System.EventHandler(this.examScheduleToolStripMenuItem1_Click);
            // 
            // studentsToolStripMenuItem
            // 
            this.studentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manageStudentInformationToolStripMenuItem,
            this.studentDetailsToolStripMenuItem,
            this.specificStudentDetailsToolStripMenuItem,
            this.applicantsToolStripMenuItem,
            this.unpaidToolStripMenuItem,
            this.paymentDetailsToolStripMenuItem});
            this.studentsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentsToolStripMenuItem.Name = "studentsToolStripMenuItem";
            this.studentsToolStripMenuItem.Size = new System.Drawing.Size(128, 39);
            this.studentsToolStripMenuItem.Text = "Students";
            // 
            // manageStudentInformationToolStripMenuItem
            // 
            this.manageStudentInformationToolStripMenuItem.Name = "manageStudentInformationToolStripMenuItem";
            this.manageStudentInformationToolStripMenuItem.Size = new System.Drawing.Size(436, 36);
            this.manageStudentInformationToolStripMenuItem.Text = "Manage Student Information";
            this.manageStudentInformationToolStripMenuItem.Click += new System.EventHandler(this.manageStudentInformationToolStripMenuItem_Click);
            // 
            // studentDetailsToolStripMenuItem
            // 
            this.studentDetailsToolStripMenuItem.Name = "studentDetailsToolStripMenuItem";
            this.studentDetailsToolStripMenuItem.Size = new System.Drawing.Size(436, 36);
            this.studentDetailsToolStripMenuItem.Text = "Overall Student Details";
            this.studentDetailsToolStripMenuItem.Click += new System.EventHandler(this.studentDetailsToolStripMenuItem_Click);
            // 
            // specificStudentDetailsToolStripMenuItem
            // 
            this.specificStudentDetailsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.profileDetailsToolStripMenuItem,
            this.attendanceDetailsToolStripMenuItem,
            this.examDetailsToolStripMenuItem});
            this.specificStudentDetailsToolStripMenuItem.Name = "specificStudentDetailsToolStripMenuItem";
            this.specificStudentDetailsToolStripMenuItem.Size = new System.Drawing.Size(436, 36);
            this.specificStudentDetailsToolStripMenuItem.Text = "Specific Student Details";
            // 
            // profileDetailsToolStripMenuItem
            // 
            this.profileDetailsToolStripMenuItem.Name = "profileDetailsToolStripMenuItem";
            this.profileDetailsToolStripMenuItem.Size = new System.Drawing.Size(320, 36);
            this.profileDetailsToolStripMenuItem.Text = "Profile Details";
            this.profileDetailsToolStripMenuItem.Click += new System.EventHandler(this.profileDetailsToolStripMenuItem_Click);
            // 
            // attendanceDetailsToolStripMenuItem
            // 
            this.attendanceDetailsToolStripMenuItem.Name = "attendanceDetailsToolStripMenuItem";
            this.attendanceDetailsToolStripMenuItem.Size = new System.Drawing.Size(320, 36);
            this.attendanceDetailsToolStripMenuItem.Text = "Attendance Details";
            this.attendanceDetailsToolStripMenuItem.Click += new System.EventHandler(this.attendanceDetailsToolStripMenuItem_Click);
            // 
            // examDetailsToolStripMenuItem
            // 
            this.examDetailsToolStripMenuItem.Name = "examDetailsToolStripMenuItem";
            this.examDetailsToolStripMenuItem.Size = new System.Drawing.Size(320, 36);
            this.examDetailsToolStripMenuItem.Text = "Exam Details";
            this.examDetailsToolStripMenuItem.Click += new System.EventHandler(this.examDetailsToolStripMenuItem_Click);
            // 
            // applicantsToolStripMenuItem
            // 
            this.applicantsToolStripMenuItem.Name = "applicantsToolStripMenuItem";
            this.applicantsToolStripMenuItem.Size = new System.Drawing.Size(436, 36);
            this.applicantsToolStripMenuItem.Text = "Applicants";
            this.applicantsToolStripMenuItem.Click += new System.EventHandler(this.applicantsToolStripMenuItem_Click);
            // 
            // unpaidToolStripMenuItem
            // 
            this.unpaidToolStripMenuItem.Name = "unpaidToolStripMenuItem";
            this.unpaidToolStripMenuItem.Size = new System.Drawing.Size(436, 36);
            this.unpaidToolStripMenuItem.Text = "Unpaid";
            this.unpaidToolStripMenuItem.Click += new System.EventHandler(this.unpaidToolStripMenuItem_Click);
            // 
            // paymentDetailsToolStripMenuItem
            // 
            this.paymentDetailsToolStripMenuItem.Name = "paymentDetailsToolStripMenuItem";
            this.paymentDetailsToolStripMenuItem.Size = new System.Drawing.Size(436, 36);
            this.paymentDetailsToolStripMenuItem.Text = "Payment Details";
            this.paymentDetailsToolStripMenuItem.Click += new System.EventHandler(this.paymentDetailsToolStripMenuItem_Click);
            // 
            // subjectsToolStripMenuItem
            // 
            this.subjectsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewSubjectToolStripMenuItem,
            this.manageSubjectDetailsToolStripMenuItem,
            this.unassignedSubjectsToolStripMenuItem});
            this.subjectsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectsToolStripMenuItem.Name = "subjectsToolStripMenuItem";
            this.subjectsToolStripMenuItem.Size = new System.Drawing.Size(124, 39);
            this.subjectsToolStripMenuItem.Text = "Subjects";
            // 
            // addNewSubjectToolStripMenuItem
            // 
            this.addNewSubjectToolStripMenuItem.Name = "addNewSubjectToolStripMenuItem";
            this.addNewSubjectToolStripMenuItem.Size = new System.Drawing.Size(372, 36);
            this.addNewSubjectToolStripMenuItem.Text = "Add Subject";
            this.addNewSubjectToolStripMenuItem.Click += new System.EventHandler(this.addNewSubjectToolStripMenuItem_Click);
            // 
            // manageSubjectDetailsToolStripMenuItem
            // 
            this.manageSubjectDetailsToolStripMenuItem.Name = "manageSubjectDetailsToolStripMenuItem";
            this.manageSubjectDetailsToolStripMenuItem.Size = new System.Drawing.Size(372, 36);
            this.manageSubjectDetailsToolStripMenuItem.Text = "Manage Subject Details";
            this.manageSubjectDetailsToolStripMenuItem.Click += new System.EventHandler(this.manageSubjectDetailsToolStripMenuItem_Click);
            // 
            // unassignedSubjectsToolStripMenuItem
            // 
            this.unassignedSubjectsToolStripMenuItem.Name = "unassignedSubjectsToolStripMenuItem";
            this.unassignedSubjectsToolStripMenuItem.Size = new System.Drawing.Size(372, 36);
            this.unassignedSubjectsToolStripMenuItem.Text = "Unassigned Subjects";
            this.unassignedSubjectsToolStripMenuItem.Click += new System.EventHandler(this.unassignedSubjectsToolStripMenuItem_Click);
            // 
            // examScheduleToolStripMenuItem
            // 
            this.examScheduleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.termToolStripMenuItem,
            this.admissionToolStripMenuItem});
            this.examScheduleToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examScheduleToolStripMenuItem.Name = "examScheduleToolStripMenuItem";
            this.examScheduleToolStripMenuItem.Size = new System.Drawing.Size(199, 39);
            this.examScheduleToolStripMenuItem.Text = "Exam Schedule";
            // 
            // termToolStripMenuItem
            // 
            this.termToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createScheduleToolStripMenuItem,
            this.manageScheduleToolStripMenuItem});
            this.termToolStripMenuItem.Name = "termToolStripMenuItem";
            this.termToolStripMenuItem.Size = new System.Drawing.Size(224, 36);
            this.termToolStripMenuItem.Text = "Term";
            // 
            // createScheduleToolStripMenuItem
            // 
            this.createScheduleToolStripMenuItem.Name = "createScheduleToolStripMenuItem";
            this.createScheduleToolStripMenuItem.Size = new System.Drawing.Size(305, 36);
            this.createScheduleToolStripMenuItem.Text = "Create Schedule";
            this.createScheduleToolStripMenuItem.Click += new System.EventHandler(this.createScheduleToolStripMenuItem_Click_1);
            // 
            // manageScheduleToolStripMenuItem
            // 
            this.manageScheduleToolStripMenuItem.Name = "manageScheduleToolStripMenuItem";
            this.manageScheduleToolStripMenuItem.Size = new System.Drawing.Size(305, 36);
            this.manageScheduleToolStripMenuItem.Text = "Manage Schedule";
            this.manageScheduleToolStripMenuItem.Click += new System.EventHandler(this.manageScheduleToolStripMenuItem_Click_1);
            // 
            // admissionToolStripMenuItem
            // 
            this.admissionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createScheduleToolStripMenuItem1,
            this.manageScheduleToolStripMenuItem1});
            this.admissionToolStripMenuItem.Name = "admissionToolStripMenuItem";
            this.admissionToolStripMenuItem.Size = new System.Drawing.Size(224, 36);
            this.admissionToolStripMenuItem.Text = "Admission";
            // 
            // createScheduleToolStripMenuItem1
            // 
            this.createScheduleToolStripMenuItem1.Name = "createScheduleToolStripMenuItem1";
            this.createScheduleToolStripMenuItem1.Size = new System.Drawing.Size(305, 36);
            this.createScheduleToolStripMenuItem1.Text = "Create Schedule";
            this.createScheduleToolStripMenuItem1.Click += new System.EventHandler(this.createScheduleToolStripMenuItem1_Click);
            // 
            // manageScheduleToolStripMenuItem1
            // 
            this.manageScheduleToolStripMenuItem1.Name = "manageScheduleToolStripMenuItem1";
            this.manageScheduleToolStripMenuItem1.Size = new System.Drawing.Size(305, 36);
            this.manageScheduleToolStripMenuItem1.Text = "Manage Schedule";
            this.manageScheduleToolStripMenuItem1.Click += new System.EventHandler(this.manageScheduleToolStripMenuItem1_Click);
            // 
            // attendanceToolStripMenuItem
            // 
            this.attendanceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.attendanceRecordToolStripMenuItem,
            this.leaveRecordToolStripMenuItem,
            this.leaveRequestToolStripMenuItem});
            this.attendanceToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attendanceToolStripMenuItem.Name = "attendanceToolStripMenuItem";
            this.attendanceToolStripMenuItem.Size = new System.Drawing.Size(160, 39);
            this.attendanceToolStripMenuItem.Text = "Attendance";
            this.attendanceToolStripMenuItem.Click += new System.EventHandler(this.attendanceToolStripMenuItem_Click);
            // 
            // attendanceRecordToolStripMenuItem
            // 
            this.attendanceRecordToolStripMenuItem.Name = "attendanceRecordToolStripMenuItem";
            this.attendanceRecordToolStripMenuItem.Size = new System.Drawing.Size(322, 36);
            this.attendanceRecordToolStripMenuItem.Text = "Attendance Record";
            this.attendanceRecordToolStripMenuItem.Click += new System.EventHandler(this.attendanceRecordToolStripMenuItem_Click);
            // 
            // leaveRecordToolStripMenuItem
            // 
            this.leaveRecordToolStripMenuItem.Name = "leaveRecordToolStripMenuItem";
            this.leaveRecordToolStripMenuItem.Size = new System.Drawing.Size(322, 36);
            this.leaveRecordToolStripMenuItem.Text = "Leave Record";
            this.leaveRecordToolStripMenuItem.Click += new System.EventHandler(this.leaveRecordToolStripMenuItem_Click);
            // 
            // leaveRequestToolStripMenuItem
            // 
            this.leaveRequestToolStripMenuItem.Name = "leaveRequestToolStripMenuItem";
            this.leaveRequestToolStripMenuItem.Size = new System.Drawing.Size(322, 36);
            this.leaveRequestToolStripMenuItem.Text = "Leave Request";
            this.leaveRequestToolStripMenuItem.Click += new System.EventHandler(this.leaveRequestToolStripMenuItem_Click);
            // 
            // paymentToolStripMenuItem
            // 
            this.paymentToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paymentToolStripMenuItem.Name = "paymentToolStripMenuItem";
            this.paymentToolStripMenuItem.Size = new System.Drawing.Size(128, 39);
            this.paymentToolStripMenuItem.Text = "Payment";
            this.paymentToolStripMenuItem.Click += new System.EventHandler(this.paymentToolStripMenuItem_Click);
            // 
            // libraryToolStripMenuItem
            // 
            this.libraryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.libraryDetailsToolStripMenuItem});
            this.libraryToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.libraryToolStripMenuItem.Name = "libraryToolStripMenuItem";
            this.libraryToolStripMenuItem.Size = new System.Drawing.Size(110, 39);
            this.libraryToolStripMenuItem.Text = "Library";
            // 
            // libraryDetailsToolStripMenuItem
            // 
            this.libraryDetailsToolStripMenuItem.Name = "libraryDetailsToolStripMenuItem";
            this.libraryDetailsToolStripMenuItem.Size = new System.Drawing.Size(255, 36);
            this.libraryDetailsToolStripMenuItem.Text = "Search Books";
            this.libraryDetailsToolStripMenuItem.Click += new System.EventHandler(this.libraryDetailsToolStripMenuItem_Click);
            // 
            // financialsToolStripMenuItem
            // 
            this.financialsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.updateFeesToolStripMenuItem,
            this.incomeToolStripMenuItem,
            this.monthlyExpenseToolStripMenuItem,
            this.manageScholarShipToolStripMenuItem});
            this.financialsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.financialsToolStripMenuItem.Name = "financialsToolStripMenuItem";
            this.financialsToolStripMenuItem.Size = new System.Drawing.Size(141, 39);
            this.financialsToolStripMenuItem.Text = "Financials";
            // 
            // updateFeesToolStripMenuItem
            // 
            this.updateFeesToolStripMenuItem.Name = "updateFeesToolStripMenuItem";
            this.updateFeesToolStripMenuItem.Size = new System.Drawing.Size(336, 36);
            this.updateFeesToolStripMenuItem.Text = "Update Fees";
            this.updateFeesToolStripMenuItem.Click += new System.EventHandler(this.updateFeesToolStripMenuItem_Click);
            // 
            // incomeToolStripMenuItem
            // 
            this.incomeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.regularStudentsToolStripMenuItem,
            this.applicantsToolStripMenuItem1});
            this.incomeToolStripMenuItem.Name = "incomeToolStripMenuItem";
            this.incomeToolStripMenuItem.Size = new System.Drawing.Size(336, 36);
            this.incomeToolStripMenuItem.Text = "Income";
            this.incomeToolStripMenuItem.Click += new System.EventHandler(this.incomeToolStripMenuItem_Click);
            // 
            // regularStudentsToolStripMenuItem
            // 
            this.regularStudentsToolStripMenuItem.Name = "regularStudentsToolStripMenuItem";
            this.regularStudentsToolStripMenuItem.Size = new System.Drawing.Size(298, 36);
            this.regularStudentsToolStripMenuItem.Text = "Regular Students";
            this.regularStudentsToolStripMenuItem.Click += new System.EventHandler(this.regularStudentsToolStripMenuItem_Click);
            // 
            // applicantsToolStripMenuItem1
            // 
            this.applicantsToolStripMenuItem1.Name = "applicantsToolStripMenuItem1";
            this.applicantsToolStripMenuItem1.Size = new System.Drawing.Size(298, 36);
            this.applicantsToolStripMenuItem1.Text = "Applicants";
            this.applicantsToolStripMenuItem1.Click += new System.EventHandler(this.applicantsToolStripMenuItem1_Click);
            // 
            // monthlyExpenseToolStripMenuItem
            // 
            this.monthlyExpenseToolStripMenuItem.Name = "monthlyExpenseToolStripMenuItem";
            this.monthlyExpenseToolStripMenuItem.Size = new System.Drawing.Size(336, 36);
            this.monthlyExpenseToolStripMenuItem.Text = "Expense";
            this.monthlyExpenseToolStripMenuItem.Click += new System.EventHandler(this.monthlyExpenseToolStripMenuItem_Click);
            // 
            // manageScholarShipToolStripMenuItem
            // 
            this.manageScholarShipToolStripMenuItem.Name = "manageScholarShipToolStripMenuItem";
            this.manageScholarShipToolStripMenuItem.Size = new System.Drawing.Size(336, 36);
            this.manageScholarShipToolStripMenuItem.Text = "Manage ScholarShip";
            this.manageScholarShipToolStripMenuItem.Click += new System.EventHandler(this.manageScholarShipToolStripMenuItem_Click);
            // 
            // mailToolStripMenuItem
            // 
            this.mailToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sendMailToolStripMenuItem,
            this.receivedMailToolStripMenuItem});
            this.mailToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mailToolStripMenuItem.Name = "mailToolStripMenuItem";
            this.mailToolStripMenuItem.Size = new System.Drawing.Size(79, 39);
            this.mailToolStripMenuItem.Text = "Mail";
            // 
            // sendMailToolStripMenuItem
            // 
            this.sendMailToolStripMenuItem.Name = "sendMailToolStripMenuItem";
            this.sendMailToolStripMenuItem.Size = new System.Drawing.Size(262, 36);
            this.sendMailToolStripMenuItem.Text = "Send Mail";
            this.sendMailToolStripMenuItem.Click += new System.EventHandler(this.sendMailToolStripMenuItem_Click);
            // 
            // receivedMailToolStripMenuItem
            // 
            this.receivedMailToolStripMenuItem.Name = "receivedMailToolStripMenuItem";
            this.receivedMailToolStripMenuItem.Size = new System.Drawing.Size(262, 36);
            this.receivedMailToolStripMenuItem.Text = "Received Mail";
            this.receivedMailToolStripMenuItem.Click += new System.EventHandler(this.receivedMailToolStripMenuItem_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1569, 42);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 31);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1623, 42);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 31);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // adminPanel
            // 
            this.adminPanel.AutoSize = true;
            this.adminPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.adminPanel.Location = new System.Drawing.Point(12, 101);
            this.adminPanel.Margin = new System.Windows.Forms.Padding(4);
            this.adminPanel.Name = "adminPanel";
            this.adminPanel.Size = new System.Drawing.Size(0, 0);
            this.adminPanel.TabIndex = 5;
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1700, 900);
            this.Controls.Add(this.adminPanel);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Admin";
            this.Padding = new System.Windows.Forms.Padding(27, 74, 27, 25);
            this.Resizable = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Style = MetroFramework.MetroColorStyle.White;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Admin_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem employeesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addEmployeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageEmployeeDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewEmployeeAttendanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem assignSubjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subjectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewSubjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageSubjectDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem libraryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem financialsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentDetailsToolStripMenuItem;
        private System.Windows.Forms.Panel adminPanel;
        private System.Windows.Forms.ToolStripMenuItem specificStudentDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profileDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attendanceDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem examDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem libraryDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem examScheduleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem termToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createScheduleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageScheduleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem admissionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createScheduleToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem manageScheduleToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendMailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem receivedMailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateFeesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageEmployeeLeavesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attendanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attendanceRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leaveRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monthlyExpenseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem incomeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applicantsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem regularStudentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applicantsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem unpaidToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageScholarShipToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageStudentInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unassignedSubjectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeClassScheduleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem examScheduleToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem leaveRequestToolStripMenuItem;
    }
}
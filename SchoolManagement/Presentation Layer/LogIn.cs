﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using SchoolManagement.Presentation_Layer;
using SchoolManagement.Data_Layer;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Net;
using SchoolManagement.Helper;

namespace SchoolManagement
{
    public partial class LogIn : MetroForm
    {
        private LoginData ld;
        private Regex mRegxExpression = new Regex(@"^([a-zA-Z0-9_\-])([a-zA-Z0-9_\-\.]*)@(\[((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\.){3}|((([a-zA-Z0-9\-]+)\.)+))([a-zA-Z]{2,}|(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\])$");
        public LogIn()
        {
            InitializeComponent();
            ld = new LoginData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int status = ld.GetUserStatus(idText.Text, passText.Text);
            if(status == 1)
            {
                Owner or = new Owner(idText.Text);
                or.Show();
                this.Hide();
            }
            else if (status == 2)
            {
                Admin ad = new Admin(idText.Text);
                ad.Show();
                this.Hide();
            }
            else if (status == 3)
            {
                HeadTeacher ht = new HeadTeacher(idText.Text);
                ht.Show();
                this.Hide();
            }
            else if (status == 4)
            {
                Teacher ta = new Teacher(idText.Text);
                ta.Show();
                this.Hide();
            }
            else if (status == 5)
            {
                Accountant ac = new Accountant(idText.Text);
                ac.Show();
                this.Hide();
            }
            else if (status == 6)
            {
                Librarian li = new Librarian(idText.Text);
                li.Show();
                this.Hide();
            }
            else if (status == 7)
            {
                new AttendanceTaker(idText.Text).Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("WRONG USERID OR PASSWORD!");
            }
        }

        private void LogIn_Load(object sender, EventArgs e)
        {
            email.Visible = false;
            send.Visible = false;
            emailLabel.Visible = false;
        }

        private void send_Click(object sender, EventArgs e)
        {
            try
            {
                if (!mRegxExpression.IsMatch(email.Text.Trim()))
                {
                    MessageBox.Show("EMAIL ISN'T IN CORRECT FORMAT!");
                }
                else
                {
                    if (ld.IsEmailExist(email.Text))
                    {
                        if (Email.SendForgotMail(email.Text.Trim(), ld))
                        {
                            MessageBox.Show("PLEASE CHECK YOUR EMAIL!");
                            email.Visible = send.Visible = emailLabel.Visible = false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("INVALID EMAIL");
                    }
                }
            }
            catch (Exception) { }
        }

        private void label3_Click(object sender, EventArgs e)
        {
            email.Visible = send.Visible = emailLabel.Visible = true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using SchoolManagement.User_Controls;

namespace SchoolManagement
{
    public partial class Owner : MetroForm
    {
        private string _userId;

        public Owner(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        private void classesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void sendMailToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (Control ct in ownerPanel.Controls)
                ct.Dispose();

            ownerPanel.Controls.Add(new SendMailUC(_userId));
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            LogIn li = new LogIn();
            li.Show();
            this.Hide();
        }

        private void studentDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in ownerPanel.Controls)
                ct.Dispose();

            ownerPanel.Controls.Add(new OverStudentsUC());
        }

        private void receiveMailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in ownerPanel.Controls)
                ct.Dispose();

            ownerPanel.Controls.Add(new ReceivedMailUC(_userId));
        }

        private void employeeDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in ownerPanel.Controls)
                ct.Dispose();

            ownerPanel.Controls.Add(new OverEmployeesUC(_userId));
        }

        private void addAdminToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in ownerPanel.Controls)
                ct.Dispose();

            ownerPanel.Controls.Add(new AddAdminUC());
        }

        private void admissionDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in ownerPanel.Controls)
                ct.Dispose();

            ownerPanel.Controls.Add(new ApplicantsUC());
        }

        private void boardExamDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void monthlySummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in ownerPanel.Controls)
                ct.Dispose();

            ownerPanel.Controls.Add(new MonthlyIncomeOwnerUC());
        }

        private void monthlyExpenseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in ownerPanel.Controls)
                ct.Dispose();

            ownerPanel.Controls.Add(new MonthlyExpenseOwnerUC());
        }

        private void applicantsFeesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in ownerPanel.Controls)
                ct.Dispose();

            ownerPanel.Controls.Add(new ApplicantsFeesOwnerUC());
        }

        private void setSalaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in ownerPanel.Controls)
                ct.Dispose();

            ownerPanel.Controls.Add(new UpdateSalaryUC());
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            foreach (Control ct in ownerPanel.Controls)
                ct.Dispose();

            ownerPanel.Controls.Add(new UserProfileUC(_userId));
        }

        private void profitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in ownerPanel.Controls)
                ct.Dispose();

            ownerPanel.Controls.Add(new ProfitUC());
        }

        private void boardExamsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in ownerPanel.Controls)
                ct.Dispose();

            ownerPanel.Controls.Add(new BoardExamOwnerUC());
        }

        private void attendanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in ownerPanel.Controls)
                ct.Dispose();

            ownerPanel.Controls.Add(new EmployeeAttendanceOwnerUC());
        }

        private void adminLeaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in ownerPanel.Controls)
                ct.Dispose();

            ownerPanel.Controls.Add(new ManageEmployeeLeaveUC(_userId));
        }

        private void leaveRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in ownerPanel.Controls)
                ct.Dispose();

            ownerPanel.Controls.Add(new EmployeeLeavesOwnerUC());
        }

        private void Owner_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void paymentDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in ownerPanel.Controls)
                ct.Dispose();

            ownerPanel.Controls.Add(new EmployeePaymentDetailsOwnerUC());
        }
    }
}

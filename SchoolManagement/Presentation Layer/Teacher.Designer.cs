﻿namespace SchoolManagement
{
    partial class Teacher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Teacher));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.studentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.markAttendanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.markToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadExamMarksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateUploadedMarksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewStudentDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profileDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewAttendanceDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewExamDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewParentDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.classesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewClassScheduleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.examGuardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attendanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attendanceRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leaveRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leaveRequestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.libraryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchBooksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendMailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.receivedMailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.teacherPanel = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.menuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.studentsToolStripMenuItem,
            this.classesToolStripMenuItem,
            this.examGuardToolStripMenuItem,
            this.attendanceToolStripMenuItem,
            this.paymentToolStripMenuItem,
            this.libraryToolStripMenuItem,
            this.mailToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(-1, 36);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1702, 43);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // studentsToolStripMenuItem
            // 
            this.studentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.markAttendanceToolStripMenuItem,
            this.uploadExamMarksToolStripMenuItem,
            this.updateUploadedMarksToolStripMenuItem,
            this.viewStudentDetailsToolStripMenuItem,
            this.viewParentDetailsToolStripMenuItem});
            this.studentsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentsToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.studentsToolStripMenuItem.Name = "studentsToolStripMenuItem";
            this.studentsToolStripMenuItem.Size = new System.Drawing.Size(128, 39);
            this.studentsToolStripMenuItem.Text = "Students";
            // 
            // markAttendanceToolStripMenuItem
            // 
            this.markAttendanceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.markToolStripMenuItem,
            this.updateToolStripMenuItem,
            this.historyToolStripMenuItem});
            this.markAttendanceToolStripMenuItem.Name = "markAttendanceToolStripMenuItem";
            this.markAttendanceToolStripMenuItem.Size = new System.Drawing.Size(331, 36);
            this.markAttendanceToolStripMenuItem.Text = "Attendance";
            this.markAttendanceToolStripMenuItem.Click += new System.EventHandler(this.markAttendanceToolStripMenuItem_Click);
            // 
            // markToolStripMenuItem
            // 
            this.markToolStripMenuItem.Name = "markToolStripMenuItem";
            this.markToolStripMenuItem.Size = new System.Drawing.Size(188, 36);
            this.markToolStripMenuItem.Text = "Mark";
            this.markToolStripMenuItem.Click += new System.EventHandler(this.markToolStripMenuItem_Click);
            // 
            // updateToolStripMenuItem
            // 
            this.updateToolStripMenuItem.Name = "updateToolStripMenuItem";
            this.updateToolStripMenuItem.Size = new System.Drawing.Size(188, 36);
            this.updateToolStripMenuItem.Text = "Update";
            this.updateToolStripMenuItem.Click += new System.EventHandler(this.updateToolStripMenuItem_Click);
            // 
            // historyToolStripMenuItem
            // 
            this.historyToolStripMenuItem.Name = "historyToolStripMenuItem";
            this.historyToolStripMenuItem.Size = new System.Drawing.Size(188, 36);
            this.historyToolStripMenuItem.Text = "History";
            this.historyToolStripMenuItem.Click += new System.EventHandler(this.historyToolStripMenuItem_Click);
            // 
            // uploadExamMarksToolStripMenuItem
            // 
            this.uploadExamMarksToolStripMenuItem.Name = "uploadExamMarksToolStripMenuItem";
            this.uploadExamMarksToolStripMenuItem.Size = new System.Drawing.Size(331, 36);
            this.uploadExamMarksToolStripMenuItem.Text = "Upload Exam Marks";
            this.uploadExamMarksToolStripMenuItem.Click += new System.EventHandler(this.uploadExamMarksToolStripMenuItem_Click);
            // 
            // updateUploadedMarksToolStripMenuItem
            // 
            this.updateUploadedMarksToolStripMenuItem.Name = "updateUploadedMarksToolStripMenuItem";
            this.updateUploadedMarksToolStripMenuItem.Size = new System.Drawing.Size(331, 36);
            this.updateUploadedMarksToolStripMenuItem.Text = "Update Marks";
            this.updateUploadedMarksToolStripMenuItem.Click += new System.EventHandler(this.updateUploadedMarksToolStripMenuItem_Click);
            // 
            // viewStudentDetailsToolStripMenuItem
            // 
            this.viewStudentDetailsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.profileDetailsToolStripMenuItem,
            this.viewAttendanceDetailsToolStripMenuItem,
            this.viewExamDetailsToolStripMenuItem});
            this.viewStudentDetailsToolStripMenuItem.Name = "viewStudentDetailsToolStripMenuItem";
            this.viewStudentDetailsToolStripMenuItem.Size = new System.Drawing.Size(331, 36);
            this.viewStudentDetailsToolStripMenuItem.Text = "Student Details";
            this.viewStudentDetailsToolStripMenuItem.Click += new System.EventHandler(this.viewStudentDetailsToolStripMenuItem_Click);
            // 
            // profileDetailsToolStripMenuItem
            // 
            this.profileDetailsToolStripMenuItem.Name = "profileDetailsToolStripMenuItem";
            this.profileDetailsToolStripMenuItem.Size = new System.Drawing.Size(320, 36);
            this.profileDetailsToolStripMenuItem.Text = "Profile Details";
            this.profileDetailsToolStripMenuItem.Click += new System.EventHandler(this.profileDetailsToolStripMenuItem_Click);
            // 
            // viewAttendanceDetailsToolStripMenuItem
            // 
            this.viewAttendanceDetailsToolStripMenuItem.Name = "viewAttendanceDetailsToolStripMenuItem";
            this.viewAttendanceDetailsToolStripMenuItem.Size = new System.Drawing.Size(320, 36);
            this.viewAttendanceDetailsToolStripMenuItem.Text = "Attendance Details";
            this.viewAttendanceDetailsToolStripMenuItem.Click += new System.EventHandler(this.viewAttendanceDetailsToolStripMenuItem_Click);
            // 
            // viewExamDetailsToolStripMenuItem
            // 
            this.viewExamDetailsToolStripMenuItem.Name = "viewExamDetailsToolStripMenuItem";
            this.viewExamDetailsToolStripMenuItem.Size = new System.Drawing.Size(320, 36);
            this.viewExamDetailsToolStripMenuItem.Text = " Exam Details";
            this.viewExamDetailsToolStripMenuItem.Click += new System.EventHandler(this.viewExamDetailsToolStripMenuItem_Click);
            // 
            // viewParentDetailsToolStripMenuItem
            // 
            this.viewParentDetailsToolStripMenuItem.Name = "viewParentDetailsToolStripMenuItem";
            this.viewParentDetailsToolStripMenuItem.Size = new System.Drawing.Size(331, 36);
            this.viewParentDetailsToolStripMenuItem.Text = "Guardian Details";
            this.viewParentDetailsToolStripMenuItem.Click += new System.EventHandler(this.viewParentDetailsToolStripMenuItem_Click);
            // 
            // classesToolStripMenuItem
            // 
            this.classesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewClassScheduleToolStripMenuItem,
            this.studentsToolStripMenuItem1});
            this.classesToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.classesToolStripMenuItem.Name = "classesToolStripMenuItem";
            this.classesToolStripMenuItem.Size = new System.Drawing.Size(110, 39);
            this.classesToolStripMenuItem.Text = "Classes";
            // 
            // viewClassScheduleToolStripMenuItem
            // 
            this.viewClassScheduleToolStripMenuItem.Name = "viewClassScheduleToolStripMenuItem";
            this.viewClassScheduleToolStripMenuItem.Size = new System.Drawing.Size(265, 36);
            this.viewClassScheduleToolStripMenuItem.Text = "Own Schedule";
            this.viewClassScheduleToolStripMenuItem.Click += new System.EventHandler(this.viewClassScheduleToolStripMenuItem_Click);
            // 
            // studentsToolStripMenuItem1
            // 
            this.studentsToolStripMenuItem1.Name = "studentsToolStripMenuItem1";
            this.studentsToolStripMenuItem1.Size = new System.Drawing.Size(265, 36);
            this.studentsToolStripMenuItem1.Text = "Students";
            this.studentsToolStripMenuItem1.Click += new System.EventHandler(this.studentsToolStripMenuItem1_Click);
            // 
            // examGuardToolStripMenuItem
            // 
            this.examGuardToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examGuardToolStripMenuItem.Name = "examGuardToolStripMenuItem";
            this.examGuardToolStripMenuItem.Size = new System.Drawing.Size(167, 39);
            this.examGuardToolStripMenuItem.Text = "Exam Guard";
            this.examGuardToolStripMenuItem.Click += new System.EventHandler(this.examGuardToolStripMenuItem_Click);
            // 
            // attendanceToolStripMenuItem
            // 
            this.attendanceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.attendanceRecordToolStripMenuItem,
            this.leaveRecordToolStripMenuItem,
            this.leaveRequestToolStripMenuItem});
            this.attendanceToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attendanceToolStripMenuItem.Name = "attendanceToolStripMenuItem";
            this.attendanceToolStripMenuItem.Size = new System.Drawing.Size(160, 39);
            this.attendanceToolStripMenuItem.Text = "Attendance";
            // 
            // attendanceRecordToolStripMenuItem
            // 
            this.attendanceRecordToolStripMenuItem.Name = "attendanceRecordToolStripMenuItem";
            this.attendanceRecordToolStripMenuItem.Size = new System.Drawing.Size(322, 36);
            this.attendanceRecordToolStripMenuItem.Text = "Attendance Record";
            this.attendanceRecordToolStripMenuItem.Click += new System.EventHandler(this.attendanceRecordToolStripMenuItem_Click);
            // 
            // leaveRecordToolStripMenuItem
            // 
            this.leaveRecordToolStripMenuItem.Name = "leaveRecordToolStripMenuItem";
            this.leaveRecordToolStripMenuItem.Size = new System.Drawing.Size(322, 36);
            this.leaveRecordToolStripMenuItem.Text = "Leave Record";
            this.leaveRecordToolStripMenuItem.Click += new System.EventHandler(this.leaveRecordToolStripMenuItem_Click);
            // 
            // leaveRequestToolStripMenuItem
            // 
            this.leaveRequestToolStripMenuItem.Name = "leaveRequestToolStripMenuItem";
            this.leaveRequestToolStripMenuItem.Size = new System.Drawing.Size(322, 36);
            this.leaveRequestToolStripMenuItem.Text = "Leave Request";
            this.leaveRequestToolStripMenuItem.Click += new System.EventHandler(this.leaveRequestToolStripMenuItem_Click);
            // 
            // paymentToolStripMenuItem
            // 
            this.paymentToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paymentToolStripMenuItem.Name = "paymentToolStripMenuItem";
            this.paymentToolStripMenuItem.Size = new System.Drawing.Size(128, 39);
            this.paymentToolStripMenuItem.Text = "Payment";
            this.paymentToolStripMenuItem.Click += new System.EventHandler(this.paymentToolStripMenuItem_Click);
            // 
            // libraryToolStripMenuItem
            // 
            this.libraryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.searchBooksToolStripMenuItem});
            this.libraryToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.libraryToolStripMenuItem.Name = "libraryToolStripMenuItem";
            this.libraryToolStripMenuItem.Size = new System.Drawing.Size(110, 39);
            this.libraryToolStripMenuItem.Text = "Library";
            // 
            // searchBooksToolStripMenuItem
            // 
            this.searchBooksToolStripMenuItem.Name = "searchBooksToolStripMenuItem";
            this.searchBooksToolStripMenuItem.Size = new System.Drawing.Size(255, 36);
            this.searchBooksToolStripMenuItem.Text = "Search Books";
            this.searchBooksToolStripMenuItem.Click += new System.EventHandler(this.searchBooksToolStripMenuItem_Click);
            // 
            // mailToolStripMenuItem
            // 
            this.mailToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sendMailToolStripMenuItem,
            this.receivedMailToolStripMenuItem});
            this.mailToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mailToolStripMenuItem.Name = "mailToolStripMenuItem";
            this.mailToolStripMenuItem.Size = new System.Drawing.Size(79, 39);
            this.mailToolStripMenuItem.Text = "Mail";
            // 
            // sendMailToolStripMenuItem
            // 
            this.sendMailToolStripMenuItem.Name = "sendMailToolStripMenuItem";
            this.sendMailToolStripMenuItem.Size = new System.Drawing.Size(262, 36);
            this.sendMailToolStripMenuItem.Text = "Send Mail";
            this.sendMailToolStripMenuItem.Click += new System.EventHandler(this.sendMailToolStripMenuItem_Click);
            // 
            // receivedMailToolStripMenuItem
            // 
            this.receivedMailToolStripMenuItem.Name = "receivedMailToolStripMenuItem";
            this.receivedMailToolStripMenuItem.Size = new System.Drawing.Size(262, 36);
            this.receivedMailToolStripMenuItem.Text = "Received Mail";
            this.receivedMailToolStripMenuItem.Click += new System.EventHandler(this.receivedMailToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1640, 42);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 31);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1585, 42);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 31);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // teacherPanel
            // 
            this.teacherPanel.Location = new System.Drawing.Point(5, 109);
            this.teacherPanel.Margin = new System.Windows.Forms.Padding(4);
            this.teacherPanel.Name = "teacherPanel";
            this.teacherPanel.Size = new System.Drawing.Size(1690, 780);
            this.teacherPanel.TabIndex = 4;
            // 
            // Teacher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1700, 900);
            this.Controls.Add(this.teacherPanel);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Teacher";
            this.Padding = new System.Windows.Forms.Padding(27, 74, 27, 25);
            this.Resizable = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Style = MetroFramework.MetroColorStyle.White;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Teacher_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem studentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem markAttendanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uploadExamMarksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewStudentDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewParentDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem classesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewClassScheduleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem libraryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchBooksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendMailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem receivedMailToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel teacherPanel;
        private System.Windows.Forms.ToolStripMenuItem viewExamDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewAttendanceDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profileDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attendanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attendanceRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leaveRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem examGuardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leaveRequestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateUploadedMarksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem markToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentsToolStripMenuItem1;
    }
}
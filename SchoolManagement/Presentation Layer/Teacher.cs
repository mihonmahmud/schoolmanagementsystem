﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using SchoolManagement.User_Controls;
using SchoolManagement.Presentation_Layer;

namespace SchoolManagement
{
    public partial class Teacher : MetroForm
    {
        private string _userId;

        public Teacher(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            LogIn li = new LogIn();
            li.Show();
            this.Hide();
        }

        private void addStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new AddStudentUC());
        }

        private void sendMailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new SendMailUC(_userId));
        }

        private void searchBooksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new SearchBookUC());
        }

        private void borrowHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new BorrowHistoryUC());
        }

        private void viewStudentDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void viewParentDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new GuardianDetailsUC());
        }

        private void viewAttendanceDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new StudentAttendanceDetailsUC());
        }

        private void profileDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new StudentDetailsUC());
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new UserProfileUC(_userId));
        }

        private void uploadExamMarksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new UploadStudentMarks(_userId));
        }

        private void viewExamDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new ExamDetailsUC());
        }

        private void viewClassScheduleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new TeacherClassScheduleUC(_userId));
        }

        private void markAttendanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void receivedMailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new ReceivedMailUC(_userId));
        }

        private void paymentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new OwnPaymentUC(_userId));
        }

        private void attendanceRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new OwnAttendanceUC(_userId));
        }

        private void leaveRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new OwnLeaveUC(_userId));
        }

        private void examGuardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new ExamGuardUC(_userId));
        }

        private void updateAttendanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void browserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void browserToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new BrowserUC());
        }

        private void leaveRequestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new LeaveRequestUC(_userId));
        }

        private void Teacher_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void updateUploadedMarksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new UpdateMarksUC(_userId));
        }

        private void markToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new MarkStudentAttendanceUC(_userId));
        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new UpdateStudentAttendanceUC(_userId));
        }

        private void historyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new StudentAttendanceHistoryUC(_userId));
        }

        private void studentsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (Control ct in teacherPanel.Controls)
                ct.Dispose();

            teacherPanel.Controls.Add(new ClassStudentsUC(_userId));
        }
    }
}

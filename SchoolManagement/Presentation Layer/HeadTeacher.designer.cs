﻿namespace SchoolManagement
{
    partial class HeadTeacher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HeadTeacher));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.teachersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assignExaminerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.termToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.admissionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assignSubjectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scheduleDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.teacherDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.markAttendanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadExamMarksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewStudentDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewExamDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewAttendanceDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profileDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewParentDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.totalMarksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.totalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.byGradesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.classesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewClassScheduleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createSectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageSectionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subjectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subjectDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unassignedSubjectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gradingLevelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.libraryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchBooksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attendanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attendanceRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leaveRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leaveRequestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.boardExamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageBoardExamDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendMailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.receivedMailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.headteacherPanel = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.menuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.teachersToolStripMenuItem,
            this.studentsToolStripMenuItem,
            this.classesToolStripMenuItem,
            this.subjectsToolStripMenuItem,
            this.libraryToolStripMenuItem,
            this.attendanceToolStripMenuItem,
            this.paymentToolStripMenuItem,
            this.boardExamToolStripMenuItem,
            this.mailToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(3, 36);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1685, 43);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // teachersToolStripMenuItem
            // 
            this.teachersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.assignExaminerToolStripMenuItem,
            this.assignSubjectsToolStripMenuItem,
            this.scheduleDetailsToolStripMenuItem,
            this.teacherDetailsToolStripMenuItem});
            this.teachersToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teachersToolStripMenuItem.Name = "teachersToolStripMenuItem";
            this.teachersToolStripMenuItem.Size = new System.Drawing.Size(130, 39);
            this.teachersToolStripMenuItem.Text = "Teachers";
            // 
            // assignExaminerToolStripMenuItem
            // 
            this.assignExaminerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.termToolStripMenuItem,
            this.admissionToolStripMenuItem});
            this.assignExaminerToolStripMenuItem.Name = "assignExaminerToolStripMenuItem";
            this.assignExaminerToolStripMenuItem.Size = new System.Drawing.Size(376, 32);
            this.assignExaminerToolStripMenuItem.Text = "Assign Examiner";
            // 
            // termToolStripMenuItem
            // 
            this.termToolStripMenuItem.Name = "termToolStripMenuItem";
            this.termToolStripMenuItem.Size = new System.Drawing.Size(218, 32);
            this.termToolStripMenuItem.Text = "Term";
            this.termToolStripMenuItem.Click += new System.EventHandler(this.termToolStripMenuItem_Click);
            // 
            // admissionToolStripMenuItem
            // 
            this.admissionToolStripMenuItem.Name = "admissionToolStripMenuItem";
            this.admissionToolStripMenuItem.Size = new System.Drawing.Size(218, 32);
            this.admissionToolStripMenuItem.Text = "Admission";
            this.admissionToolStripMenuItem.Click += new System.EventHandler(this.admissionToolStripMenuItem_Click);
            // 
            // assignSubjectsToolStripMenuItem
            // 
            this.assignSubjectsToolStripMenuItem.Name = "assignSubjectsToolStripMenuItem";
            this.assignSubjectsToolStripMenuItem.Size = new System.Drawing.Size(376, 32);
            this.assignSubjectsToolStripMenuItem.Text = "Assign Subjects";
            this.assignSubjectsToolStripMenuItem.Click += new System.EventHandler(this.assignSubjectsToolStripMenuItem_Click);
            // 
            // scheduleDetailsToolStripMenuItem
            // 
            this.scheduleDetailsToolStripMenuItem.Name = "scheduleDetailsToolStripMenuItem";
            this.scheduleDetailsToolStripMenuItem.Size = new System.Drawing.Size(376, 32);
            this.scheduleDetailsToolStripMenuItem.Text = "Teacher Schedule ";
            this.scheduleDetailsToolStripMenuItem.Click += new System.EventHandler(this.scheduleDetailsToolStripMenuItem_Click);
            // 
            // teacherDetailsToolStripMenuItem
            // 
            this.teacherDetailsToolStripMenuItem.Name = "teacherDetailsToolStripMenuItem";
            this.teacherDetailsToolStripMenuItem.Size = new System.Drawing.Size(376, 32);
            this.teacherDetailsToolStripMenuItem.Text = "Teacher Contact Details";
            this.teacherDetailsToolStripMenuItem.Click += new System.EventHandler(this.teacherDetailsToolStripMenuItem_Click);
            // 
            // studentsToolStripMenuItem
            // 
            this.studentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.markAttendanceToolStripMenuItem,
            this.uploadExamMarksToolStripMenuItem,
            this.viewStudentDetailsToolStripMenuItem,
            this.viewParentDetailsToolStripMenuItem,
            this.totalMarksToolStripMenuItem});
            this.studentsToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentsToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.studentsToolStripMenuItem.Name = "studentsToolStripMenuItem";
            this.studentsToolStripMenuItem.Size = new System.Drawing.Size(122, 39);
            this.studentsToolStripMenuItem.Text = "Students";
            // 
            // markAttendanceToolStripMenuItem
            // 
            this.markAttendanceToolStripMenuItem.Name = "markAttendanceToolStripMenuItem";
            this.markAttendanceToolStripMenuItem.Size = new System.Drawing.Size(331, 32);
            this.markAttendanceToolStripMenuItem.Text = "Mark Attendance";
            this.markAttendanceToolStripMenuItem.Click += new System.EventHandler(this.markAttendanceToolStripMenuItem_Click);
            // 
            // uploadExamMarksToolStripMenuItem
            // 
            this.uploadExamMarksToolStripMenuItem.Name = "uploadExamMarksToolStripMenuItem";
            this.uploadExamMarksToolStripMenuItem.Size = new System.Drawing.Size(331, 32);
            this.uploadExamMarksToolStripMenuItem.Text = "Upload Exam Marks";
            this.uploadExamMarksToolStripMenuItem.Click += new System.EventHandler(this.uploadExamMarksToolStripMenuItem_Click);
            // 
            // viewStudentDetailsToolStripMenuItem
            // 
            this.viewStudentDetailsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewExamDetailsToolStripMenuItem,
            this.viewAttendanceDetailsToolStripMenuItem,
            this.profileDetailsToolStripMenuItem});
            this.viewStudentDetailsToolStripMenuItem.Name = "viewStudentDetailsToolStripMenuItem";
            this.viewStudentDetailsToolStripMenuItem.Size = new System.Drawing.Size(331, 32);
            this.viewStudentDetailsToolStripMenuItem.Text = "Student Details";
            this.viewStudentDetailsToolStripMenuItem.Click += new System.EventHandler(this.viewStudentDetailsToolStripMenuItem_Click);
            // 
            // viewExamDetailsToolStripMenuItem
            // 
            this.viewExamDetailsToolStripMenuItem.Name = "viewExamDetailsToolStripMenuItem";
            this.viewExamDetailsToolStripMenuItem.Size = new System.Drawing.Size(320, 32);
            this.viewExamDetailsToolStripMenuItem.Text = " Exam Details";
            this.viewExamDetailsToolStripMenuItem.Click += new System.EventHandler(this.viewExamDetailsToolStripMenuItem_Click);
            // 
            // viewAttendanceDetailsToolStripMenuItem
            // 
            this.viewAttendanceDetailsToolStripMenuItem.Name = "viewAttendanceDetailsToolStripMenuItem";
            this.viewAttendanceDetailsToolStripMenuItem.Size = new System.Drawing.Size(320, 32);
            this.viewAttendanceDetailsToolStripMenuItem.Text = "Attendance Details";
            this.viewAttendanceDetailsToolStripMenuItem.Click += new System.EventHandler(this.viewAttendanceDetailsToolStripMenuItem_Click);
            // 
            // profileDetailsToolStripMenuItem
            // 
            this.profileDetailsToolStripMenuItem.Name = "profileDetailsToolStripMenuItem";
            this.profileDetailsToolStripMenuItem.Size = new System.Drawing.Size(320, 32);
            this.profileDetailsToolStripMenuItem.Text = "Profile Details";
            this.profileDetailsToolStripMenuItem.Click += new System.EventHandler(this.profileDetailsToolStripMenuItem_Click);
            // 
            // viewParentDetailsToolStripMenuItem
            // 
            this.viewParentDetailsToolStripMenuItem.Name = "viewParentDetailsToolStripMenuItem";
            this.viewParentDetailsToolStripMenuItem.Size = new System.Drawing.Size(331, 32);
            this.viewParentDetailsToolStripMenuItem.Text = "Guardian Details";
            this.viewParentDetailsToolStripMenuItem.Click += new System.EventHandler(this.viewParentDetailsToolStripMenuItem_Click);
            // 
            // totalMarksToolStripMenuItem
            // 
            this.totalMarksToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.totalToolStripMenuItem,
            this.byGradesToolStripMenuItem});
            this.totalMarksToolStripMenuItem.Name = "totalMarksToolStripMenuItem";
            this.totalMarksToolStripMenuItem.Size = new System.Drawing.Size(331, 32);
            this.totalMarksToolStripMenuItem.Text = "Marks Details";
            this.totalMarksToolStripMenuItem.Click += new System.EventHandler(this.totalMarksToolStripMenuItem_Click);
            // 
            // totalToolStripMenuItem
            // 
            this.totalToolStripMenuItem.Name = "totalToolStripMenuItem";
            this.totalToolStripMenuItem.Size = new System.Drawing.Size(219, 32);
            this.totalToolStripMenuItem.Text = "Total ";
            this.totalToolStripMenuItem.Click += new System.EventHandler(this.totalToolStripMenuItem_Click);
            // 
            // byGradesToolStripMenuItem
            // 
            this.byGradesToolStripMenuItem.Name = "byGradesToolStripMenuItem";
            this.byGradesToolStripMenuItem.Size = new System.Drawing.Size(219, 32);
            this.byGradesToolStripMenuItem.Text = "By Grades";
            this.byGradesToolStripMenuItem.Click += new System.EventHandler(this.byGradesToolStripMenuItem_Click);
            // 
            // classesToolStripMenuItem
            // 
            this.classesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewClassScheduleToolStripMenuItem,
            this.createSectionToolStripMenuItem,
            this.manageSectionsToolStripMenuItem});
            this.classesToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.classesToolStripMenuItem.Name = "classesToolStripMenuItem";
            this.classesToolStripMenuItem.Size = new System.Drawing.Size(115, 39);
            this.classesToolStripMenuItem.Text = "Classes";
            // 
            // viewClassScheduleToolStripMenuItem
            // 
            this.viewClassScheduleToolStripMenuItem.Name = "viewClassScheduleToolStripMenuItem";
            this.viewClassScheduleToolStripMenuItem.Size = new System.Drawing.Size(299, 32);
            this.viewClassScheduleToolStripMenuItem.Text = "Own Schedule";
            this.viewClassScheduleToolStripMenuItem.Click += new System.EventHandler(this.viewClassScheduleToolStripMenuItem_Click);
            // 
            // createSectionToolStripMenuItem
            // 
            this.createSectionToolStripMenuItem.Name = "createSectionToolStripMenuItem";
            this.createSectionToolStripMenuItem.Size = new System.Drawing.Size(299, 32);
            this.createSectionToolStripMenuItem.Text = "Create Section";
            this.createSectionToolStripMenuItem.Click += new System.EventHandler(this.createSectionToolStripMenuItem_Click);
            // 
            // manageSectionsToolStripMenuItem
            // 
            this.manageSectionsToolStripMenuItem.Name = "manageSectionsToolStripMenuItem";
            this.manageSectionsToolStripMenuItem.Size = new System.Drawing.Size(299, 32);
            this.manageSectionsToolStripMenuItem.Text = "Manage Sections";
            this.manageSectionsToolStripMenuItem.Click += new System.EventHandler(this.manageSectionsToolStripMenuItem_Click);
            // 
            // subjectsToolStripMenuItem
            // 
            this.subjectsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subjectDetailsToolStripMenuItem,
            this.unassignedSubjectsToolStripMenuItem,
            this.gradingLevelToolStripMenuItem});
            this.subjectsToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectsToolStripMenuItem.Name = "subjectsToolStripMenuItem";
            this.subjectsToolStripMenuItem.Size = new System.Drawing.Size(122, 39);
            this.subjectsToolStripMenuItem.Text = "Subjects";
            // 
            // subjectDetailsToolStripMenuItem
            // 
            this.subjectDetailsToolStripMenuItem.Name = "subjectDetailsToolStripMenuItem";
            this.subjectDetailsToolStripMenuItem.Size = new System.Drawing.Size(335, 32);
            this.subjectDetailsToolStripMenuItem.Text = "Subject Details";
            this.subjectDetailsToolStripMenuItem.Click += new System.EventHandler(this.subjectDetailsToolStripMenuItem_Click);
            // 
            // unassignedSubjectsToolStripMenuItem
            // 
            this.unassignedSubjectsToolStripMenuItem.Name = "unassignedSubjectsToolStripMenuItem";
            this.unassignedSubjectsToolStripMenuItem.Size = new System.Drawing.Size(335, 32);
            this.unassignedSubjectsToolStripMenuItem.Text = "Unassigned Subjects";
            this.unassignedSubjectsToolStripMenuItem.Click += new System.EventHandler(this.unassignedSubjectsToolStripMenuItem_Click);
            // 
            // gradingLevelToolStripMenuItem
            // 
            this.gradingLevelToolStripMenuItem.Name = "gradingLevelToolStripMenuItem";
            this.gradingLevelToolStripMenuItem.Size = new System.Drawing.Size(335, 32);
            this.gradingLevelToolStripMenuItem.Text = "Grading Level";
            this.gradingLevelToolStripMenuItem.Click += new System.EventHandler(this.gradingLevelToolStripMenuItem_Click);
            // 
            // libraryToolStripMenuItem
            // 
            this.libraryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.searchBooksToolStripMenuItem});
            this.libraryToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.libraryToolStripMenuItem.Name = "libraryToolStripMenuItem";
            this.libraryToolStripMenuItem.Size = new System.Drawing.Size(105, 39);
            this.libraryToolStripMenuItem.Text = "Library";
            // 
            // searchBooksToolStripMenuItem
            // 
            this.searchBooksToolStripMenuItem.Name = "searchBooksToolStripMenuItem";
            this.searchBooksToolStripMenuItem.Size = new System.Drawing.Size(254, 32);
            this.searchBooksToolStripMenuItem.Text = "Search Books";
            this.searchBooksToolStripMenuItem.Click += new System.EventHandler(this.searchBooksToolStripMenuItem_Click);
            // 
            // attendanceToolStripMenuItem
            // 
            this.attendanceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.attendanceRecordToolStripMenuItem,
            this.leaveRecordToolStripMenuItem,
            this.leaveRequestToolStripMenuItem});
            this.attendanceToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attendanceToolStripMenuItem.Name = "attendanceToolStripMenuItem";
            this.attendanceToolStripMenuItem.Size = new System.Drawing.Size(163, 39);
            this.attendanceToolStripMenuItem.Text = "Attendance";
            // 
            // attendanceRecordToolStripMenuItem
            // 
            this.attendanceRecordToolStripMenuItem.Name = "attendanceRecordToolStripMenuItem";
            this.attendanceRecordToolStripMenuItem.Size = new System.Drawing.Size(325, 32);
            this.attendanceRecordToolStripMenuItem.Text = "Attendance Record";
            this.attendanceRecordToolStripMenuItem.Click += new System.EventHandler(this.attendanceRecordToolStripMenuItem_Click);
            // 
            // leaveRecordToolStripMenuItem
            // 
            this.leaveRecordToolStripMenuItem.Name = "leaveRecordToolStripMenuItem";
            this.leaveRecordToolStripMenuItem.Size = new System.Drawing.Size(325, 32);
            this.leaveRecordToolStripMenuItem.Text = "Leave Record";
            this.leaveRecordToolStripMenuItem.Click += new System.EventHandler(this.leaveRecordToolStripMenuItem_Click);
            // 
            // leaveRequestToolStripMenuItem
            // 
            this.leaveRequestToolStripMenuItem.Name = "leaveRequestToolStripMenuItem";
            this.leaveRequestToolStripMenuItem.Size = new System.Drawing.Size(325, 32);
            this.leaveRequestToolStripMenuItem.Text = "Leave Request";
            this.leaveRequestToolStripMenuItem.Click += new System.EventHandler(this.leaveRequestToolStripMenuItem_Click);
            // 
            // paymentToolStripMenuItem
            // 
            this.paymentToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paymentToolStripMenuItem.Name = "paymentToolStripMenuItem";
            this.paymentToolStripMenuItem.Size = new System.Drawing.Size(128, 39);
            this.paymentToolStripMenuItem.Text = "Payment";
            this.paymentToolStripMenuItem.Click += new System.EventHandler(this.paymentToolStripMenuItem_Click);
            // 
            // boardExamToolStripMenuItem
            // 
            this.boardExamToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manageBoardExamDetailsToolStripMenuItem});
            this.boardExamToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.boardExamToolStripMenuItem.Name = "boardExamToolStripMenuItem";
            this.boardExamToolStripMenuItem.Size = new System.Drawing.Size(166, 39);
            this.boardExamToolStripMenuItem.Text = "Board Exam";
            this.boardExamToolStripMenuItem.Click += new System.EventHandler(this.boardExamToolStripMenuItem_Click);
            // 
            // manageBoardExamDetailsToolStripMenuItem
            // 
            this.manageBoardExamDetailsToolStripMenuItem.Name = "manageBoardExamDetailsToolStripMenuItem";
            this.manageBoardExamDetailsToolStripMenuItem.Size = new System.Drawing.Size(429, 32);
            this.manageBoardExamDetailsToolStripMenuItem.Text = "Manage Board Exam Details";
            this.manageBoardExamDetailsToolStripMenuItem.Click += new System.EventHandler(this.manageBoardExamDetailsToolStripMenuItem_Click);
            // 
            // mailToolStripMenuItem
            // 
            this.mailToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sendMailToolStripMenuItem,
            this.receivedMailToolStripMenuItem});
            this.mailToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mailToolStripMenuItem.Name = "mailToolStripMenuItem";
            this.mailToolStripMenuItem.Size = new System.Drawing.Size(76, 39);
            this.mailToolStripMenuItem.Text = "Mail";
            // 
            // sendMailToolStripMenuItem
            // 
            this.sendMailToolStripMenuItem.Name = "sendMailToolStripMenuItem";
            this.sendMailToolStripMenuItem.Size = new System.Drawing.Size(264, 32);
            this.sendMailToolStripMenuItem.Text = "Send Mail";
            this.sendMailToolStripMenuItem.Click += new System.EventHandler(this.sendMailToolStripMenuItem_Click);
            // 
            // receivedMailToolStripMenuItem
            // 
            this.receivedMailToolStripMenuItem.Name = "receivedMailToolStripMenuItem";
            this.receivedMailToolStripMenuItem.Size = new System.Drawing.Size(264, 32);
            this.receivedMailToolStripMenuItem.Text = "Received Mail";
            this.receivedMailToolStripMenuItem.Click += new System.EventHandler(this.receivedMailToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1625, 43);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 31);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1575, 44);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 31);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // headteacherPanel
            // 
            this.headteacherPanel.Location = new System.Drawing.Point(4, 105);
            this.headteacherPanel.Margin = new System.Windows.Forms.Padding(4);
            this.headteacherPanel.Name = "headteacherPanel";
            this.headteacherPanel.Size = new System.Drawing.Size(1690, 780);
            this.headteacherPanel.TabIndex = 5;
            // 
            // HeadTeacher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1700, 900);
            this.Controls.Add(this.headteacherPanel);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HeadTeacher";
            this.Padding = new System.Windows.Forms.Padding(27, 74, 27, 25);
            this.Resizable = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Style = MetroFramework.MetroColorStyle.White;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.HeadTeacher_FormClosing);
            this.Load += new System.EventHandler(this.HeadTeacher_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem studentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem markAttendanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uploadExamMarksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewStudentDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewExamDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewAttendanceDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profileDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewParentDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem classesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewClassScheduleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem libraryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchBooksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendMailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem receivedMailToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ToolStripMenuItem teachersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem assignSubjectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem teacherDetailsToolStripMenuItem;
        private System.Windows.Forms.Panel headteacherPanel;
        private System.Windows.Forms.ToolStripMenuItem scheduleDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem boardExamToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageBoardExamDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createSectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attendanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attendanceRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leaveRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem assignExaminerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem termToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem admissionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem totalMarksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageSectionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subjectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subjectDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unassignedSubjectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gradingLevelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem totalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem byGradesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leaveRequestToolStripMenuItem;
    }
}
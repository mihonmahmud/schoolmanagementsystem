﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using SchoolManagement.User_Controls;


namespace SchoolManagement
{
    public partial class HeadTeacher : MetroForm
    {
        private string _userId;
        public HeadTeacher(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        private void HeadTeacher_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            LogIn li = new LogIn();
            li.Show();
            this.Hide();
        }

        private void addTeachersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void sendMailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new SendMailUC(_userId));
        }

        private void borrowHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new BorrowHistoryUC());
        }

        private void profileDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new StudentDetailsUC());
        }

        private void viewParentDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new GuardianDetailsUC());
        }

        private void searchBooksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new SearchBookUC());
        }

        private void teacherDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new TeacherDetailsUC());
        }

        private void assignSubjectsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new AssignSubjectsUC());
        }

        private void scheduleDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new TeachersScheduleUC());
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new UserProfileUC(_userId));
        }

        private void uploadExamMarksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new UploadMarksUC(_userId));
        }

        private void viewExamDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new ExamDetailsUC());
        }

        private void viewAttendanceDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new StudentAttendanceDetailsUC());
        }

        private void markAttendanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new MarkStudentAttendanceUC(_userId));
        }

        private void viewClassScheduleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new TeacherClassScheduleUC(_userId));
        }

        private void viewStudentDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new StudentDetailsUC());
        }

        private void boardExamToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void manageBoardExamDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new BoardExamDetailsUC());
        }

        private void manageAdmissionTestDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void receivedMailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new ReceivedMailUC(_userId));
        }

        private void paymentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new OwnPaymentUC(_userId));
        }

        private void createSectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new AddSectionUC());
        }

        private void attendanceRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new OwnAttendanceUC(_userId));
        }

        private void leaveRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new OwnLeaveUC(_userId));
        }

        private void termToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new HeadTermExamUC());
        }

        private void totalMarksToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void manageSectionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new ManageSectionsUC());
        }

        private void admissionTestToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void subjectDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new SubjectDetailsUC());
        }

        private void unassignedSubjectsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new UnassignedUC());
        }

        private void admissionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new HeadAdmissionUC());
        }

        private void HeadTeacher_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void gradingLevelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new GradingLevelUC());
        }

        private void totalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new StudentTotalMarksUC());
        }

        private void byGradesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new StudentsByGradesUC());
        }

        private void leaveRequestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in headteacherPanel.Controls)
                ct.Dispose();

            headteacherPanel.Controls.Add(new LeaveRequestUC(_userId));
        }
    }
 }

    


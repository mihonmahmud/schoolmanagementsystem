﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using SchoolManagement.User_Controls;

namespace SchoolManagement
{
    public partial class Accountant : MetroForm
    {
        private string _userId;

        public Accountant(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        private void sendMailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new SendMailUC(_userId));
        }

        private void receiveMailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new ReceivedMailUC(_userId));
        }

        private void viewFeesDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new PayDetailsUC());
        }

        private void addFeesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void viewSalaryDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new SalaryDetailsUC());
        }

        private void giveSalaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new PaySalaryUC());
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            LogIn li = new LogIn();
            li.Show();
            this.Hide();
        }

        private void addStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new AddStudentUC());
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new UserProfileUC(_userId));
        }

        private void collectFeesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new AdmissionFeesUC());
        }

        private void inventoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new InventoryUC());
        }

        private void attendanceRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new OwnAttendanceUC(_userId));
        }

        private void leaveRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new OwnLeaveUC(_userId));
        }

        private void paymentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new OwnPaymentUC(_userId));
        }

        private void regularStudentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new IncomeUC());
        }

        private void applicantsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new ApplicantsIncomeUC());
        }

        private void expenseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new MonthlyExpenseUC());
        }

        private void unpaidToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new UnpaidUC());
        }

        private void collectDueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void updateClassToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new UpdateClassUC());
        }

        private void collectFeesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new AddFeesUC());
        }

        private void collectDueToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new DueCollectUC());
        }

        private void Accountant_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void collectionHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in accountantPanel.Controls)
                ct.Dispose();

            accountantPanel.Controls.Add(new CollectionHistoryUC());
        }
    }
}

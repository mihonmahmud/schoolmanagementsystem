﻿namespace SchoolManagement
{
    partial class Accountant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Accountant));
            this.accountantPanel = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.studentsFeesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateClassToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewFeesDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unpaidToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeesSalaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.giveSalaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewSalaryDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.admissionFeesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.collectFeesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.financialsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.collectFeesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.collectDueToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.incomeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.regularStudentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applicantsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.expenseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attendanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attendanceRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leaveRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendMailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.receiveMailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.collectionHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // accountantPanel
            // 
            this.accountantPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.accountantPanel.Location = new System.Drawing.Point(5, 97);
            this.accountantPanel.Margin = new System.Windows.Forms.Padding(4);
            this.accountantPanel.Name = "accountantPanel";
            this.accountantPanel.Size = new System.Drawing.Size(1690, 780);
            this.accountantPanel.TabIndex = 6;
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.menuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.studentsFeesToolStripMenuItem,
            this.employeesSalaryToolStripMenuItem,
            this.inventoryToolStripMenuItem,
            this.admissionFeesToolStripMenuItem,
            this.financialsToolStripMenuItem,
            this.attendanceToolStripMenuItem,
            this.paymentToolStripMenuItem,
            this.mailToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(3, 34);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1673, 43);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // studentsFeesToolStripMenuItem
            // 
            this.studentsFeesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addStudentToolStripMenuItem,
            this.updateClassToolStripMenuItem,
            this.viewFeesDetailsToolStripMenuItem,
            this.unpaidToolStripMenuItem});
            this.studentsFeesToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentsFeesToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.studentsFeesToolStripMenuItem.Name = "studentsFeesToolStripMenuItem";
            this.studentsFeesToolStripMenuItem.Size = new System.Drawing.Size(122, 39);
            this.studentsFeesToolStripMenuItem.Text = "Students";
            // 
            // addStudentToolStripMenuItem
            // 
            this.addStudentToolStripMenuItem.Name = "addStudentToolStripMenuItem";
            this.addStudentToolStripMenuItem.Size = new System.Drawing.Size(285, 32);
            this.addStudentToolStripMenuItem.Text = "Add Student";
            this.addStudentToolStripMenuItem.Click += new System.EventHandler(this.addStudentToolStripMenuItem_Click);
            // 
            // updateClassToolStripMenuItem
            // 
            this.updateClassToolStripMenuItem.Name = "updateClassToolStripMenuItem";
            this.updateClassToolStripMenuItem.Size = new System.Drawing.Size(285, 32);
            this.updateClassToolStripMenuItem.Text = "Update Class";
            this.updateClassToolStripMenuItem.Click += new System.EventHandler(this.updateClassToolStripMenuItem_Click);
            // 
            // viewFeesDetailsToolStripMenuItem
            // 
            this.viewFeesDetailsToolStripMenuItem.Name = "viewFeesDetailsToolStripMenuItem";
            this.viewFeesDetailsToolStripMenuItem.Size = new System.Drawing.Size(285, 32);
            this.viewFeesDetailsToolStripMenuItem.Text = "Payment Details";
            this.viewFeesDetailsToolStripMenuItem.Click += new System.EventHandler(this.viewFeesDetailsToolStripMenuItem_Click);
            // 
            // unpaidToolStripMenuItem
            // 
            this.unpaidToolStripMenuItem.Name = "unpaidToolStripMenuItem";
            this.unpaidToolStripMenuItem.Size = new System.Drawing.Size(285, 32);
            this.unpaidToolStripMenuItem.Text = "Unpaid";
            this.unpaidToolStripMenuItem.Click += new System.EventHandler(this.unpaidToolStripMenuItem_Click);
            // 
            // employeesSalaryToolStripMenuItem
            // 
            this.employeesSalaryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.giveSalaryToolStripMenuItem,
            this.viewSalaryDetailsToolStripMenuItem});
            this.employeesSalaryToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeesSalaryToolStripMenuItem.Name = "employeesSalaryToolStripMenuItem";
            this.employeesSalaryToolStripMenuItem.Size = new System.Drawing.Size(153, 39);
            this.employeesSalaryToolStripMenuItem.Text = "Employees";
            // 
            // giveSalaryToolStripMenuItem
            // 
            this.giveSalaryToolStripMenuItem.Name = "giveSalaryToolStripMenuItem";
            this.giveSalaryToolStripMenuItem.Size = new System.Drawing.Size(255, 32);
            this.giveSalaryToolStripMenuItem.Text = "Pay Salary";
            this.giveSalaryToolStripMenuItem.Click += new System.EventHandler(this.giveSalaryToolStripMenuItem_Click);
            // 
            // viewSalaryDetailsToolStripMenuItem
            // 
            this.viewSalaryDetailsToolStripMenuItem.Name = "viewSalaryDetailsToolStripMenuItem";
            this.viewSalaryDetailsToolStripMenuItem.Size = new System.Drawing.Size(255, 32);
            this.viewSalaryDetailsToolStripMenuItem.Text = "Salary Details";
            this.viewSalaryDetailsToolStripMenuItem.Click += new System.EventHandler(this.viewSalaryDetailsToolStripMenuItem_Click);
            // 
            // inventoryToolStripMenuItem
            // 
            this.inventoryToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inventoryToolStripMenuItem.Name = "inventoryToolStripMenuItem";
            this.inventoryToolStripMenuItem.Size = new System.Drawing.Size(133, 39);
            this.inventoryToolStripMenuItem.Text = "Inventory";
            this.inventoryToolStripMenuItem.Click += new System.EventHandler(this.inventoryToolStripMenuItem_Click);
            // 
            // admissionFeesToolStripMenuItem
            // 
            this.admissionFeesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.collectFeesToolStripMenuItem});
            this.admissionFeesToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.admissionFeesToolStripMenuItem.Name = "admissionFeesToolStripMenuItem";
            this.admissionFeesToolStripMenuItem.Size = new System.Drawing.Size(146, 39);
            this.admissionFeesToolStripMenuItem.Text = "Admission";
            // 
            // collectFeesToolStripMenuItem
            // 
            this.collectFeesToolStripMenuItem.Name = "collectFeesToolStripMenuItem";
            this.collectFeesToolStripMenuItem.Size = new System.Drawing.Size(241, 32);
            this.collectFeesToolStripMenuItem.Text = "Collect Fees";
            this.collectFeesToolStripMenuItem.Click += new System.EventHandler(this.collectFeesToolStripMenuItem_Click);
            // 
            // financialsToolStripMenuItem
            // 
            this.financialsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.collectFeesToolStripMenuItem1,
            this.collectionHistoryToolStripMenuItem,
            this.collectDueToolStripMenuItem1,
            this.incomeToolStripMenuItem,
            this.expenseToolStripMenuItem});
            this.financialsToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.financialsToolStripMenuItem.Name = "financialsToolStripMenuItem";
            this.financialsToolStripMenuItem.Size = new System.Drawing.Size(142, 39);
            this.financialsToolStripMenuItem.Text = "Financials";
            // 
            // collectFeesToolStripMenuItem1
            // 
            this.collectFeesToolStripMenuItem1.Name = "collectFeesToolStripMenuItem1";
            this.collectFeesToolStripMenuItem1.Size = new System.Drawing.Size(300, 32);
            this.collectFeesToolStripMenuItem1.Text = "Collect Fees";
            this.collectFeesToolStripMenuItem1.Click += new System.EventHandler(this.collectFeesToolStripMenuItem1_Click);
            // 
            // collectDueToolStripMenuItem1
            // 
            this.collectDueToolStripMenuItem1.Name = "collectDueToolStripMenuItem1";
            this.collectDueToolStripMenuItem1.Size = new System.Drawing.Size(300, 32);
            this.collectDueToolStripMenuItem1.Text = "Collect Due";
            this.collectDueToolStripMenuItem1.Click += new System.EventHandler(this.collectDueToolStripMenuItem1_Click);
            // 
            // incomeToolStripMenuItem
            // 
            this.incomeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.regularStudentsToolStripMenuItem,
            this.applicantsToolStripMenuItem});
            this.incomeToolStripMenuItem.Name = "incomeToolStripMenuItem";
            this.incomeToolStripMenuItem.Size = new System.Drawing.Size(300, 32);
            this.incomeToolStripMenuItem.Text = "Income";
            // 
            // regularStudentsToolStripMenuItem
            // 
            this.regularStudentsToolStripMenuItem.Name = "regularStudentsToolStripMenuItem";
            this.regularStudentsToolStripMenuItem.Size = new System.Drawing.Size(290, 32);
            this.regularStudentsToolStripMenuItem.Text = "Regular Students";
            this.regularStudentsToolStripMenuItem.Click += new System.EventHandler(this.regularStudentsToolStripMenuItem_Click);
            // 
            // applicantsToolStripMenuItem
            // 
            this.applicantsToolStripMenuItem.Name = "applicantsToolStripMenuItem";
            this.applicantsToolStripMenuItem.Size = new System.Drawing.Size(290, 32);
            this.applicantsToolStripMenuItem.Text = "Applicants";
            this.applicantsToolStripMenuItem.Click += new System.EventHandler(this.applicantsToolStripMenuItem_Click);
            // 
            // expenseToolStripMenuItem
            // 
            this.expenseToolStripMenuItem.Name = "expenseToolStripMenuItem";
            this.expenseToolStripMenuItem.Size = new System.Drawing.Size(300, 32);
            this.expenseToolStripMenuItem.Text = "Expense";
            this.expenseToolStripMenuItem.Click += new System.EventHandler(this.expenseToolStripMenuItem_Click);
            // 
            // attendanceToolStripMenuItem
            // 
            this.attendanceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.attendanceRecordToolStripMenuItem,
            this.leaveRecordToolStripMenuItem});
            this.attendanceToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attendanceToolStripMenuItem.Name = "attendanceToolStripMenuItem";
            this.attendanceToolStripMenuItem.Size = new System.Drawing.Size(163, 39);
            this.attendanceToolStripMenuItem.Text = "Attendance";
            // 
            // attendanceRecordToolStripMenuItem
            // 
            this.attendanceRecordToolStripMenuItem.Name = "attendanceRecordToolStripMenuItem";
            this.attendanceRecordToolStripMenuItem.Size = new System.Drawing.Size(325, 32);
            this.attendanceRecordToolStripMenuItem.Text = "Attendance Record";
            this.attendanceRecordToolStripMenuItem.Click += new System.EventHandler(this.attendanceRecordToolStripMenuItem_Click);
            // 
            // leaveRecordToolStripMenuItem
            // 
            this.leaveRecordToolStripMenuItem.Name = "leaveRecordToolStripMenuItem";
            this.leaveRecordToolStripMenuItem.Size = new System.Drawing.Size(325, 32);
            this.leaveRecordToolStripMenuItem.Text = "Leave Record";
            this.leaveRecordToolStripMenuItem.Click += new System.EventHandler(this.leaveRecordToolStripMenuItem_Click);
            // 
            // paymentToolStripMenuItem
            // 
            this.paymentToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paymentToolStripMenuItem.Name = "paymentToolStripMenuItem";
            this.paymentToolStripMenuItem.Size = new System.Drawing.Size(128, 39);
            this.paymentToolStripMenuItem.Text = "Payment";
            this.paymentToolStripMenuItem.Click += new System.EventHandler(this.paymentToolStripMenuItem_Click);
            // 
            // mailToolStripMenuItem
            // 
            this.mailToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sendMailToolStripMenuItem,
            this.receiveMailToolStripMenuItem});
            this.mailToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mailToolStripMenuItem.Name = "mailToolStripMenuItem";
            this.mailToolStripMenuItem.Size = new System.Drawing.Size(76, 39);
            this.mailToolStripMenuItem.Text = "Mail";
            // 
            // sendMailToolStripMenuItem
            // 
            this.sendMailToolStripMenuItem.Name = "sendMailToolStripMenuItem";
            this.sendMailToolStripMenuItem.Size = new System.Drawing.Size(248, 32);
            this.sendMailToolStripMenuItem.Text = "Send Mail";
            this.sendMailToolStripMenuItem.Click += new System.EventHandler(this.sendMailToolStripMenuItem_Click);
            // 
            // receiveMailToolStripMenuItem
            // 
            this.receiveMailToolStripMenuItem.Name = "receiveMailToolStripMenuItem";
            this.receiveMailToolStripMenuItem.Size = new System.Drawing.Size(248, 32);
            this.receiveMailToolStripMenuItem.Text = "Receive Mail";
            this.receiveMailToolStripMenuItem.Click += new System.EventHandler(this.receiveMailToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1613, 47);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 31);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1561, 46);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 31);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // collectionHistoryToolStripMenuItem
            // 
            this.collectionHistoryToolStripMenuItem.Name = "collectionHistoryToolStripMenuItem";
            this.collectionHistoryToolStripMenuItem.Size = new System.Drawing.Size(300, 32);
            this.collectionHistoryToolStripMenuItem.Text = "Collection History";
            this.collectionHistoryToolStripMenuItem.Click += new System.EventHandler(this.collectionHistoryToolStripMenuItem_Click);
            // 
            // Accountant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1700, 900);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.accountantPanel);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Accountant";
            this.Padding = new System.Windows.Forms.Padding(20, 74, 20, 20);
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Style = MetroFramework.MetroColorStyle.White;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Accountant_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel accountantPanel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ToolStripMenuItem studentsFeesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewFeesDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeesSalaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewSalaryDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem giveSalaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendMailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem receiveMailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addStudentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem admissionFeesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem collectFeesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attendanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attendanceRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leaveRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem financialsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem incomeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem regularStudentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applicantsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem expenseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unpaidToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateClassToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem collectFeesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem collectDueToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem collectionHistoryToolStripMenuItem;
    }
}
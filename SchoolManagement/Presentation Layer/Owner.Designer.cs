﻿namespace SchoolManagement
{
    partial class Owner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Owner));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.studentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addAdminToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adminLeaveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setSalaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attendanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leaveRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.classesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.admissionDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.boardExamsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monthlySummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monthlyExpenseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applicantsFeesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mailToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sendMailToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.receiveMailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.ownerPanel = new System.Windows.Forms.Panel();
            this.paymentDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.menuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.studentsToolStripMenuItem,
            this.classesToolStripMenuItem,
            this.boardExamsToolStripMenuItem,
            this.paymentToolStripMenuItem,
            this.mailToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 37);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1699, 43);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // studentsToolStripMenuItem
            // 
            this.studentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.employeeDetailsToolStripMenuItem,
            this.addAdminToolStripMenuItem,
            this.adminLeaveToolStripMenuItem,
            this.setSalaryToolStripMenuItem,
            this.attendanceToolStripMenuItem,
            this.leaveRecordToolStripMenuItem});
            this.studentsToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentsToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.studentsToolStripMenuItem.Name = "studentsToolStripMenuItem";
            this.studentsToolStripMenuItem.Size = new System.Drawing.Size(149, 39);
            this.studentsToolStripMenuItem.Text = "Employess";
            // 
            // employeeDetailsToolStripMenuItem
            // 
            this.employeeDetailsToolStripMenuItem.Name = "employeeDetailsToolStripMenuItem";
            this.employeeDetailsToolStripMenuItem.Size = new System.Drawing.Size(391, 32);
            this.employeeDetailsToolStripMenuItem.Text = "Employees";
            this.employeeDetailsToolStripMenuItem.Click += new System.EventHandler(this.employeeDetailsToolStripMenuItem_Click);
            // 
            // addAdminToolStripMenuItem
            // 
            this.addAdminToolStripMenuItem.Name = "addAdminToolStripMenuItem";
            this.addAdminToolStripMenuItem.Size = new System.Drawing.Size(391, 32);
            this.addAdminToolStripMenuItem.Text = "Add Admin";
            this.addAdminToolStripMenuItem.Click += new System.EventHandler(this.addAdminToolStripMenuItem_Click);
            // 
            // adminLeaveToolStripMenuItem
            // 
            this.adminLeaveToolStripMenuItem.Name = "adminLeaveToolStripMenuItem";
            this.adminLeaveToolStripMenuItem.Size = new System.Drawing.Size(391, 32);
            this.adminLeaveToolStripMenuItem.Text = "Manage Leave";
            this.adminLeaveToolStripMenuItem.Click += new System.EventHandler(this.adminLeaveToolStripMenuItem_Click);
            // 
            // setSalaryToolStripMenuItem
            // 
            this.setSalaryToolStripMenuItem.Name = "setSalaryToolStripMenuItem";
            this.setSalaryToolStripMenuItem.Size = new System.Drawing.Size(391, 32);
            this.setSalaryToolStripMenuItem.Text = "Set Salary";
            this.setSalaryToolStripMenuItem.Click += new System.EventHandler(this.setSalaryToolStripMenuItem_Click);
            // 
            // attendanceToolStripMenuItem
            // 
            this.attendanceToolStripMenuItem.Name = "attendanceToolStripMenuItem";
            this.attendanceToolStripMenuItem.Size = new System.Drawing.Size(391, 32);
            this.attendanceToolStripMenuItem.Text = "Daily Attendance Record";
            this.attendanceToolStripMenuItem.Click += new System.EventHandler(this.attendanceToolStripMenuItem_Click);
            // 
            // leaveRecordToolStripMenuItem
            // 
            this.leaveRecordToolStripMenuItem.Name = "leaveRecordToolStripMenuItem";
            this.leaveRecordToolStripMenuItem.Size = new System.Drawing.Size(391, 32);
            this.leaveRecordToolStripMenuItem.Text = "Leave Record";
            this.leaveRecordToolStripMenuItem.Click += new System.EventHandler(this.leaveRecordToolStripMenuItem_Click);
            // 
            // classesToolStripMenuItem
            // 
            this.classesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.studentDetailsToolStripMenuItem,
            this.admissionDetailsToolStripMenuItem});
            this.classesToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.classesToolStripMenuItem.Name = "classesToolStripMenuItem";
            this.classesToolStripMenuItem.Size = new System.Drawing.Size(122, 39);
            this.classesToolStripMenuItem.Text = "Students";
            this.classesToolStripMenuItem.Click += new System.EventHandler(this.classesToolStripMenuItem_Click);
            // 
            // studentDetailsToolStripMenuItem
            // 
            this.studentDetailsToolStripMenuItem.Name = "studentDetailsToolStripMenuItem";
            this.studentDetailsToolStripMenuItem.Size = new System.Drawing.Size(223, 32);
            this.studentDetailsToolStripMenuItem.Text = "Students";
            this.studentDetailsToolStripMenuItem.Click += new System.EventHandler(this.studentDetailsToolStripMenuItem_Click);
            // 
            // admissionDetailsToolStripMenuItem
            // 
            this.admissionDetailsToolStripMenuItem.Name = "admissionDetailsToolStripMenuItem";
            this.admissionDetailsToolStripMenuItem.Size = new System.Drawing.Size(223, 32);
            this.admissionDetailsToolStripMenuItem.Text = "Applicants";
            this.admissionDetailsToolStripMenuItem.Click += new System.EventHandler(this.admissionDetailsToolStripMenuItem_Click);
            // 
            // boardExamsToolStripMenuItem
            // 
            this.boardExamsToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.boardExamsToolStripMenuItem.Name = "boardExamsToolStripMenuItem";
            this.boardExamsToolStripMenuItem.Size = new System.Drawing.Size(177, 39);
            this.boardExamsToolStripMenuItem.Text = "Board Exams";
            this.boardExamsToolStripMenuItem.Click += new System.EventHandler(this.boardExamsToolStripMenuItem_Click);
            // 
            // paymentToolStripMenuItem
            // 
            this.paymentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.paymentDetailsToolStripMenuItem,
            this.monthlySummaryToolStripMenuItem,
            this.monthlyExpenseToolStripMenuItem,
            this.applicantsFeesToolStripMenuItem,
            this.profitToolStripMenuItem});
            this.paymentToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paymentToolStripMenuItem.Name = "paymentToolStripMenuItem";
            this.paymentToolStripMenuItem.Size = new System.Drawing.Size(142, 39);
            this.paymentToolStripMenuItem.Text = "Financials";
            // 
            // monthlySummaryToolStripMenuItem
            // 
            this.monthlySummaryToolStripMenuItem.Name = "monthlySummaryToolStripMenuItem";
            this.monthlySummaryToolStripMenuItem.Size = new System.Drawing.Size(293, 32);
            this.monthlySummaryToolStripMenuItem.Text = "Monthly Income";
            this.monthlySummaryToolStripMenuItem.Click += new System.EventHandler(this.monthlySummaryToolStripMenuItem_Click);
            // 
            // monthlyExpenseToolStripMenuItem
            // 
            this.monthlyExpenseToolStripMenuItem.Name = "monthlyExpenseToolStripMenuItem";
            this.monthlyExpenseToolStripMenuItem.Size = new System.Drawing.Size(293, 32);
            this.monthlyExpenseToolStripMenuItem.Text = "Monthly Expense";
            this.monthlyExpenseToolStripMenuItem.Click += new System.EventHandler(this.monthlyExpenseToolStripMenuItem_Click);
            // 
            // applicantsFeesToolStripMenuItem
            // 
            this.applicantsFeesToolStripMenuItem.Name = "applicantsFeesToolStripMenuItem";
            this.applicantsFeesToolStripMenuItem.Size = new System.Drawing.Size(293, 32);
            this.applicantsFeesToolStripMenuItem.Text = "Applicants Fees";
            this.applicantsFeesToolStripMenuItem.Click += new System.EventHandler(this.applicantsFeesToolStripMenuItem_Click);
            // 
            // profitToolStripMenuItem
            // 
            this.profitToolStripMenuItem.Name = "profitToolStripMenuItem";
            this.profitToolStripMenuItem.Size = new System.Drawing.Size(293, 32);
            this.profitToolStripMenuItem.Text = "Revenue";
            this.profitToolStripMenuItem.Click += new System.EventHandler(this.profitToolStripMenuItem_Click);
            // 
            // mailToolStripMenuItem1
            // 
            this.mailToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sendMailToolStripMenuItem1,
            this.receiveMailToolStripMenuItem});
            this.mailToolStripMenuItem1.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mailToolStripMenuItem1.Name = "mailToolStripMenuItem1";
            this.mailToolStripMenuItem1.Size = new System.Drawing.Size(76, 39);
            this.mailToolStripMenuItem1.Text = "Mail";
            // 
            // sendMailToolStripMenuItem1
            // 
            this.sendMailToolStripMenuItem1.Name = "sendMailToolStripMenuItem1";
            this.sendMailToolStripMenuItem1.Size = new System.Drawing.Size(264, 32);
            this.sendMailToolStripMenuItem1.Text = "Send Mail";
            this.sendMailToolStripMenuItem1.Click += new System.EventHandler(this.sendMailToolStripMenuItem1_Click);
            // 
            // receiveMailToolStripMenuItem
            // 
            this.receiveMailToolStripMenuItem.Name = "receiveMailToolStripMenuItem";
            this.receiveMailToolStripMenuItem.Size = new System.Drawing.Size(264, 32);
            this.receiveMailToolStripMenuItem.Text = "Received Mail";
            this.receiveMailToolStripMenuItem.Click += new System.EventHandler(this.receiveMailToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1640, 45);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 31);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1586, 44);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 31);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // ownerPanel
            // 
            this.ownerPanel.Location = new System.Drawing.Point(8, 101);
            this.ownerPanel.Margin = new System.Windows.Forms.Padding(4);
            this.ownerPanel.Name = "ownerPanel";
            this.ownerPanel.Size = new System.Drawing.Size(1690, 780);
            this.ownerPanel.TabIndex = 5;
            // 
            // paymentDetailsToolStripMenuItem
            // 
            this.paymentDetailsToolStripMenuItem.Name = "paymentDetailsToolStripMenuItem";
            this.paymentDetailsToolStripMenuItem.Size = new System.Drawing.Size(293, 32);
            this.paymentDetailsToolStripMenuItem.Text = "Payment Details";
            this.paymentDetailsToolStripMenuItem.Click += new System.EventHandler(this.paymentDetailsToolStripMenuItem_Click);
            // 
            // Owner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1700, 900);
            this.Controls.Add(this.ownerPanel);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Owner";
            this.Padding = new System.Windows.Forms.Padding(27, 74, 27, 25);
            this.Resizable = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Style = MetroFramework.MetroColorStyle.White;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Owner_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem studentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem classesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ToolStripMenuItem mailToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sendMailToolStripMenuItem1;
        private System.Windows.Forms.Panel ownerPanel;
        private System.Windows.Forms.ToolStripMenuItem employeeDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addAdminToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem admissionDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem receiveMailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monthlySummaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monthlyExpenseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applicantsFeesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setSalaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem boardExamsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attendanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adminLeaveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leaveRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentDetailsToolStripMenuItem;
    }
}
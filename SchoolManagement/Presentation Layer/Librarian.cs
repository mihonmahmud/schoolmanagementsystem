﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using SchoolManagement.User_Controls;

namespace SchoolManagement
{
    public partial class Librarian : MetroForm
    {
        private string _userId;

        public Librarian(string userId)
        {
            InitializeComponent();
            _userId = userId;
        }

        private void sendMailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in librarianPanel.Controls)
                ct.Dispose();

            librarianPanel.Controls.Add(new SendMailUC(_userId));
        }

        private void searchBooksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in librarianPanel.Controls)
                ct.Dispose();

            librarianPanel.Controls.Add(new SearchBookUC());
        }

        private void addBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in librarianPanel.Controls)
                ct.Dispose();

            librarianPanel.Controls.Add(new AddBookUC());
        }

        private void manageBookDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in librarianPanel.Controls)
                ct.Dispose();

            librarianPanel.Controls.Add(new ManageBookUC());
        }

        private void borrowBooksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in librarianPanel.Controls)
                ct.Dispose();

            librarianPanel.Controls.Add(new BorrowBookUC());
        }

        private void borrowHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in librarianPanel.Controls)
                ct.Dispose();

            librarianPanel.Controls.Add(new BorrowHistoryUC());
        }

        private void manageBorrowedBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in librarianPanel.Controls)
                ct.Dispose();

            librarianPanel.Controls.Add(new ManageBorrowBookUC());
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            LogIn li = new LogIn();
            this.Hide();
            li.Show();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            foreach (Control ct in librarianPanel.Controls)
                ct.Dispose();

            librarianPanel.Controls.Add(new UserProfileUC(_userId));
        }

        private void paymentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in librarianPanel.Controls)
                ct.Dispose();

            librarianPanel.Controls.Add(new OwnPaymentUC(_userId));
        }

        private void receivedMailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in librarianPanel.Controls)
                ct.Dispose();

            librarianPanel.Controls.Add(new ReceivedMailUC(_userId));
        }

        private void attendanceRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in librarianPanel.Controls)
                ct.Dispose();

            librarianPanel.Controls.Add(new OwnAttendanceUC(_userId));
        }

        private void leaveRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Control ct in librarianPanel.Controls)
                ct.Dispose();

            librarianPanel.Controls.Add(new OwnLeaveUC(_userId));
        }

        private void Librarian_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
